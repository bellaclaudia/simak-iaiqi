<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Set_pa_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Pa_mhs_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $pa = $this->Pa_mhs_m;

        $validation = $this->form_validation;
        $validation->set_rules($pa->rules());
        if ($validation->run()) {
            $pa->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembimbing Akademik Mahasiswa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("set_pa_mhs");
        }
        $data["title"] = "Pembimbing Akademik Mahasiswa";

        $data['data_pa'] = $this->Pa_mhs_m->getAll();
        $data['dosen'] = $this->Pa_mhs_m->getDosen();
        $data['kelas'] = $this->Pa_mhs_m->getKelas();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pa/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $data['dosen'] = $this->Pa_mhs_m->getDosen();
        $data['kelas'] = $this->Pa_mhs_m->getKelas();
        $this->form_validation->set_rules('id_dosen', 'id_dosen', 'required');
        $this->form_validation->set_rules('id_kelas', 'id_kelas', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembimbing Akademik Mahasiswa gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('set_pa_mhs');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "id_dosen" => $this->input->post('id_dosen'),
                "id_kelas" => $this->input->post('id_kelas'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('dosen_pembimbing_akademik_mhs', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembimbing Akademik Mahasiswa berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('set_pa_mhs');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembimbing Akademik Mahasiswa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('set_pa_mhs');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('dosen_pembimbing_akademik_mhs');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembimbing Akademik Mahasiswa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('set_pa_mhs');
        }
    }
}
