<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dosen_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_status_dosen = $this->input->post('id_status_dosen');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_status_dosen == '') $id_status_dosen = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $data["title"] = "Dosen";
        $data['data_status_dosen'] = $this->dosen_m->getStatusDosen();
        $data["data_prodi"] = $this->dosen_m->getProgramStudi();
        $data["data_dosen"] = $this->dosen_m->getAll($id_status_dosen, $id_prodi);

        $data["id_status_dosen"] = $id_status_dosen;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('dosen/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('dosen/excel', $data);
    }

    public function tambah()
    {
        $dosen = $this->dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($dosen->rules());
        if ($validation->run()) {
            $dosen->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen");
        }
        $data["title"] = "Tambah Data Dosen";
        $data['prodi'] = $this->dosen_m->getProgramStudi();
        $data['agama'] = $this->dosen_m->getAgama();
        $data['goldar'] = $this->dosen_m->getGoldar();
        $data['jabatan_akademik_dosen'] = $this->dosen_m->getJabatanAkademikDosen();
        $data['provinsi'] = $this->dosen_m->getProvinsi();
        $data['kab_kota'] = $this->dosen_m->getKabKota();
        $data['status_dosen'] = $this->dosen_m->getStatusDosen();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('dosen');

        $dosen = $this->dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($dosen->rules());

        if ($validation->run()) {
            $this->dosen_m->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen");
        }
        $data["title"] = "Edit Data Dosen";
        $data["data_dosen"] = $this->dosen_m->getById($id);
        $data['prodi'] = $this->dosen_m->getProgramStudi();
        $data['agama'] = $this->dosen_m->getAgama();
        $data['goldar'] = $this->dosen_m->getGoldar();
        $data['jabatan_akademik_dosen'] = $this->dosen_m->getJabatanAkademikDosen();
        $data['provinsi'] = $this->dosen_m->getProvinsi();
        $data['kab_kota'] = $this->dosen_m->getKabKota();
        $data['status_dosen'] = $this->dosen_m->getStatusDosen();
        if (!$data["data_dosen"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/edit', $data);
        $this->load->view('templates/footer');
    }

    public function view($id = null)
    {
        if (!isset($id)) redirect('dosen');
        $data["title"] = "View Data dosen";
        $data["data_dosen"] = $this->dosen_m->getView($id);
        if (!$data["data_dosen"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('dosen');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen');
        }
    }

    // 31-10-2021
    public function biodata_dosen()
    {

        $dosen = $this->dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($dosen->rules());

        if ($validation->run()) {
            $this->dosen_m->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen berhasil diupdate.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        }

        $sqlxx = " select id FROM dosen WHERE nidn = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_dosen = $hasilxx->id;
        }

        $data["title"] = "Biodata Dosen";
        $data["data_dosen"] = $this->dosen_m->getById($id_dosen);
        $data['prodi'] = $this->dosen_m->getProgramStudi();
        $data['agama'] = $this->dosen_m->getAgama();
        $data['goldar'] = $this->dosen_m->getGoldar();
        $data['jabatan_akademik_dosen'] = $this->dosen_m->getJabatanAkademikDosen();
        $data['provinsi'] = $this->dosen_m->getProvinsi();
        $data['kab_kota'] = $this->dosen_m->getKabKota();
        $data['status_dosen'] = $this->dosen_m->getStatusDosen();
        if (!$data["data_dosen"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/biodata_dosen', $data);
        $this->load->view('templates/footer');
    }

    public function riwayat_pendidikan_dosen()
    {
        $sqlxx = " select id FROM dosen WHERE nidn = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_dosen = $hasilxx->id;
        }

        $data["title"] = "Riwayat Pendidikan Dosen";
        $data["data_riwayat_pend_dosen"] = $this->dosen_m->get_riwayat_pendidikan_dosen($id_dosen);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/riwayat_pendidikan_dosen', $data);
        $this->load->view('templates/footer');
    }

    // 09-11-2021
    public function set_bobot_komponen_nilai()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik. bikin kayak form view biasa, contek dari view biaya kuliah mhs, ada form tambah data utk input

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Set Bobot Komponen Nilai";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();
        $data["data_bobot"] = $this->dosen_m->get_data_bobot_komponen_nilai($id_semester, $id_ta, $id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/set_bobot_komponen_nilai', $data);
        $this->load->view('templates/footer');
    }

    public function add_bobot_komponen_nilai()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $id_komponen_nilai = $this->input->post('id_komponen_nilai');
        $persentase = $this->input->post('persentase');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == 1) {
            // cek apakah udh ada data komponen, jika sudah ada maka munculkan message
            $sqlxx = " select id FROM bobot_komponen_nilai_dosen WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' 
                        AND id_tahun_akademik = '$id_ta' AND id_komponen_nilai = '$id_komponen_nilai' AND id_mata_kuliah = '$id_mata_kuliah' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Bobot Komponen Nilai ini sudah pernah diinput. Silahkan ulangi lagi.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

                $data["id_semester"] = $id_semester;
                $data["id_ta"] = $id_ta;
                $data["id_mata_kuliah"] = $id_mata_kuliah;
                $data["id_komponen_nilai"] = $id_komponen_nilai;

                $data["title"] = "Tambah Data Bobot Komponen Nilai";
                $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
                $data["data_semester"] = $this->dosen_m->get_semester();
                $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
                //$data["data_mk"] = $this->dosen_m->get_mata_kuliah($id_prodi);
                $data["data_mk"] = $this->dosen_m->get_mata_kuliah2($id_semester);

                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('dosen/add_bobot_komponen_nilai', $data);
                $this->load->view('templates/footer');
            } else {
                // simpan ke tabel bobot, dan redirect ke view data lg
                $tgl_skrg = date('Y-m-d H:i:s');

                $this->db->trans_begin();

                $data = array(
                    'id_dosen' => $id_dosen,
                    'id_semester' => $id_semester,
                    'id_tahun_akademik' => $id_ta,
                    'id_mata_kuliah' => $id_mata_kuliah,
                    'id_komponen_nilai' => $id_komponen_nilai,
                    'persentase' => $persentase,
                    'tgl_input' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );

                $this->db->insert('bobot_komponen_nilai_dosen', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Bobot Komponen Nilai berhasil disimpan. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button></div>');

                redirect("dosen/set_bobot_komponen_nilai");
            }
        } else {
            // munculkan pilihan tahun akademik dan semester
            $data["title"] = "Tambah Data Bobot Komponen Nilai";
            $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
            $data["data_semester"] = $this->dosen_m->get_semester();
            $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
            //$data["data_mk"] = $this->dosen_m->get_mata_kuliah($id_prodi);
            $data["data_mk"] = $this->dosen_m->get_mata_kuliah2(1);

            if ($id_semester == '') $id_semester = 0;
            if ($id_ta == '') $id_ta = 0;
            if ($id_mata_kuliah == '') $id_mata_kuliah = 0;
            if ($id_komponen_nilai == '') $id_komponen_nilai = 0;

            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;
            $data["id_mata_kuliah"] = $id_mata_kuliah;
            $data["id_komponen_nilai"] = $id_komponen_nilai;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('dosen/add_bobot_komponen_nilai', $data);
            $this->load->view('templates/footer');
        }
    }

    public function hapus_bobot_komponen_nilai($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Komponen Nilai gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen/set_bobot_komponen_nilai");
        } else {
            $this->db->where('id', $id);
            $this->db->delete('bobot_komponen_nilai_dosen');

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Komponen Nilai berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen/set_bobot_komponen_nilai");
        }
    }

    // edit
    public function edit_bobot_komponen_nilai($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Komponen Nilai gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("dosen/set_bobot_komponen_nilai");
        } else {
            // ambil id dosen
            $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                        WHERE b.username = '" . $this->session->userdata['username'] . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nidn    = $hasilxx->nidn;
                $id_dosen = $hasilxx->id;
                $nama_dosen    = $hasilxx->nama;
                $id_prodi    = $hasilxx->id_program_studi;
            }

            $id_ta = $this->input->post('id_ta');
            $id_semester = $this->input->post('id_semester');
            $id_mata_kuliah = $this->input->post('id_mata_kuliah');
            $id_komponen_nilai = $this->input->post('id_komponen_nilai');
            $persentase = $this->input->post('persentase');
            $is_cek = $this->input->post('is_cek');

            if ($is_cek == 1) {
                // cek apakah udh ada data komponen, jika sudah ada maka munculkan message. 13-12-2021 sementara dikomen dulu
                //     $sqlxx = " select id FROM bobot_komponen_nilai_dosen WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' 
                //  AND id_tahun_akademik = '$id_ta' AND id_komponen_nilai = '$id_komponen_nilai' AND id_mata_kuliah = '$id_mata_kuliah' ";
                //     $queryxx = $this->db->query($sqlxx);
                //     if ($queryxx->num_rows() > 0) {
                //         $hasilxx = $queryxx->row();
                //         $idcek = $hasilxx->id;

                //         if ($idcek != $id) {
                //             $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                //             Data Bobot Komponen Nilai ini sudah pernah diinput. Silahkan ulangi lagi.
                //             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                //             <span aria-hidden="true">&times;</span>
                //             </button></div>');

                //             $data["id_semester"] = $id_semester;
                //             $data["id_ta"] = $id_ta;
                //             $data["id_mata_kuliah"] = $id_mata_kuliah;
                //             $data["id_komponen_nilai"] = $id_komponen_nilai;

                //             $data["title"] = "Tambah Data Bobot Komponen Nilai";
                //             $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
                //             $data["data_semester"] = $this->dosen_m->get_semester();
                //             $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
                //             $data["data_mk"] = $this->dosen_m->get_mata_kuliah2($id_semester);

                //             $this->load->view('templates/header', $data);
                //             $this->load->view('templates/menu');
                //             $this->load->view('dosen/add_bobot_komponen_nilai', $data);
                //             $this->load->view('templates/footer');
                //         }
                //     } 
                //else {
                // simpan ke tabel bobot, dan redirect ke view data lg
                $tgl_skrg = date('Y-m-d H:i:s');

                $this->db->trans_begin();

                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    //'id_dosen' => $id_dosen,
                    'id_semester' => $id_semester,
                    'id_tahun_akademik' => $id_ta,
                    'id_mata_kuliah' => $id_mata_kuliah,
                    'id_komponen_nilai' => $id_komponen_nilai,
                    'persentase' => $persentase,
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );

                $this->db->where('id', $id);
                $this->db->update('bobot_komponen_nilai_dosen', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Data Bobot Komponen Nilai berhasil diupdate. 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');

                redirect("dosen/set_bobot_komponen_nilai");
                //}
            } else {
                // munculkan pilihan tahun akademik dan semester
                $data["title"] = "Edit Data Bobot Komponen Nilai";
                $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
                $data["data_semester"] = $this->dosen_m->get_semester();
                $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();

                $data_bobot = $this->dosen_m->get_data_bobot_komponen_nilai_byid($id);

                $data["id_semester"] = $data_bobot->id_semester;
                $data["id_ta"] = $data_bobot->id_tahun_akademik;
                $data["id_mata_kuliah"] = $data_bobot->id_mata_kuliah;
                $data["id_komponen_nilai"] = $data_bobot->id_komponen_nilai;
                $data["persentase"] = $data_bobot->persentase;
                $data["data_mk"] = $this->dosen_m->get_mata_kuliah2($data_bobot->id_semester);

                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('dosen/edit_bobot_komponen_nilai', $data);
                $this->load->view('templates/footer');
            }
        }
        // -------------
    }

    // view nilai di login dosen
    public function view_nilai_mk()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data Nilai Mata Kuliah";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();
        $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
        //$data["data_nilai"] = $this->dosen_m->get_data_khs($id_semester, $id_ta, $id_dosen);
        $data["data_mk"] = $this->dosen_m->get_data_mk($id_semester, $id_ta, $id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view_nilai_mk', $data);
        $this->load->view('templates/footer');
    }

    // 12-11-2021
    public function view_jadwal_kuliah_dosen()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data Jadwal Kuliah";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();
        $data["data_jadwal"] = $this->dosen_m->get_jadwal_mengajar($id_semester, $id_ta, $id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view_jadwal_kuliah_dosen', $data);
        $this->load->view('templates/footer');
    }

    // 13-11-2021 persetujuan KRS oleh dosen
    public function view_krs_waiting_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data KRS Mahasiswa";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();
        $data["data_krs"] = $this->dosen_m->get_data_krs_waiting($id_semester, $id_ta, $id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view_krs_waiting_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function detail_krs($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_reject = $this->input->post('is_reject');
        if ($is_reject == 1) {
            $is_reject = $this->input->post('is_reject');
            $alasan_ditolak = $this->input->post('alasan_ditolak');

            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d');
            $data = array(
                'alasan_ditolak' => $alasan_ditolak,
                'status_verifikasi' => 2,
                'tgl_verifikasi' => $tgl_skrg,
                'id_dosen_verifikator' => $id_dosen
            );

            $this->db->where('id', $id_krs);
            $this->db->update('krs_mhs', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS berhasil ditolak. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_krs_waiting_mhs');
        }

        $data["title"] = "Data KRS Mahasiswa";
        $detail_krs = $this->dosen_m->get_detail_krs_waiting($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/detail_krs', $data);
        $this->load->view('templates/footer');
    }

    public function approve_krs($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS gagal disetujui. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_krs_waiting_mhs');
        } else {
            // ambil id mhs
            $sqlxx = " select id_mhs, id_semester, id_tahun_akademik FROM krs_mhs WHERE id = '" . $id . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $id_mhs    = $hasilxx->id_mhs;
                $id_semester    = $hasilxx->id_semester;
                $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
            }

            $this->db->trans_begin();

            $tgl_verifikasi = date('Y-m-d');
            $tgl_skrg = date('Y-m-d H:i:s');

            // update status verifikasi
            $data = array(
                'status_verifikasi' => 1,
                'tgl_verifikasi' => $tgl_verifikasi,
                'id_dosen_verifikator' => $id_dosen,
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('krs_mhs', $data);

            // update status mhs menjadi aktif
            $data = array(
                'id_status_mhs' => 1
            );

            $this->db->where('id', $id_mhs);
            $this->db->update('mahasiswa', $data);

            // 12-12-2021 cek apakah sudah ada data, jika blm, maka insert
            $sqlxx2 = " select id FROM riwayat_status_mhs WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                                AND id_mhs = '$id_mhs' ";
            $queryxx2 = $this->db->query($sqlxx2);
            if ($queryxx2->num_rows() == 0) {
                // insert status mhs ke riwayat_status_mhs
                $data = array(
                    'id_mhs' => $id_mhs,
                    'id_semester' => $id_semester,
                    'id_tahun_akademik' => $id_tahun_akademik,
                    'id_status_mhs' => 1,
                    'id_mhs' => $id_mhs,
                    'tgl_input' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );

                $this->db->insert('riwayat_status_mhs', $data);
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS berhasil disetujui. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_krs_waiting_mhs');
        }
    }

    // 16-11-2021 form isi nilai
    public function input_nilai($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas, $id_komponen_nilai)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_simpan = $this->input->post('simpan');
        if ($is_simpan == 1) {
            $id_nilai = $this->input->post('id_nilai');
            $id_mhs = $this->input->post('id_mhs');
            $baru = $this->input->post('baru');
            $nilai_angka = $this->input->post('nilai_angka');
            $id_krs_detail = $this->input->post('id_krs_detail');
            $jumdata = count($id_nilai);

            //echo $id_mata_kuliah . " " . $id_semester . " " . $id_tahun_akademik . " " . $id_kelas . " " . $id_komponen_nilai . "<br>";
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');

            for ($i = 0; $i < $jumdata; $i++) {
                //echo $id_krs_detail[$i] . " " . $nilai_angka[$i] . "<br>";
                if ($baru[$i] == 1) {
                    // jika baru, cek dulu apakah sudah ada data di khs_mhs dan khs_mhs_detail 
                    // jika blm, maka insert ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                    // cek khs_mhs
                    $sqlxx = " select a.id FROM khs_mhs a  
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' ";
                    // echo $sqlxx;
                    // die();
                    $queryxx = $this->db->query($sqlxx);
                    if ($queryxx->num_rows() == 0) {
                        // insert ke khs_mhs, khs_mhs_mk, khs_mhs_mk_detail
                        $data = array(
                            'id_semester' => $id_semester,
                            'id_tahun_akademik' => $id_tahun_akademik,
                            'id_mhs' => $id_mhs[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs', $data);

                        $id_khs = $this->db->insert_id();

                        $data = array(
                            'id_khs_mhs' => $id_khs,
                            'id_krs_mhs_detail' => $id_krs_detail[$i],
                            'id_mata_kuliah' => $id_mata_kuliah,
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk', $data);

                        $id_khs_mk = $this->db->insert_id();

                        $data = array(
                            'id_khs_mk' => $id_khs_mk,
                            'id_komponen_nilai' => $id_komponen_nilai,
                            'nilai_angka' => $nilai_angka[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk_detail', $data);
                    } else {
                        // cek khs_mhs_mk ada atau ga
                        $sqlxx2 = " select a.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ";
                        // echo $sqlxx2;
                        // die();
                        $queryxx2 = $this->db->query($sqlxx2);
                        if ($queryxx2->num_rows() == 0) {
                            // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT id FROM khs_mhs 
                            WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                               AND id_mhs = '" . $id_mhs[$i] . "' ");
                            $hasilrow3 = $query3->row();
                            $id_khs    = $hasilrow3->id;

                            $data = array(
                                'id_khs_mhs' => $id_khs,
                                'id_krs_mhs_detail' => $id_krs_detail[$i],
                                'id_mata_kuliah' => $id_mata_kuliah,
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk', $data);

                            $id_khs_mk = $this->db->insert_id();

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai,
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        } else {
                            // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT b.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                            WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ");
                            $hasilrow3 = $query3->row();
                            $id_khs_mk   = $hasilrow3->id;

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai,
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        }
                    }
                } else {
                    // jika udah pernah diinput maka update ke tabel khs_mhs_mk_detail where id_nilai = $id_nilai

                    $data = array(
                        'nilai_angka' => $nilai_angka[$i],
                        'tgl_update' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->where('id', $id_nilai[$i]);
                    $this->db->update('khs_mhs_mk_detail', $data);
                }
            }
            //die();

            // contek dari submit prioritas pembayaran di modul hutang_rekanan
            // for ($i = 0; $i < count($id_kelengkapan_dokumen); $i++) {
            // 	if ($urutan[$i] != '0') {
            // 		$data = array(
            // 			'id_kelengkapan_dokumen' => $id_kelengkapan_dokumen[$i],
            // 			'id_prioritas_pembayaran' => $id_header,
            // 			'urutan' => $urutan[$i]
            // 		);

            // 		$this->db->insert('tbl_hr_prioritas_pembayaran_detail', $data);

            // 		// ------------- 24-04-2019 update statusnya di tabel kelengkapan_dokumen ----------------
            // 		$queryxx = $this->db->query(" UPDATE tbl_hr_kelengkapan_dokumen SET status_prioritas = 't'
            // 				WHERE id = '$id_kelengkapan_dokumen[$i]' ");

            // 		$mingguke_kladhutang = $tahun . "-" . $bulan . "-" . "M" . $minggu_ke;
            // 		$this->db->query(" UPDATE tbl_kasir_klad_hutang SET periode_pembayaran = '$mingguke_kladhutang',
            // 						status_terakhir_hutang = 'KADEPKEU_HUTANG_APPROVED'
            // 						WHERE id_kelengkapan_dokumen = '$id_kelengkapan_dokumen[$i]' ");

            // 		$query3 = $this->db->query(" SELECT no_bukti_bank FROM tbl_kasir_klad_hutang WHERE id_kelengkapan_dokumen = '$id_kelengkapan_dokumen[$i]' ");
            // 		$hasilrow3 = $query3->row();
            // 		$no_bukti_bank	= $hasilrow3->no_bukti_bank;

            // 		// 23-07-2020 cek apakah sudah pernah insert. kalo belum, insert
            // 		$queryxx	= $this->db->query(" SELECT id FROM tbl_kasir_renc_pemb_hutang 
            // 											WHERE id_no_bukti_bank = '" . $no_bukti_bank . "' ");
            // 		if ($queryxx->num_rows() == 0) {
            // 			// 3. insert ke tbl_kasir_renc_pemb_hutang. ambil juga no_bukti_bank dari tbl_kasir_klad_hutang utk insertnya
            // 			$data_rencana = array(
            // 				'id_no_bukti_bank' => $no_bukti_bank,
            // 				'nilai_staff_keu' => null,
            // 				'nilai_mgr_keu' => 0,
            // 				'nilai_ka_keu' => $nominal_netto[$i],
            // 				'status_renc_hutang' => 'RENCANA_PEMBAYARAN_HUTANG_KAKEU',
            // 				'sisa_hutang' => $nominal_netto[$i],
            // 				'parent' => 0
            // 			);

            // 			$this->db->insert('tbl_kasir_renc_pemb_hutang', $data_rencana);
            // 		} else {
            // 			$sqlxx = " UPDATE tbl_kasir_renc_pemb_hutang SET nilai_ka_keu = '$nominal_netto[$i]', 
            // 						   sisa_hutang = '$nominal_netto[$i]'
            // 						   where id_no_bukti_bank = '$no_bukti_bank' ";
            // 			$this->db->query($sqlxx);
            // 		}

            // 		// ga dipake
            // 		//$this->db->query(" UPDATE tbl_kasir_klad_hutang SET status_permohonan_pemb_hr = 't' 
            // 		//			WHERE no_bukti_bank = '$no_bukti_bank' ");
            // 	}
            // 	// ------------------- END 24-04-2019 ----------------------------------------------------

            // } // end for
            // ---------------------------------------------------------------

            //$tgl_skrg = date('Y-m-d');
            // $data = array(
            //     'alasan_ditolak' => $alasan_ditolak,
            //     'status_verifikasi' => 2,
            //     'tgl_verifikasi' => $tgl_skrg,
            //     'id_dosen_verifikator' => $id_dosen
            // );

            // $this->db->where('id', $id_krs);
            // $this->db->update('krs_mhs', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data nilai berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_nilai_mk');
        }

        $data["title"] = "Data Nilai Mahasiswa";
        $detail_nilai = $this->dosen_m->get_detail_nilai($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_prodi, $id_kelas, $id_komponen_nilai);

        //if ($queryxx->num_rows() > 0) {
        if (is_array($detail_nilai)) {
            $nama_semester    = $detail_nilai[0]['nama_semester'];
            $tahun    = $detail_nilai[0]['tahun'];
            $nama_tahun_akademik    = $detail_nilai[0]['nama_tahun_akademik'];
            $nama_prodi    = $detail_nilai[0]['nama_prodi'];
            $nama_jenjang    = $detail_nilai[0]['nama_jenjang'];
            $nama_mk    = $detail_nilai[0]['nama_mk'];
            $nama_komponen    = $detail_nilai[0]['nama_komponen'];
            $nama_kelas    = $detail_nilai[0]['nama_kelas'];

            $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
            $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";

            $jumdata = count($detail_nilai);

            $data["id_semester"] = $id_semester;
            $data["id_tahun_akademik"] = $id_tahun_akademik;
            $data["nama_semester"] = $nama_semester;
            $data["thn_akademiknya"] = $thn_akademiknya;
            $data["prodinya"] = $prodinya;
            //}

            $data["detail_nilai"] = $detail_nilai;
            $data["id_mata_kuliah"] = $id_mata_kuliah;
            $data["nama_mk"] = $nama_mk;
            $data["id_komponen_nilai"] = $id_komponen_nilai;
            $data["nama_komponen"] = $nama_komponen;
            $data["nama_kelas"] = $nama_kelas;
            $data["id_kelas"] = $id_kelas;
            $data["jumdata"] = $jumdata;
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Belum ada mahasiswa yang mengambil mata kuliah ini. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            $data["detail_nilai"] = $detail_nilai;
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/input_nilai', $data);
        $this->load->view('templates/footer');
    }

    // 20-11-2021
    public function view_sp_waiting_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data SP Mahasiswa";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();
        $data["data_sp"] = $this->dosen_m->get_data_sp_waiting($id_semester, $id_ta, $id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view_sp_waiting_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function detail_sp($id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_reject = $this->input->post('is_reject');
        if ($is_reject == 1) {
            $is_reject = $this->input->post('is_reject');
            $alasan_ditolak = $this->input->post('alasan_ditolak');

            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d');
            $data = array(
                'alasan_ditolak' => $alasan_ditolak,
                'status_verifikasi' => 2,
                'tgl_verifikasi' => $tgl_skrg,
                'id_dosen_verifikator' => $id_dosen
            );

            $this->db->where('id', $id_sp);
            $this->db->update('sp_mhs', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP berhasil ditolak. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_sp_waiting_mhs');
        }

        $data["title"] = "Data SP Mahasiswa";
        $detail_sp = $this->dosen_m->get_detail_sp_waiting($id_sp);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_sp->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_sp"] = $detail_sp->result();
        $data["id_sp"] = $id_sp;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/detail_sp', $data);
        $this->load->view('templates/footer');
    }

    public function approve_sp($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP gagal disetujui. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_sp_waiting_mhs');
        } else {
            // ambil id mhs
            $sqlxx = " select id_mhs, id_semester, id_tahun_akademik FROM sp_mhs WHERE id = '" . $id . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $id_mhs    = $hasilxx->id_mhs;
                $id_semester    = $hasilxx->id_semester;
                $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
            }

            $this->db->trans_begin();

            $tgl_verifikasi = date('Y-m-d');
            $tgl_skrg = date('Y-m-d H:i:s');

            // update status verifikasi
            $data = array(
                'status_verifikasi' => 1,
                'tgl_verifikasi' => $tgl_verifikasi,
                'id_dosen_verifikator' => $id_dosen,
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('sp_mhs', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP berhasil disetujui. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_sp_waiting_mhs');
        }
    }

    // 25-11-2021 proses nilai akhir
    public function proses_nilai_akhir($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $this->db->trans_begin();
        //--------------------------------------------------------------------------------------------------
        // 1. cek data bobot komponen nilai di tabel bobot_komponen_nilai_dosen berdasarkan sem thn akademik (atau ga perlu sem thn akademik? sementara pake dulu). kalo ada, maka pake acuan itu
        // kalo ga ada, maka pake acuan default di master_komponen_nilai
        // 1.1. cek yg id_mata_kuliahnya sesuai dgn variabel parameter
        $sqlxx = " select persentase, id_komponen_nilai FROM bobot_komponen_nilai_dosen
                   WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                   AND id_mata_kuliah = '$id_mata_kuliah' ORDER BY id_komponen_nilai ";
        // echo $sqlxx;
        // die();
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            //$hasilxx = $queryxx->row();
            // ini pake perulangan
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_komponen_nilai    = $rowxx->id_komponen_nilai;
                $persentase = $rowxx->persentase;

                // eksekusi proses data berdasarkan komponen nilai di mata kuliah yg bersangkutan
                // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi'

                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $hasil = $query->result();
                    foreach ($hasil as $row) {
                        // update persentase
                        $tgl_skrg = date('Y-m-d H:i:s');
                        $data = array(
                            'persentase' => $persentase,
                            'tgl_update' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->where('id', $row->id_nilai);
                        $this->db->update('khs_mhs_mk_detail', $data);
                    }
                }
            }

            // 3. get data mhs yg mengambil mk
            // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
            $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $hasil = $query->result();
                foreach ($hasil as $row) {
                    // query ke khs_mhs_mk_detail utk hitung nilai akhir
                    $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                    $query2 = $this->db->query($sql2);
                    if ($query2->num_rows() > 0) {
                        $hasil2 = $query2->result();

                        $subtotal_per_komponen = 0;
                        foreach ($hasil2 as $row2) {
                            $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                            $subtotal_per_komponen += $hitung;
                        }
                    }

                    // konversi
                    //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                    $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                    $queryxx4 = $this->db->query($sqlxx4);
                    if ($queryxx4->num_rows() > 0) {
                        $hasilxx4 = $queryxx4->row();
                        $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                    }

                    // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                    $tgl_skrg = date('Y-m-d H:i:s');
                    $data = array(
                        'nilai_akhir' => $subtotal_per_komponen,
                        'id_nilai_huruf' => $id_nilai_huruf,
                        'tgl_update' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->where('id', $row->id_khs_mhs_mk);
                    $this->db->update('khs_mhs_mk', $data);
                }

                // insert ke tabel history_proses_nilai_akhir
                $data = array(
                    'id_semester' => $id_semester,
                    'id_tahun_akademik' => $id_tahun_akademik,
                    'id_mata_kuliah' => $id_mata_kuliah,
                    'id_kelas' => $id_kelas,
                    'tgl_proses' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->insert('history_proses_nilai_akhir', $data);
            }
        } else {
            // 1.2. query lagi dari bobot_komponen_nilai_dosen utk mata kuliah yg 0
            $sqlxx2 = " select persentase, id_komponen_nilai FROM bobot_komponen_nilai_dosen
                   WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                   AND id_mata_kuliah = '0' ORDER BY id_komponen_nilai ";
            $queryxx2 = $this->db->query($sqlxx2);
            if ($queryxx2->num_rows() > 0) {
                //$hasilxx2 = $queryxx2->row();
                // ini pake perulangan
                $hasilxx2 = $queryxx2->result();
                foreach ($hasilxx2 as $rowxx2) {
                    $id_komponen_nilai    = $rowxx2->id_komponen_nilai;
                    $persentase = $rowxx2->persentase;

                    // eksekusi proses data berdasarkan komponen nilai tsb
                    // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                    // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                    $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                    $query = $this->db->query($sql);
                    if ($query->num_rows() > 0) {
                        $hasil = $query->result();
                        foreach ($hasil as $row) {
                            // update persentase
                            $tgl_skrg = date('Y-m-d H:i:s');
                            $data = array(
                                'persentase' => $persentase,
                                'tgl_update' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->where('id', $row->id_nilai);
                            $this->db->update('khs_mhs_mk_detail', $data);
                        }
                    }
                }

                // 3. get data mhs yg mengambil mk
                // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
                $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $hasil = $query->result();
                    foreach ($hasil as $row) {
                        // query ke khs_mhs_mk_detail utk hitung nilai akhir
                        $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                        $query2 = $this->db->query($sql2);
                        if ($query2->num_rows() > 0) {
                            $hasil2 = $query2->result();

                            $subtotal_per_komponen = 0;
                            foreach ($hasil2 as $row2) {
                                $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                                $subtotal_per_komponen += $hitung;
                            }
                        }

                        // konversi
                        //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                        $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                        $queryxx4 = $this->db->query($sqlxx4);
                        if ($queryxx4->num_rows() > 0) {
                            $hasilxx4 = $queryxx4->row();
                            $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                        }

                        // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                        $tgl_skrg = date('Y-m-d H:i:s');
                        $data = array(
                            'nilai_akhir' => $subtotal_per_komponen,
                            'id_nilai_huruf' => $id_nilai_huruf,
                            'tgl_update' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->where('id', $row->id_khs_mhs_mk);
                        $this->db->update('khs_mhs_mk', $data);
                    }

                    // insert ke tabel history_proses_nilai_akhir
                    $data = array(
                        'id_semester' => $id_semester,
                        'id_tahun_akademik' => $id_tahun_akademik,
                        'id_mata_kuliah' => $id_mata_kuliah,
                        'id_kelas' => $id_kelas,
                        'tgl_proses' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->insert('history_proses_nilai_akhir', $data);
                }
            } else {
                // 1.3. query ambil dari master_komponen_nilai
                $sqlxx3 = " select persentase, id FROM master_komponen_nilai ORDER BY id ";
                $queryxx3 = $this->db->query($sqlxx3);
                if ($queryxx3->num_rows() > 0) {
                    //$hasilxx3 = $queryxx3->row();
                    // ini pake perulangan
                    $hasilxx3 = $queryxx3->result();

                    //$subtotal_per_komponen = 0;
                    foreach ($hasilxx3 as $rowxx3) {
                        $id_komponen_nilai    = $rowxx3->id;
                        $persentase = $rowxx3->persentase;

                        // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                        // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                        $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                        $query = $this->db->query($sql);
                        if ($query->num_rows() > 0) {
                            $hasil = $query->result();
                            foreach ($hasil as $row) {
                                // update persentase
                                $tgl_skrg = date('Y-m-d H:i:s');
                                $data = array(
                                    'persentase' => $persentase,
                                    'tgl_update' => $tgl_skrg,
                                    'user_update_by' => $this->session->userdata['username']
                                );
                                $this->db->where('id', $row->id_nilai);
                                $this->db->update('khs_mhs_mk_detail', $data);
                            }
                        }
                    }

                    // 3. get data mhs yg mengambil mk
                    // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
                    $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                    $query = $this->db->query($sql);
                    if ($query->num_rows() > 0) {
                        $hasil = $query->result();
                        foreach ($hasil as $row) {
                            // query ke khs_mhs_mk_detail utk hitung nilai akhir
                            $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                            $query2 = $this->db->query($sql2);
                            if ($query2->num_rows() > 0) {
                                $hasil2 = $query2->result();

                                $subtotal_per_komponen = 0;
                                foreach ($hasil2 as $row2) {
                                    $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                                    $subtotal_per_komponen += $hitung;
                                }
                            }

                            // konversi
                            //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                            $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                            $queryxx4 = $this->db->query($sqlxx4);
                            if ($queryxx4->num_rows() > 0) {
                                $hasilxx4 = $queryxx4->row();
                                $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                            }

                            // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                            $tgl_skrg = date('Y-m-d H:i:s');
                            $data = array(
                                'nilai_akhir' => $subtotal_per_komponen,
                                'id_nilai_huruf' => $id_nilai_huruf,
                                'tgl_update' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->where('id', $row->id_khs_mhs_mk);
                            $this->db->update('khs_mhs_mk', $data);
                        }

                        // insert ke tabel history_proses_nilai_akhir
                        $data = array(
                            'id_semester' => $id_semester,
                            'id_tahun_akademik' => $id_tahun_akademik,
                            'id_mata_kuliah' => $id_mata_kuliah,
                            'id_kelas' => $id_kelas,
                            'tgl_proses' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('history_proses_nilai_akhir', $data);
                    }
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        // --------------------------------------------------------------------------------------------------

        // $is_simpan = $this->input->post('simpan');
        // if ($is_simpan == 1) {
        //     $id_nilai = $this->input->post('id_nilai');
        //     $id_mhs = $this->input->post('id_mhs');
        //     $baru = $this->input->post('baru');
        //     $nilai_angka = $this->input->post('nilai_angka');
        //     $id_krs_detail = $this->input->post('id_krs_detail');
        //     $jumdata = count($id_nilai);

        //     //echo $id_mata_kuliah . " " . $id_semester . " " . $id_tahun_akademik . " " . $id_kelas . " " . $id_komponen_nilai . "<br>";
        //     $this->db->trans_begin();

        //     $tgl_skrg = date('Y-m-d H:i:s');

        //     for ($i = 0; $i < $jumdata; $i++) {
        //         //echo $id_krs_detail[$i] . " " . $nilai_angka[$i] . "<br>";
        //         if ($baru[$i] == 1) {
        //             // jika baru, cek dulu apakah sudah ada data di khs_mhs dan khs_mhs_detail 
        //             // jika blm, maka insert ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

        //             // cek khs_mhs
        //             $sqlxx = " select a.id FROM khs_mhs a  
        //                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
        //                        AND a.id_mhs = '" . $id_mhs[$i] . "' ";
        //             // echo $sqlxx;
        //             // die();
        //             $queryxx = $this->db->query($sqlxx);
        //             if ($queryxx->num_rows() == 0) {
        //                 // insert ke khs_mhs, khs_mhs_mk, khs_mhs_mk_detail
        //                 $data = array(
        //                     'id_semester' => $id_semester,
        //                     'id_tahun_akademik' => $id_tahun_akademik,
        //                     'id_mhs' => $id_mhs[$i],
        //                     'tgl_input' => $tgl_skrg,
        //                     'user_update_by' => $this->session->userdata['username']
        //                 );
        //                 $this->db->insert('khs_mhs', $data);

        //                 $id_khs = $this->db->insert_id();

        //                 $data = array(
        //                     'id_khs_mhs' => $id_khs,
        //                     'id_krs_mhs_detail' => $id_krs_detail[$i],
        //                     'id_mata_kuliah' => $id_mata_kuliah,
        //                     'tgl_input' => $tgl_skrg,
        //                     'user_update_by' => $this->session->userdata['username']
        //                 );
        //                 $this->db->insert('khs_mhs_mk', $data);

        //                 $id_khs_mk = $this->db->insert_id();

        //                 $data = array(
        //                     'id_khs_mk' => $id_khs_mk,
        //                     'id_komponen_nilai' => $id_komponen_nilai,
        //                     'nilai_angka' => $nilai_angka[$i],
        //                     'tgl_input' => $tgl_skrg,
        //                     'user_update_by' => $this->session->userdata['username']
        //                 );
        //                 $this->db->insert('khs_mhs_mk_detail', $data);
        //             } else {
        //                 // cek khs_mhs_mk ada atau ga
        //                 $sqlxx2 = " select a.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
        //                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
        //                        AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ";
        //                 // echo $sqlxx2;
        //                 // die();
        //                 $queryxx2 = $this->db->query($sqlxx2);
        //                 if ($queryxx2->num_rows() == 0) {
        //                     // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
        //                     $query3 = $this->db->query(" SELECT id FROM khs_mhs 
        //                     WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
        //                        AND id_mhs = '" . $id_mhs[$i] . "' ");
        //                     $hasilrow3 = $query3->row();
        //                     $id_khs    = $hasilrow3->id;

        //                     $data = array(
        //                         'id_khs_mhs' => $id_khs,
        //                         'id_krs_mhs_detail' => $id_krs_detail[$i],
        //                         'id_mata_kuliah' => $id_mata_kuliah,
        //                         'tgl_input' => $tgl_skrg,
        //                         'user_update_by' => $this->session->userdata['username']
        //                     );
        //                     $this->db->insert('khs_mhs_mk', $data);

        //                     $id_khs_mk = $this->db->insert_id();

        //                     $data = array(
        //                         'id_khs_mk' => $id_khs_mk,
        //                         'id_komponen_nilai' => $id_komponen_nilai,
        //                         'nilai_angka' => $nilai_angka[$i],
        //                         'tgl_input' => $tgl_skrg,
        //                         'user_update_by' => $this->session->userdata['username']
        //                     );
        //                     $this->db->insert('khs_mhs_mk_detail', $data);
        //                 } else {
        //                     // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
        //                     $query3 = $this->db->query(" SELECT b.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
        //                     WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
        //                        AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ");
        //                     $hasilrow3 = $query3->row();
        //                     $id_khs_mk   = $hasilrow3->id;

        //                     $data = array(
        //                         'id_khs_mk' => $id_khs_mk,
        //                         'id_komponen_nilai' => $id_komponen_nilai,
        //                         'nilai_angka' => $nilai_angka[$i],
        //                         'tgl_input' => $tgl_skrg,
        //                         'user_update_by' => $this->session->userdata['username']
        //                     );
        //                     $this->db->insert('khs_mhs_mk_detail', $data);
        //                 }
        //             }
        //         } else {
        //             // jika udah pernah diinput maka update ke tabel khs_mhs_mk_detail where id_nilai = $id_nilai

        //             $data = array(
        //                 'nilai_angka' => $nilai_angka[$i],
        //                 'tgl_update' => $tgl_skrg,
        //                 'user_update_by' => $this->session->userdata['username']
        //             );
        //             $this->db->where('id', $id_nilai[$i]);
        //             $this->db->update('khs_mhs_mk_detail', $data);
        //         }
        //     }

        //     if ($this->db->trans_status() === FALSE) {
        //         $this->db->trans_rollback();
        //     } else {
        //         $this->db->trans_commit();
        //     }

        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Proses nilai akhir berhasil. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect('dosen/view_nilai_mk');
        //}

        // $data["title"] = "Data Nilai Mahasiswa";
        // $detail_nilai = $this->dosen_m->get_detail_nilai($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_prodi, $id_kelas, $id_komponen_nilai);

        // //if ($queryxx->num_rows() > 0) {
        // if (is_array($detail_nilai)) {
        //     $nama_semester    = $detail_nilai[0]['nama_semester'];
        //     $tahun    = $detail_nilai[0]['tahun'];
        //     $nama_tahun_akademik    = $detail_nilai[0]['nama_tahun_akademik'];
        //     $nama_prodi    = $detail_nilai[0]['nama_prodi'];
        //     $nama_jenjang    = $detail_nilai[0]['nama_jenjang'];
        //     $nama_mk    = $detail_nilai[0]['nama_mk'];
        //     $nama_komponen    = $detail_nilai[0]['nama_komponen'];
        //     $nama_kelas    = $detail_nilai[0]['nama_kelas'];

        //     $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        //     $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";

        //     $jumdata = count($detail_nilai);
        // }

        // $data["id_semester"] = $id_semester;
        // $data["id_tahun_akademik"] = $id_tahun_akademik;
        // $data["nama_semester"] = $nama_semester;
        // $data["thn_akademiknya"] = $thn_akademiknya;
        // $data["prodinya"] = $prodinya;
        // //}

        // $data["detail_nilai"] = $detail_nilai;
        // $data["id_mata_kuliah"] = $id_mata_kuliah;
        // $data["nama_mk"] = $nama_mk;
        // $data["id_komponen_nilai"] = $id_komponen_nilai;
        // $data["nama_komponen"] = $nama_komponen;
        // $data["nama_kelas"] = $nama_kelas;
        // $data["id_kelas"] = $id_kelas;
        // $data["jumdata"] = $jumdata;

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('dosen/input_nilai', $data);
        // $this->load->view('templates/footer');
    }

    // 13-12-2021
    public function get_mata_kuliah()
    {
        // ini get mata kuliah by semester
        $id_semester     = $this->input->post('id_semester', TRUE);
        $data['data_mk'] = $this->dosen_m->get_mata_kuliah2($id_semester);
        $this->load->view('dosen/vlistmk', $data);
        return true;
    }

    // 28-12-2021
    public function input_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_simpan = $this->input->post('simpan');
        if ($is_simpan == 1) {
            $id_nilai = $this->input->post('id_nilai');
            $id_komponen_nilai = $this->input->post('id_komponen_nilai');
            $id_mhs = $this->input->post('id_mhs');
            //$baru = $this->input->post('baru');
            $nilai_angka = $this->input->post('nilai_angka');
            $id_krs_detail = $this->input->post('id_krs_detail');
            $jumdata = count($id_nilai);

            // print_r($id_nilai);
            //print_r($id_mhs);
            // die();

            //echo $id_mata_kuliah . " " . $id_semester . " " . $id_tahun_akademik . " " . $id_kelas . " " . $id_komponen_nilai . "<br>";
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');

            for ($i = 0; $i < $jumdata; $i++) {
                //echo $id_krs_detail[$i] . " " . $nilai_angka[$i] . "<br>";

                // 28-12-2021
                if ($id_nilai[$i] == '0') {
                    // jika baru, cek dulu apakah sudah ada data di khs_mhs dan khs_mhs_detail 
                    // jika blm, maka insert ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                    // cek khs_mhs
                    $sqlxx = " select a.id FROM khs_mhs a  
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' ";
                    // echo $sqlxx;
                    // die();
                    $queryxx = $this->db->query($sqlxx);
                    if ($queryxx->num_rows() == 0) {
                        // insert ke khs_mhs, khs_mhs_mk, khs_mhs_mk_detail
                        $data = array(
                            'id_semester' => $id_semester,
                            'id_tahun_akademik' => $id_tahun_akademik,
                            'id_mhs' => $id_mhs[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs', $data);

                        $id_khs = $this->db->insert_id();

                        $data = array(
                            'id_khs_mhs' => $id_khs,
                            'id_krs_mhs_detail' => $id_krs_detail[$i],
                            'id_mata_kuliah' => $id_mata_kuliah,
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk', $data);

                        $id_khs_mk = $this->db->insert_id();

                        $data = array(
                            'id_khs_mk' => $id_khs_mk,
                            'id_komponen_nilai' => $id_komponen_nilai[$i],
                            'nilai_angka' => $nilai_angka[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk_detail', $data);
                    } else {
                        // cek khs_mhs_mk ada atau ga
                        $sqlxx2 = " select a.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ";
                        // echo $sqlxx2;
                        // die();
                        $queryxx2 = $this->db->query($sqlxx2);
                        if ($queryxx2->num_rows() == 0) {
                            // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT id FROM khs_mhs 
                            WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                               AND id_mhs = '" . $id_mhs[$i] . "' ");
                            $hasilrow3 = $query3->row();
                            $id_khs    = $hasilrow3->id;

                            $data = array(
                                'id_khs_mhs' => $id_khs,
                                'id_krs_mhs_detail' => $id_krs_detail[$i],
                                'id_mata_kuliah' => $id_mata_kuliah,
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk', $data);

                            $id_khs_mk = $this->db->insert_id();

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai[$i],
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        } else {
                            // ambil id_khs_mk dan insert khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT b.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                            WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ");
                            $hasilrow3 = $query3->row();
                            $id_khs_mk   = $hasilrow3->id;

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai[$i],
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        }
                    }
                } else {
                    // jika udah pernah diinput maka update ke tabel khs_mhs_mk_detail where id_nilai = $id_nilai

                    $data = array(
                        'nilai_angka' => $nilai_angka[$i],
                        'tgl_update' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->where('id', $id_nilai[$i]);
                    $this->db->update('khs_mhs_mk_detail', $data);
                }
                // --------------- end 28-12-2021 --------------------------------
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data nilai berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('dosen/view_nilai_mk');
        }

        $data["title"] = "Data Nilai Mahasiswa";
        $detail_nilai = $this->dosen_m->get_detail_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_prodi, $id_kelas); // $id_komponen_nilai)

        //if ($queryxx->num_rows() > 0) {
        if (is_array($detail_nilai)) {
            // echo "<pre>";
            // print_r($detail_nilai);
            // echo "</pre>";
            // die();
            $nama_semester    = $detail_nilai[0]['nama_semester'];
            $tahun    = $detail_nilai[0]['tahun'];
            $nama_tahun_akademik    = $detail_nilai[0]['nama_tahun_akademik'];
            $nama_prodi    = $detail_nilai[0]['nama_prodi'];
            $nama_jenjang    = $detail_nilai[0]['nama_jenjang'];
            $kode_mk    = $detail_nilai[0]['kode_mk'];
            $nama_mk    = $detail_nilai[0]['nama_mk'];
            //$nama_komponen    = $detail_nilai[0]['nama_komponen'];
            $nama_kelas    = $detail_nilai[0]['nama_kelas'];

            $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
            $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";

            $jumdata = count($detail_nilai);

            $data["id_semester"] = $id_semester;
            $data["id_tahun_akademik"] = $id_tahun_akademik;
            $data["nama_semester"] = $nama_semester;
            $data["thn_akademiknya"] = $thn_akademiknya;
            $data["prodinya"] = $prodinya;
            //}

            $data["detail_nilai"] = $detail_nilai;
            $data["id_mata_kuliah"] = $id_mata_kuliah;
            $data["kode_mk"] = $kode_mk;
            $data["nama_mk"] = $nama_mk;
            //$data["id_komponen_nilai"] = $id_komponen_nilai;
            //$data["nama_komponen"] = $nama_komponen;
            $data["nama_kelas"] = $nama_kelas;
            $data["id_kelas"] = $id_kelas;
            $data["jumdata"] = $jumdata;
            $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Belum ada mahasiswa yang mengambil mata kuliah ini. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            $data["detail_nilai"] = $detail_nilai;
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/input_nilai_gabungan', $data);
        $this->load->view('templates/footer');
    }

    // 29-12-2021
    // view KHS mhs
    public function view_khs_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');
        $id_mhs = $this->input->post('id_mhs');
        $is_cari = $this->input->post('is_cari');

        // if ($id_semester == '') $id_semester = 0;
        // if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data KHS Mahasiswa";
        $data["data_thn_akademik"] = $this->dosen_m->get_tahun_akademik();
        $data["data_semester"] = $this->dosen_m->get_semester();

        if ($is_cari != '')
            $data["data_khs_mhs"] = $this->dosen_m->get_data_khs_mhs($id_semester, $id_ta, $id_mhs);

        $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();
        $data['data_mhs'] = $this->dosen_m->get_mhs_by_dosen_pa($id_dosen);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;
        $data["id_mhs"] = $id_mhs;
        $data["is_cari"] = $is_cari;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dosen/view_khs_mhs', $data);
        $this->load->view('templates/footer');
    }

    // 30-12-2021
    public function view_khs_mhs_pdf($id_ta, $id_semester, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id dosen
        $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nidn    = $hasilxx->nidn;
            $id_dosen = $hasilxx->id;
            $nama_dosen    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $data["data_khs_mhs"] = $this->dosen_m->get_data_khs_mhs($id_semester, $id_ta, $id_mhs);
        $data["data_komponen"] = $this->dosen_m->get_komponen_nilai();

        $this->load->view('dosen/view_khs_mhs_pdf', $data);
    }
}
