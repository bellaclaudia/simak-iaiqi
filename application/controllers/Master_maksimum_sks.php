<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_maksimum_sks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_maksimum_sks_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $maxsks = $this->Master_maksimum_sks_m;

        $validation = $this->form_validation;
        $validation->set_rules($maxsks->rules());
        if ($validation->run()) {
            $maxsks->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Maksimum SKS berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_maksimum_sks");
        }
        $data["title"] = "Maksimum SKS";

        $data['data_maksimum_sks'] = $this->Master_maksimum_sks_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('maksimum_sks/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('max_sks', 'max_sks', 'required');
        $this->form_validation->set_rules('ipk_batas_awal', 'ipk_batas_awal', 'required');
        $this->form_validation->set_rules('ipk_batas_akhir', 'ipk_batas_akhir', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Maksimum SKS gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_maksimum_sks');
        } else {
            $data = array(
                //"id" => $_POST['id'],
                "max_sks" => $this->input->post('max_sks'),
                "ipk_batas_awal" => $this->input->post('ipk_batas_awal'),
                "ipk_batas_akhir" => $this->input->post('ipk_batas_akhir'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_max_sks', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Maksimum SKS berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_maksimum_sks');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Maksimum SKS gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_maksimum_sks');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_max_sks');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Maksimum SKS berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_maksimum_sks');
        }
    }
}
