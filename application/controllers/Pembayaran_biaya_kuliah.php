<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_biaya_kuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Pembayaran_biaya_kuliah_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    // 07-11-2021
    public function index()
    {
        // ambil list thn akademik. bikin kayak form view biasa, contek dari list calon mhs, ada form tambah data utk input pembayaran

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');
        $id_mhs = $this->input->post('id_mhs');
        $id_prodi = $this->input->post('id_prodi');
        $jenis_biaya = $this->input->post('jenis_biaya');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;
        if ($id_mhs == '') $id_mhs = 0;
        if ($id_prodi == '') $id_prodi = 0;
        if ($jenis_biaya == '') $jenis_biaya = 0;

        $data["title"] = "Pembayaran Biaya Kuliah Reguler";
        $data["data_thn_akademik"] = $this->Pembayaran_biaya_kuliah_m->get_tahun_akademik();
        $data["data_semester"] = $this->Pembayaran_biaya_kuliah_m->get_semester();
        $data["data_prodi"] = $this->Pembayaran_biaya_kuliah_m->getProgramStudi();
        $data["data_mhs"] = $this->Pembayaran_biaya_kuliah_m->get_mhs_by_pembayaran();

        $data["data_pembayaran"] = $this->Pembayaran_biaya_kuliah_m->get_data_pembayaran_biaya($id_semester, $id_ta, $id_prodi, $id_mhs, $jenis_biaya);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["jenis_biaya"] = $jenis_biaya;

        // echo $jenis_biaya;
        // die();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pembayaran_biaya_kuliah/index', $data);
        $this->load->view('templates/footer');
    }

    public function add_pembayaran()
    {

        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        $id_mhs = $this->input->post('id_mhs');
        $jenis_biaya = $this->input->post('jenis_biaya');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == 1) {
            // cek apakah udh ada data pembayaran, jika sudah ada maka munculkan statusnya
            $sqlxx = " select id, status_verifikasi FROM pembayaran_biaya_kuliah WHERE id_mhs = '$id_mhs' AND id_semester = '$id_semester' 
                        AND id_tahun_akademik = '$id_ta' AND jenis_biaya = '$jenis_biaya' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $status_verifikasi    = $hasilxx->status_verifikasi;

                if ($status_verifikasi == '0')
                    $desc_status = "Waiting verifikasi";
                else
                    $desc_status = "Sudah approve admin";

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Pembayaran untuk semester tahun akademik ini sudah pernah dilakukan dgn status ' . $desc_status . ' 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

                $data["id_semester"] = $id_semester;
                $data["id_ta"] = $id_ta;
                $data["id_prodi"] = $id_prodi;
                $data["id_mhs"] = $id_mhs;
                $data["jenis_biaya"] = $jenis_biaya;

                // munculkan pilihan tahun akademik dan semester
                $data["title"] = "Tambah Data Pembayaran Reguler";
                $data["data_thn_akademik"] = $this->Pembayaran_biaya_kuliah_m->get_tahun_akademik();
                $data["data_semester"] = $this->Pembayaran_biaya_kuliah_m->get_semester();
                $data["data_prodi"] = $this->Pembayaran_biaya_kuliah_m->getProgramStudi();
                $data["data_mhs"] = $this->Pembayaran_biaya_kuliah_m->get_mhs_by_prodi($id_prodi);

                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('pembayaran_biaya_kuliah/add_pembayaran', $data);
                $this->load->view('templates/footer');
            } else {
                // redirect ke form input konfirmasi pembayaran
                redirect("pembayaran_biaya_kuliah/add_pembayaran_next/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs . "/" . $jenis_biaya);
            }
        } else {
            // munculkan pilihan tahun akademik dan semester
            $data["title"] = "Tambah Data Pembayaran Reguler";
            $data["data_thn_akademik"] = $this->Pembayaran_biaya_kuliah_m->get_tahun_akademik();
            $data["data_semester"] = $this->Pembayaran_biaya_kuliah_m->get_semester();
            $data["data_prodi"] = $this->Pembayaran_biaya_kuliah_m->getProgramStudi();
            $data["data_mhs"] = $this->Pembayaran_biaya_kuliah_m->get_mhs_by_prodi($id_prodi);

            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;
            $data["id_prodi"] = $id_prodi;
            $data["id_mhs"] = $id_mhs;
            $data["jenis_biaya"] = $jenis_biaya;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('pembayaran_biaya_kuliah/add_pembayaran', $data);
            $this->load->view('templates/footer');
        }
    }

    public function add_pembayaran_next($id_ta, $id_semester, $id_prodi, $id_mhs, $jenis_biaya)
    {
        // ambil nim dan nama mhs
        $sqlxx = " select nama, nim FROM mahasiswa 
                   WHERE id = '" . $id_mhs . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $nama_mhs    = $hasilxx->nama;
        }

        $data["data_thn_akademik"] = $this->Pembayaran_biaya_kuliah_m->get_tahun_akademik();
        $data["data_semester"] = $this->Pembayaran_biaya_kuliah_m->get_semester();
        $data["data_prodi"] = $this->Pembayaran_biaya_kuliah_m->getProgramStudi();
        $data["data_mhs"] = $this->Pembayaran_biaya_kuliah_m->get_mhs_by_prodi($id_prodi);

        if ($jenis_biaya == 1)
            $nama_jenis = 'REGULER';
        else
            $nama_jenis = 'SP';

        // ambil list biaya dari biaya_kuliah_persemester
        $sqlxx = " SELECT a.id_biaya_kuliah, a.jumlah, b.nama as nama_biaya, c.nama as nama_sem, d.nama as nama_ta, d.tahun 
                   FROM biaya_kuliah_persemester a 
                   INNER JOIN master_biaya_kuliah b ON a.id_biaya_kuliah = b.id
                   INNER JOIN master_semester c ON c.id = a.id_semester
                   INNER JOIN master_tahun_akademik d ON d.id = a.id_tahun_akademik
                   WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_program_studi = '$id_prodi'
                   AND b.jenis = '$nama_jenis' ORDER BY a.id ";

        $queryxx = $this->db->query($sqlxx);
        $jumlahnya = 0;
        if ($queryxx->num_rows() > 0) {
            $data_biaya = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $nama_sem    = $rowxx->nama_sem;
                $nama_ta    = $rowxx->tahun . " (" . $rowxx->nama_ta . ")";
                $nama_biaya    = $rowxx->nama_biaya;
                $jumlah    = $rowxx->jumlah;
                $jumlahnya += $jumlah;

                $data_biaya[] = array(
                    'nama_sem' => $nama_sem,
                    'nama_ta' => $nama_ta,
                    'nama' => $nama_biaya,
                    'nominal' => $jumlah
                );
            }
        } else {
            $data_biaya = '';
            // ambil nama sem dan thn akademik
            $sqlxx = " select nama FROM master_semester WHERE id = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_sem    = $hasilxx->nama;
            }

            $sqlxx = " select nama, tahun FROM master_tahun_akademik WHERE id = '$id_ta' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_ta    = $hasilxx->tahun . " (" . $hasilxx->nama . ")";
            }

            $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a 
                        INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id 
                        WHERE a.id = '$id_prodi' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_prodi    = $hasilxx->nama_prodi . " (" . $hasilxx->nama_jenjang . ")";
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data tagihan biaya untuk Tahun Akademik ' . $nama_ta . ' ' . $nama_sem . ' prodi ' . $nama_prodi . ' jenis biaya ' . $nama_jenis . ' belum diinput oleh admin. Silahkan input terlebih dahulu, terimakasih 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            redirect("pembayaran_biaya_kuliah/add_pembayaran");
        }

        // ambil list rekening kampus
        $sqlxx = " select id, nama_bank, no_rekening, atas_nama FROM master_rekening ORDER BY id ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $data_rek = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_rekening    = $rowxx->id;
                $nama_bank    = $rowxx->nama_bank;
                $no_rekening    = $rowxx->no_rekening;
                $atas_nama    = $rowxx->atas_nama;

                $data_rek[] = array(
                    'id_rekening' => $id_rekening,
                    'nama_bank' => $nama_bank,
                    'no_rekening' => $no_rekening,
                    'atas_nama' => $atas_nama
                );
            }
        } else {
            $data_rek = '';
        }

        $is_simpan = $this->input->post('is_simpan');
        if ($is_simpan == '1') {
            $jumbayar = $this->input->post('jumbayar');
            $id_rekening = $this->input->post('id_rekening');
            $jenis_bayar = $this->input->post('jenis_bayar');
            $tgl_bayar = $this->input->post('tgl_bayar');
            //$jenis_biaya = $this->input->post('jenis_biaya');

            if ($jenis_bayar == '1') {
                $id_rekening = 0;
            }
            $tgl_skrg = date('Y-m-d H:i:s');

            $this->db->trans_begin();

            $data = array(
                'id_mhs' => $id_mhs,
                'id_semester' => $id_semester,
                'id_tahun_akademik' => $id_ta,
                'jenis_biaya' => $jenis_biaya,
                'nominal' => $jumbayar,
                'id_rekening' => $id_rekening,
                'jenis_bayar' => $jenis_bayar,
                'status_verifikasi' => 1, // langsung otomatis dianggap approve
                'tgl_bayar' => $tgl_bayar,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('pembayaran_biaya_kuliah', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    Data Konfirmasi Pembayaran Biaya Kuliah berhasil disimpan dan otomatis approve oleh admin
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button></div>');
            redirect("pembayaran_biaya_kuliah");
        } else {
            $data["title"] = "Tambah Data Pembayaran";
            $data['data_biaya'] = $data_biaya;
            $data['jumlahnya'] = $jumlahnya;
            $data['data_rek'] = $data_rek;
            $data['id_mhs'] = $id_mhs;
            $data['mhs'] = $nim . " - " . $nama_mhs;
            $data['nama_sem'] = $nama_sem;
            $data['nama_ta'] = $nama_ta;
            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;
            $data["jenis_biaya"] = $jenis_biaya;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('pembayaran_biaya_kuliah/add_pembayaran_next', $data);
            $this->load->view('templates/footer');
        }

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('mahasiswa/add_pembayaran_reguler_next', $data);
        // $this->load->view('templates/footer');

        //---------------------------------------------------------------------------

    }

    public function approve_pembayaran($id)
    {

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran gagal diapprove. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pembayaran_biaya_kuliah');
        } else {
            // update ke tabel pembayaran_biaya_kuliah di field status_verifikasi
            $data = array(
                "status_verifikasi" => '1',
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $id);
            $this->db->update('pembayaran_biaya_kuliah', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data pembyaran berhasil diapprove. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pembayaran_biaya_kuliah');
        }
    }

    public function hapus_pembayaran($id)
    {

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("pembayaran_biaya_kuliah");
        } else {
            $this->db->where('id', $id);
            $this->db->delete('pembayaran_biaya_kuliah');

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("pembayaran_biaya_kuliah");
        }
    }

    public function get_mhs_by_prodi()
    {
        $id_prodi = $this->input->post('id_prodi', TRUE);
        $data['data_mhs'] = $this->Pembayaran_biaya_kuliah_m->get_mhs_by_prodi($id_prodi);
        $this->load->view('pembayaran_biaya_kuliah/vlistmhs', $data);
        return true;
    }

    // public function index()
    // {
    //     $pembayaran_biaya_kuliah = $this->pembayaran_biaya_kuliah_m;

    //     $validation = $this->form_validation;
    //     $validation->set_rules($pembayaran_biaya_kuliah->rules());
    //     if ($validation->run()) {
    //         $pembayaran_biaya_kuliah->save();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Pembayaran Biaya Kuliah berhasil disimpan. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("pembayaran_biaya_kuliah");
    //     }
    //     $data["title"] = "Pembayaran Biaya Kuliah";

    //     $data['data_pembayaran_biaya_kuliah'] = $this->pembayaran_biaya_kuliah_m->getAll();
    //     $data['semester'] = $this->pembayaran_biaya_kuliah_m->getSemester();
    //     $data['tahun_akademik'] = $this->pembayaran_biaya_kuliah_m->getTahunAkademik();
    //     $data["data_prodi"] = $this->pembayaran_biaya_kuliah_m->getProgramStudi();
    //     $data['mahasiswa'] = $this->pembayaran_biaya_kuliah_m->getMahasiswa();
    //     $this->load->view('templates/header', $data);
    //     $this->load->view('templates/menu');
    //     $this->load->view('pembayaran_biaya_kuliah/index', $data);
    //     $this->load->view('templates/footer');
    // }

    // public function edit($id = null)
    // {
    //     $data['semester'] = $this->pembayaran_biaya_kuliah_m->getSemester();
    //     $data['tahun_akademik'] = $this->pembayaran_biaya_kuliah_m->getTahunAkademik();
    //     $data['biaya_kuliah'] = $this->pembayaran_biaya_kuliah_m->getBiayaKuliah();
    //     $this->form_validation->set_rules('id_semester', 'id_semester', 'required');
    //     $this->form_validation->set_rules('id_tahun_akademik', 'id_tahun_akademik', 'required');
    //     $this->form_validation->set_rules('id_biaya_kuliah', 'id_biaya_kuliah', 'required');
    //     $this->form_validation->set_rules('id_mhs', 'id_mhs', 'required');
    //     $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
    //     if ($this->form_validation->run() == FALSE) {
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Pembayaran Biaya Kuliah gagal diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('pembayaran_biaya_kuliah');
    //     } else {
    //         $data = array(
    //             //"id" => $this->input->post('id'),
    //             "id_semester" => $this->input->post('id_semester'),
    //             "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
    //             "id_program_studi" => $this->input->post('id_program_studi'),
    //             "id_biaya_kuliah" => $this->input->post('id_biaya_kuliah'),
    //             "id_mhs" => $this->input->post('id_mhs'),
    //             "jumlah" => $this->input->post('jumlah'),
    //             "tgl_update" => date('Y-m-d H:i:s'),
    //             "user_update_by" => $this->session->userdata['username']
    //         );
    //         $this->db->where('id', $this->input->post('id'));
    //         $this->db->update('pembayaran_biaya_kuliah', $data);
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Pembayaran Biaya Kuliah berhasil diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('pembayaran_biaya_kuliah');
    //     }
    // }

    // public function hapus($id)
    // {
    //     if ($id == "") {
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Pembayaran Biaya Kuliah gagal dihapus. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('pembayaran_biaya_kuliah');
    //     } else {
    //         $this->db->where('id', $id);
    //         $this->db->delete('pembayaran_biaya_kuliah');
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Pembayaran Biaya Kuliah berhasil dihapus. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('pembayaran_biaya_kuliah');
    //     }
    // }

    // public function get_mhs_by_prodi()
    // {
    //     $id_program_studi = $this->input->post('id_program_studi', TRUE);
    //     $data['mahasiswa'] = $this->pembayaran_biaya_kuliah_m->get_mhs_by_prodi($id_program_studi);
    //     $this->load->view('pembayaran_biaya_kuliah/vlistmhs', $data);
    //     return true;
    // }
}
