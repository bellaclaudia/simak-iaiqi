<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_komponen_nilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_komponen_nilai_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $komp_nilai = $this->Master_komponen_nilai_m;
        $validation = $this->form_validation;
        $validation->set_rules($komp_nilai->rules());
        if ($validation->run()) {
            $komp_nilai->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Komponen Nilai berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_komponen_nilai");
        }
        $data["title"] = "Komponen Nilai";
        $data["data_komponen_nilai"] = $this->Master_komponen_nilai_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('komponen_nilai/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('persentase', 'persentase', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Komponen Nilai gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_komponen_nilai');
        } else {
            $data = array(
                //"id" => $_POST['id'],
                "kode" => $this->input->post('kode'),
                "nama" => $this->input->post('nama'),
                "persentase" => $this->input->post('persentase'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'),);
            $this->db->update('master_komponen_nilai', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Komponen Nilai berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_komponen_nilai');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Komponen Nilai gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_komponen_nilai');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_komponen_nilai');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Komponen Nilai berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_komponen_nilai');
        }
    }
}
