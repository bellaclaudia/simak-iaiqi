<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Khs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Khs_m");
        $this->load->model("Transkrip_nilai_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {

        //$is_view = $this->input->post('is_view');
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        $excel_pddikti_akm = $this->input->post('excel_pddikti_akm');
        $excel_pddikti_nilai_mhs = $this->input->post('excel_pddikti_nilai_mhs');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        //$data["is_view"] = $is_view;
        $data["title"] = "KHS";

        $data["data_khs"] = $this->Khs_m->getAll($id_ta, $id_semester, $id_prodi);
        $data["data_prodi"] = $this->Khs_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Khs_m->get_tahun_akademik();
        $data["data_semester"] = $this->Khs_m->get_semester();
        $data["data_komponen"] = $this->Khs_m->get_komponen_nilai();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        if ($excel_pddikti_akm == '' && $excel_pddikti_nilai_mhs == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('khs/index', $data);
            $this->load->view('templates/footer');
        } else {
            if ($excel_pddikti_akm != '') {
                $data["data_khs_excel"] = $this->Khs_m->get_khs_pddikti_akm($id_ta, $id_semester, $id_prodi);
                $this->load->view('khs/excel_pddikti_akm', $data);
            } else if ($excel_pddikti_nilai_mhs != '') {
                $data["data_khs_excel"] = $this->Khs_m->get_khs_pddikti_nilai_mhs($id_ta, $id_semester, $id_prodi);
                $this->load->view('khs/excel_pddikti_nilai_mhs', $data);
            }
        }
    }

    // 02-12-2021
    public function view_nilai($id_khs)
    {

        $data["title"] = "Data Nilai Mhs";
        $detail_khs = $this->Khs_m->get_detail_khs($id_khs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_khs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_khs"] = $detail_khs->result();
        $data["id_khs"] = $id_khs;
        $data["data_komponen"] = $this->Khs_m->get_komponen_nilai();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/view_nilai', $data);
        $this->load->view('templates/footer');
    }

    // public function tambah()
    // {
    //     if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
    //         redirect('login');
    //     }
    //     $krs = $this->Krs_m;
    //     $validation = $this->form_validation;
    //     $validation->set_rules($krs->rules());
    //     if ($validation->run()) {
    //         $id_ta = $this->input->post('id_ta');
    //         $id_semester = $this->input->post('id_semester');
    //         $id_prodi = $this->input->post('id_prodi');
    //         $id_mhs = $this->input->post('id_mhs');

    //         redirect("krs/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
    //         // ------------------------------------------

    //     } else {
    //         $data["title"] = "Tambah Data KRS";
    //         $data["data_prodi"] = $this->Krs_m->getProgramStudi();
    //         $data["data_thn_akademik"] = $this->Krs_m->get_tahun_akademik();
    //         $data["data_semester"] = $this->Krs_m->get_semester();

    //         $this->load->view('templates/header', $data);
    //         $this->load->view('templates/menu');
    //         $this->load->view('krs/add', $data);
    //         $this->load->view('templates/footer');
    //     }
    // }

    public function add_detail_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // $id_ta = $this->input->post('id_ta');
        // $id_semester = $this->input->post('id_semester');
        // $id_prodi = $this->input->post('id_prodi');
        // $id_mhs = $this->input->post('id_mhs');

        $is_simpan_mk = $this->input->post('is_simpan_mk');
        $submit_khs = $this->input->post('submit_khs');

        if ($is_simpan_mk == '1' && $submit_khs == '') {
            $id_mk = $this->input->post('id_mk');
            $nilai_huruf = $this->input->post('nilai_huruf');

            if ($id_mk != '') {
                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel temp
                $this->db->trans_begin();

                $data = array(
                    'id_tahun_akademik' => $id_ta,
                    'id_semester' => $id_semester,
                    'id_program_studi' => $id_prodi,
                    'id_mhs' => $id_mhs,
                    'id_mata_kuliah' => $id_mk,
                    'sks' => $sks,
                    'id_nilai_huruf' => $nilai_huruf
                );

                $this->db->insert('khs_mhs_pindahan_temp_mk', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("khs/add_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }

        if ($submit_khs == '1') {
            // simpan ke khs_mhs
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_tahun_akademik' => $id_ta,
                'id_semester' => $id_semester,
                'id_mhs' => $id_mhs,
                'is_konversi_nilai_mhs_pindahan' => 1,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('khs_mhs', $data);

            $id_khs = $this->db->insert_id();

            // simpan ke khs_mhs_mk
            // ambil data dari khs_mhs_pindahan_temp_mk.
            $sqlxx = " select id_mata_kuliah, sks, id_nilai_huruf FROM khs_mhs_pindahan_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
             AND id_semester = '$id_semester' ORDER BY id ";

            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_mk_temp   = $rowxx->id_mata_kuliah;
                    $sks_temp    = $rowxx->sks;
                    $id_nilai_huruf_temp = $rowxx->id_nilai_huruf;

                    $data = array(
                        'id_khs_mhs' => $id_khs,
                        'id_mata_kuliah' => $id_mk_temp,
                        'id_nilai_huruf' => $id_nilai_huruf_temp,
                        //'sks' => $sks_temp
                        'tgl_input' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );

                    $this->db->insert('khs_mhs_mk', $data);
                }
            }

            // hapus kembali dari khs_mhs_pindahan_temp_mk
            $this->db->where('id_mhs', $id_mhs);
            $this->db->where('id_tahun_akademik', $id_ta);
            $this->db->where('id_semester', $id_semester);
            $this->db->delete('khs_mhs_pindahan_temp_mk');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Nilai Mhs Pindahan berhasil disimpan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

            redirect("khs/mhs_pindahan");
        }

        // ambil nama ta
        $sqlxx = " select tahun, nama FROM master_tahun_akademik where id = '$id_ta' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $tahun    = $hasilxx->tahun;
        $nama_ta    = $hasilxx->nama;
        //}

        // ambil nama semester
        $sqlxx = " select nama FROM master_semester where id = '$id_semester' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_semester    = $hasilxx->nama;

        // ambil nama prodi
        $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a
                            INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id where a.id = '$id_prodi' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        // ambil nama mhs
        $sqlxx = " select nim, nama FROM mahasiswa where id = '$id_mhs' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama;

        $data["id_ta"] = $id_ta;
        $data["nama_ta"] = $tahun . " (" . $nama_ta . ")";
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["nama_prodi"] = $nama_prodi . " (" . $nama_jenjang . ")";
        $data["mhs"] = $nim . " - " . $nama_mhs;
        $data["id_mhs"] = $id_mhs;

        $data["data_temp_mk"] = $this->Khs_m->get_khs_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_mk"] = $this->Khs_m->get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_nilai_huruf"] = $this->Khs_m->get_nilai_huruf();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/add_detail_mk', $data);
        $this->load->view('templates/footer');
    }

    // 30-11-2021
    public function get_mhs_pindahan_by_prodi()
    {
        // ini get data mhs by prodi dan blm diinput nilainya di KHS
        $id_prodi     = $this->input->post('id_prodi', TRUE);
        $data['data_mhs'] = $this->Khs_m->get_mhs_pindahan_by_prodi($id_prodi);
        $this->load->view('khs/vlistmhspindahan', $data);
        return true;
    }

    public function hapus_temp_mk($id, $id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/add_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('khs_mhs_pindahan_temp_mk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/add_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }
    }

    // 30-11-2021
    public function hapus_nilai_mhs_pindahan($id)
    {

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Nilai Mhs Pindahan gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/mhs_pindahan");
        } else {
            $this->db->trans_begin();

            $this->db->where('id_khs_mhs', $id);
            $this->db->delete('khs_mhs_mk');

            $this->db->where('id', $id);
            $this->db->delete('khs_mhs');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Nilai Mhs Pindahan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/mhs_pindahan");
        }
    }

    // 14-11-2021. 01-12-2021
    public function view_nilai_mhs_pindahan($id_khs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data Nilai Mhs Pindahan";
        $detail_khs = $this->Khs_m->get_detail_khs($id_khs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_khs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_khs"] = $detail_khs->result();
        $data["id_khs"] = $id_khs;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/view_nilai_mhs_pindahan', $data);
        $this->load->view('templates/footer');
    }

    // 01-12-2021
    public function edit_nilai_mhs_pindahan($id_khs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data Nilai Mhs Pindahan";
        $detail_khs = $this->Khs_m->get_detail_khs($id_khs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_khs->row();
        $id_ta    = $hasilxx->id_tahun_akademik;
        $id_semester    = $hasilxx->id_semester;
        $id_prodi    = $hasilxx->id_program_studi;
        $id_mhs    = $hasilxx->id_mhs;

        $nama_semester    = $hasilxx->nama_semester;
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $is_simpan_mk = $this->input->post('is_simpan_mk');

        if ($is_simpan_mk == '1') {
            $id_mk = $this->input->post('id_mk');
            $nilai_huruf = $this->input->post('nilai_huruf');

            if ($id_mk != '') {
                // save ke tabel khs_mhs_mk
                $this->db->trans_begin();

                $tgl_skrg = date('Y-m-d H:i:s');

                $data = array(
                    'id_khs_mhs' => $id_khs,
                    'id_mata_kuliah' => $id_mk,
                    'id_nilai_huruf' => $nilai_huruf,
                    'tgl_input' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );

                $this->db->insert('khs_mhs_mk', $data);

                // update data di khs_mhs

                $data = array(
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $id_khs);
                $this->db->update('khs_mhs', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("khs/edit_nilai_mhs_pindahan/" . $id_khs);
        }

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_khs"] = $detail_khs->result();
        $data["id_khs"] = $id_khs;
        $data["data_mk"] = $this->Khs_m->get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_nilai_huruf"] = $this->Khs_m->get_nilai_huruf();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/edit_nilai_mhs_pindahan', $data);
        $this->load->view('templates/footer');
    }

    // 01-12-2021 hapus detail mk nilai mhs pindahan di form edit
    public function hapus_detail_mk($id, $id_khs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/edit_nilai_mhs_pindahan/" . $id_khs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('khs_mhs_mk');

            // update data di khs_mhs
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_khs);
            $this->db->update('khs_mhs', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("khs/edit_nilai_mhs_pindahan/" . $id_khs);
        }
    }

    // 29-11-2021 data nilai mhs pindahan
    public function mhs_pindahan()
    {

        //$is_view = $this->input->post('is_view');
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        //$data["is_view"] = $is_view;
        $data["title"] = "Data Nilai Mhs Pindahan";

        $data["data_khs"] = $this->Khs_m->getAll_nilai_mhs_pindahan($id_ta, $id_semester, $id_prodi);
        $data["data_prodi"] = $this->Khs_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Khs_m->get_tahun_akademik();
        $data["data_semester"] = $this->Khs_m->get_semester();
        //$data["data_komponen"] = $this->Khs_m->get_komponen_nilai();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/mhs_pindahan', $data);
        $this->load->view('templates/footer');
    }

    public function add_nilai_mhs_pindahan()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        $khs = $this->Khs_m;
        $validation = $this->form_validation;
        $validation->set_rules($khs->rules());
        if ($validation->run()) {
            $id_ta = $this->input->post('id_ta');
            $id_semester = $this->input->post('id_semester');
            $id_prodi = $this->input->post('id_prodi');
            $id_mhs = $this->input->post('id_mhs');

            redirect("khs/add_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
            // ------------------------------------------

        } else {
            $data["title"] = "Tambah Data Nilai Mhs Pindahan";
            $data["data_prodi"] = $this->Khs_m->getProgramStudi();
            $data["data_thn_akademik"] = $this->Khs_m->get_tahun_akademik();
            $data["data_semester"] = $this->Khs_m->get_semester();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('khs/add_nilai_mhs_pindahan', $data);
            $this->load->view('templates/footer');
        }
    }

    // 16-01-2022
    public function cetak_khs_html($id_khs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data Nilai Mhs";
        $detail_khs = $this->Khs_m->get_detail_khs($id_khs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_khs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $jenis_semester    = $hasilxx->jenis_semester;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;
        $nama_dosen_pa    = $hasilxx->nama_dosen;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nim"] = $nim;
        $data["nama_mhs"] = $nama_mhs;
        $data["nama_dosen_pa"] = $nama_dosen_pa;
        //}

        $data["detail_khs"] = $detail_khs->result();
        $data["id_khs"] = $id_khs;
        $data["data_komponen"] = $this->Khs_m->get_komponen_nilai();

        // 12-04-2022 get ipk
        $detail_transkrip = $this->Transkrip_nilai_m->get_detail_transkrip($hasilxx->id_mhs);
        $total_sks = 0;
        $total_angka_bobot = 0;
        foreach ($detail_transkrip->result() as $row) :
            $total_sks += $row->sks;
            $angka_bobot = $row->bobot * $row->sks;
            $total_angka_bobot += $angka_bobot;
        endforeach;

        $ipk = round($total_angka_bobot / $total_sks, 2);
        $data["ipk"] = $ipk;

        // get ip semester lalu. ambil tahun semester sebelumnya
        if ($jenis_semester == '2') {
            $jenis_sem_prev = '1';
            $tahun_prev = $tahun;
        } else {
            $jenis_sem_prev = '2';
            $tahun_prev = $tahun - 1;
        }

        // query get id khs sem lalu
        $sqlxx = " SELECT a.id FROM khs_mhs a INNER JOIN master_tahun_akademik b ON a.id_tahun_akademik = b.id WHERE b.jenis_semester = '$jenis_sem_prev'
                   AND b.tahun = '$tahun_prev' ";
        //echo $sqlxx;
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_khs_semlalu    = $hasilxx->id;
        } else
            $id_khs_semlalu = 0;

        if ($id_khs_semlalu != 0) {
            // sks semester
            $sqlsks = " SELECT sum(b.sks) as jumsks FROM khs_mhs_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id WHERE a.id_khs_mhs = '$id_khs_semlalu' ";
            $querysks = $this->db->query($sqlsks);
            if ($querysks->num_rows() > 0) {
                $hasilsks = $querysks->row();
                $jumsks    = $hasilsks->jumsks;
            } else
                $jumsks = 0;

            // hitung ip semester
            // 1. ambil total grade * bobot
            $sqlsks = " SELECT sum(c.sks*b.bobot) as gradebobot FROM khs_mhs_mk a INNER JOIN master_bobot_nilai b ON a.id_nilai_huruf = b.id_nilai_huruf
                    INNER JOIN master_mata_kuliah c ON c.id = a.id_mata_kuliah 
                    WHERE a.id_khs_mhs = '$id_khs_semlalu' ";
            //echo $sqlsks . "<br>";
            $querysks = $this->db->query($sqlsks);
            if ($querysks->num_rows() > 0) {
                $hasilsks = $querysks->row();
                $gradebobot    = $hasilsks->gradebobot;
            } else
                $gradebobot = 0;

            $ip_semester_lalu = round($gradebobot / $jumsks, 2);
        } else
            $ip_semester_lalu = '-';

        $data["ip_semester_lalu"] = $ip_semester_lalu;

        $this->load->view('khs/cetak_khs_html', $data);
    }

    public function view_nilai_mk()
    {
        // // ambil id dosen
        // $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
        //            WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        // $queryxx = $this->db->query($sqlxx);
        // if ($queryxx->num_rows() > 0) {
        //     $hasilxx = $queryxx->row();
        //     $nidn    = $hasilxx->nidn;
        //     $id_dosen = $hasilxx->id;
        //     $nama_dosen    = $hasilxx->nama;
        //     $id_prodi    = $hasilxx->id_program_studi;
        // }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');
        $id_kelas = $this->input->post('id_kelas');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;
        if ($id_kelas == '') $id_kelas = 0;

        $data["title"] = "Data Nilai Mata Kuliah";
        $data["data_thn_akademik"] = $this->Khs_m->get_tahun_akademik();
        $data["data_semester"] = $this->Khs_m->get_semester();
        $data["data_komponen"] = $this->Khs_m->get_komponen_nilai();
        $data["data_kelas"] = $this->Khs_m->getKelas();
        $data["data_mk"] = $this->Khs_m->get_data_mk($id_semester, $id_ta, $id_kelas);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;
        $data["id_kelas"] = $id_kelas;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/view_nilai_mk', $data);
        $this->load->view('templates/footer');
    }

    // 04-02-2022 contek dari login dosen
    public function input_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas)
    {
        // if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
        //     redirect('login');
        // }

        $is_simpan = $this->input->post('simpan');
        if ($is_simpan == 1) {
            $id_nilai = $this->input->post('id_nilai');
            $id_komponen_nilai = $this->input->post('id_komponen_nilai');
            $id_mhs = $this->input->post('id_mhs');
            //$baru = $this->input->post('baru');
            $nilai_angka = $this->input->post('nilai_angka');
            $id_krs_detail = $this->input->post('id_krs_detail');
            $jumdata = count($id_nilai);

            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');

            for ($i = 0; $i < $jumdata; $i++) {
                //echo $id_krs_detail[$i] . " " . $nilai_angka[$i] . "<br>";

                // 28-12-2021
                if ($id_nilai[$i] == '0') {
                    // jika baru, cek dulu apakah sudah ada data di khs_mhs dan khs_mhs_detail 
                    // jika blm, maka insert ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                    // cek khs_mhs
                    $sqlxx = " select a.id FROM khs_mhs a  
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' ";
                    // echo $sqlxx;
                    // die();
                    $queryxx = $this->db->query($sqlxx);
                    if ($queryxx->num_rows() == 0) {
                        // insert ke khs_mhs, khs_mhs_mk, khs_mhs_mk_detail
                        $data = array(
                            'id_semester' => $id_semester,
                            'id_tahun_akademik' => $id_tahun_akademik,
                            'id_mhs' => $id_mhs[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs', $data);

                        $id_khs = $this->db->insert_id();

                        $data = array(
                            'id_khs_mhs' => $id_khs,
                            'id_krs_mhs_detail' => $id_krs_detail[$i],
                            'id_mata_kuliah' => $id_mata_kuliah,
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk', $data);

                        $id_khs_mk = $this->db->insert_id();

                        $data = array(
                            'id_khs_mk' => $id_khs_mk,
                            'id_komponen_nilai' => $id_komponen_nilai[$i],
                            'nilai_angka' => $nilai_angka[$i],
                            'tgl_input' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('khs_mhs_mk_detail', $data);
                    } else {
                        // cek khs_mhs_mk ada atau ga
                        $sqlxx2 = " select a.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                               WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ";
                        // echo $sqlxx2;
                        // die();
                        $queryxx2 = $this->db->query($sqlxx2);
                        if ($queryxx2->num_rows() == 0) {
                            // ambil id_khs_mhs dan insert ke khs_mhs_mk dan khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT id FROM khs_mhs 
                            WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                               AND id_mhs = '" . $id_mhs[$i] . "' ");
                            $hasilrow3 = $query3->row();
                            $id_khs    = $hasilrow3->id;

                            $data = array(
                                'id_khs_mhs' => $id_khs,
                                'id_krs_mhs_detail' => $id_krs_detail[$i],
                                'id_mata_kuliah' => $id_mata_kuliah,
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk', $data);

                            $id_khs_mk = $this->db->insert_id();

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai[$i],
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        } else {
                            // ambil id_khs_mk dan insert khs_mhs_mk_detail
                            $query3 = $this->db->query(" SELECT b.id FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
                            WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik'
                               AND a.id_mhs = '" . $id_mhs[$i] . "' AND b.id_mata_kuliah = '$id_mata_kuliah' ");
                            $hasilrow3 = $query3->row();
                            $id_khs_mk   = $hasilrow3->id;

                            $data = array(
                                'id_khs_mk' => $id_khs_mk,
                                'id_komponen_nilai' => $id_komponen_nilai[$i],
                                'nilai_angka' => $nilai_angka[$i],
                                'tgl_input' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->insert('khs_mhs_mk_detail', $data);
                        }
                    }
                } else {
                    // jika udah pernah diinput maka update ke tabel khs_mhs_mk_detail where id_nilai = $id_nilai

                    $data = array(
                        'nilai_angka' => $nilai_angka[$i],
                        'tgl_update' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->where('id', $id_nilai[$i]);
                    $this->db->update('khs_mhs_mk_detail', $data);
                }
                // --------------- end 28-12-2021 --------------------------------
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data nilai berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('khs/view_nilai_mk');
        }

        $data["title"] = "Data Nilai Mahasiswa";
        $detail_nilai = $this->Khs_m->get_detail_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas); // $id_komponen_nilai)

        //if ($queryxx->num_rows() > 0) {
        if (is_array($detail_nilai)) {
            // echo "<pre>";
            // print_r($detail_nilai);
            // echo "</pre>";
            // die();
            $nama_semester    = $detail_nilai[0]['nama_semester'];
            $tahun    = $detail_nilai[0]['tahun'];
            $nama_tahun_akademik    = $detail_nilai[0]['nama_tahun_akademik'];
            $nama_prodi    = $detail_nilai[0]['nama_prodi'];
            $nama_jenjang    = $detail_nilai[0]['nama_jenjang'];
            $kode_mk    = $detail_nilai[0]['kode_mk'];
            $nama_mk    = $detail_nilai[0]['nama_mk'];
            //$nama_komponen    = $detail_nilai[0]['nama_komponen'];
            $nama_kelas    = $detail_nilai[0]['nama_kelas'];

            $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
            $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";

            $jumdata = count($detail_nilai);

            $data["id_semester"] = $id_semester;
            $data["id_tahun_akademik"] = $id_tahun_akademik;
            $data["nama_semester"] = $nama_semester;
            $data["thn_akademiknya"] = $thn_akademiknya;
            $data["prodinya"] = $prodinya;
            //}

            $data["detail_nilai"] = $detail_nilai;
            $data["id_mata_kuliah"] = $id_mata_kuliah;
            $data["kode_mk"] = $kode_mk;
            $data["nama_mk"] = $nama_mk;
            //$data["id_komponen_nilai"] = $id_komponen_nilai;
            //$data["nama_komponen"] = $nama_komponen;
            $data["nama_kelas"] = $nama_kelas;
            $data["id_kelas"] = $id_kelas;
            $data["jumdata"] = $jumdata;
            $data["data_komponen"] = $this->Khs_m->get_komponen_nilai();
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Belum ada mahasiswa yang mengambil mata kuliah ini. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            $data["detail_nilai"] = $detail_nilai;
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('khs/input_nilai_gabungan', $data);
        $this->load->view('templates/footer');
    }

    // 13-06-2022 contek dr login dosen
    public function proses_nilai_akhir($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas, $id_dosen)
    {
        // if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
        //     redirect('login');
        // }

        // ambil id dosen
        // $sqlxx = " select a.id, a.nama, a.nidn, a.id_program_studi FROM dosen a INNER JOIN master_user b ON a.id_user = b.id
        //            WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        // $queryxx = $this->db->query($sqlxx);
        // if ($queryxx->num_rows() > 0) {
        //     $hasilxx = $queryxx->row();
        //     $nidn    = $hasilxx->nidn;
        //     $id_dosen = $hasilxx->id;
        //     $nama_dosen    = $hasilxx->nama;
        //     $id_prodi    = $hasilxx->id_program_studi;
        // }

        $this->db->trans_begin();
        //--------------------------------------------------------------------------------------------------
        // 1. cek data bobot komponen nilai di tabel bobot_komponen_nilai_dosen berdasarkan sem thn akademik (atau ga perlu sem thn akademik? sementara pake dulu). kalo ada, maka pake acuan itu
        // kalo ga ada, maka pake acuan default di master_komponen_nilai
        // 1.1. cek yg id_mata_kuliahnya sesuai dgn variabel parameter
        $sqlxx = " select persentase, id_komponen_nilai FROM bobot_komponen_nilai_dosen
                   WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                   AND id_mata_kuliah = '$id_mata_kuliah' ORDER BY id_komponen_nilai ";
        // echo $sqlxx;
        // die();
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            //$hasilxx = $queryxx->row();
            // ini pake perulangan
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_komponen_nilai    = $rowxx->id_komponen_nilai;
                $persentase = $rowxx->persentase;

                // eksekusi proses data berdasarkan komponen nilai di mata kuliah yg bersangkutan
                // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi'

                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $hasil = $query->result();
                    foreach ($hasil as $row) {
                        // update persentase
                        $tgl_skrg = date('Y-m-d H:i:s');
                        $data = array(
                            'persentase' => $persentase,
                            'tgl_update' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->where('id', $row->id_nilai);
                        $this->db->update('khs_mhs_mk_detail', $data);
                    }
                }
            }

            // 3. get data mhs yg mengambil mk
            // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
            $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $hasil = $query->result();
                foreach ($hasil as $row) {
                    // query ke khs_mhs_mk_detail utk hitung nilai akhir
                    $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                    $query2 = $this->db->query($sql2);
                    if ($query2->num_rows() > 0) {
                        $hasil2 = $query2->result();

                        $subtotal_per_komponen = 0;
                        foreach ($hasil2 as $row2) {
                            $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                            $subtotal_per_komponen += $hitung;
                        }
                    }

                    // konversi
                    //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                    $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                    $queryxx4 = $this->db->query($sqlxx4);
                    if ($queryxx4->num_rows() > 0) {
                        $hasilxx4 = $queryxx4->row();
                        $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                    }

                    // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                    $tgl_skrg = date('Y-m-d H:i:s');
                    $data = array(
                        'nilai_akhir' => $subtotal_per_komponen,
                        'id_nilai_huruf' => $id_nilai_huruf,
                        'tgl_update' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->where('id', $row->id_khs_mhs_mk);
                    $this->db->update('khs_mhs_mk', $data);
                }

                // insert ke tabel history_proses_nilai_akhir
                $data = array(
                    'id_semester' => $id_semester,
                    'id_tahun_akademik' => $id_tahun_akademik,
                    'id_mata_kuliah' => $id_mata_kuliah,
                    'id_kelas' => $id_kelas,
                    'tgl_proses' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->insert('history_proses_nilai_akhir', $data);
            }
        } else {
            // 1.2. query lagi dari bobot_komponen_nilai_dosen utk mata kuliah yg 0
            $sqlxx2 = " select persentase, id_komponen_nilai FROM bobot_komponen_nilai_dosen
                   WHERE id_dosen = '$id_dosen' AND id_semester = '$id_semester' AND id_tahun_akademik = '$id_tahun_akademik'
                   AND id_mata_kuliah = '0' ORDER BY id_komponen_nilai ";
            $queryxx2 = $this->db->query($sqlxx2);
            if ($queryxx2->num_rows() > 0) {
                //$hasilxx2 = $queryxx2->row();
                // ini pake perulangan
                $hasilxx2 = $queryxx2->result();
                foreach ($hasilxx2 as $rowxx2) {
                    $id_komponen_nilai    = $rowxx2->id_komponen_nilai;
                    $persentase = $rowxx2->persentase;

                    // eksekusi proses data berdasarkan komponen nilai tsb
                    // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                    // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                    $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                    $query = $this->db->query($sql);
                    if ($query->num_rows() > 0) {
                        $hasil = $query->result();
                        foreach ($hasil as $row) {
                            // update persentase
                            $tgl_skrg = date('Y-m-d H:i:s');
                            $data = array(
                                'persentase' => $persentase,
                                'tgl_update' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->where('id', $row->id_nilai);
                            $this->db->update('khs_mhs_mk_detail', $data);
                        }
                    }
                }

                // 3. get data mhs yg mengambil mk
                // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
                $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $hasil = $query->result();
                    foreach ($hasil as $row) {
                        // query ke khs_mhs_mk_detail utk hitung nilai akhir
                        $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                        $query2 = $this->db->query($sql2);
                        if ($query2->num_rows() > 0) {
                            $hasil2 = $query2->result();

                            $subtotal_per_komponen = 0;
                            foreach ($hasil2 as $row2) {
                                $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                                $subtotal_per_komponen += $hitung;
                            }
                        }

                        // konversi
                        //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                        $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                        $queryxx4 = $this->db->query($sqlxx4);
                        if ($queryxx4->num_rows() > 0) {
                            $hasilxx4 = $queryxx4->row();
                            $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                        }

                        // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                        $tgl_skrg = date('Y-m-d H:i:s');
                        $data = array(
                            'nilai_akhir' => $subtotal_per_komponen,
                            'id_nilai_huruf' => $id_nilai_huruf,
                            'tgl_update' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->where('id', $row->id_khs_mhs_mk);
                        $this->db->update('khs_mhs_mk', $data);
                    }

                    // insert ke tabel history_proses_nilai_akhir
                    $data = array(
                        'id_semester' => $id_semester,
                        'id_tahun_akademik' => $id_tahun_akademik,
                        'id_mata_kuliah' => $id_mata_kuliah,
                        'id_kelas' => $id_kelas,
                        'tgl_proses' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );
                    $this->db->insert('history_proses_nilai_akhir', $data);
                }
            } else {
                // 1.3. query ambil dari master_komponen_nilai
                $sqlxx3 = " select persentase, id FROM master_komponen_nilai ORDER BY id ";
                $queryxx3 = $this->db->query($sqlxx3);
                if ($queryxx3->num_rows() > 0) {
                    //$hasilxx3 = $queryxx3->row();
                    // ini pake perulangan
                    $hasilxx3 = $queryxx3->result();

                    //$subtotal_per_komponen = 0;
                    foreach ($hasilxx3 as $rowxx3) {
                        $id_komponen_nilai    = $rowxx3->id;
                        $persentase = $rowxx3->persentase;

                        // eksekusi proses nilai akhir berdasarkan id_komponen_nilai dan persentase yg udh dicek diatas
                        // 2. update persentase di khs_mhs_mk_detail. query ke tabel khs_mhs, khs_mhs_mk, khs_mhs_mk_detail

                        $sql = " SELECT a.id as id_khs, a1.id as id_mhs, c.id as id_nilai 
                                FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                                INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                                INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                                INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                                WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                                AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                                AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                        $query = $this->db->query($sql);
                        if ($query->num_rows() > 0) {
                            $hasil = $query->result();
                            foreach ($hasil as $row) {
                                // update persentase
                                $tgl_skrg = date('Y-m-d H:i:s');
                                $data = array(
                                    'persentase' => $persentase,
                                    'tgl_update' => $tgl_skrg,
                                    'user_update_by' => $this->session->userdata['username']
                                );
                                $this->db->where('id', $row->id_nilai);
                                $this->db->update('khs_mhs_mk_detail', $data);
                            }
                        }
                    }

                    // 3. get data mhs yg mengambil mk
                    // query get data mhs utk hitung totalnya. ke khs_mhs relasi ke khs_mhs_mk, kemudian hitung tiap2 komponen berdasarkan persentasenya
                    $sql = " SELECT a.id, a.id_mhs, b.id as id_khs_mhs_mk FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                            INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs
                            WHERE a.id_semester = '$id_semester' 
                            AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                            AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi' 

                    $query = $this->db->query($sql);
                    if ($query->num_rows() > 0) {
                        $ada_data = 1;
                        $hasil = $query->result();
                        foreach ($hasil as $row) {
                            // query ke khs_mhs_mk_detail utk hitung nilai akhir
                            $sql2 = " SELECT persentase, nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id_khs_mhs_mk' ";
                            $query2 = $this->db->query($sql2);
                            if ($query2->num_rows() > 0) {
                                $hasil2 = $query2->result();

                                $subtotal_per_komponen = 0;
                                foreach ($hasil2 as $row2) {
                                    $hitung = ($row2->persentase / 100) * $row2->nilai_angka;
                                    $subtotal_per_komponen += $hitung;
                                }
                            }

                            // konversi
                            //konversi nilai akhir ke nilai huruf dari tabel master_level_nilai
                            $sqlxx4 = " SELECT id_nilai_huruf FROM master_level_nilai WHERE $subtotal_per_komponen >= nilai_batas_awal 
                                        AND $subtotal_per_komponen <= nilai_batas_akhir ";
                            $queryxx4 = $this->db->query($sqlxx4);
                            if ($queryxx4->num_rows() > 0) {
                                $hasilxx4 = $queryxx4->row();
                                $id_nilai_huruf = $hasilxx4->id_nilai_huruf;
                            }

                            // update nilai_akhir dan id_nilai_huruf ke khs_mhs_mk
                            $tgl_skrg = date('Y-m-d H:i:s');
                            $data = array(
                                'nilai_akhir' => $subtotal_per_komponen,
                                'id_nilai_huruf' => $id_nilai_huruf,
                                'tgl_update' => $tgl_skrg,
                                'user_update_by' => $this->session->userdata['username']
                            );
                            $this->db->where('id', $row->id_khs_mhs_mk);
                            $this->db->update('khs_mhs_mk', $data);
                        }

                        // insert ke tabel history_proses_nilai_akhir
                        $data = array(
                            'id_semester' => $id_semester,
                            'id_tahun_akademik' => $id_tahun_akademik,
                            'id_mata_kuliah' => $id_mata_kuliah,
                            'id_kelas' => $id_kelas,
                            'tgl_proses' => $tgl_skrg,
                            'user_update_by' => $this->session->userdata['username']
                        );
                        $this->db->insert('history_proses_nilai_akhir', $data);
                    } else {
                        $ada_data = 0;
                    }
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        if ($ada_data == 1) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Proses nilai akhir berhasil. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Proses nilai akhir gagal. Belum ada mahasiswa yang mengambil mata kuliah ini. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        }

        redirect('khs/view_nilai_mk');
    }
}
