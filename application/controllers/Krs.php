<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Krs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Krs_m");
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        //$is_view = $this->input->post('is_view');
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        $id_kelas = $this->input->post('id_kelas');
        $excel_pddikti = $this->input->post('excel_pddikti');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;
        if ($id_kelas == '') $id_kelas = 0;

        //$data["is_view"] = $is_view;
        $data["title"] = "KRS";

        $data["data_krs"] = $this->Krs_m->getAll($id_ta, $id_semester, $id_prodi, $id_kelas);
        $data["data_prodi"] = $this->Krs_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Krs_m->get_tahun_akademik();
        $data["data_semester"] = $this->Krs_m->get_semester();
        $data["data_kelas"] = $this->Krs_m->getKelas();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_kelas"] = $id_kelas;

        if ($excel_pddikti == '') {
            // $this->load->view('templates/header', $data);
            // $this->load->view('templates/menu');
            // $this->load->view('khs/index', $data);
            // $this->load->view('templates/footer');

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('krs/index', $data);
            $this->load->view('templates/footer');
        } else {
            $data["data_krs_excel"] = $this->Krs_m->get_krs_pddikti($id_ta, $id_semester, $id_prodi);
            $this->load->view('krs/excel_pddikti', $data);
        }
    }

    public function tambah()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        $krs = $this->Krs_m;
        $validation = $this->form_validation;
        $validation->set_rules($krs->rules());
        if ($validation->run()) {
            $id_ta = $this->input->post('id_ta');
            $id_semester = $this->input->post('id_semester');
            $id_prodi = $this->input->post('id_prodi');
            $id_mhs = $this->input->post('id_mhs');

            redirect("krs/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
            // ------------------------------------------

        } else {
            $data["title"] = "Tambah Data KRS";
            $data["data_prodi"] = $this->Krs_m->getProgramStudi();
            $data["data_thn_akademik"] = $this->Krs_m->get_tahun_akademik();
            $data["data_semester"] = $this->Krs_m->get_semester();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('krs/add', $data);
            $this->load->view('templates/footer');
        }
    }

    public function tambah_detail_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        // $id_ta = $this->input->post('id_ta');
        // $id_semester = $this->input->post('id_semester');
        // $id_prodi = $this->input->post('id_prodi');
        // $id_mhs = $this->input->post('id_mhs');

        $is_simpan_mk = $this->input->post('is_simpan_mk');
        $submit_krs = $this->input->post('submit_krs');

        if ($is_simpan_mk == '1' && $submit_krs == '') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // modif 17-11-2021 ga usah pake id jadwal

                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel temp
                $this->db->trans_begin();

                $data = array(
                    'id_tahun_akademik' => $id_ta,
                    'id_semester' => $id_semester,
                    'id_program_studi' => $id_prodi,
                    'id_mhs' => $id_mhs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks
                );

                $this->db->insert('krs_mhs_temp_mk', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("krs/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }

        if ($submit_krs == '1') {
            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM krs_mhs_temp_mk where id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
                        AND id_semester = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            //if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // simpan ke krs_mhs
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_tahun_akademik' => $id_ta,
                'id_semester' => $id_semester,
                'id_mhs' => $id_mhs,
                'total_sks' => $total_sks,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('krs_mhs', $data);

            $id_krs = $this->db->insert_id();

            // simpan ke krs_mhs_detail
            // ambil data dari krs_mhs_temp_mk. 17-11-2021 ga pake relasi ke jadwal kuliah
            // $sqlxx = " select id_mata_kuliah, id_jadwal_kuliah, sks FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
            // AND id_semester = '$id_semester' ORDER BY id ";
            $sqlxx = " select id_mata_kuliah, sks FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
             AND id_semester = '$id_semester' ORDER BY id ";

            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_mk_temp   = $rowxx->id_mata_kuliah;
                    $sks_temp    = $rowxx->sks;
                    //$id_jadwal_temp = $rowxx->id_jadwal_kuliah;

                    // 13-11-2021 cek apakah pernah mengambil mk tsb. jika pernah, maka status = '2' (mengulang)
                    $sqlxx2 = " select b.id FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs WHERE a.id_mhs = '$id_mhs'
                                AND b.id_mata_kuliah = '$id_mk_temp' ";
                    $queryxx2 = $this->db->query($sqlxx2);
                    if ($queryxx2->num_rows() > 0) {
                        $statusnya = 2;
                    } else
                        $statusnya = 1;

                    $data = array(
                        'id_krs_mhs' => $id_krs,
                        'id_mata_kuliah' => $id_mk_temp,
                        'sks' => $sks_temp,
                        //'id_jadwal_kuliah' => $id_jadwal_temp,
                        'status' => $statusnya
                    );

                    $this->db->insert('krs_mhs_detail', $data);
                }
            }

            // hapus kembali dari krs_mhs_temp_mk
            $this->db->where('id_mhs', $id_mhs);
            $this->db->where('id_tahun_akademik', $id_ta);
            $this->db->where('id_semester', $id_semester);
            $this->db->delete('krs_mhs_temp_mk');

            // insert ke tabel riwayat_status_mhs. 10-12-2021
            // 12-12-2021 cek apakah sudah ada data, jika blm, maka insert. RALAT: INI DIEKSEKUSI PADA SAAT PERSETUJUAN KRS
            // $sqlxx2 = " select id FROM riwayat_status_mhs WHERE id_semester = '$id_semester' AND id_tahun_akademik = '$id_ta'
            //                     AND id_mhs = '$id_mhs' ";
            // $queryxx2 = $this->db->query($sqlxx2);
            // if ($queryxx2->num_rows() == 0) {
            //     $tgl_skrg = date('Y-m-d H:i:s');
            //     $data = array(
            //         'id_mhs' => $id_mhs,
            //         'id_semester' => $id_semester,
            //         'id_tahun_akademik' => $id_ta,
            //         'id_status_mhs' => 1,
            //         'tgl_input' => $tgl_skrg,
            //         'user_update_by' => $this->session->userdata['username']
            //     );
            //     $this->db->insert('riwayat_status_mhs', $data);
            // }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data KRS berhasil disimpan. Selanjutnya menunggu persetujuan dari dosen PA. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

            redirect("krs");
        }

        // ambil nama ta
        $sqlxx = " select tahun, nama FROM master_tahun_akademik where id = '$id_ta' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $tahun    = $hasilxx->tahun;
        $nama_ta    = $hasilxx->nama;
        //}

        // ambil nama semester
        $sqlxx = " select nama FROM master_semester where id = '$id_semester' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_semester    = $hasilxx->nama;

        // ambil nama prodi
        $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a
                            INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id where a.id = '$id_prodi' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        // ambil nama mhs
        $sqlxx = " select nim, nama FROM mahasiswa where id = '$id_mhs' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama;

        $data["id_ta"] = $id_ta;
        $data["nama_ta"] = $tahun . " (" . $nama_ta . ")";
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["nama_prodi"] = $nama_prodi . " (" . $nama_jenjang . ")";
        $data["mhs"] = $nim . " - " . $nama_mhs;
        $data["id_mhs"] = $id_mhs;

        $data["data_temp_mk"] = $this->Krs_m->get_krs_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_mk"] = $this->Krs_m->get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('krs/add_mk', $data);
        $this->load->view('templates/footer');
    }

    // 24-10-2021
    public function get_mhs_by_prodi_verifiedkeu()
    {
        // ini get data mhs by prodi dan sudah bayar biaya kuliah reguler di semester dan thn akademik yg dipilih
        $id_prodi     = $this->input->post('id_prodi', TRUE);
        $id_ta     = $this->input->post('id_ta', TRUE);
        $id_sem     = $this->input->post('id_sem', TRUE);
        $data['data_mhs'] = $this->Krs_m->get_mhs_by_prodi_verifiedkeu($id_prodi, $id_ta, $id_sem);
        $this->load->view('krs/vlistmhs', $data);
        return true;
    }

    public function hapus_temp_mk($id, $id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('krs_mhs_temp_mk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }
    }

    // 09-11-2021
    public function hapus($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs");
        } else {
            $this->db->trans_begin();

            $this->db->where('id_krs_mhs', $id);
            $this->db->delete('krs_mhs_detail');

            $this->db->where('id', $id);
            $this->db->delete('krs_mhs');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs");
        }
    }

    // 14-11-2021
    public function view($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data KRS Mahasiswa";
        $detail_krs = $this->Krs_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('krs/view', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data KRS Mahasiswa";
        $detail_krs = $this->Krs_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $id_ta    = $hasilxx->id_tahun_akademik;
        $id_semester    = $hasilxx->id_semester;
        $id_prodi    = $hasilxx->id_program_studi;
        $id_mhs    = $hasilxx->id_mhs;

        $nama_semester    = $hasilxx->nama_semester;
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $is_simpan_mk = $this->input->post('is_simpan_mk');

        if ($is_simpan_mk == '1') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // 14-11-2021 cek apakah pernah mengambil mk tsb. jika pernah, maka status = '2' (mengulang)
                $sqlxx2 = " select b.id FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs WHERE a.id_mhs = '$id_mhs'
                AND b.id_mata_kuliah = '$id_mk' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $statusnya = 2;
                } else
                    $statusnya = 1;

                // save ke tabel krs_mhs_detail
                $this->db->trans_begin();

                $data = array(
                    'id_krs_mhs' => $id_krs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks,
                    'status' => $statusnya
                );

                $this->db->insert('krs_mhs_detail', $data);

                // hitung total sks
                $sqlxx = " select sum(sks) as jum FROM krs_mhs_detail where id_krs_mhs = '$id_krs' ";
                $queryxx = $this->db->query($sqlxx);
                $hasilxx = $queryxx->row();
                $total_sks    = $hasilxx->jum;

                // update data di krs_mhs
                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    'total_sks' => $total_sks,
                    'status_verifikasi' => 0,
                    'id_dosen_verifikator' => 0,
                    'alasan_ditolak' => '',
                    'tgl_verifikasi' => '',
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $id_krs);
                $this->db->update('krs_mhs', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("krs/edit/" . $id_krs);
        }

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;
        $data["data_mk"] = $this->Krs_m->get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('krs/edit', $data);
        $this->load->view('templates/footer');
    }

    // hapus detail mk di form edit
    public function hapus_detail_mk($id, $id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs/edit/" . $id_krs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('krs_mhs_detail');

            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM krs_mhs_detail where id_krs_mhs = '$id_krs' ";
            $queryxx = $this->db->query($sqlxx);
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // update data di krs_mhs
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'total_sks' => $total_sks,
                'status_verifikasi' => 0,
                'id_dosen_verifikator' => 0,
                'alasan_ditolak' => '',
                'tgl_verifikasi' => '',
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_krs);
            $this->db->update('krs_mhs', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("krs/edit/" . $id_krs);
        }
    }

    // 21-12-2021 export excel
    public function export_excel($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $detail_krs = $this->Krs_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;
        $nama_fakultas    = $hasilxx->nama_fakultas;
        $nama_dosen    = $hasilxx->nama_dosen;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["nama_fakultas"] = $nama_fakultas;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        $data["nim"] = $nim;
        $data["nama_dosen"] = $nama_dosen;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        $this->load->view('krs/excel_permhs', $data);
        //$this->load->view('templates/footer');
    }
}
