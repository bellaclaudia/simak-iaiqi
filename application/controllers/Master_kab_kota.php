<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_kab_kota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_kab_kota_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $provinsi = $this->Master_kab_kota_m;
        $validation = $this->form_validation;
        $validation->set_rules($provinsi->rules());
        if ($validation->run()) {
            $provinsi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kabupaten/Kota berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_kab_kota");
        }
        $data["title"] = "Kabupaten/Kota";
        $data["data_kab_kota"] = $this->Master_kab_kota_m->getAll();
        $data['provinsi'] = $this->Master_kab_kota_m->getProvinsi();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('kab_kota/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $data['provinsi'] = $this->Master_kab_kota_m->getProvinsi();
        $this->form_validation->set_rules('id_provinsi', 'id_provinsi', 'required');
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kabupaten/Kota gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kab_kota');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "id_provinsi" => $this->input->post('id_provinsi'),
                "kode" => $this->input->post('kode'),
                "nama" => $this->input->post('nama'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_kabupaten_kota', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kabupaten/Kota berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kab_kota');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kabupaten/Kota gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kab_kota');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_kabupaten_kota');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kabupaten/Kota berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kab_kota');
        }
    }
}
