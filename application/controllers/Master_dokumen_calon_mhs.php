<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_dokumen_calon_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_dokumen_calon_mhs_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $Masterdok = $this->Master_dokumen_calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($Masterdok->rules());
        if ($validation->run()) {
            $Masterdok->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dokumen Persyaratan Calon Mhs berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_dokumen_calon_mhs");
        }
        $data["title"] = "Master Dokumen Persyaratan Calon Mhs";
        $data["data_dokumen"] = $this->Master_dokumen_calon_mhs_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('dokumen_calon_mhs/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dokumen Persyaratan Calon Mhs gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('Master_dokumen_calon_mhs');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "kode" => $_POST['kode'],
                "nama" => $_POST['nama'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_dokumen_calon_mhs', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dokumen Persyaratan Calon Mhs berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('Master_dokumen_calon_mhs');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dokumen Persyaratan Calon Mhs gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('Master_dokumen_calon_mhs');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_dokumen_calon_mhs');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dokumen Persyaratan Calon Mhs berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('Master_dokumen_calon_mhs');
        }
    }
}
