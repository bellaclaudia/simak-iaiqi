<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_status_dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_status_dosen_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $provinsi = $this->Master_status_dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($provinsi->rules());
        if ($validation->run()) {
            $provinsi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Status Dosen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_status_dosen");
        }
        $data["title"] = "Status Dosen";
        $data["data_status_dosen"] = $this->Master_status_dosen_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('status_dosen/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Status Dosen gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_status_dosen');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "kode" => $_POST['kode'],
                "nama" => $_POST['nama'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => 'Bella Claudia'
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_status_dosen', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Status Dosen berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_status_dosen');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Status Dosen gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_status_dosen');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_status_dosen');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Status Dosen berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_status_dosen');
        }
    }
}
