<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mahasiswa_m");
        $this->load->model("Transkrip_nilai_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_angkatan = $this->input->post('id_angkatan');
        $id_prodi = $this->input->post('id_prodi');
        $id_kelas = $this->input->post('id_kelas');
        $excel = $this->input->post('excel');
        $excel_pddikti = $this->input->post('excel_pddikti');

        if ($id_angkatan == '') $id_angkatan = 0;
        if ($id_prodi == '') $id_prodi = 0;
        if ($id_kelas == '') $id_kelas = 0;

        $data["title"] = "Mahasiswa";
        $data['data_angkatan'] = $this->Mahasiswa_m->get_angkatan();
        $data["data_prodi"] = $this->Mahasiswa_m->getProgramStudi();
        $data["data_kelas"] = $this->Mahasiswa_m->getKelas();
        $data["data_mahasiswa"] = $this->Mahasiswa_m->getAll($id_angkatan, $id_prodi, $id_kelas);

        $data["id_angkatan"] = $id_angkatan;
        $data["id_prodi"] = $id_prodi;
        $data["id_kelas"] = $id_kelas;

        // if ($excel == '') {
        //     $this->load->view('templates/header', $data);
        //     $this->load->view('templates/menu');
        //     $this->load->view('mahasiswa/index', $data);
        //     $this->load->view('templates/footer');
        // } else
        //     $this->load->view('mahasiswa/excel', $data);

        if ($excel == '' and $excel_pddikti == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mahasiswa/index', $data);
            $this->load->view('templates/footer');
        } else if ($excel_pddikti == '') {
            $this->load->view('mahasiswa/excel', $data);
        } else
            $this->load->view('mahasiswa/excel_pddikti', $data);
    }

    public function tambah()
    {
        $mahasiswa = $this->Mahasiswa_m;
        $validation = $this->form_validation;
        $validation->set_rules($mahasiswa->rules());
        if ($validation->run()) {
            $mahasiswa->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa");
        }
        $data["title"] = "Tambah Data Mahasiswa";
        $data['prodi'] = $this->Mahasiswa_m->getProgramStudi();
        $data['agama'] = $this->Mahasiswa_m->getAgama();
        $data['goldar'] = $this->Mahasiswa_m->getGoldar();
        $data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Mahasiswa_m->getProvinsi();
        $data['kab_kota'] = $this->Mahasiswa_m->getKabKota();
        $data['status_mhs'] = $this->Mahasiswa_m->getStatusMhs();
        $data['kelas'] = $this->Mahasiswa_m->getKelas();
        $data['jenis_pembiayaan'] = $this->Mahasiswa_m->get_jenis_pembiayaan();
        $data['angkatan'] = $this->Mahasiswa_m->get_angkatan();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('mahasiswa');

        $mahasiswa = $this->Mahasiswa_m;
        $validation = $this->form_validation;
        $validation->set_rules($mahasiswa->rules());

        if ($validation->run()) {
            $mahasiswa->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa");
        }
        $data["title"] = "Edit Data Mahasiswa";
        $data["data_mahasiswa"] = $mahasiswa->getById($id);
        $data['prodi'] = $this->Mahasiswa_m->getProgramStudi();
        $data['agama'] = $this->Mahasiswa_m->getAgama();
        $data['goldar'] = $this->Mahasiswa_m->getGoldar();
        $data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Mahasiswa_m->getProvinsi();
        $data['kab_kota'] = $this->Mahasiswa_m->getKabKota();
        $data['status_mhs'] = $this->Mahasiswa_m->getStatusMhs();
        $data['kelas'] = $this->Mahasiswa_m->getKelas();
        $data['jenis_pembiayaan'] = $this->Mahasiswa_m->get_jenis_pembiayaan();
        $data['angkatan'] = $this->Mahasiswa_m->get_angkatan();
        if (!$data["data_mahasiswa"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/edit', $data);
        $this->load->view('templates/footer');
    }

    public function view($id = null)
    {
        if (!isset($id)) redirect('mahasiswa');
        $data["title"] = "View Data Mahasiswa";
        $data["data_mahasiswa"] = $this->Mahasiswa_m->getView($id);
        if (!$data["data_mahasiswa"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('mahasiswa');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('mahasiswa');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('mahasiswa');
        }
    }

    public function biodata_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $mhs = $this->Mahasiswa_m;
        $validation = $this->form_validation;
        $validation->set_rules($mhs->rules());
        if ($validation->run()) {
            $mhs->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa berhasil diupdate.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/biodata_mhs");
        }

        $sqlxx = "select a.id FROM mahasiswa a LEFT JOIN calon_mahasiswa b ON a.id_calon_mhs = b.id 
                  WHERE a.nim = '" . $this->session->userdata['username'] . "' 
                  OR b.no_pendaftaran = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id = $hasilxx->id;
        }

        $data["title"] = "Biodata Mahasiswa";
        $data["data_mahasiswa"] = $mhs->getById($id);
        $data['prodi'] = $this->Mahasiswa_m->getProgramStudi();
        $data['agama'] = $this->Mahasiswa_m->getAgama();
        $data['goldar'] = $this->Mahasiswa_m->getGoldar();
        $data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Mahasiswa_m->getProvinsi();
        $data['kab_kota'] = $this->Mahasiswa_m->getKabKota();
        $data['status_mhs'] = $this->Mahasiswa_m->getStatusMhs();
        $data['kelas'] = $this->Mahasiswa_m->getKelas();
        $data['jenis_pembiayaan'] = $this->Mahasiswa_m->get_jenis_pembiayaan();
        $data['angkatan'] = $this->Mahasiswa_m->get_angkatan();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/biodata_mhs', $data);
        $this->load->view('templates/footer');
    }

    // 05-11-2021
    public function view_biaya_kuliah_reguler_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik. bikin kayak form view biasa, contek dari list calon mhs, ada form tambah data utk input pembayaran

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Pembayaran Biaya Kuliah Reguler";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_pembayaran"] = $this->Mahasiswa_m->get_data_pembayaran_biaya_reguler($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_biaya_kuliah_reguler_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function add_pembayaran_reguler()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == 1) {
            // cek apakah udh ada data pembayaran, jika sudah ada maka munculkan statusnya
            $sqlxx = " select id, status_verifikasi FROM pembayaran_biaya_kuliah WHERE id_mhs = '$id_mhs' AND id_semester = '$id_semester' 
                        AND id_tahun_akademik = '$id_ta' AND jenis_biaya = '1' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $status_verifikasi    = $hasilxx->status_verifikasi;

                if ($status_verifikasi == '0')
                    $desc_status = "Waiting verifikasi";
                else
                    $desc_status = "Sudah approve admin";

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Pembayaran untuk semester tahun akademik ini sudah pernah dilakukan dgn status ' . $desc_status . ' 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

                $data["id_semester"] = $id_semester;
                $data["id_ta"] = $id_ta;
                // munculkan pilihan tahun akademik dan semester
                $data["title"] = "Tambah Data Pembayaran Reguler";
                $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
                $data["data_semester"] = $this->Mahasiswa_m->get_semester();

                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('mahasiswa/add_pembayaran_reguler', $data);
                $this->load->view('templates/footer');
            } else {
                // redirect ke form input konfirmasi pembayaran
                redirect("mahasiswa/add_pembayaran_reguler_next/" . $id_ta . "/" . $id_semester);
            }
        } else {
            // munculkan pilihan tahun akademik dan semester
            $data["title"] = "Tambah Data Pembayaran Reguler";
            $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
            $data["data_semester"] = $this->Mahasiswa_m->get_semester();

            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mahasiswa/add_pembayaran_reguler', $data);
            $this->load->view('templates/footer');
        }
    }

    public function add_pembayaran_reguler_next($id_ta, $id_semester)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();

        // ambil list biaya dari biaya_kuliah_persemester
        $sqlxx = " SELECT a.id_biaya_kuliah, a.jumlah, b.nama as nama_biaya, c.nama as nama_sem, d.nama as nama_ta, d.tahun 
                   FROM biaya_kuliah_persemester a 
                   INNER JOIN master_biaya_kuliah b ON a.id_biaya_kuliah = b.id
                   INNER JOIN master_semester c ON c.id = a.id_semester
                   INNER JOIN master_tahun_akademik d ON d.id = a.id_tahun_akademik
                   WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_program_studi = '$id_prodi'
                   AND b.jenis = 'REGULER' ORDER BY a.id ";
        $queryxx = $this->db->query($sqlxx);
        $jumlahnya = 0;
        if ($queryxx->num_rows() > 0) {
            $data_biaya = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $nama_sem    = $rowxx->nama_sem;
                $nama_ta    = $rowxx->tahun . " (" . $rowxx->nama_ta . ")";
                $nama_biaya    = $rowxx->nama_biaya;
                $jumlah    = $rowxx->jumlah;
                $jumlahnya += $jumlah;

                $data_biaya[] = array(
                    'nama_sem' => $nama_sem,
                    'nama_ta' => $nama_ta,
                    'nama' => $nama_biaya,
                    'nominal' => $jumlah
                );
            }
        } else {
            $data_biaya = '';
            // ambil nama sem dan thn akademik
            $sqlxx = " select nama FROM master_semester WHERE id = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_sem    = $hasilxx->nama;
            }

            $sqlxx = " select nama, tahun FROM master_tahun_akademik WHERE id = '$id_ta' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_ta    = $hasilxx->tahun . " (" . $hasilxx->nama . ")";
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data tagihan biaya untuk Tahun Akademik ' . $nama_ta . ' ' . $nama_sem . ' belum disetting oleh admin. Silahkan tunggu pemberitahuan selanjutnya dari admin, terimakasih 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            redirect("mahasiswa/add_pembayaran_reguler/");
        }

        // ambil list rekening kampus
        $sqlxx = " select id, nama_bank, no_rekening, atas_nama FROM master_rekening ORDER BY id ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $data_rek = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_rekening    = $rowxx->id;
                $nama_bank    = $rowxx->nama_bank;
                $no_rekening    = $rowxx->no_rekening;
                $atas_nama    = $rowxx->atas_nama;

                $data_rek[] = array(
                    'id_rekening' => $id_rekening,
                    'nama_bank' => $nama_bank,
                    'no_rekening' => $no_rekening,
                    'atas_nama' => $atas_nama
                );
            }
        } else {
            $data_rek = '';
        }

        $is_simpan = $this->input->post('is_simpan');
        if ($is_simpan == '1') {
            $jumbayar = $this->input->post('jumbayar');
            $id_rekening = $this->input->post('id_rekening');
            $jenis_bayar = $this->input->post('jenis_bayar');
            $tgl_bayar = $this->input->post('tgl_bayar');

            if ($jenis_bayar == '1') {
                $id_rekening = 0;
            }
            $tgl_skrg = date('Y-m-d H:i:s');

            $this->db->trans_begin();

            $data = array(
                'id_mhs' => $id_mhs,
                'id_semester' => $id_semester,
                'id_tahun_akademik' => $id_ta,
                'jenis_biaya' => 1,
                'nominal' => $jumbayar,
                'id_rekening' => $id_rekening,
                'jenis_bayar' => $jenis_bayar,
                'tgl_bayar' => $tgl_bayar,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('pembayaran_biaya_kuliah', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    Data Konfirmasi Pembayaran Biaya Kuliah Reguler berhasil disimpan. Admin akan memverifikasi data pembayaran anda. Terimakasih. 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button></div>');
            redirect("mahasiswa/pembayaran_reguler_success");
        } else {
            $data["title"] = "Tambah Data Pembayaran Reguler";
            $data['data_biaya'] = $data_biaya;
            $data['jumlahnya'] = $jumlahnya;
            $data['data_rek'] = $data_rek;
            $data['id_mhs'] = $id_mhs;
            $data['mhs'] = $nim . " - " . $nama_mhs;
            $data['nama_sem'] = $nama_sem;
            $data['nama_ta'] = $nama_ta;
            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mahasiswa/add_pembayaran_reguler_next', $data);
            $this->load->view('templates/footer');
        }

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('mahasiswa/add_pembayaran_reguler_next', $data);
        // $this->load->view('templates/footer');

        //---------------------------------------------------------------------------

    }

    public function pembayaran_reguler_success()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/pembayaran_reguler_success');
        $this->load->view('templates/footer');
    }

    public function hapus_pembayaran_reguler($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran Reguler gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_biaya_kuliah_reguler_mhs/");
        } else {
            $this->db->where('id', $id);
            $this->db->delete('pembayaran_biaya_kuliah');

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran Reguler berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_biaya_kuliah_reguler_mhs/");
        }
    }

    // 06-11-2021 pembayaran biaya SP
    public function view_biaya_sp_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik. bikin kayak form view biasa, contek dari list calon mhs, ada form tambah data utk input pembayaran

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Pembayaran Biaya Kuliah SP";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_pembayaran"] = $this->Mahasiswa_m->get_data_pembayaran_biaya_sp($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_biaya_sp_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function add_pembayaran_sp()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == 1) {
            // cek apakah udh ada data pembayaran, jika sudah ada maka munculkan statusnya
            $sqlxx = " select id, status_verifikasi FROM pembayaran_biaya_kuliah WHERE id_mhs = '$id_mhs' AND id_semester = '$id_semester' 
                        AND id_tahun_akademik = '$id_ta' AND jenis_biaya = '2' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $status_verifikasi    = $hasilxx->status_verifikasi;

                if ($status_verifikasi == '0')
                    $desc_status = "Waiting verifikasi";
                else
                    $desc_status = "Sudah approve admin";

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Pembayaran SP untuk semester tahun akademik ini sudah pernah dilakukan dgn status ' . $desc_status . ' 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

                $data["id_semester"] = $id_semester;
                $data["id_ta"] = $id_ta;
                // munculkan pilihan tahun akademik dan semester
                $data["title"] = "Tambah Data Pembayaran SP";
                $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
                $data["data_semester"] = $this->Mahasiswa_m->get_semester();

                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('mahasiswa/add_pembayaran_sp', $data);
                $this->load->view('templates/footer');
            } else {
                // redirect ke form input konfirmasi pembayaran
                redirect("mahasiswa/add_pembayaran_sp_next/" . $id_ta . "/" . $id_semester);
            }
        } else {
            // munculkan pilihan tahun akademik dan semester
            $data["title"] = "Tambah Data Pembayaran SP";
            $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
            $data["data_semester"] = $this->Mahasiswa_m->get_semester();

            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mahasiswa/add_pembayaran_sp', $data);
            $this->load->view('templates/footer');
        }
    }

    public function add_pembayaran_sp_next($id_ta, $id_semester)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();

        // ambil list biaya dari biaya_kuliah_persemester
        $sqlxx = " SELECT a.id_biaya_kuliah, a.jumlah, b.nama as nama_biaya, c.nama as nama_sem, d.nama as nama_ta, d.tahun 
                   FROM biaya_kuliah_sp a 
                   INNER JOIN master_biaya_kuliah b ON a.id_biaya_kuliah = b.id
                   INNER JOIN master_semester c ON c.id = a.id_semester
                   INNER JOIN master_tahun_akademik d ON d.id = a.id_tahun_akademik
                   WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_program_studi = '$id_prodi'
                   AND b.jenis = 'SP' ORDER BY a.id ";
        $queryxx = $this->db->query($sqlxx);
        $jumlahnya = 0;
        if ($queryxx->num_rows() > 0) {
            $data_biaya = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $nama_sem    = $rowxx->nama_sem;
                $nama_ta    = $rowxx->tahun . " (" . $rowxx->nama_ta . ")";
                $nama_biaya    = $rowxx->nama_biaya;
                $jumlah    = $rowxx->jumlah;
                $jumlahnya += $jumlah;

                $data_biaya[] = array(
                    'nama_sem' => $nama_sem,
                    'nama_ta' => $nama_ta,
                    'nama' => $nama_biaya,
                    'nominal' => $jumlah
                );
            }
        } else {
            $data_biaya = '';
            // ambil nama sem dan thn akademik
            $sqlxx = " select nama FROM master_semester WHERE id = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_sem    = $hasilxx->nama;
            }

            $sqlxx = " select nama, tahun FROM master_tahun_akademik WHERE id = '$id_ta' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_ta    = $hasilxx->tahun . " (" . $hasilxx->nama . ")";
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data tagihan biaya SP untuk Tahun Akademik ' . $nama_ta . ' ' . $nama_sem . ' belum disetting oleh admin. Silahkan tunggu pemberitahuan selanjutnya dari admin, terimakasih 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            redirect("mahasiswa/add_pembayaran_sp/");
        }

        // ambil list rekening kampus
        $sqlxx = " select id, nama_bank, no_rekening, atas_nama FROM master_rekening ORDER BY id ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $data_rek = array();
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_rekening    = $rowxx->id;
                $nama_bank    = $rowxx->nama_bank;
                $no_rekening    = $rowxx->no_rekening;
                $atas_nama    = $rowxx->atas_nama;

                $data_rek[] = array(
                    'id_rekening' => $id_rekening,
                    'nama_bank' => $nama_bank,
                    'no_rekening' => $no_rekening,
                    'atas_nama' => $atas_nama
                );
            }
        } else {
            $data_rek = '';
        }

        $is_simpan = $this->input->post('is_simpan');
        if ($is_simpan == '1') {
            $jumbayar = $this->input->post('jumbayar');
            $id_rekening = $this->input->post('id_rekening');
            $jenis_bayar = $this->input->post('jenis_bayar');
            $tgl_bayar = $this->input->post('tgl_bayar');

            if ($jenis_bayar == '1') {
                $id_rekening = 0;
            }
            $tgl_skrg = date('Y-m-d H:i:s');

            $this->db->trans_begin();

            $data = array(
                'id_mhs' => $id_mhs,
                'id_semester' => $id_semester,
                'id_tahun_akademik' => $id_ta,
                'jenis_biaya' => 2,
                'nominal' => $jumbayar,
                'id_rekening' => $id_rekening,
                'jenis_bayar' => $jenis_bayar,
                'tgl_bayar' => $tgl_bayar,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('pembayaran_biaya_kuliah', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    Data Konfirmasi Pembayaran Biaya Kuliah SP berhasil disimpan. Admin akan memverifikasi data pembayaran anda. Terimakasih. 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button></div>');
            redirect("mahasiswa/pembayaran_sp_success");
        } else {
            $data["title"] = "Tambah Data Pembayaran SP";
            $data['data_biaya'] = $data_biaya;
            $data['jumlahnya'] = $jumlahnya;
            $data['data_rek'] = $data_rek;
            $data['id_mhs'] = $id_mhs;
            $data['mhs'] = $nim . " - " . $nama_mhs;
            $data['nama_sem'] = $nama_sem;
            $data['nama_ta'] = $nama_ta;
            $data["id_semester"] = $id_semester;
            $data["id_ta"] = $id_ta;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mahasiswa/add_pembayaran_sp_next', $data);
            $this->load->view('templates/footer');
        }

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('mahasiswa/add_pembayaran_reguler_next', $data);
        // $this->load->view('templates/footer');

        //---------------------------------------------------------------------------

    }

    public function pembayaran_sp_success()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/pembayaran_sp_success');
        $this->load->view('templates/footer');
    }

    public function hapus_pembayaran_sp($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran SP gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_biaya_sp_mhs/");
        } else {
            $this->db->where('id', $id);
            $this->db->delete('pembayaran_biaya_kuliah');

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pembayaran SP berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_biaya_sp_mhs/");
        }
    }

    // 12-11-2021 data status mhs login mhs
    public function view_status_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data Status Mahasiswa";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_status_mhs"] = $this->Mahasiswa_m->get_data_status_mhs($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_status_mhs', $data);
        $this->load->view('templates/footer');
    }

    // view dosen PA
    public function view_dosen_pa()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // $id_semester = $this->input->post('id_semester');
        // $id_ta = $this->input->post('id_ta');

        // if ($id_semester == '') $id_semester = 0;
        // if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data Status Mahasiswa";
        // $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        // $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_dosen_pa"] = $this->Mahasiswa_m->get_data_dosen_pa($id_mhs);

        // $data["id_semester"] = $id_semester;
        // $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_dosen_pa', $data);
        $this->load->view('templates/footer');
    }

    // view KHS mhs
    public function view_khs_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $tgl_skrg = date('Y-m-d');

        // 25-01-2022 ambil data tahun akademik berjalan. kalo blm bayar uang kuliah maka tidak bisa muncul datanya
        $sqltgl = " SELECT id, jenis_semester FROM master_tahun_akademik WHERE 
                    '$tgl_skrg' >= tgl_mulai_semester AND '$tgl_skrg' <= tgl_selesai_semester ";
        // echo $sqltgl;
        // die();
        $querytgl = $this->db->query($sqltgl);
        if ($querytgl->num_rows() > 0) {
            $hasiltgl = $querytgl->row();
            $jenis_semester    = $hasiltgl->jenis_semester;
            $id_ta    = $hasiltgl->id;

            $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                    FROM pembayaran_biaya_kuliah a 
                    INNER JOIN master_semester b ON a.id_semester = b.id 
                    INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                    WHERE a.id_tahun_akademik = '$id_ta' AND a.jenis_biaya = '1' AND a.status_verifikasi = '1' AND a.id_mhs = '$id_mhs' ";

            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() == 0) {
                // $sqlxx2 = " select b.nama as nama_semester FROM master_semester b 
                //             WHERE b.id = '$id_semester' ";
                // $queryxx2 = $this->db->query($sqlxx2);
                // if ($queryxx2->num_rows() > 0) {
                //     $hasilxx2 = $queryxx2->row();
                //     $nama_semester    = $hasilxx2->nama_semester;
                // }

                if ($jenis_semester == '1')
                    $nama_jenis_semester = 'Semester Ganjil';
                else
                    $nama_jenis_semester = 'Semester Genap';

                $sqlxx2 = " select b.nama as nama_tahun_akademik FROM master_tahun_akademik b 
                            WHERE b.id = '$id_ta' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_tahun_akademik    = $hasilxx2->nama_tahun_akademik;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Anda belum melakukan pembayaran biaya kuliah ' . $nama_jenis_semester . ' tahun ' . $nama_tahun_akademik . '. Silahkan lakukan pembayaran terlebih dahulu 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                $blm_bayar = 1;
                //redirect("mahasiswa/input_krs/");
            } else
                $blm_bayar = 0;
        }

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');
        $is_cari = $this->input->post('is_cari');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data KHS Mahasiswa";
        if ($blm_bayar == 0) {
            $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
            $data["data_semester"] = $this->Mahasiswa_m->get_semester();

            $data["data_khs_mhs"] = $this->Mahasiswa_m->get_data_khs_mhs($id_semester, $id_ta, $id_mhs);
            $data["data_komponen"] = $this->Mahasiswa_m->get_komponen_nilai();
        }


        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;
        $data["blm_bayar"] = $blm_bayar;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_khs_mhs', $data);
        $this->load->view('templates/footer');
    }

    // 14-11-2021 view KRS mhs
    public function view_krs_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data KHS Mahasiswa";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_krs_mhs"] = $this->Mahasiswa_m->get_data_krs_mhs($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_krs_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function detail_krs($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data KRS Mahasiswa";
        $detail_krs = $this->Mahasiswa_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/detail_krs', $data);
        $this->load->view('templates/footer');
    }

    public function hapus_krs($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_krs_mhs");
        } else {
            $this->db->trans_begin();

            $this->db->where('id_krs_mhs', $id);
            $this->db->delete('krs_mhs_detail');

            $this->db->where('id', $id);
            $this->db->delete('krs_mhs');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_krs_mhs");
        }
    }

    public function input_krs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // $krs = $this->Mahasiswa_m;
        // $validation = $this->form_validation;
        // $validation->set_rules($krs->rules());
        // if ($validation->run()) {
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == '1') {
            // 10-12-2021 cek apakah sudah pembayaran biaya kuliah.
            // modif 25-01-2022 request IAIQI. pengecekan keuangan dilepas aja. ini dipindah ke KHS
            /*$sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                    FROM pembayaran_biaya_kuliah a 
                    INNER JOIN master_semester b ON a.id_semester = b.id 
                    INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                    WHERE a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_ta' AND a.jenis_biaya = '1' AND a.status_verifikasi = '1' AND a.id_mhs = '$id_mhs' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() == 0) {
                $sqlxx2 = " select b.nama as nama_semester FROM master_semester b 
                            WHERE b.id = '$id_semester' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_semester    = $hasilxx2->nama_semester;
                }

                $sqlxx2 = " select b.nama as nama_tahun_akademik FROM master_tahun_akademik b 
                            WHERE b.id = '$id_ta' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_tahun_akademik    = $hasilxx2->nama_tahun_akademik;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Anda belum melakukan pembayaran biaya kuliah ' . $nama_semester . ' tahun ' . $nama_tahun_akademik . '. Silahkan lakukan pembayaran terlebih dahulu 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                redirect("mahasiswa/input_krs/");
            } 
            else { */
            // cek data KRS, apakah sudah ada. jika sudah ada maka munculkan pesan
            $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                        FROM krs_mhs a INNER JOIN master_semester b ON a.id_semester = b.id 
                        INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_mhs = '$id_mhs'
                        ";
            //      AND a.id_mhs in (select id_mhs FROM pembayaran_biaya_kuliah WHERE id_semester = '$id_semester' 
            // AND id_tahun_akademik = '$id_ta' AND jenis_biaya = '1' AND status_verifikasi = '1')
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_semester    = $hasilxx->nama_semester;
                $nama_tahun_akademik    = $hasilxx->tahun . " (" . $hasilxx->nama_tahun_akademik . ")";

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Data KRS ' . $nama_semester . ' tahun ' . $nama_tahun_akademik . ' sudah pernah diinput. Silahkan ulangi lagi 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                redirect("mahasiswa/input_krs/");
            } else {
                redirect("mahasiswa/input_krs_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
            }
            // }
        }

        // ------------------------------------------

        // } else {
        $data["title"] = "Tambah Data KRS";
        $data["data_prodi"] = $this->Mahasiswa_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/input_krs', $data);
        $this->load->view('templates/footer');
        // }
    }

    public function input_krs_detail_mk($id_ta, $id_semester, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_simpan_mk = $this->input->post('is_simpan_mk');
        $submit_krs = $this->input->post('submit_krs');

        if ($is_simpan_mk == '1' && $submit_krs == '') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel temp
                $this->db->trans_begin();

                $data = array(
                    'id_tahun_akademik' => $id_ta,
                    'id_semester' => $id_semester,
                    'id_program_studi' => $id_prodi,
                    'id_mhs' => $id_mhs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks
                );

                $this->db->insert('krs_mhs_temp_mk', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("mahasiswa/input_krs_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        }

        if ($submit_krs == '1') {
            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM krs_mhs_temp_mk where id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
                        AND id_semester = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            //if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // simpan ke krs_mhs
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_tahun_akademik' => $id_ta,
                'id_semester' => $id_semester,
                'id_mhs' => $id_mhs,
                'total_sks' => $total_sks,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('krs_mhs', $data);

            $id_krs = $this->db->insert_id();

            // simpan ke krs_mhs_detail
            // ambil data dari krs_mhs_temp_mk
            $sqlxx = " select id_mata_kuliah, sks FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
            AND id_semester = '$id_semester' ORDER BY id ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_mk_temp   = $rowxx->id_mata_kuliah;
                    $sks_temp    = $rowxx->sks;
                    //$id_jadwal_temp = $rowxx->id_jadwal_kuliah;

                    // 13-11-2021 cek apakah pernah mengambil mk tsb. jika pernah, maka status = '2' (mengulang)
                    $sqlxx2 = " select b.id FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs WHERE a.id_mhs = '$id_mhs'
                                AND b.id_mata_kuliah = '$id_mk_temp' ";
                    $queryxx2 = $this->db->query($sqlxx2);
                    if ($queryxx2->num_rows() > 0) {
                        $statusnya = 2;
                    } else
                        $statusnya = 1;

                    $data = array(
                        'id_krs_mhs' => $id_krs,
                        'id_mata_kuliah' => $id_mk_temp,
                        'sks' => $sks_temp,
                        //'id_jadwal_kuliah' => $id_jadwal_temp,
                        'status' => $statusnya
                    );

                    $this->db->insert('krs_mhs_detail', $data);
                }
            }

            // hapus kembali dari krs_mhs_temp_mk
            $this->db->where('id_mhs', $id_mhs);
            $this->db->where('id_tahun_akademik', $id_ta);
            $this->db->where('id_semester', $id_semester);
            $this->db->delete('krs_mhs_temp_mk');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data KRS berhasil disimpan. Selanjutnya menunggu persetujuan dari dosen PA. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

            redirect("mahasiswa/view_krs_mhs");
        }

        // ambil nama ta
        $sqlxx = " select tahun, nama FROM master_tahun_akademik where id = '$id_ta' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $tahun    = $hasilxx->tahun;
        $nama_ta    = $hasilxx->nama;
        //}

        // ambil nama semester
        $sqlxx = " select nama FROM master_semester where id = '$id_semester' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_semester    = $hasilxx->nama;

        // ambil nama mhs
        $sqlxx = " select nim, nama FROM mahasiswa where id = '$id_mhs' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama;

        // ambil nama prodi
        $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a
                            INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id where a.id = '$id_prodi' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $data["id_ta"] = $id_ta;
        $data["nama_ta"] = $tahun . " (" . $nama_ta . ")";
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["nama_prodi"] = $nama_prodi . " (" . $nama_jenjang . ")";
        $data["mhs"] = $nim . " - " . $nama_mhs;
        $data["id_mhs"] = $id_mhs;

        $data["data_temp_mk"] = $this->Mahasiswa_m->get_krs_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_mk"] = $this->Mahasiswa_m->get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/input_krs_mk', $data);
        $this->load->view('templates/footer');
    }

    // hapus temp mk KRS
    public function hapus_temp_mk($id, $id_ta, $id_semester, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/input_krs_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('krs_mhs_temp_mk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/input_krs_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        }
    }

    public function edit_krs($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data KRS Mahasiswa";
        $detail_krs = $this->Mahasiswa_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $id_ta    = $hasilxx->id_tahun_akademik;
        $id_semester    = $hasilxx->id_semester;
        $id_prodi    = $hasilxx->id_program_studi;
        $id_mhs    = $hasilxx->id_mhs;

        $nama_semester    = $hasilxx->nama_semester;
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $is_simpan_mk = $this->input->post('is_simpan_mk');

        if ($is_simpan_mk == '1') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // 14-11-2021 cek apakah pernah mengambil mk tsb. jika pernah, maka status = '2' (mengulang)
                $sqlxx2 = " select b.id FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs WHERE a.id_mhs = '$id_mhs'
                AND b.id_mata_kuliah = '$id_mk' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $statusnya = 2;
                } else
                    $statusnya = 1;

                // save ke tabel krs_mhs_detail
                $this->db->trans_begin();

                $data = array(
                    'id_krs_mhs' => $id_krs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks,
                    'status' => $statusnya
                );

                $this->db->insert('krs_mhs_detail', $data);

                // hitung total sks
                $sqlxx = " select sum(sks) as jum FROM krs_mhs_detail where id_krs_mhs = '$id_krs' ";
                $queryxx = $this->db->query($sqlxx);
                $hasilxx = $queryxx->row();
                $total_sks    = $hasilxx->jum;

                // update data di krs_mhs
                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    'total_sks' => $total_sks,
                    'status_verifikasi' => 0,
                    'id_dosen_verifikator' => 0,
                    'alasan_ditolak' => '',
                    'tgl_verifikasi' => '',
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $id_krs);
                $this->db->update('krs_mhs', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("mahasiswa/edit_krs/" . $id_krs);
        }

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;
        $data["data_mk"] = $this->Mahasiswa_m->get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/edit_krs', $data);
        $this->load->view('templates/footer');
    }

    // hapus detail mk di form edit
    public function hapus_detail_mk($id, $id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/edit_krs/" . $id_krs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('krs_mhs_detail');

            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM krs_mhs_detail where id_krs_mhs = '$id_krs' ";
            $queryxx = $this->db->query($sqlxx);
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // update data di krs_mhs
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'total_sks' => $total_sks,
                'status_verifikasi' => 0,
                'id_dosen_verifikator' => 0,
                'alasan_ditolak' => '',
                'tgl_verifikasi' => '',
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_krs);
            $this->db->update('krs_mhs', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/edit_krs/" . $id_krs);
        }
    }

    // 20-11-2021 view SP
    public function view_sp_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data SP Mahasiswa";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_sp_mhs"] = $this->Mahasiswa_m->get_data_sp_mhs($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_sp_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function detail_sp($id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data SP Mahasiswa";
        $detail_sp = $this->Mahasiswa_m->get_detail_sp($id_sp);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_sp->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_sp"] = $detail_sp->result();
        $data["id_sp"] = $id_sp;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/detail_sp', $data);
        $this->load->view('templates/footer');
    }

    public function hapus_sp($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_sp_mhs");
        } else {
            $this->db->trans_begin();

            $this->db->where('id_sp_mhs', $id);
            $this->db->delete('sp_mhs_detail');

            $this->db->where('id', $id);
            $this->db->delete('sp_mhs');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/view_sp_mhs");
        }
    }

    public function input_sp()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // $krs = $this->Mahasiswa_m;
        // $validation = $this->form_validation;
        // $validation->set_rules($krs->rules());
        // if ($validation->run()) {
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $is_cek = $this->input->post('is_cek');

        if ($is_cek == '1') {
            // 10-12-2021 cek apakah sudah pembayaran biaya kuliah SP
            $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                    FROM pembayaran_biaya_kuliah a 
                    INNER JOIN master_semester b ON a.id_semester = b.id 
                    INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                    WHERE a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_ta' AND a.jenis_biaya = '2' AND a.status_verifikasi = '1' AND a.id_mhs = '$id_mhs' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() == 0) {
                $sqlxx2 = " select b.nama as nama_semester FROM master_semester b 
                            WHERE b.id = '$id_semester' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_semester    = $hasilxx2->nama_semester;
                }

                $sqlxx2 = " select b.nama as nama_tahun_akademik FROM master_tahun_akademik b 
                            WHERE b.id = '$id_ta' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_tahun_akademik    = $hasilxx2->nama_tahun_akademik;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Anda belum melakukan pembayaran biaya SP ' . $nama_semester . ' tahun ' . $nama_tahun_akademik . '. Silahkan lakukan pembayaran terlebih dahulu 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                redirect("mahasiswa/input_sp/");
            } else {
                // cek data SP, apakah sudah ada. jika sudah ada maka munculkan pesan
                $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                        FROM sp_mhs a INNER JOIN master_semester b ON a.id_semester = b.id 
                        INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_mhs = '$id_mhs'
                        ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    $nama_semester    = $hasilxx->nama_semester;
                    $nama_tahun_akademik    = $hasilxx->tahun . " (" . $hasilxx->nama_tahun_akademik . ")";

                    $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Data SP ' . $nama_semester . ' tahun ' . $nama_tahun_akademik . ' sudah pernah diinput. Silahkan ulangi lagi 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                    redirect("mahasiswa/input_sp/");
                } else {
                    redirect("mahasiswa/input_sp_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
                }
            }
            // -------------------------

            // cek data SP, apakah sudah ada. jika sudah ada maka munculkan pesan
            // $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
            //         FROM sp_mhs a INNER JOIN master_semester b ON a.id_semester = b.id 
            //         INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
            //         WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_ta' AND a.id_mhs = '$id_mhs' ";
            // $queryxx = $this->db->query($sqlxx);
            // if ($queryxx->num_rows() > 0) {
            //     $hasilxx = $queryxx->row();
            //     $nama_semester    = $hasilxx->nama_semester;
            //     $nama_tahun_akademik    = $hasilxx->tahun . " (" . $hasilxx->nama_tahun_akademik . ")";

            //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            //     Data SP ' . $nama_semester . ' tahun ' . $nama_tahun_akademik . ' sudah pernah diinput. Silahkan ulangi lagi 
            //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            //     <span aria-hidden="true">&times;</span>
            //     </button></div>');
            //     redirect("mahasiswa/input_sp/");
            // } else {
            //     redirect("mahasiswa/input_sp_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
            // }
        }

        // ------------------------------------------

        // } else {
        $data["title"] = "Tambah Data SP";
        $data["data_prodi"] = $this->Mahasiswa_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/input_sp', $data);
        $this->load->view('templates/footer');
        // }
    }

    // detail mk
    public function input_sp_detail_mk($id_ta, $id_semester, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        $is_simpan_mk = $this->input->post('is_simpan_mk');
        $submit_krs = $this->input->post('submit_krs');

        if ($is_simpan_mk == '1' && $submit_krs == '') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel temp
                $this->db->trans_begin();

                $data = array(
                    'id_tahun_akademik' => $id_ta,
                    'id_semester' => $id_semester,
                    'id_program_studi' => $id_prodi,
                    'id_mhs' => $id_mhs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks
                );

                $this->db->insert('sp_mhs_temp_mk', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("mahasiswa/input_sp_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        }

        if ($submit_krs == '1') {
            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM sp_mhs_temp_mk where id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
                        AND id_semester = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            //if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // simpan ke krs_mhs
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_tahun_akademik' => $id_ta,
                'id_semester' => $id_semester,
                'id_mhs' => $id_mhs,
                'total_sks' => $total_sks,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('sp_mhs', $data);

            $id_krs = $this->db->insert_id();

            // simpan ke sp_mhs_detail
            // ambil data dari sp_mhs_temp_mk
            $sqlxx = " select id_mata_kuliah, sks FROM sp_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
            AND id_semester = '$id_semester' ORDER BY id ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_mk_temp   = $rowxx->id_mata_kuliah;
                    $sks_temp    = $rowxx->sks;
                    //$id_jadwal_temp = $rowxx->id_jadwal_kuliah;

                    $data = array(
                        'id_sp_mhs' => $id_krs,
                        'id_mata_kuliah' => $id_mk_temp,
                        'sks' => $sks_temp
                    );

                    $this->db->insert('sp_mhs_detail', $data);
                }
            }

            // hapus kembali dari sp_mhs_temp_mk
            $this->db->where('id_mhs', $id_mhs);
            $this->db->where('id_tahun_akademik', $id_ta);
            $this->db->where('id_semester', $id_semester);
            $this->db->delete('sp_mhs_temp_mk');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data SP berhasil disimpan. Selanjutnya menunggu persetujuan dari dosen PA. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

            redirect("mahasiswa/view_sp_mhs");
        }

        // ambil nama ta
        $sqlxx = " select tahun, nama FROM master_tahun_akademik where id = '$id_ta' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $tahun    = $hasilxx->tahun;
        $nama_ta    = $hasilxx->nama;
        //}

        // ambil nama semester
        $sqlxx = " select nama FROM master_semester where id = '$id_semester' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_semester    = $hasilxx->nama;

        // ambil nama mhs
        $sqlxx = " select nim, nama FROM mahasiswa where id = '$id_mhs' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama;

        // ambil nama prodi
        $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a
                            INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id where a.id = '$id_prodi' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $data["id_ta"] = $id_ta;
        $data["nama_ta"] = $tahun . " (" . $nama_ta . ")";
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["nama_prodi"] = $nama_prodi . " (" . $nama_jenjang . ")";
        $data["mhs"] = $nim . " - " . $nama_mhs;
        $data["id_mhs"] = $id_mhs;

        $data["data_temp_mk"] = $this->Mahasiswa_m->get_sp_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_mk"] = $this->Mahasiswa_m->get_mk_by_prodi_semester_sp($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/input_sp_mk', $data);
        $this->load->view('templates/footer');
    }

    // hapus temp mk SP
    public function hapus_temp_mk_sp($id, $id_ta, $id_semester, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/input_sp_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('sp_mhs_temp_mk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/input_sp_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_mhs);
        }
    }

    // edit SP
    public function edit_sp($id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data SP Mahasiswa";
        $detail_sp = $this->Mahasiswa_m->get_detail_sp($id_sp);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_sp->row();
        $id_ta    = $hasilxx->id_tahun_akademik;
        $id_semester    = $hasilxx->id_semester;
        $id_prodi    = $hasilxx->id_program_studi;
        $id_mhs    = $hasilxx->id_mhs;

        $nama_semester    = $hasilxx->nama_semester;
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $is_simpan_mk = $this->input->post('is_simpan_mk');

        if ($is_simpan_mk == '1') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel krs_mhs_detail
                $this->db->trans_begin();

                $data = array(
                    'id_sp_mhs' => $id_sp,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks
                );

                $this->db->insert('sp_mhs_detail', $data);

                // hitung total sks
                $sqlxx = " select sum(sks) as jum FROM sp_mhs_detail where id_sp_mhs = '$id_sp' ";
                $queryxx = $this->db->query($sqlxx);
                $hasilxx = $queryxx->row();
                $total_sks    = $hasilxx->jum;

                // update data di sp_mhs
                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    'total_sks' => $total_sks,
                    'status_verifikasi' => 0,
                    'id_dosen_verifikator' => 0,
                    'alasan_ditolak' => '',
                    'tgl_verifikasi' => '',
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $id_sp);
                $this->db->update('sp_mhs', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("mahasiswa/edit_sp/" . $id_sp);
        }

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_sp"] = $detail_sp->result();
        $data["id_sp"] = $id_sp;
        $data["data_mk"] = $this->Mahasiswa_m->get_mk_by_prodi_semester_sp_formedit($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/edit_sp', $data);
        $this->load->view('templates/footer');
    }

    // hapus detail mk di form edit SP
    public function hapus_detail_mk_sp($id, $id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/edit_sp/" . $id_sp);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('sp_mhs_detail');

            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM sp_mhs_detail where id_sp_mhs = '$id_sp' ";
            $queryxx = $this->db->query($sqlxx);
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // update data di krs_mhs
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'total_sks' => $total_sks,
                'status_verifikasi' => 0,
                'id_dosen_verifikator' => 0,
                'alasan_ditolak' => '',
                'tgl_verifikasi' => '',
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_sp);
            $this->db->update('sp_mhs', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("mahasiswa/edit_sp/" . $id_sp);
        }
    }

    // 20-11-2021
    public function view_jadwal_kuliah_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id mhs
        $sqlxx = " select a.id, a.nama, a.nim, a.id_program_studi FROM mahasiswa a INNER JOIN master_user b ON a.id_user = b.id
                   WHERE b.username = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nim    = $hasilxx->nim;
            $id_mhs = $hasilxx->id;
            $nama_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi;
        }

        // ambil list thn akademik, semester, mata kuliah

        $id_semester = $this->input->post('id_semester');
        $id_ta = $this->input->post('id_ta');

        if ($id_semester == '') $id_semester = 0;
        if ($id_ta == '') $id_ta = 0;

        $data["title"] = "Data Jadwal Kuliah";
        $data["data_thn_akademik"] = $this->Mahasiswa_m->get_tahun_akademik();
        $data["data_semester"] = $this->Mahasiswa_m->get_semester();
        $data["data_jadwal"] = $this->Mahasiswa_m->get_data_mk($id_semester, $id_ta, $id_mhs);

        $data["id_semester"] = $id_semester;
        $data["id_ta"] = $id_ta;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mahasiswa/view_jadwal_kuliah_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function transkrip_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // 25-01-2022 dikomen
        //$id_tahun_akademik = $this->input->post('id_tahun_akademik');
        //$pdf = $this->input->post('pdf');

        //if ($id_tahun_akademik == '') $id_tahun_akademik = 0;

        $sqlxx = "select a.id, a.id_program_studi FROM mahasiswa a WHERE a.nim = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id = $hasilxx->id;
            $id_program_studi = $hasilxx->id_program_studi;
        }
        $tgl_skrg = date('Y-m-d');

        // 25-01-2022 ambil data tahun akademik berjalan. kalo blm bayar uang kuliah maka tidak bisa muncul datanya
        $sqltgl = " SELECT id, jenis_semester FROM master_tahun_akademik WHERE 
                    '$tgl_skrg' >= tgl_mulai_semester AND '$tgl_skrg' <= tgl_selesai_semester ";
        // echo $sqltgl;
        // die();
        $querytgl = $this->db->query($sqltgl);
        if ($querytgl->num_rows() > 0) {
            $hasiltgl = $querytgl->row();
            $jenis_semester    = $hasiltgl->jenis_semester;
            $id_ta    = $hasiltgl->id;

            $sqlxx = " select a.id, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik 
                    FROM pembayaran_biaya_kuliah a 
                    INNER JOIN master_semester b ON a.id_semester = b.id 
                    INNER JOIN master_tahun_akademik c ON a.id_tahun_akademik = c.id
                    WHERE a.id_tahun_akademik = '$id_ta' AND a.jenis_biaya = '1' AND a.status_verifikasi = '1' AND a.id_mhs = '$id' ";

            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() == 0) {
                // $sqlxx2 = " select b.nama as nama_semester FROM master_semester b 
                //             WHERE b.id = '$id_semester' ";
                // $queryxx2 = $this->db->query($sqlxx2);
                // if ($queryxx2->num_rows() > 0) {
                //     $hasilxx2 = $queryxx2->row();
                //     $nama_semester    = $hasilxx2->nama_semester;
                // }

                if ($jenis_semester == '1')
                    $nama_jenis_semester = 'Semester Ganjil';
                else
                    $nama_jenis_semester = 'Semester Genap';

                $sqlxx2 = " select b.nama as nama_tahun_akademik FROM master_tahun_akademik b 
                            WHERE b.id = '$id_ta' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $nama_tahun_akademik    = $hasilxx2->nama_tahun_akademik;
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        Anda belum melakukan pembayaran biaya kuliah ' . $nama_jenis_semester . ' tahun ' . $nama_tahun_akademik . '. Silahkan lakukan pembayaran terlebih dahulu 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button></div>');
                $blm_bayar = 1;
                //redirect("mahasiswa/input_krs/");
            } else
                $blm_bayar = 0;
        }

        $data["title"] = "Transkrip Nilai";
        //$data["tahun_akademik"] = $this->Transkrip_nilai_m->getTahunAkademik();
        //$data["mhs"] = $this->Transkrip_nilai_m->getMahasiswa();

        if ($blm_bayar == 0)
            $data["data_transkrip_nilai"] = $this->Transkrip_nilai_m->getPerMhs($id);

        //$data["id_tahun_akademik"] = $id_tahun_akademik;
        $data["id_mhs"] = $id;
        $data["blm_bayar"] = $blm_bayar;

        //if ($pdf == '') {
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('transkrip_nilai/mhs', $data);
        $this->load->view('templates/footer');
        // } else
        //     $this->load->view('transkrip_nilai/pdf', $data);
    }

    // 21-12-2021 export excel
    public function export_excel($id_krs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $detail_krs = $this->Mahasiswa_m->get_detail_krs($id_krs);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_krs->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $id_mhs    = $hasilxx->id_mhs;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;
        $nama_fakultas    = $hasilxx->nama_fakultas;

        // 15-06-2022 ambil nama dosen pembimbing
        //$nama_dosen    = $hasilxx->nama_dosen;
        $sqlxx2 = "select a.nama as nama_dosen FROM dosen a LEFT JOIN dosen_pembimbing_akademik_mhs b ON a.id = b.id_dosen
                  LEFT JOIN mahasiswa c ON c.id_kelas = b.id_kelas
                  WHERE c.id = '" . $id_mhs . "' ";
        $queryxx2 = $this->db->query($sqlxx2);
        if ($queryxx2->num_rows() > 0) {
            $hasilxx2 = $queryxx2->row();
            $nama_dosen    = $hasilxx2->nama_dosen;
        } else
            $nama_dosen = "";

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["nama_fakultas"] = $nama_fakultas;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        $data["nim"] = $nim;
        $data["nama_dosen"] = $nama_dosen;
        //}

        $data["detail_krs"] = $detail_krs->result();
        $data["id_krs"] = $id_krs;

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/menu');
        $this->load->view('mahasiswa/excel_permhs', $data);
        //$this->load->view('templates/footer');
    }
}
