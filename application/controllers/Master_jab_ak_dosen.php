<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_jab_ak_dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_jab_ak_dosen_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $provinsi = $this->Master_jab_ak_dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($provinsi->rules());
        if ($validation->run()) {
            $provinsi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jabatan Akademik Dosen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_jab_ak_dosen");
        }
        $data["title"] = "Jabatan Akademik Dosen";
        $data["data_jab_ak_dosen"] = $this->Master_jab_ak_dosen_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('jab_ak_dosen/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jabatan Akademik Dosen gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jab_ak_dosen');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "kode" => $_POST['kode'],
                "nama" => $_POST['nama'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_jabatan_akademik_dosen', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jabatan Akademik Dosen berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jab_ak_dosen');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jabatan Akademik Dosen gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jab_ak_dosen');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_jabatan_akademik_dosen');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jabatan Akademik Dosen berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jab_ak_dosen');
        }
    }
}
