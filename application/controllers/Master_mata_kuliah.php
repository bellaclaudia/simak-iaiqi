<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_mata_kuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_mata_kuliah_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        //$id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        //$cari = $this->input->post('cari');
        $excel = $this->input->post('excel');

        //if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        // echo $excel;
        // die();

        $data["title"] = "Mata Kuliah";
        $data["data_mata_kuliah"] = $this->Master_mata_kuliah_m->getAll($id_semester, $id_prodi);
        $data["data_prodi"] = $this->Master_mata_kuliah_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Master_mata_kuliah_m->getTahunAkademik();
        $data["data_semester"] = $this->Master_mata_kuliah_m->get_semester();

        //$data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('mata_kuliah/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('mata_kuliah/excel', $data);
    }

    public function export($id_ta, $id_semester, $id_prodi)
    {
        $id_ta = $this->input->get('id_ta');
        $id_semester = $this->input->get('id_semester');
        $id_prodi = $this->input->get('id_prodi');
        // print_r($id_ta); die();

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $data["title"] = "Mata Kuliah";
        $data_mata_kuliah = $this->Master_mata_kuliah_m->getAll($id_ta, $id_semester, $id_prodi);
        $data = array('title' => 'Master Mata Kuliah', 'data_mata_kuliah' => $data_mata_kuliah);
        $this->load->view('mata_kuliah/export', $data);
    }

    public function template()
    {
        $data["title"] = "Template Mata Kuliah";
        $data["data_template_matkul"] = $this->Master_mata_kuliah_m->getTemplateMataKuliah()->result_array();
        $data['semester'] = $this->Master_mata_kuliah_m->getSemester();
        $data['program_studi'] = $this->Master_mata_kuliah_m->getProgramStudi();
        $data['tahun_akademik'] = $this->Master_mata_kuliah_m->getTahunAkademik();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mata_kuliah/template', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $MataKuliah = $this->Master_mata_kuliah_m;
        $validation = $this->form_validation;
        $validation->set_rules($MataKuliah->rules());
        if ($validation->run()) {
            $MataKuliah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_mata_kuliah");
        }
        $data["title"] = "Tambah Data Master Mata Kuliah";
        $data['jenis_mk'] = $this->Master_mata_kuliah_m->getJenisMk();
        $data['metode_pembelajaran'] = $this->Master_mata_kuliah_m->getMetodePembelajaran();
        $data['semester'] = $this->Master_mata_kuliah_m->getSemester();
        $data['program_studi'] = $this->Master_mata_kuliah_m->getProgramStudi();
        $data['tahun_akademik'] = $this->Master_mata_kuliah_m->getTahunAkademik();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mata_kuliah/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('master_mata_kuliah');

        $MataKuliah = $this->Master_mata_kuliah_m;
        $validation = $this->form_validation;
        $validation->set_rules($MataKuliah->rules());

        if ($validation->run()) {
            $MataKuliah->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("master_mata_kuliah");
        }
        $data["title"] = "Edit Data Mata Kuliah";
        $data["data_mata_kuliah"] = $MataKuliah->getById($id);
        $data['jenis_mk'] = $this->Master_mata_kuliah_m->getJenisMk();
        $data['metode_pembelajaran'] = $this->Master_mata_kuliah_m->getMetodePembelajaran();
        $data['semester'] = $this->Master_mata_kuliah_m->getSemester();
        $data['program_studi'] = $this->Master_mata_kuliah_m->getProgramStudi();
        $data['tahun_akademik'] = $this->Master_mata_kuliah_m->getTahunAkademik();
        if (!$data["data_mata_kuliah"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mata_kuliah/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_mata_kuliah');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_mata_kuliah');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_mata_kuliah');
        }
    }

    public function submit_template()
    {
        $nama_admin = $this->session->userdata['username'];
        $dir = './uploads/template_matkul/';
        $config['allowed_types'] = 'csv';
        $config['overwrite'] = false;
        $config['max_size'] = 5120;
        $config['upload_path'] = $dir;
        $this->load->library('upload', $config);
        $error = false;

        if (!empty($_FILES['file_csv']['name'])) {

            $up = $this->upload->do_upload('file_csv');
            if ($up) {
                $upl = $this->upload->data();
            } else {
                $this->upload->display_errors();
                $error = true;
            }
        }
        $post = $this->input->post();
        $input = array(
            'nama_file' => $upl['file_name'],
            'tgl_input' => date('Y-m-d H:i'),
            'user_update_by' => $nama_admin
        );
        $this->Master_mata_kuliah_m->actUploadTemplate($input);
        $this->template();
    }

    function reload()
    {
        $data['listdata'] = $this->Master_mata_kuliah_m->getTemplateMataKuliah()->result_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('mata_kuliah/template', $data);
        $this->load->view('templates/footer');
    }

    function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 0, $delimiter)) !== FALSE) {

                if (!$header) {
                    $header = $row;
                } else {
                    if (count($header) != count($row)) {
                        continue;
                    }

                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }

    function cobaCSV()
    {
        $lokasi = 'uploads/file_triwulan/2017_PUSAT_7.csv';
        $a = $this->csv_to_array($lokasi, ',');
        print_r($a);
    }

    function proses_data($id)
    {
        $nama_admin = $this->session->userdata['username'];
        $post = $this->input->post();
        $filedata = $this->Master_mata_kuliah_m->getTemplateMataKuliah($id)->row_array();
        $path_file = './uploads/template_matkul/' . $filedata['nama_file'];
        $aardata = $this->csv_to_array($path_file, ',');
        $x = 0;
        $arr_log = array();
        if (isset($aardata[0]['kode'])) {
            $arrUpdate = array();
            foreach ($aardata as $r => $val) {
                $input = array(
                    'id_jenis_mk' => $val['id_jenis_mk'],
                    'kode' => $val['kode'],
                    'nama' => $val['nama'],
                    'sks' => $val['sks'],
                    'sks_tatap_muka' => $val['sks_tatap_muka'],
                    'sks_praktik' => $val['sks_praktik'],
                    'sks_praktik_lapangan' => $val['sks_praktik_lapangan'],
                    'sks_simulasi' => $val['sks_simulasi'],
                    'id_metode_pembelajaran' => $val['id_metode_pembelajaran'],
                    'tgl_mulai_efektif' => $val['tgl_mulai_efektif'],
                    'tgl_akhir_efektif' => $val['tgl_akhir_efektif'],
                    'id_semester' => $val['id_semester'],
                    'id_program_studi' => $val['id_program_studi'],
                    'id_tahun_akademik' => $val['id_tahun_akademik'],
                    'tgl_input' => date('Y-m-d H:i:s'),
                    'user_update_by' => $this->session->userdata['username']
                );
                // print_r($input); die();
                // $this->Master_mata_kuliah_m->updateDataPph21($input);
                $data = $this->Master_mata_kuliah_m->getDataTemplate($val['kode'], $val['id_tahun_akademik'], $val['id_program_studi'])->row_array();
                if (!empty($data['kode'] and $data['id_tahun_akademik'] and $data['id_program_studi'])) {
                    $input['id_jenis_mk'] = $val['id_jenis_mk'];
                    $input['kode'] = $val['kode'];
                    $input['nama'] = $val['nama'];
                    $input['sks'] = $val['sks'];
                    $input['sks_tatap_muka'] = $val['sks_tatap_muka'];
                    $input['sks_praktik'] = $val['sks_praktik'];
                    $input['sks_praktik_lapangan'] = $val['sks_praktik_lapangan'];
                    $input['sks_simulasi'] = $val['sks_simulasi'];
                    $input['id_metode_pembelajaran'] = $val['id_metode_pembelajaran'];
                    $input['tgl_mulai_efektif'] = $val['tgl_mulai_efektif'];
                    $input['tgl_akhir_efektif'] = $val['tgl_akhir_efektif'];
                    $input['id_semester'] = $val['id_semester'];
                    $input['id_program_studi'] = $val['id_program_studi'];
                    $input['id_tahun_akademik'] = $val['id_tahun_akademik'];
                    $input['tgl_update'] = date('Y-m-d H:i:s');
                    $input['user_update_by'] = $this->session->userdata['username'];
                    $this->Master_mata_kuliah_m->updateData('update', $input);
                } else {
                    $this->Master_mata_kuliah_m->updateData('add', $input);
                }
                $a = $this->db->affected_rows();
                // echo $a;
                $arr_log[] = array(
                    'status' => $a == 1 ? 'Berhasil' : 'Gagal'
                );
            }
            $up = $this->Master_mata_kuliah_m->updateDataTemplate(array(
                'id' => $id, 'process_time' => date('Y-m-d H:i'), 'process_by' => $nama_admin
            ));
            $er = array('error' => 0, 'log' => $arr_log);
            $this->template();
        } else {
            $er = array('error' => 1, 'log' => $arr_log);
            $this->template();
        }
    }
}
