<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajar_kuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pengajar_kuliah_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $pengajar_kuliah = $this->pengajar_kuliah_m;

        $validation = $this->form_validation;
        $validation->set_rules($pengajar_kuliah->rules());
        if ($validation->run()) {
            $pengajar_kuliah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen Pengajar Kuliah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("pengajar_kuliah");
        }
        $data["title"] = "Dosen Pengajar Kuliah";

        $data['data_pengajar_kuliah'] = $this->pengajar_kuliah_m->getAll($id_ta, $id_semester, $id_prodi);
        $data['dosen'] = $this->pengajar_kuliah_m->getDataDosen();
        $data['jadwal_kuliah'] = $this->pengajar_kuliah_m->getJadwalKuliah();

        $data["data_prodi"] = $this->pengajar_kuliah_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->pengajar_kuliah_m->get_tahun_akademik();
        $data["data_semester"] = $this->pengajar_kuliah_m->get_semester();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('pengajar_kuliah/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('pengajar_kuliah/excel', $data);
    }

    public function edit($id = null)
    {
        $data['dosen'] = $this->pengajar_kuliah_m->getDataDosen();
        $data['jadwal_kuliah'] = $this->pengajar_kuliah_m->getJadwalKuliah();
        $this->form_validation->set_rules('id_dosen2', 'id_dosen', 'required');
        $this->form_validation->set_rules('id_jadwal_kuliah2', 'id_jadwal_kuliah', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen Pengajar Kuliah gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengajar_kuliah');
        } else {
            // echo $this->input->post('id');
            // die();
            $data = array(
                //"id" => $this->input->post('id'),
                "id_dosen" => $this->input->post('id_dosen2'),
                "id_jadwal_kuliah" => $this->input->post('id_jadwal_kuliah2'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('dosen_pengajar_kuliah', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen Pengajar Kuliah berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengajar_kuliah');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen Pengajar Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengajar_kuliah');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('dosen_pengajar_kuliah');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Dosen Pengajar Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengajar_kuliah');
        }
    }

    // 18-11-2021
    public function get_jadwal_by_prodi_dosen()
    {
        // ini get data jadwal by prodi dosen
        $id_dosen     = $this->input->post('id_dosen', TRUE);
        $data['data_jadwal'] = $this->pengajar_kuliah_m->getJadwalKuliah($id_dosen);
        $this->load->view('pengajar_kuliah/vlistjadwal', $data);
        return true;
    }
}
