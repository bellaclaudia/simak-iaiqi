<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_user_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $user = $this->Master_user_m;

        $validation = $this->form_validation;
        $validation->set_rules($user->rules());
        if ($validation->run()) {
            $user->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_user");
        }
        $data["title"] = "User";

        $data['data_user'] = $this->Master_user_m->getAll();
        $data['grup_user'] = $this->Master_user_m->getGrupUser();
        $data['prodi'] = $this->Master_user_m->getProgramStudi();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    // public function index(){
    //     $user = $this->Master_user_m;

    //     $validation = $this->form_validation;
    //     $validation->set_rules($user->rules());
    //     if ($validation->run()) {
    //         $user->save();
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data User berhasil disimpan. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect("master_user");
    //     }
    //     $data["title"] = "User";

    //     $this->load->database();
    //     $jumlah_data = $this->Master_user_m->jumlah_data();
    //     $this->load->library('pagination');
    //     $config['base_url'] = base_url().'index.php/master_user/index/';
    //     $config['total_rows'] = $jumlah_data;
    //     $config['per_page'] = 10;
    //     $from = $this->uri->segment(3);
    //     $this->pagination->initialize($config);     
    //     $data['data_user'] = $this->Master_user_m->data($config['per_page'],$from);
    //     $this->load->view('templates/header', $data);
    //     $this->load->view('templates/menu');
    //     $this->load->view('user/index', $data);
    //     $this->load->view('templates/footer');
    // }

    // 14-06-2022 dikomen. ganti pake function yg baru
    /*public function edit($id = null)
    {
        $data['grup_user'] = $this->Master_user_m->getGrupUser();
        $data['prodi'] = $this->Master_user_m->getProgramStudi();
        $this->form_validation->set_rules('id_grup_user', 'id_grup_user', 'required');
        $this->form_validation->set_rules('id_program_studi', 'id_program_studi', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "id_grup_user" => $this->input->post('id_grup_user'),
                "id_program_studi" => $this->input->post('id_program_studi'),
                "username" => $this->input->post('username'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        }
    } */

    // 14-06-2022
    public function edit($id)
    {
        $is_simpan = $this->input->post('is_simpan');

        if ($is_simpan == 1) {
            $passwdnya = $this->input->post('passwdnya');

            if (trim($passwdnya) != '') {
                $data = array(
                    "userpass" => md5($passwdnya),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
            } else {
                $data = array(
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
            }

            $this->db->where('id', $id);
            $this->db->update('master_user', $data);

            // file foto
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['max_size']             = 5120;
            //$config['encrypt_name']         = TRUE;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'USERfoto_' . $id . '_' . $timestamp;

            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_foto']['tmp_name'])) {

                $up = $this->upload->do_upload('file_foto');
                if ($up) {
                    $upl = $this->upload->data();
                    $data['file_foto'] = $upl['file_name'];
                    $file_foto = $data['file_foto'];

                    //=========== hapus file lama
                    $this->db->select('file_foto');
                    $this->db->where('id', $id);
                    $q = $this->db->get('master_user')->row_array();
                    if (isset($q)) {
                        if (!empty($q['file_foto'])) {
                            unlink($dir . '/' . $q['file_foto']);
                        }
                    }

                    // update fotonya
                    $data = array(
                        "file_foto" => $file_foto
                    );

                    $this->db->where('id', $id);
                    $this->db->update('master_user', $data);
                } else {
                    $error = 1;
                }
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil diupdate. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
        }


        $data["title"] = "Edit Data User";
        $data["data_user"] = $this->Master_user_m->get_user_by_id($id);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('user/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_user');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        }
    }

    // 14-06-2022 ini udh ga dipake lg
    public function changepassword($id = null)
    {
        $this->form_validation->set_rules('userpass', 'userpass', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Password gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "userpass" => md5($this->input->post('userpass')),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Password berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        }
    }
}
