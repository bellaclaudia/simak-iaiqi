<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_rekening extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_rekening_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $rekening = $this->Master_rekening_m;
        $validation = $this->form_validation;
        $validation->set_rules($rekening->rules());
        if ($validation->run()) {
            $rekening->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Rekening Kampus berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_rekening");
        }
        $data["title"] = "Rekening Kampus";
        $data["data_rekening"] = $this->Master_rekening_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('rekening/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('nama_bank', 'nama_bank', 'required');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required');
        $this->form_validation->set_rules('atas_nama', 'atas_nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Rekening Kampus gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_rekening');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "nama_bank" => $_POST['nama_bank'],
                "no_rekening" => $_POST['no_rekening'],
                "atas_nama" => $this->input->post('atas_nama'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => 'Bella Claudia'
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_rekening', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Rekening Kampus berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_rekening');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Rekening Kampus gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_rekening');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_rekening');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Rekening Kampus berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_rekening');
        }
    }
}
