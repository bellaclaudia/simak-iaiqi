<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_status_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("riwayat_status_mhs_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $data["title"] = "Riwayat Status Mahasiswa";
        $data["data_riwayat_status_mhs"] = $this->riwayat_status_mhs_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_status_mhs/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
    	$riwayat_status_mhs = $this->riwayat_status_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($riwayat_status_mhs->rules());
        if ($validation->run()) {
            $riwayat_status_mhs->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Status Mahasiswa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("riwayat_status_mhs");
        }
        $data["title"] = "Tambah Data Riwayat Status Mahasiswa";
        $data['mahasiswa'] = $this->riwayat_status_mhs_m->getMahasiswa();
        $data['tahun_akademik'] = $this->riwayat_status_mhs_m->getTahunAkademik();
        $data['semester'] = $this->riwayat_status_mhs_m->getSemester();
        $data['status_mhs'] = $this->riwayat_status_mhs_m->getStatusMhs();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_status_mhs/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('riwayat_status_mhs');

        $riwayat_status_mhs = $this->riwayat_status_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($riwayat_status_mhs->rules());

        if ($validation->run()) {
            $riwayat_status_mhs->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Status Mahasiswa berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("riwayat_status_mhs");
        }
        $data["title"] = "Edit Data Riwayat Status Mahasiswa";
        $data["data_riwayat_status_mhs"] = $riwayat_status_mhs->getById($id);
        $data['mahasiswa'] = $this->riwayat_status_mhs_m->getMahasiswa();
        $data['tahun_akademik'] = $this->riwayat_status_mhs_m->getTahunAkademik();
        $data['semester'] = $this->riwayat_status_mhs_m->getSemester();
        $data['status_mhs'] = $this->riwayat_status_mhs_m->getStatusMhs();
        if (!$data["data_riwayat_status_mhs"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_status_mhs/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Status Mahasiswa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat_status_mhs');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('riwayat_status_mhs');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Status Mahasiswa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat_status_mhs');
        }
    }
}
