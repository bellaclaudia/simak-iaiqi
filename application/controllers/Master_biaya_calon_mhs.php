<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_biaya_calon_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_biaya_calon_mhs_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_ta = $this->input->post('id_ta');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_ta == '') $id_ta = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $biaya_calon_mhs = $this->Master_biaya_calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($biaya_calon_mhs->rules());
        if ($validation->run()) {
            $biaya_calon_mhs->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Calon Mahasiswa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_biaya_calon_mhs");
        }
        $data["title"] = "Biaya Calon Mahasiswa";
        $data["data_prodi"] = $this->Master_biaya_calon_mhs_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Master_biaya_calon_mhs_m->get_tahun_akademik();
        // 01-06-2022
        $data["data_kategori_kelas"] = $this->Master_biaya_calon_mhs_m->get_kategori_kelas();
        $data["data_biaya_calon_mhs"] = $this->Master_biaya_calon_mhs_m->getAll($id_ta, $id_prodi);

        $data["id_ta"] = $id_ta;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('biaya_calon_mhs/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('biaya_calon_mhs/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('nominal', 'nominal', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Calon Mahasiswa gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_biaya_calon_mhs');
        } else {
            $data = array(
                //"id" => $_POST['id'],
                "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
                "id_program_studi" => $this->input->post('id_program_studi'),
                "kode" => $this->input->post('kode'),
                "nama" => $this->input->post('nama'),
                "jenis_biaya" => $this->input->post('jenis_biaya'),
                "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
                "nominal" => $this->input->post('nominal'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_biaya_calon_mhs', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Calon Mahasiswa berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_biaya_calon_mhs');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Calon Mahasiswa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_biaya_calon_mhs');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_biaya_calon_mhs');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Calon Mahasiswa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_biaya_calon_mhs');
        }
    }
}
