<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_pend_dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Riwayat_pend_dosen_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
   
    }

    public function index()
    {
        $data["title"] = "Riwayat Pendidikan Dosen";
        $data["data_riwayat_pend_dosen"] = $this->Riwayat_pend_dosen_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_pend_dosen/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
    	$riwayat = $this->Riwayat_pend_dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($riwayat->rules());
        if ($validation->run()) {
            $riwayat->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Pendidikan Dosen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("riwayat_pend_dosen");
        }
        $data["title"] = "Tambah Data Master Riwayat Pendidikan Dosen";
        $data['dosen'] = $this->Riwayat_pend_dosen_m->getDataDosen();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_pend_dosen/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('riwayat_pend_dosen');

        $riwayat = $this->Riwayat_pend_dosen_m;
        $validation = $this->form_validation;
        $validation->set_rules($riwayat->rules());

        if ($validation->run()) {
            $riwayat->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Pendidikan Dosen berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("riwayat_pend_dosen");
        }
        $data["title"] = "Edit Data Riwayat Pendidikan Dosen";
        $data["data_riwayat_pend_dosen"] = $riwayat->getById($id);
        $data['dosen'] = $this->Riwayat_pend_dosen_m->getDataDosen();
        if (!$data["data_riwayat_pend_dosen"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat_pend_dosen/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Pendidikan Dosen gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat_pend_dosen');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('riwayat_pendidikan_dosen');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Pendidikan Dosen berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat_pend_dosen');
        }
    }
}
