<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_jenis_pembiayaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_jenis_pembiayaan_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $jnspemb = $this->Master_jenis_pembiayaan_m;
        $validation = $this->form_validation;
        $validation->set_rules($jnspemb->rules());
        if ($validation->run()) {
            $jnspemb->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jenis Pembiayaan berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_jenis_pembiayaan");
        }
        $data["title"] = "Master Jenis Pembiayaan";
        $data["data_jenis_pembiayaan"] = $this->Master_jenis_pembiayaan_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('jenis_pembiayaan/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jenis Pembiayaan gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jenis_pembiayaan');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "kode" => $_POST['kode'],
                "nama" => $_POST['nama'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_jenis_pembiayaan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jenis Pembiayaan berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jenis_pembiayaan');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jenis Pembiayaan gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jenis_pembiayaan');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_jenis_pembiayaan');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jenis Pembiayaan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jenis_pembiayaan');
        }
    }
}
