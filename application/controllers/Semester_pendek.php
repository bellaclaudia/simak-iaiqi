<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semester_pendek extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Semester_pendek_m");
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        //$is_view = $this->input->post('is_view');
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        //$data["is_view"] = $is_view;
        $data["title"] = "Semester Pendek";

        $data["data_sp"] = $this->Semester_pendek_m->getAll($id_ta, $id_semester, $id_prodi);
        $data["data_prodi"] = $this->Semester_pendek_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->Semester_pendek_m->get_tahun_akademik();
        $data["data_semester"] = $this->Semester_pendek_m->get_semester();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('semester_pendek/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        $sp = $this->Semester_pendek_m;
        $validation = $this->form_validation;
        $validation->set_rules($sp->rules());
        if ($validation->run()) {
            $id_ta = $this->input->post('id_ta');
            $id_semester = $this->input->post('id_semester');
            $id_prodi = $this->input->post('id_prodi');
            $id_mhs = $this->input->post('id_mhs');

            redirect("semester_pendek/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
            // ------------------------------------------

        } else {
            $data["title"] = "Tambah Data SP";
            $data["data_prodi"] = $this->Semester_pendek_m->getProgramStudi();
            $data["data_thn_akademik"] = $this->Semester_pendek_m->get_tahun_akademik();
            $data["data_semester"] = $this->Semester_pendek_m->get_semester();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('semester_pendek/add', $data);
            $this->load->view('templates/footer');
        }
    }

    public function tambah_detail_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        // $id_ta = $this->input->post('id_ta');
        // $id_semester = $this->input->post('id_semester');
        // $id_prodi = $this->input->post('id_prodi');
        // $id_mhs = $this->input->post('id_mhs');

        $is_simpan_mk = $this->input->post('is_simpan_mk');
        $submit_krs = $this->input->post('submit_krs');

        if ($is_simpan_mk == '1' && $submit_krs == '') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // modif 17-11-2021 ga usah pake id jadwal

                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel temp
                $this->db->trans_begin();

                $data = array(
                    'id_tahun_akademik' => $id_ta,
                    'id_semester' => $id_semester,
                    'id_program_studi' => $id_prodi,
                    'id_mhs' => $id_mhs,
                    'id_mata_kuliah' => $id_mk,
                    //'id_jadwal_kuliah' => $id_jadwal,
                    'sks' => $sks
                );

                $this->db->insert('sp_mhs_temp_mk', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("semester_pendek/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }

        if ($submit_krs == '1') {
            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM sp_mhs_temp_mk where id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
                        AND id_semester = '$id_semester' ";
            $queryxx = $this->db->query($sqlxx);
            //if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // simpan ke krs_mhs
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_tahun_akademik' => $id_ta,
                'id_semester' => $id_semester,
                'id_mhs' => $id_mhs,
                'total_sks' => $total_sks,
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );

            $this->db->insert('sp_mhs', $data);

            $id_sp = $this->db->insert_id();

            // simpan ke sp_mhs_detail
            // ambil data dari krs_mhs_temp_mk. 17-11-2021 ga pake relasi ke jadwal kuliah
            // $sqlxx = " select id_mata_kuliah, id_jadwal_kuliah, sks FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
            // AND id_semester = '$id_semester' ORDER BY id ";
            $sqlxx = " select id_mata_kuliah, sks FROM sp_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
             AND id_semester = '$id_semester' ORDER BY id ";

            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_mk_temp   = $rowxx->id_mata_kuliah;
                    $sks_temp    = $rowxx->sks;
                    //$id_jadwal_temp = $rowxx->id_jadwal_kuliah;

                    $data = array(
                        'id_sp_mhs' => $id_sp,
                        'id_mata_kuliah' => $id_mk_temp,
                        'sks' => $sks_temp
                    );

                    $this->db->insert('sp_mhs_detail', $data);
                }
            }

            // hapus kembali dari sp_mhs_temp_mk
            $this->db->where('id_mhs', $id_mhs);
            $this->db->where('id_tahun_akademik', $id_ta);
            $this->db->where('id_semester', $id_semester);
            $this->db->delete('sp_mhs_temp_mk');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data SP berhasil disimpan. Selanjutnya menunggu persetujuan dari dosen PA. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');

            redirect("semester_pendek");
        }

        // ambil nama ta
        $sqlxx = " select tahun, nama FROM master_tahun_akademik where id = '$id_ta' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $tahun    = $hasilxx->tahun;
        $nama_ta    = $hasilxx->nama;
        //}

        // ambil nama semester
        $sqlxx = " select nama FROM master_semester where id = '$id_semester' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_semester    = $hasilxx->nama;

        // ambil nama prodi
        $sqlxx = " select a.nama as nama_prodi, b.nama as nama_jenjang FROM master_program_studi a
                            INNER JOIN master_jenjang_studi b ON a.id_jenjang_studi = b.id where a.id = '$id_prodi' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        // ambil nama mhs
        $sqlxx = " select nim, nama FROM mahasiswa where id = '$id_mhs' ";
        $queryxx = $this->db->query($sqlxx);
        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $queryxx->row();
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama;

        $data["id_ta"] = $id_ta;
        $data["nama_ta"] = $tahun . " (" . $nama_ta . ")";
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["nama_prodi"] = $nama_prodi . " (" . $nama_jenjang . ")";
        $data["mhs"] = $nim . " - " . $nama_mhs;
        $data["id_mhs"] = $id_mhs;

        $data["data_temp_mk"] = $this->Semester_pendek_m->get_sp_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs);
        $data["data_mk"] = $this->Semester_pendek_m->get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('semester_pendek/add_mk', $data);
        $this->load->view('templates/footer');
    }

    // 24-10-2021
    public function get_mhs_by_prodi_verifiedkeu()
    {
        // ini get data mhs by prodi dan sudah bayar biaya kuliah reguler di semester dan thn akademik yg dipilih
        $id_prodi     = $this->input->post('id_prodi', TRUE);
        $id_ta     = $this->input->post('id_ta', TRUE);
        $id_sem     = $this->input->post('id_sem', TRUE);
        $data['data_mhs'] = $this->Semester_pendek_m->get_mhs_by_prodi_verifiedkeu($id_prodi, $id_ta, $id_sem);
        $this->load->view('semester_pendek/vlistmhs', $data);
        return true;
    }

    public function hapus_temp_mk($id, $id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('sp_mhs_temp_mk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek/tambah_detail_mk/" . $id_ta . "/" . $id_semester . "/" . $id_prodi . "/" . $id_mhs);
        }
    }

    // 09-11-2021
    public function hapus($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek");
        } else {
            $this->db->trans_begin();

            $this->db->where('id_sp_mhs', $id);
            $this->db->delete('sp_mhs_detail');

            $this->db->where('id', $id);
            $this->db->delete('sp_mhs');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data SP berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek");
        }
    }

    // 14-11-2021
    public function view($id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data SP Mahasiswa";
        $detail_sp = $this->Semester_pendek_m->get_detail_sp($id_sp);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_sp->row();
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $data["nama_semester"] = $nama_semester;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_sp"] = $detail_sp->result();
        $data["id_sp"] = $id_sp;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('semester_pendek/view', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data SP Mahasiswa";
        $detail_sp = $this->Semester_pendek_m->get_detail_sp($id_sp);

        //if ($queryxx->num_rows() > 0) {
        $hasilxx = $detail_sp->row();
        $id_ta    = $hasilxx->id_tahun_akademik;
        $id_semester    = $hasilxx->id_semester;
        $id_prodi    = $hasilxx->id_program_studi;
        $id_mhs    = $hasilxx->id_mhs;

        $nama_semester    = $hasilxx->nama_semester;
        $nama_semester    = $hasilxx->nama_semester;
        $tahun    = $hasilxx->tahun;
        $nama_tahun_akademik    = $hasilxx->nama_tahun_akademik;
        $nim    = $hasilxx->nim;
        $nama_mhs    = $hasilxx->nama_mhs;
        $nama_prodi    = $hasilxx->nama_prodi;
        $nama_jenjang    = $hasilxx->nama_jenjang;

        $thn_akademiknya = $tahun . " (" . $nama_tahun_akademik . ")";
        $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
        $mhsnya = $nim . " - " . $nama_mhs;

        $is_simpan_mk = $this->input->post('is_simpan_mk');

        if ($is_simpan_mk == '1') {
            $id_mk_jadwal = $this->input->post('id_mk_jadwal');

            if ($id_mk_jadwal != '') {
                // 09-11-2021 explode id_mk dan id_jadwal
                // $pisah1 = explode(",", $id_mk_jadwal);
                // $id_mk = $pisah1[0];
                // $id_jadwal = $pisah1[1];

                $id_mk = $id_mk_jadwal;

                // ambil sks
                $sqlxx = " select sks FROM master_mata_kuliah where id = '$id_mk' ";
                $queryxx = $this->db->query($sqlxx);
                //if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $sks    = $hasilxx->sks;

                // save ke tabel sp_mhs_detail
                $this->db->trans_begin();

                $data = array(
                    'id_sp_mhs' => $id_sp,
                    'id_mata_kuliah' => $id_mk,
                    'sks' => $sks,
                );

                $this->db->insert('sp_mhs_detail', $data);

                // hitung total sks
                $sqlxx = " select sum(sks) as jum FROM sp_mhs_detail where id_sp_mhs = '$id_sp' ";
                $queryxx = $this->db->query($sqlxx);
                $hasilxx = $queryxx->row();
                $total_sks    = $hasilxx->jum;

                // update data di krs_mhs
                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    'total_sks' => $total_sks,
                    'status_verifikasi' => 0,
                    'id_dosen_verifikator' => 0,
                    'alasan_ditolak' => '',
                    'tgl_verifikasi' => '',
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $id_sp);
                $this->db->update('sp_mhs', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Data Mata Kuliah berhasil ditambahkan. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
            }

            redirect("semester_pendek/edit/" . $id_sp);
        }

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["nama_semester"] = $nama_semester;
        $data["id_prodi"] = $id_prodi;
        $data["id_mhs"] = $id_mhs;
        $data["thn_akademiknya"] = $thn_akademiknya;
        $data["prodinya"] = $prodinya;
        $data["mhsnya"] = $mhsnya;
        $data["nama_mhs"] = $nama_mhs;
        //}

        $data["detail_sp"] = $detail_sp->result();
        $data["id_sp"] = $id_sp;
        $data["data_mk"] = $this->Semester_pendek_m->get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('semester_pendek/edit', $data);
        $this->load->view('templates/footer');
    }

    // hapus detail mk di form edit
    public function hapus_detail_mk($id, $id_sp)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek/edit/" . $id_sp);
        } else {
            $this->db->where('id', $id);
            $this->db->delete('sp_mhs_detail');

            // hitung total sks
            $sqlxx = " select sum(sks) as jum FROM sp_mhs_detail where id_sp_mhs = '$id_sp' ";
            $queryxx = $this->db->query($sqlxx);
            $hasilxx = $queryxx->row();
            $total_sks    = $hasilxx->jum;

            // update data di krs_mhs
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'total_sks' => $total_sks,
                'status_verifikasi' => 0,
                'id_dosen_verifikator' => 0,
                'alasan_ditolak' => '',
                'tgl_verifikasi' => '',
                'tgl_update' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_sp);
            $this->db->update('sp_mhs', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mata Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("semester_pendek/edit/" . $id_sp);
        }
    }
}
