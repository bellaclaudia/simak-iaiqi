<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengumuman_akademik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pengumuman_akademik_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $data["title"] = "Pengumuman Akademik";
        $data["data_pengumuman_akademik"] = $this->pengumuman_akademik_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pengumuman_akademik/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
    	$pengumuman_akademik = $this->pengumuman_akademik_m;
        $validation = $this->form_validation;
        $validation->set_rules($pengumuman_akademik->rules());
        if ($validation->run()) {
            $pengumuman_akademik->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pengumuman Akademik berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("pengumuman_akademik");
        }
        $data["title"] = "Tambah Data Master Pengumuman Akademik";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pengumuman_akademik/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('pengumuman_akademik');

        $pengumuman_akademik = $this->pengumuman_akademik_m;
        $validation = $this->form_validation;
        $validation->set_rules($pengumuman_akademik->rules());

        if ($validation->run()) {
            $pengumuman_akademik->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pengumuman Akademik berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          	</button></div>');
            redirect("pengumuman_akademik");
        }
        $data["title"] = "Edit Data Pengumuman Akademik";
        $data["data_pengumuman_akademik"] = $pengumuman_akademik->getById($id);
        if (!$data["data_pengumuman_akademik"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pengumuman_akademik/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pengumuman Akademik gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengumuman_akademik');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('pengumuman_akademik');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pengumuman Akademik berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pengumuman_akademik');
        }
    }
}
