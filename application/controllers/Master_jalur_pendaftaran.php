<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_jalur_pendaftaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_jalur_pendaftaran_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $jalurpendaftaran = $this->Master_jalur_pendaftaran_m;
        $validation = $this->form_validation;
        $validation->set_rules($jalurpendaftaran->rules());
        if ($validation->run()) {
            $jalurpendaftaran->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jalur Pendaftaran berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_jalur_pendaftaran");
        }
        $data["title"] = "Jalur Pendaftaran";
        $data["data_jalur_pendaftaran"] = $this->Master_jalur_pendaftaran_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('jalur_pendaftaran/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jalur Pendaftaran gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jalur_pendaftaran');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "kode" => $_POST['kode'],
                "nama" => $_POST['nama'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => 'Bella Claudia'
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_jalur_pendaftaran', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jalur Pendaftaran berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jalur_pendaftaran');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jalur Pendaftaran gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jalur_pendaftaran');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_jalur_pendaftaran');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jalur Pendaftaran berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_jalur_pendaftaran');
        }
    }
}
