<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumni extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("alumni_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_angkatan = $this->input->post('id_angkatan');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_angkatan == '') $id_angkatan = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $data["title"] = "Alumni";
        $data['data_angkatan'] = $this->alumni_m->get_angkatan();
        $data["data_prodi"] = $this->alumni_m->getProgramStudi();
        $data["data_alumni"] = $this->alumni_m->getAll($id_angkatan, $id_prodi);

        $data["id_angkatan"] = $id_angkatan;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('alumni/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('alumni/excel', $data);
    }

    public function tambah()
    {
        $alumni = $this->alumni_m;
        $validation = $this->form_validation;
        $validation->set_rules($alumni->rules());
        if ($validation->run()) {
            $alumni->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Alumni berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("alumni");
        }
        $data["title"] = "Tambah Data Alumni";
        $data['prodi'] = $this->alumni_m->getProgramStudi();
        $data['agama'] = $this->alumni_m->getAgama();
        $data['goldar'] = $this->alumni_m->getGoldar();
        $data['provinsi'] = $this->alumni_m->getProvinsi();
        $data['kab_kota'] = $this->alumni_m->getKabKota();
        $data['status_mhs'] = $this->alumni_m->getStatusMhs();
        $data['data_angkatan'] = $this->alumni_m->get_angkatan();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('alumni/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('alumni');

        $alumni = $this->alumni_m;
        $validation = $this->form_validation;
        $validation->set_rules($alumni->rules());

        if ($validation->run()) {
            $alumni->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Alumni berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("alumni");
        }
        $data["title"] = "Edit Data Alumni";
        $data["data_alumni"] = $alumni->getById($id);
        $data['prodi'] = $this->alumni_m->getProgramStudi();
        $data['agama'] = $this->alumni_m->getAgama();
        $data['goldar'] = $this->alumni_m->getGoldar();
        $data['provinsi'] = $this->alumni_m->getProvinsi();
        $data['kab_kota'] = $this->alumni_m->getKabKota();
        $data['status_mhs'] = $this->alumni_m->getStatusMhs();
        if (!$data["data_alumni"]) show_404();

        $data['data_angkatan'] = $this->alumni_m->get_angkatan();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('alumni/edit', $data);
        $this->load->view('templates/footer');
    }

    public function view($id = null)
    {
        if (!isset($id)) redirect('alumni');
        $data["title"] = "View Data alumni";
        $data["data_alumni"] = $this->alumni_m->getView($id);
        if (!$data["data_alumni"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('alumni/view', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Alumni gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('alumni');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('alumni');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Alumni berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('alumni');
        }
    }
}
