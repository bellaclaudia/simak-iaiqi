<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transkrip_nilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Transkrip_nilai_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        //$id_tahun_akademik = $this->input->post('id_tahun_akademik');
        $id_mhs = $this->input->post('id_mhs');
        $pdf = $this->input->post('pdf');
        $is_cari = $this->input->post('is_cari');

        //if ($id_tahun_akademik == '') $id_tahun_akademik = 0;

        $data["title"] = "Transkrip Nilai";
        //$data["tahun_akademik"] = $this->Transkrip_nilai_m->getTahunAkademik();
        $data["predikat"] = $this->Transkrip_nilai_m->getPredikatKelulusan();
        $data["mhs"] = $this->Transkrip_nilai_m->getMahasiswa();
        if ($is_cari == '1')
            $data["data_transkrip_nilai"] = $this->Transkrip_nilai_m->getAll($id_mhs);

        //$data["id_tahun_akademik"] = $id_tahun_akademik;
        $data["id_mhs"] = $id_mhs;
        $data["is_cari"] = $is_cari;

        if ($pdf == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('transkrip_nilai/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('transkrip_nilai/pdf', $data);
    }

    // 16-01-2022
    public function cetak_transkrip_html($id_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "Data Transkrip Mhs";
        $detail_transkrip = $this->Transkrip_nilai_m->get_detail_transkrip($id_mhs);

        if ($detail_transkrip->num_rows() > 0) {
            $hasilxx = $detail_transkrip->row();
            $nim    = $hasilxx->nim;
            $nama_mhs    = $hasilxx->nama_mhs;
            $nama_prodi    = $hasilxx->nama_prodi;
            $nama_jenjang    = $hasilxx->nama_jenjang;

            $prodinya = $nama_prodi . " (" . $nama_jenjang . ")";
            $mhsnya = $nim . " - " . $nama_mhs;

            $data["prodinya"] = $prodinya;
            $data["mhsnya"] = $mhsnya;
            $data["nama_mhs"] = $nama_mhs;
            $data["nim"] = $nim;

            $data["tempat_lahir"] = $hasilxx->tempat_lahir;
            $data["tgl_lahir"] = $hasilxx->tgl_lahir;
            $data["nomor_ijazah"] = $hasilxx->nomor_ijazah;
            $data["tgl_lulus"] = $hasilxx->tgl_lulus;
            $data["nomor_transkrip"] = $hasilxx->nomor_transkrip;
            $data["judul_skripsi"] = $hasilxx->judul_skripsi;
            //}

            $data["detail_transkrip"] = $detail_transkrip->result();
            $data["id_mhs"] = $id_mhs;

            $this->load->view('transkrip_nilai/cetak_transkrip_html', $data);
        } else {
            echo "Data nilai belum ada. Silahkan pilih mahasiswa yang lain";
            die();
        }
    }
}
