<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_gelombang_calon_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_gelombang_calon_mhs_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $gelombang = $this->Master_gelombang_calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($gelombang->rules());
        if ($validation->run()) {
            $gelombang->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Gelombang Calon Mahasiswa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_gelombang_calon_mhs");
        }
        $data["title"] = "Gelombang Calon Mahasiswa";
        $data["data_gelombang_calon_mhs"] = $this->Master_gelombang_calon_mhs_m->getAll();
        $data['tahun_akademik'] = $this->Master_gelombang_calon_mhs_m->getTahunAkademik();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('gelombang_calon_mhs/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_tahun_akademik', 'id_tahun_akademik', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('tgl_awal', 'tgl_awal', 'required');
        $this->form_validation->set_rules('tgl_akhir', 'tgl_akhir', 'required');
        $this->form_validation->set_rules('lokasi', 'lokasi', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Gelombang Calon Mahasiswa gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_gelombang_calon_mhs');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "id_tahun_akademik" => $_POST['id_tahun_akademik'],
                "nama" => $_POST['nama'],
                "tgl_awal" => $_POST['tgl_awal'],
                "tgl_akhir" => $_POST['tgl_akhir'],
                "lokasi" => $_POST['lokasi'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => 'Bella Claudia'
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('master_gelombang_calon_mhs', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Gelombang Calon Mahasiswa berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_gelombang_calon_mhs');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Gelombang Calon Mahasiswa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_gelombang_calon_mhs');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_gelombang_calon_mhs');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Gelombang Calon Mahasiswa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_gelombang_calon_mhs');
        }
    }
}
