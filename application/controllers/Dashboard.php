<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        } else {
            $data["title"] = "Dashboard";
            $data["data_kalender_akademik"] = $this->dashboard_m->getKalenderAkademik();
            $data["data_pengumuman_akademik"] = $this->dashboard_m->getPengumumanAkademik();
            $this->load->view('templates/header',$data);
            $this->load->view('templates/menu');
            $this->load->view('dashboard',$data);
            $this->load->view('templates/footer');
        }
    }
}
