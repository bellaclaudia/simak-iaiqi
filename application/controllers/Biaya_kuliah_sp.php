<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_kuliah_sp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("biaya_kuliah_sp_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $biaya_kuliah_sp = $this->biaya_kuliah_sp_m;

        $validation = $this->form_validation;
        $validation->set_rules($biaya_kuliah_sp->rules());
        if ($validation->run()) {
            $biaya_kuliah_sp->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah SP berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("biaya_kuliah_sp");
        }
        $data["title"] = "Biaya Kuliah SP";

        $data['data_biaya_kuliah_sp'] = $this->biaya_kuliah_sp_m->getAll();
        $data['semester'] = $this->biaya_kuliah_sp_m->getSemester();
        $data['tahun_akademik'] = $this->biaya_kuliah_sp_m->getTahunAkademik();
        $data["data_prodi"] = $this->biaya_kuliah_sp_m->getProgramStudi();
        $data['biaya_kuliah'] = $this->biaya_kuliah_sp_m->getBiayaKuliah();
        $data['mahasiswa'] = $this->biaya_kuliah_sp_m->getMahasiswa();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('biaya_kuliah_sp/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $data['semester'] = $this->biaya_kuliah_sp_m->getSemester();
        $data['tahun_akademik'] = $this->biaya_kuliah_sp_m->getTahunAkademik();
        $data['biaya_kuliah'] = $this->biaya_kuliah_sp_m->getBiayaKuliah();
        $this->form_validation->set_rules('id_semester', 'id_semester', 'required');
        $this->form_validation->set_rules('id_tahun_akademik', 'id_tahun_akademik', 'required');
        $this->form_validation->set_rules('id_biaya_kuliah', 'id_biaya_kuliah', 'required');
        $this->form_validation->set_rules('id_mhs', 'id_mhs', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah SP gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_sp');
        } else {
            $data = array(
                //"id" => $this->input->post('id'),
                "id_semester" => $this->input->post('id_semester'),
                "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
                "id_program_studi" => $this->input->post('id_program_studi'),
                "id_biaya_kuliah" => $this->input->post('id_biaya_kuliah'),
                "id_mhs" => $this->input->post('id_mhs'),
                "jumlah" => $this->input->post('jumlah'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('biaya_kuliah_sp', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah SP berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_sp');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah SP gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_sp');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('biaya_kuliah_sp');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah SP berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_sp');
        }
    }

    public function get_mhs_by_prodi()
    {
        $id_program_studi = $this->input->post('id_program_studi', TRUE);
        $data['mahasiswa'] = $this->biaya_kuliah_sp_m->get_mhs_by_prodi($id_program_studi);
        $this->load->view('biaya_kuliah_sp/vlistmhs', $data);
        return true;
    }
}
