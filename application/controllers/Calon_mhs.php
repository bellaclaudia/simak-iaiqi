<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calon_mhs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Calon_mhs_m");
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $id_gelombang = $this->input->post('id_gelombang_calon_mhs');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_gelombang == '') $id_gelombang = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $data["title"] = "Calon Mahasiswa";
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data["data_prodi"] = $this->Calon_mhs_m->getProgramStudi();
        $data["data_calon_mhs"] = $this->Calon_mhs_m->getAll($id_gelombang, $id_prodi);

        $data["id_gelombang"] = $id_gelombang;
        $data["id_prodi"] = $id_prodi;
        // echo $id_gelombang;
        // die();

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('calon_mhs/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('calon_mhs/excel', $data);
    }

    public function tambah()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        $calonmhs = $this->Calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($calonmhs->rules());
        if ($validation->run()) {
            //$calonmhs->save();
            // ------------------------------------------
            $id_gelombang = $this->input->post('id_gelombang_calon_mhs');

            // generate no_pendaftaran. format (sementara): pmb tahun + idgelombang + no urut. contoh: pmb202111
            // cek no urut terakhir berdasarkan id gelombang
            $sqlxx = " SELECT no_urut_daftar FROM calon_mahasiswa WHERE id_gelombang_calon_mhs = '$id_gelombang' ORDER BY no_urut_daftar DESC LIMIT 1 ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $no_urut_daftar    = $hasilxx->no_urut_daftar;
                $no_urut_daftar++;
            } else
                $no_urut_daftar = 1;

            // ambil tahun dari gelombang_calon_mhs
            $sqlxx = " select b.tahun FROM master_gelombang_calon_mhs a 
                    INNER JOIN master_tahun_akademik b ON b.id = a.id_tahun_akademik
                    WHERE a.id = '$id_gelombang' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $tahun    = $hasilxx->tahun;
            }

            // set no pendaftaran
            $no_pendaftaran = "pmb" . $tahun . $id_gelombang . $no_urut_daftar;

            // ambil nama grup user
            $grup_user = 3;
            $sqlxx = " select nama_grup FROM master_grup_user WHERE id = '$grup_user' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_grup_user    = $hasilxx->nama_grup;
            }

            $this->db->trans_begin();

            // inset ke tabel master_user, usernamenya pake dari no_pendaftaran, passwordnya juga samain
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_grup_user' => '3',
                'id_program_studi' => $this->input->post('id_program_studi_pilihan'),
                'username' => $no_pendaftaran,
                'userpass' => md5($no_pendaftaran),
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $no_pendaftaran
            );

            $this->db->insert('master_user', $data);

            $id_usernya = $this->db->insert_id();

            // 22-10-2021 get total biaya calon mhs, ini utk di field total_biaya
            $sqlxx = " select sum(nominal) as jum FROM master_biaya_calon_mhs ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $total_biaya    = $hasilxx->jum;

                if ($total_biaya == '')
                    $total_biaya = 0;
            } else
                $total_biaya = 0;

            // file foto UPDATE 23-10-2021
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            //$config['encrypt_name'] = FALSE;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBfoto_' . $id_usernya . '_' . $timestamp;
            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_foto']['tmp_name'])) {

                $up = $this->upload->do_upload('file_foto');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_foto = $upl['file_name'];

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_foto = '';
            }

            // file dokumen
            $config['allowed_types'] = 'pdf';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBdoc_' . $id_usernya . '_' . $timestamp;

            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_dokumen']['tmp_name'])) {
                $this->load->library('upload', $config);
                $up = $this->upload->do_upload('file_dokumen');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_dokumen = $upl['file_name'];

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_dokumen = '';
            }

            // 28-12-2021 file riwayat penyakit
            // $config['allowed_types'] = 'pdf';
            // $config['overwrite'] = false;
            // $config['max_size'] = 5120;
            // $dir = './uploads/';
            // if (!file_exists($dir)) {
            //     mkdir($dir, 777, true);
            // }

            // $config['upload_path'] = $dir;
            // $timestamp = date('YmdHis');
            // $config['file_name'] = 'PMBrwytpen' . $id_usernya . '_' . $timestamp;

            // $this->load->library('upload', $config);
            // $error = 0;
            // if (!empty($_FILES['file_riwayat_penyakit']['tmp_name'])) {
            //     $this->load->library('upload', $config);
            //     $up = $this->upload->do_upload('file_riwayat_penyakit');
            //     if ($up) {
            //         $upl = $this->upload->data();
            //         $file_riwayat_penyakit = $upl['file_name'];

            //         unset($this->upload);
            //     } else {
            //         $error = 1;
            //         $file_riwayat_penyakit = '';
            //     }
            // } else {
            //     $file_riwayat_penyakit = '';
            // }

            $data = array(
                "id_program_studi_pilihan" => $this->input->post('id_program_studi_pilihan'),
                "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
                "no_pendaftaran" => $no_pendaftaran,
                "id_user" => $id_usernya,
                "no_urut_daftar" => $no_urut_daftar,
                "id_gelombang_calon_mhs" => $this->input->post('id_gelombang_calon_mhs'),
                "nama" => $this->input->post('nama'),
                "alamat" => $this->input->post('alamat'),
                "kelurahan" => $this->input->post('kelurahan'),
                "kecamatan" => $this->input->post('kecamatan'),
                "kode_pos" => $this->input->post('kode_pos'),
                "tempat_lahir" => $this->input->post('tempat_lahir'),
                "tgl_lahir" => $this->input->post('tgl_lahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "nik" => $this->input->post('nik'),
                "nisn" => $this->input->post('nisn'),
                "id_agama" => $this->input->post('id_agama'),
                "id_golongan_darah" => $this->input->post('id_golongan_darah'),
                "id_provinsi" => $this->input->post('id_provinsi'),
                "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
                "no_telp" => $this->input->post('no_telp'),
                "email" => $this->input->post('email'),
                "nama_ayah" => $this->input->post('nama_ayah'),
                "nama_ibu" => $this->input->post('nama_ibu'),
                "ukuran_jas" => $this->input->post('ukuran_jas'),
                "tgl_input" => date('Y-m-d H:i:s'),
                "registered_by" => $nama_grup_user,
                "user_update_by" => $no_pendaftaran,
                "total_biaya" => $total_biaya,
                "file_foto" => $file_foto,
                "file_dokumen" => $file_dokumen,
                "riwayat_penyakit" => $this->input->post('riwayat_penyakit')
            );
            $this->db->insert('calon_mahasiswa', $data);

            //$idnya = $this->db->insert_id();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            // ------------------------------------------
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mhs berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("calon_mhs");
        }
        $data["title"] = "Tambah Data Calon Mahasiswa";
        $data['prodi'] = $this->Calon_mhs_m->getProgramStudi();
        // 01-06-2022
        $data["data_kategori_kelas"] = $this->Calon_mhs_m->get_kategori_kelas();
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data['agama'] = $this->Calon_mhs_m->getAgama();
        $data['goldar'] = $this->Calon_mhs_m->getGoldar();
        //$data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Calon_mhs_m->getProvinsi();
        $data['kab_kota'] = $this->Calon_mhs_m->getKabKota();
        $data['list_dokumen'] = $this->Calon_mhs_m->get_master_dokumen();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/add', $data);
        $this->load->view('templates/footer');
    }

    public function pendaftaran()
    {
        if (isset($this->session->userdata['logged_in'])) {
            redirect('dashboard');
        }

        $calonmhs = $this->Calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($calonmhs->rules());
        if ($validation->run()) {
            //$calonmhs->save();
            // ---------------------------------------------------
            $id_gelombang = $this->input->post('id_gelombang_calon_mhs');

            // generate no_pendaftaran. format (sementara): pmb tahun + idgelombang + no urut. contoh: pmb202111
            // cek no urut terakhir berdasarkan id gelombang
            $sqlxx = " SELECT no_urut_daftar FROM calon_mahasiswa WHERE id_gelombang_calon_mhs = '$id_gelombang' ORDER BY no_urut_daftar DESC LIMIT 1 ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $no_urut_daftar    = $hasilxx->no_urut_daftar;
                $no_urut_daftar++;
            } else
                $no_urut_daftar = 1;

            // ambil tahun dari gelombang_calon_mhs
            $sqlxx = " select b.tahun FROM master_gelombang_calon_mhs a 
                    INNER JOIN master_tahun_akademik b ON b.id = a.id_tahun_akademik
                    WHERE a.id = '$id_gelombang' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $tahun    = $hasilxx->tahun;
            }

            // set no pendaftaran
            $no_pendaftaran = "pmb" . $tahun . $id_gelombang . $no_urut_daftar;

            // ambil nama grup user
            $grup_user = 3;
            $sqlxx = " select nama_grup FROM master_grup_user WHERE id = '$grup_user' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $nama_grup_user    = $hasilxx->nama_grup;
            }

            $this->db->trans_begin();

            // inset ke tabel master_user, usernamenya pake dari no_pendaftaran, passwordnya juga samain
            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                'id_grup_user' => '3',
                'id_program_studi' => $this->input->post('id_program_studi_pilihan'),
                'username' => $no_pendaftaran,
                'userpass' => md5($no_pendaftaran),
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $no_pendaftaran
            );

            $this->db->insert('master_user', $data);

            $id_usernya = $this->db->insert_id();

            // 22-10-2021 get total biaya calon mhs, ini utk di field total_biaya
            $sqlxx = " select sum(nominal) as jum FROM master_biaya_calon_mhs ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $total_biaya    = $hasilxx->jum;

                if ($total_biaya == '')
                    $total_biaya = 0;
            } else
                $total_biaya = 0;

            // file foto UPDATE 23-10-2021
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            //$config['encrypt_name'] = FALSE;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBfoto_' . $id_usernya . '_' . $timestamp;
            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_foto']['tmp_name'])) {

                $up = $this->upload->do_upload('file_foto');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_foto = $upl['file_name'];

                    unset($this->upload);
                } else {
                    $error = 1;
                    $file_foto = '';
                }
            } else {
                $file_foto = '';
            }

            // file dokumen
            $config['allowed_types'] = 'pdf';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBdoc_' . $id_usernya . '_' . $timestamp;

            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_dokumen']['tmp_name'])) {
                $this->load->library('upload', $config);
                $up = $this->upload->do_upload('file_dokumen');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_dokumen = $upl['file_name'];

                    unset($this->upload);
                } else {
                    $error = 1;
                    $file_dokumen = '';
                }
            } else {
                $file_dokumen = '';
            }

            // 27-12-2021 file riwayat penyakit
            // $config['allowed_types'] = 'pdf';
            // $config['overwrite'] = false;
            // $config['max_size'] = 5120;
            // $dir = './uploads/';
            // if (!file_exists($dir)) {
            //     mkdir($dir, 777, true);
            // }

            // $config['upload_path'] = $dir;
            // $timestamp = date('YmdHis');
            // $config['file_name'] = 'PMBrwytpen' . $id_usernya . '_' . $timestamp;

            // $this->load->library('upload', $config);
            // $error = 0;
            // if (!empty($_FILES['file_riwayat_penyakit']['tmp_name'])) {
            //     $this->load->library('upload', $config);
            //     $up = $this->upload->do_upload('file_riwayat_penyakit');
            //     if ($up) {
            //         $upl = $this->upload->data();
            //         $file_riwayat_penyakit = $upl['file_name'];

            //         unset($this->upload);
            //     } else {
            //         $error = 1;
            //         $file_riwayat_penyakit = '';
            //     }
            // } else {
            //     $file_riwayat_penyakit = '';
            // }

            $data = array(
                "id_program_studi_pilihan" => $this->input->post('id_program_studi_pilihan'),
                "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
                "no_pendaftaran" => $no_pendaftaran,
                "id_user" => $id_usernya,
                "no_urut_daftar" => $no_urut_daftar,
                "id_gelombang_calon_mhs" => $this->input->post('id_gelombang_calon_mhs'),
                "nama" => $this->input->post('nama'),
                "alamat" => $this->input->post('alamat'),
                "kelurahan" => $this->input->post('kelurahan'),
                "kecamatan" => $this->input->post('kecamatan'),
                "kode_pos" => $this->input->post('kode_pos'),
                "tempat_lahir" => $this->input->post('tempat_lahir'),
                "tgl_lahir" => $this->input->post('tgl_lahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "nik" => $this->input->post('nik'),
                "nisn" => $this->input->post('nisn'),
                "id_agama" => $this->input->post('id_agama'),
                "id_golongan_darah" => $this->input->post('id_golongan_darah'),
                "id_provinsi" => $this->input->post('id_provinsi'),
                "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
                "no_telp" => $this->input->post('no_telp'),
                "email" => $this->input->post('email'),
                "nama_ayah" => $this->input->post('nama_ayah'),
                "nama_ibu" => $this->input->post('nama_ibu'),
                "ukuran_jas" => $this->input->post('ukuran_jas'),
                "tgl_input" => date('Y-m-d H:i:s'),
                "registered_by" => $nama_grup_user,
                "user_update_by" => $no_pendaftaran,
                "total_biaya" => $total_biaya,
                "file_foto" => $file_foto,
                "file_dokumen" => $file_dokumen,
                "riwayat_penyakit" => $this->input->post('riwayat_penyakit')
            );
            $this->db->insert('calon_mahasiswa', $data);

            //$idnya = $this->db->insert_id();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            // ---------------------------------------------------
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data berhasil disimpan. Silahkan login menggunakan username dan password berikut untuk tahapan lebih lanjut:<br>
            Username: ' . $no_pendaftaran . '<br>
            Password: ' . $no_pendaftaran . '<br><br>
            Klik <a href="' . base_url('login') . '"><b>disini</b></a> untuk login 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("calon_mhs/pendaftaran_success");
        }
        $data["title"] = "Pendaftaran Calon Mahasiswa Baru";
        $data['prodi'] = $this->Calon_mhs_m->getProgramStudi();
        // 01-06-2022
        $data["data_kategori_kelas"] = $this->Calon_mhs_m->get_kategori_kelas();
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data['agama'] = $this->Calon_mhs_m->getAgama();
        $data['goldar'] = $this->Calon_mhs_m->getGoldar();
        //$data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Calon_mhs_m->getProvinsi();
        $data['kab_kota'] = $this->Calon_mhs_m->getKabKota();
        $data['list_dokumen'] = $this->Calon_mhs_m->get_master_dokumen();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/pendaftaran_nonlogin', $data);
        $this->load->view('templates/footer');
    }

    public function pendaftaran_success()
    {
        // $data["title"] = "Calon Mahasiswa";
        // $data["data_calon_mhs"] = $this->Calon_mhs_m->getAll();
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/pendaftaran_nonlogin_success');
        $this->load->view('templates/footer');
    }

    // 22-10-2021
    public function hapus($id, $no_pendaftaran)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa dgn no pendaftaran <b>' . $no_pendaftaran . '</b> gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('calon_mhs');
        } else {
            $this->db->trans_begin();

            // hapus di tabel user berdasarkan no pendaftaran
            $this->db->where('username', $no_pendaftaran);
            $this->db->delete('master_user');

            // hapus di tabel calon_mahasiswa
            $this->db->where('id', $id);
            $this->db->delete('calon_mahasiswa');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa dgn no pendaftaran <b>' . $no_pendaftaran . '</b> berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('calon_mhs');
        }
    }

    public function set_status_seleksi($id)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil nama calon mhs dan no pendaftaran
        $sqlxx = " select no_pendaftaran, nama, status_seleksi FROM calon_mahasiswa WHERE id = '$id' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nama    = $hasilxx->nama;
            $no_pendaftaran    = $hasilxx->no_pendaftaran;
            $status_seleksi = $hasilxx->status_seleksi;
        }

        $is_simpan = $this->input->post('is_simpan');
        $status_seleksi1 = $this->input->post('status_seleksi');

        if ($is_simpan == '1') {
            $this->db->trans_begin();

            $tgl_skrg = date('Y-m-d H:i:s');
            $data = array(
                "status_seleksi" => $status_seleksi1,
                "tgl_update" => $tgl_skrg,
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $id);
            $this->db->update('calon_mahasiswa', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Set Status Seleksi ' . $nama . ' (no pendaftaran: ' . $no_pendaftaran . ') berhasil diupdate! 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("calon_mhs");
        } else {
            $data["title"] = "Set Status Seleksi Calon Mahasiswa";
            $data['nama'] = $nama;
            $data['no_pendaftaran'] = $no_pendaftaran;
            $data['status_seleksi'] = $status_seleksi;
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('calon_mhs/set_status_seleksi', $data);
            $this->load->view('templates/footer');
        }
    }

    // register
    public function registrasi_mahasiswa($id, $no_pendaftaran)
    {

        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa dgn no pendaftaran <b>' . $no_pendaftaran . '</b> gagal diregistrasi. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('calon_mhs');
        } else {
            // query ke tabel calon_mahasiswa. dan insert ke field2 yg cocok dgn tabel mahasiswa
            // ambil field2 yg cocok
            $sqlxx = " select no_pendaftaran, nama, alamat, id_provinsi, id_kabupaten_kota, jenis_kelamin, nik, tempat_lahir, tgl_lahir, id_agama,
                    id_golongan_darah, id_program_studi_pilihan, email, no_telp, file_foto, nama_ayah, nama_ibu, id_gelombang_calon_mhs, kelurahan, kecamatan, kode_pos, ukuran_jas, nisn
                    FROM calon_mahasiswa WHERE id = '$id' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                //$id_user = $hasilxx->id_user;
                $no_pendaftaran = $hasilxx->no_pendaftaran;
                $nama    = $hasilxx->nama;
                $alamat    = $hasilxx->alamat;
                $kelurahan    = $hasilxx->kelurahan;
                $kecamatan    = $hasilxx->kecamatan;
                $kode_pos    = $hasilxx->kode_pos;
                $id_provinsi = $hasilxx->id_provinsi;
                $id_kabupaten_kota = $hasilxx->id_kabupaten_kota;
                $jenis_kelamin = $hasilxx->jenis_kelamin;
                $nik = $hasilxx->nik;
                $nisn = $hasilxx->nisn;
                $tempat_lahir = $hasilxx->tempat_lahir;
                $tgl_lahir = $hasilxx->tgl_lahir;
                $id_agama = $hasilxx->id_agama;
                $id_golongan_darah = $hasilxx->id_golongan_darah;
                $id_program_studi_pilihan = $hasilxx->id_program_studi_pilihan;
                $email = $hasilxx->email;
                $no_telp = $hasilxx->no_telp;
                $file_foto = $hasilxx->file_foto;
                $nama_ayah = $hasilxx->nama_ayah;
                $nama_ibu = $hasilxx->nama_ibu;
                $ukuran_jas = $hasilxx->ukuran_jas;
                $id_gelombang_calon_mhs = $hasilxx->id_gelombang_calon_mhs;

                $this->db->trans_begin();

                $sqlxx = " SELECT nim FROM mahasiswa WHERE id_program_studi = '$id_program_studi_pilihan' ORDER BY nim DESC LIMIT 1 ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    //$nim_urut = substr($hasilxx->nim,-3);
                    $nim_urut = substr($hasilxx->nim, 6, 3);
                    $nim_urut++;
                    if ($nim_urut < 100 && $nim_urut > 9)
                        $nim_urut = '0' . $nim_urut;
                    else if ($nim_urut < 100 && $nim_urut < 10)
                        $nim_urut = '00' . $nim_urut;
                } else {
                    $nim_urut = 1;
                    $nim_urut = '00' . $nim_urut;
                }

                // ambil tahun dari gelombang_calon_mhs
                $sqlxx = " select b.id as id_tahun_akademik, b.tahun FROM master_gelombang_calon_mhs a 
                        INNER JOIN master_tahun_akademik b ON b.id = a.id_tahun_akademik
                        WHERE a.id = '$id_gelombang_calon_mhs' ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    $tahun = substr($hasilxx->tahun, -2);
                    // $tahun    = $hasilxx->tahun;
                    $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
                }

                $sqlxx = " select a.kode as kode_fakultas, b.kode as kode_prodi FROM master_fakultas a 
                        INNER JOIN master_program_studi b ON a.id = b.id_fakultas
                        WHERE b.id = '$id_program_studi_pilihan' ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    $kode_fakultas    = $hasilxx->kode_fakultas;
                    $kode_prodi    = $hasilxx->kode_prodi;
                }

                // set no pendaftaran
                $nim = $tahun . $kode_fakultas . $kode_prodi . $nim_urut;

                // insert ke tabel mahasiswa
                $data = array(
                    "id_program_studi" => $id_program_studi_pilihan,
                    "id_calon_mhs" => $id,
                    //"id_user" => $id_user,
                    "nama" => $nama,
                    "nim" => $nim,
                    "alamat" => $alamat,
                    "kelurahan" => $kelurahan,
                    "kecamatan" => $kecamatan,
                    "kode_pos" => $kode_pos,
                    "tempat_lahir" => $tempat_lahir,
                    "tgl_lahir" => $tgl_lahir,
                    "jenis_kelamin" => $jenis_kelamin,
                    "nik" => $nik,
                    "nisn" => $nisn,
                    "id_agama" => $id_agama,
                    "id_golongan_darah" => $id_golongan_darah,
                    "id_provinsi" => $id_provinsi,
                    "id_kabupaten_kota" => $id_kabupaten_kota,
                    "no_telp" => $no_telp,
                    "email" => $email,
                    "nama_ayah" => $nama_ayah,
                    "nama_ibu" => $nama_ibu,
                    "id_status_mhs" => '1',
                    "file_foto" => $file_foto,
                    "tgl_input" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );

                $this->db->insert('mahasiswa', $data);
                $id_mhs = $this->db->insert_id();

                // insert ke tabel riwayat_status_mhs. nanti aja. ini diinsert pada saat data KRS dibuat. 10-12-2021 dibikin disini
                // 12-12-2021 cek apakah sudah ada data, jika blm, maka insert
                $sqlxx2 = " select id FROM riwayat_status_mhs WHERE id_semester = '1' AND id_tahun_akademik = '$id_tahun_akademik'
                            AND id_mhs = '$id_mhs' ";
                $queryxx2 = $this->db->query($sqlxx2);
                if ($queryxx2->num_rows() == 0) {
                    $tgl_skrg = date('Y-m-d H:i:s');
                    $data = array(
                        'id_mhs' => $id_mhs,
                        'id_semester' => 1,
                        'id_tahun_akademik' => $id_tahun_akademik,
                        'id_status_mhs' => 1,
                        'tgl_input' => $tgl_skrg,
                        'user_update_by' => $this->session->userdata['username']
                    );

                    $this->db->insert('riwayat_status_mhs', $data);
                }

                // update ke tabel master_user di field id_grup_user menjadi 4 (mahasiswa)
                // modif 10-12-2021, create user baru dgn username=nim
                $data = array(
                    'id_grup_user' => '4',
                    'id_program_studi' => $id_program_studi_pilihan,
                    'username' => $nim,
                    'userpass' => md5($nim),
                    'tgl_input' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );

                $this->db->insert('master_user', $data);
                $id_user = $this->db->insert_id();

                $data = array(
                    "id_user" => $id_user
                );
                $this->db->where('id', $id_mhs);
                $this->db->update('mahasiswa', $data);

                // $data = array(
                //     "id_grup_user" => '4',
                //     "tgl_update" => date('Y-m-d H:i:s'),
                //     "user_update_by" => $this->session->userdata['username']
                // );
                // $this->db->where('username', $no_pendaftaran);
                // $this->db->update('master_user', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa dgn no pendaftaran <b>' . $no_pendaftaran . '</b> berhasil diregistrasi ke data mahasiswa. Silahkan dilengkapi biodatanya di menu Mahasiswa. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('calon_mhs');
        }
    }

    // 23-10-2021
    public function view_biaya_calon_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id calon mhs
        $sqlxx = " select a.id, a.nama, a.no_pendaftaran, a.id_program_studi_pilihan, b.id_tahun_akademik FROM calon_mahasiswa a
                    INNER JOIN master_gelombang_calon_mhs b ON a.id_gelombang_calon_mhs = b.id
                   WHERE a.no_pendaftaran = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $no_pendaftaran    = $hasilxx->no_pendaftaran;
            $id_calon_mhs = $hasilxx->id;
            $nama_calon_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi_pilihan;
            $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
        }

        // cek apakah calon mhs udah pernah input pembayaran. jika sudah, maka munculkan informasi bahwa sudah bayar
        $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
                    FROM pembayaran_biaya_calon_mhs a 
                    LEFT JOIN master_rekening b ON b.id = a.id_rekening
                    WHERE a.id_calon_mhs = '$id_calon_mhs' AND a.jenis_biaya = '1' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();

            $pisah1 = explode("-", $hasilxx->tgl_bayar);
            $tgl1 = $pisah1[2];
            $bln1 = $pisah1[1];
            $thn1 = $pisah1[0];
            $hasilxx->tgl_bayar = $tgl1 . "-" . $bln1 . "-" . $thn1;

            $data["title"] = "Biaya Pendaftaran";
            $data['nominal'] = $hasilxx->nominal;
            $data['jenis_bayar'] = $hasilxx->jenis_bayar;
            $data['tgl_bayar'] = $hasilxx->tgl_bayar;
            $data['nama_bank'] = $hasilxx->nama_bank;
            $data['no_rekening'] = $hasilxx->no_rekening;
            $data['atas_nama'] = $hasilxx->atas_nama;
            $data['nama_calon_mhs'] = $nama_calon_mhs;
            $data['no_pendaftaran'] = $no_pendaftaran;
            $data['jenis_biaya'] = 1;
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('calon_mhs/pembayaran_calon_mhs_done', $data);
            $this->load->view('templates/footer');
        } else {
            // ambil list biaya dari master_biaya_calon_mhs
            $sqlxx = " select nama, nominal FROM master_biaya_calon_mhs WHERE id_tahun_akademik = '$id_tahun_akademik' 
                       AND id_program_studi = '$id_prodi' AND jenis_biaya = '1'  ORDER BY id ";
            $queryxx = $this->db->query($sqlxx);
            $jumlahnya = 0;
            if ($queryxx->num_rows() > 0) {
                $data_biaya = array();
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $nama    = $rowxx->nama;
                    $nominal    = $rowxx->nominal;
                    $jumlahnya += $nominal;

                    $data_biaya[] = array(
                        'nama' => $nama,
                        'nominal' => $nominal
                    );
                }
            } else {
                $data_biaya = '';
            }


            // ambil list rekening kampus
            $sqlxx = " select id, nama_bank, no_rekening, atas_nama FROM master_rekening ORDER BY id ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $data_rek = array();
                $hasilxx = $queryxx->result();
                foreach ($hasilxx as $rowxx) {
                    $id_rekening    = $rowxx->id;
                    $nama_bank    = $rowxx->nama_bank;
                    $no_rekening    = $rowxx->no_rekening;
                    $atas_nama    = $rowxx->atas_nama;

                    $data_rek[] = array(
                        'id_rekening' => $id_rekening,
                        'nama_bank' => $nama_bank,
                        'no_rekening' => $no_rekening,
                        'atas_nama' => $atas_nama
                    );
                }
            } else {
                $data_rek = '';
            }

            $is_simpan = $this->input->post('is_simpan');

            if ($is_simpan == '1') {
                $jumbayar = $this->input->post('jumbayar');
                $id_rekening = $this->input->post('id_rekening');
                $jenis_bayar = $this->input->post('jenis_bayar');
                $tgl_bayar = $this->input->post('tgl_bayar');
                //$id_calon_mhs = $this->input->post('id_calon_mhs');

                if ($jenis_bayar == '1') {
                    $id_rekening = 0;
                }

                $tgl_skrg = date('Y-m-d H:i:s');

                $this->db->trans_begin();

                $data = array(
                    'id_calon_mhs' => $id_calon_mhs,
                    'jenis_biaya' => 1,
                    'nominal' => $jumbayar,
                    'id_rekening' => $id_rekening,
                    'jenis_bayar' => $jenis_bayar,
                    'tgl_bayar' => $tgl_bayar,
                    'tgl_input' => $tgl_skrg,
                    'user_update_by' => $no_pendaftaran
                );

                $this->db->insert('pembayaran_biaya_calon_mhs', $data);

                // update status bayar
                $data = array(
                    "status_pembayaran_pendaftaran" => '1',
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
                $this->db->where('id', $id_calon_mhs);
                $this->db->update('calon_mahasiswa', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Konfirmasi Pembayaran Biaya Pendaftaran berhasil disimpan. Panitia akan memverifikasi data pembayaran anda. Pengumuman status seleksi akan diumumkan lebih lanjut, terimakasih. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
                redirect("calon_mhs/pembayaran_biaya_calon_mhs_success");
            } else {
                $data["title"] = "Biaya Pendaftaran";
                $data['data_biaya'] = $data_biaya;
                $data['jumlahnya'] = $jumlahnya;
                $data['data_rek'] = $data_rek;
                $data['id_calon_mhs'] = $id_calon_mhs;
                $data['nama_calon_mhs'] = $nama_calon_mhs;
                $data['no_pendaftaran'] = $no_pendaftaran;
                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('calon_mhs/view_biaya_calon_mhs', $data);
                $this->load->view('templates/footer');
            }
        } // end if jika sudah pernah input
    }

    public function pembayaran_biaya_calon_mhs_success()
    {
        // $data["title"] = "Calon Mahasiswa";
        // $data["data_calon_mhs"] = $this->Calon_mhs_m->getAll();
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/pembayaran_biaya_calon_mhs_success');
        $this->load->view('templates/footer');
    }

    public function biodata_calon_mhs()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $calonmhs = $this->Calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($calonmhs->rules());
        if ($validation->run()) {
            $this->db->trans_begin();

            // -------------------------- start 28-12-2021 -------------------------------
            // file foto UPDATE 23-10-2021
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            //$config['encrypt_name'] = FALSE;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');

            // ambil id user
            $sqlxx = " select id_user, file_foto, file_dokumen FROM calon_mahasiswa WHERE no_pendaftaran = '" . $this->session->userdata['username'] . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $id_usernya    = $hasilxx->id_user;
                $file_foto_lama    = $hasilxx->file_foto;
                $file_dokumen_lama    = $hasilxx->file_dokumen;
            }

            $config['file_name'] = 'PMBfoto_' . $id_usernya . '_' . $timestamp;
            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_foto']['tmp_name'])) {

                $up = $this->upload->do_upload('file_foto');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_foto = $upl['file_name'];

                    //=========== hapus file lama
                    if ($file_foto_lama != '')
                        unlink($dir . '/' . $file_foto_lama);

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_foto = $file_foto_lama;
            }

            // file dokumen
            $config['allowed_types'] = 'pdf';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBdoc_' . $id_usernya . '_' . $timestamp;

            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_dokumen']['tmp_name'])) {
                $this->load->library('upload', $config);
                $up = $this->upload->do_upload('file_dokumen');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_dokumen = $upl['file_name'];

                    //=========== hapus file lama
                    if ($file_dokumen_lama != '')
                        unlink($dir . '/' . $file_dokumen_lama);

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_dokumen = $file_dokumen_lama;
            }

            // 28-12-2021 file riwayat penyakit
            // $config['allowed_types'] = 'pdf';
            // $config['overwrite'] = false;
            // $config['max_size'] = 5120;
            // $dir = './uploads/';
            // if (!file_exists($dir)) {
            //     mkdir($dir, 777, true);
            // }

            // $config['upload_path'] = $dir;
            // $timestamp = date('YmdHis');
            // $config['file_name'] = 'PMBrwytpen' . $id_usernya . '_' . $timestamp;

            // $this->load->library('upload', $config);
            // $error = 0;
            // if (!empty($_FILES['file_riwayat_penyakit']['tmp_name'])) {
            //     $this->load->library('upload', $config);
            //     $up = $this->upload->do_upload('file_riwayat_penyakit');
            //     if ($up) {
            //         $upl = $this->upload->data();
            //         $file_riwayat_penyakit = $upl['file_name'];

            //         //=========== hapus file lama
            //         if ($file_riwayat_penyakit_lama != '')
            //             unlink($dir . '/' . $file_riwayat_penyakit_lama);

            //         unset($this->upload);
            //     } else {
            //         $error = 1;
            //     }
            // } else {
            //     $file_riwayat_penyakit = $file_riwayat_penyakit_lama;
            // }
            // -------------------------- end -------------------------------------------

            // update data
            $data = array(
                "id_program_studi_pilihan" => $this->input->post('id_program_studi_pilihan'),
                "id_gelombang_calon_mhs" => $this->input->post('id_gelombang_calon_mhs'),
                "nama" => $this->input->post('nama'),
                "alamat" => $this->input->post('alamat'),
                "kelurahan" => $this->input->post('kelurahan'),
                "kecamatan" => $this->input->post('kecamatan'),
                "kode_pos" => $this->input->post('kode_pos'),
                "tempat_lahir" => $this->input->post('tempat_lahir'),
                "tgl_lahir" => $this->input->post('tgl_lahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "nik" => $this->input->post('nik'),
                "nisn" => $this->input->post('nisn'),
                "id_agama" => $this->input->post('id_agama'),
                "id_golongan_darah" => $this->input->post('id_golongan_darah'),
                "id_provinsi" => $this->input->post('id_provinsi'),
                "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
                "no_telp" => $this->input->post('no_telp'),
                "ukuran_jas" => $this->input->post('ukuran_jas'),
                "email" => $this->input->post('email'),
                "nama_ayah" => $this->input->post('nama_ayah'),
                "nama_ibu" => $this->input->post('nama_ibu'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username'],
                "file_foto" => $file_foto,
                "file_dokumen" => $file_dokumen,
                "riwayat_penyakit" => $this->input->post('riwayat_penyakit')
            );

            $this->db->where('no_pendaftaran', $this->session->userdata['username']);
            $this->db->update('calon_mahasiswa', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa berhasil diupdate! 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        }

        $sqlxx = " select id FROM calon_mahasiswa WHERE no_pendaftaran = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_calon_mhs = $hasilxx->id;
        }

        $data["title"] = "Biodata Calon Mahasiswa Baru";
        $data['biodata'] = $this->Calon_mhs_m->get_calon_mhs_by_id($id_calon_mhs);
        $data['prodi'] = $this->Calon_mhs_m->getProgramStudi();
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data['agama'] = $this->Calon_mhs_m->getAgama();
        $data['goldar'] = $this->Calon_mhs_m->getGoldar();
        //$data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Calon_mhs_m->getProvinsi();
        $data['kab_kota'] = $this->Calon_mhs_m->getKabKota();
        $data['list_dokumen'] = $this->Calon_mhs_m->get_master_dokumen();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/biodata_calon_mhs', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id_calon_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $calonmhs = $this->Calon_mhs_m;
        $validation = $this->form_validation;
        $validation->set_rules($calonmhs->rules());
        if ($validation->run()) {
            // update data
            $id_calon_mhs = $this->input->post('id_calon_mhs');

            $this->db->trans_begin();

            // -------------------------- start 28-12-2021 -------------------------------
            // file foto UPDATE 23-10-2021
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            //$config['encrypt_name'] = FALSE;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');

            // ambil id user
            $sqlxx = " select id_user, file_foto, file_dokumen FROM calon_mahasiswa WHERE id = '" . $id_calon_mhs . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $id_usernya    = $hasilxx->id_user;
                $file_foto_lama    = $hasilxx->file_foto;
                $file_dokumen_lama    = $hasilxx->file_dokumen;
            }

            $config['file_name'] = 'PMBfoto_' . $id_usernya . '_' . $timestamp;
            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_foto']['tmp_name'])) {

                $up = $this->upload->do_upload('file_foto');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_foto = $upl['file_name'];

                    //=========== hapus file lama
                    if ($file_foto_lama != '')
                        unlink($dir . '/' . $file_foto_lama);

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_foto = $file_foto_lama;
            }

            // file dokumen
            $config['allowed_types'] = 'pdf';
            $config['overwrite'] = false;
            $config['max_size'] = 5120;
            $dir = './uploads/';
            if (!file_exists($dir)) {
                mkdir($dir, 777, true);
            }

            $config['upload_path'] = $dir;
            $timestamp = date('YmdHis');
            $config['file_name'] = 'PMBdoc_' . $id_usernya . '_' . $timestamp;

            $this->load->library('upload', $config);
            $error = 0;
            if (!empty($_FILES['file_dokumen']['tmp_name'])) {
                $this->load->library('upload', $config);
                $up = $this->upload->do_upload('file_dokumen');
                if ($up) {
                    $upl = $this->upload->data();
                    $file_dokumen = $upl['file_name'];

                    //=========== hapus file lama
                    if ($file_dokumen_lama != '')
                        unlink($dir . '/' . $file_dokumen_lama);

                    unset($this->upload);
                } else {
                    $error = 1;
                }
            } else {
                $file_dokumen = $file_dokumen_lama;
            }

            // 28-12-2021 file riwayat penyakit
            // $config['allowed_types'] = 'pdf';
            // $config['overwrite'] = false;
            // $config['max_size'] = 5120;
            // $dir = './uploads/';
            // if (!file_exists($dir)) {
            //     mkdir($dir, 777, true);
            // }

            // $config['upload_path'] = $dir;
            // $timestamp = date('YmdHis');
            // $config['file_name'] = 'PMBrwytpen' . $id_usernya . '_' . $timestamp;

            // $this->load->library('upload', $config);
            // $error = 0;
            // if (!empty($_FILES['file_riwayat_penyakit']['tmp_name'])) {
            //     $this->load->library('upload', $config);
            //     $up = $this->upload->do_upload('file_riwayat_penyakit');
            //     if ($up) {
            //         $upl = $this->upload->data();
            //         $file_riwayat_penyakit = $upl['file_name'];

            //         //=========== hapus file lama
            //         if ($file_riwayat_penyakit_lama != '')
            //             unlink($dir . '/' . $file_riwayat_penyakit_lama);

            //         unset($this->upload);
            //     } else {
            //         $error = 1;
            //     }
            // } else {
            //     $file_riwayat_penyakit = $file_riwayat_penyakit_lama;
            // }
            // -------------------------- end -------------------------------------------

            $data = array(
                "id_program_studi_pilihan" => $this->input->post('id_program_studi_pilihan'),
                "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
                "id_gelombang_calon_mhs" => $this->input->post('id_gelombang_calon_mhs'),
                "nama" => $this->input->post('nama'),
                "alamat" => $this->input->post('alamat'),
                "kelurahan" => $this->input->post('kelurahan'),
                "kecamatan" => $this->input->post('kecamatan'),
                "kode_pos" => $this->input->post('kode_pos'),
                "tempat_lahir" => $this->input->post('tempat_lahir'),
                "tgl_lahir" => $this->input->post('tgl_lahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "nik" => $this->input->post('nik'),
                "nisn" => $this->input->post('nisn'),
                "id_agama" => $this->input->post('id_agama'),
                "id_golongan_darah" => $this->input->post('id_golongan_darah'),
                "id_provinsi" => $this->input->post('id_provinsi'),
                "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
                "no_telp" => $this->input->post('no_telp'),
                "ukuran_jas" => $this->input->post('ukuran_jas'),
                "email" => $this->input->post('email'),
                "nama_ayah" => $this->input->post('nama_ayah'),
                "nama_ibu" => $this->input->post('nama_ibu'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username'],
                "file_foto" => $file_foto,
                "file_dokumen" => $file_dokumen,
                "riwayat_penyakit" => $this->input->post('riwayat_penyakit')
            );
            $this->db->where('id', $id_calon_mhs);
            $this->db->update('calon_mahasiswa', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Calon Mahasiswa berhasil diupdate! 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("calon_mhs");
        }

        $data["title"] = "Edit Data Calon Mahasiswa Baru";
        $data['biodata'] = $this->Calon_mhs_m->get_calon_mhs_by_id($id_calon_mhs);
        $data['prodi'] = $this->Calon_mhs_m->getProgramStudi();
        // 01-06-2022
        $data["data_kategori_kelas"] = $this->Calon_mhs_m->get_kategori_kelas();
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data['agama'] = $this->Calon_mhs_m->getAgama();
        $data['goldar'] = $this->Calon_mhs_m->getGoldar();
        //$data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Calon_mhs_m->getProvinsi();
        $data['kab_kota'] = $this->Calon_mhs_m->getKabKota();
        $data['list_dokumen'] = $this->Calon_mhs_m->get_master_dokumen();
        $data['id_calon_mhs'] = $id_calon_mhs;
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/edit', $data);
        $this->load->view('templates/footer');
    }

    public function view($id_calon_mhs)
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $data["title"] = "View Data Calon Mahasiswa Baru";
        $data['biodata'] = $this->Calon_mhs_m->get_calon_mhs_by_id($id_calon_mhs);
        $data['prodi'] = $this->Calon_mhs_m->getProgramStudi();
        $data['gelombang'] = $this->Calon_mhs_m->getGelombang();
        $data['agama'] = $this->Calon_mhs_m->getAgama();
        $data['goldar'] = $this->Calon_mhs_m->getGoldar();
        //$data['jalur_pendaftaran'] = $this->Mahasiswa_m->getJalurPendaftaran();
        $data['provinsi'] = $this->Calon_mhs_m->getProvinsi();
        $data['kab_kota'] = $this->Calon_mhs_m->getKabKota();
        $data['id_calon_mhs'] = $id_calon_mhs;
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/view', $data);
        $this->load->view('templates/footer');
    }

    // 28-10-2021
    public function view_biaya_upp()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        // ambil id calon mhs
        $sqlxx = " select a.id, a.nama, a.no_pendaftaran, a.id_program_studi_pilihan, a.status_seleksi, a.id_kategori_kelas,
                    b.id_tahun_akademik FROM calon_mahasiswa a
                    INNER JOIN master_gelombang_calon_mhs b ON a.id_gelombang_calon_mhs = b.id
                   WHERE a.no_pendaftaran = '" . $this->session->userdata['username'] . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $no_pendaftaran    = $hasilxx->no_pendaftaran;
            $id_calon_mhs = $hasilxx->id;
            $nama_calon_mhs    = $hasilxx->nama;
            $id_prodi    = $hasilxx->id_program_studi_pilihan;
            $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
            $status_seleksi = $hasilxx->status_seleksi;
            // 01-06-2022
            $id_kategori_kelas = $hasilxx->id_kategori_kelas;
        }

        if ($status_seleksi == '0' || $status_seleksi == '2') {
            $data['status_seleksi'] = $status_seleksi;
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('calon_mhs/waiting_status_seleksi', $data);
            $this->load->view('templates/footer');
        } else {

            // cek apakah calon mhs udah pernah input pembayaran. jika sudah, maka munculkan informasi bahwa sudah bayar
            $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
                    FROM pembayaran_biaya_calon_mhs a 
                    LEFT JOIN master_rekening b ON b.id = a.id_rekening
                    WHERE a.id_calon_mhs = '$id_calon_mhs' AND a.jenis_biaya = '2' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();

                $pisah1 = explode("-", $hasilxx->tgl_bayar);
                $tgl1 = $pisah1[2];
                $bln1 = $pisah1[1];
                $thn1 = $pisah1[0];
                $hasilxx->tgl_bayar = $tgl1 . "-" . $bln1 . "-" . $thn1;

                $data["title"] = "Biaya UPP";
                $data['nominal'] = $hasilxx->nominal;
                $data['jenis_bayar'] = $hasilxx->jenis_bayar;
                $data['tgl_bayar'] = $hasilxx->tgl_bayar;
                $data['nama_bank'] = $hasilxx->nama_bank;
                $data['no_rekening'] = $hasilxx->no_rekening;
                $data['atas_nama'] = $hasilxx->atas_nama;
                $data['nama_calon_mhs'] = $nama_calon_mhs;
                $data['no_pendaftaran'] = $no_pendaftaran;
                $data['jenis_biaya'] = 2;
                $this->load->view('templates/header', $data);
                $this->load->view('templates/menu');
                $this->load->view('calon_mhs/pembayaran_calon_mhs_done', $data);
                $this->load->view('templates/footer');
            } else {
                // ambil list biaya dari master_biaya_calon_mhs
                $sqlxx = " select a.nama, a.nominal, b.nama as nama_kategori_kelas FROM master_biaya_calon_mhs a 
                        INNER JOIN master_kategori_kelas b ON a.id_kategori_kelas = b.id
                        WHERE a.id_tahun_akademik = '$id_tahun_akademik' 
                       AND a.id_program_studi = '$id_prodi' AND a.jenis_biaya = '2' 
                       AND a.id_kategori_kelas = '$id_kategori_kelas'  ORDER BY a.id ";
                // echo $sqlxx;
                // die();
                $queryxx = $this->db->query($sqlxx);
                $jumlahnya = 0;
                if ($queryxx->num_rows() > 0) {
                    $data_biaya = array();
                    $hasilxx = $queryxx->result();
                    foreach ($hasilxx as $rowxx) {
                        $nama    = $rowxx->nama;
                        $nominal    = $rowxx->nominal;
                        $nama_kategori_kelas    = $rowxx->nama_kategori_kelas;
                        $jumlahnya += $nominal;

                        $data_biaya[] = array(
                            'nama' => $nama,
                            'nominal' => $nominal,
                            'nama_kategori_kelas' => $nama_kategori_kelas
                        );
                    }
                } else {
                    $data_biaya = '';
                }


                // ambil list rekening kampus
                $sqlxx = " select id, nama_bank, no_rekening, atas_nama FROM master_rekening ORDER BY id ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $data_rek = array();
                    $hasilxx = $queryxx->result();
                    foreach ($hasilxx as $rowxx) {
                        $id_rekening    = $rowxx->id;
                        $nama_bank    = $rowxx->nama_bank;
                        $no_rekening    = $rowxx->no_rekening;
                        $atas_nama    = $rowxx->atas_nama;

                        $data_rek[] = array(
                            'id_rekening' => $id_rekening,
                            'nama_bank' => $nama_bank,
                            'no_rekening' => $no_rekening,
                            'atas_nama' => $atas_nama
                        );
                    }
                } else {
                    $data_rek = '';
                }

                $is_simpan = $this->input->post('is_simpan');

                if ($is_simpan == '1') {
                    $jumbayar = $this->input->post('jumbayar');
                    $id_rekening = $this->input->post('id_rekening');
                    $jenis_bayar = $this->input->post('jenis_bayar');
                    $tgl_bayar = $this->input->post('tgl_bayar');
                    //$id_calon_mhs = $this->input->post('id_calon_mhs');

                    if ($jenis_bayar == '1') {
                        $id_rekening = 0;
                    }
                    $tgl_skrg = date('Y-m-d H:i:s');

                    $this->db->trans_begin();

                    $data = array(
                        'id_calon_mhs' => $id_calon_mhs,
                        'jenis_biaya' => 2,
                        'nominal' => $jumbayar,
                        'id_rekening' => $id_rekening,
                        'jenis_bayar' => $jenis_bayar,
                        'tgl_bayar' => $tgl_bayar,
                        'tgl_input' => $tgl_skrg,
                        'user_update_by' => $no_pendaftaran
                    );

                    $this->db->insert('pembayaran_biaya_calon_mhs', $data);

                    // update status bayar
                    $data = array(
                        "status_pembayaran_upp" => '1',
                        "tgl_update" => date('Y-m-d H:i:s'),
                        "user_update_by" => $this->session->userdata['username']
                    );
                    $this->db->where('id', $id_calon_mhs);
                    $this->db->update('calon_mahasiswa', $data);

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Konfirmasi Pembayaran Biaya UPP berhasil disimpan. Panitia akan memverifikasi data pembayaran anda. Terimakasih. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
                    redirect("calon_mhs/pembayaran_biaya_upp_success");
                } else {
                    $data["title"] = "Biaya UPP";
                    $data['data_biaya'] = $data_biaya;
                    $data['jumlahnya'] = $jumlahnya;
                    $data['data_rek'] = $data_rek;
                    $data['id_calon_mhs'] = $id_calon_mhs;
                    $data['nama_calon_mhs'] = $nama_calon_mhs;
                    $data['no_pendaftaran'] = $no_pendaftaran;
                    $this->load->view('templates/header', $data);
                    $this->load->view('templates/menu');
                    $this->load->view('calon_mhs/view_biaya_upp', $data);
                    $this->load->view('templates/footer');
                }
            } // end if jika sudah pernah input
        }
    }

    public function pembayaran_biaya_upp_success()
    {
        // $data["title"] = "Calon Mahasiswa";
        // $data["data_calon_mhs"] = $this->Calon_mhs_m->getAll();
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('calon_mhs/pembayaran_biaya_upp_success');
        $this->load->view('templates/footer');
    }
}
