<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_prodi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_prodi_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $prodi = $this->Master_prodi_m;

        $validation = $this->form_validation;
        $validation->set_rules($prodi->rules());
        if ($validation->run()) {
            $prodi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Program Studi berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_prodi");
        }
        $data["title"] = "Program Studi";

        $data['data_prodi'] = $this->Master_prodi_m->getAll();
        $data['fakultas'] = $this->Master_prodi_m->getFakultas();
        $data['jenjang'] = $this->Master_prodi_m->getJenjangStudi();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('prodi/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $data['fakultas'] = $this->Master_prodi_m->getFakultas();
        $data['jenjang'] = $this->Master_prodi_m->getJenjangStudi();
        $this->form_validation->set_rules('id_fakultas', 'id_fakultas', 'required');
        $this->form_validation->set_rules('id_jenjang_studi', 'id_jenjang_studi', 'required');
        $this->form_validation->set_rules('kode', 'kode', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Program Studi gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_prodi');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "id_fakultas" => $this->input->post('id_fakultas'),
                "id_jenjang_studi" => $this->input->post('id_jenjang_studi'),
                "kode" => $this->input->post('kode'),
                "nama" => $this->input->post('nama'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_program_studi', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Program Studi berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_prodi');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Program Studi gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_prodi');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_program_studi');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Program Studi berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_prodi');
        }
    }
}
