<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_tahun_akademik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_tahun_akademik_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $data["title"] = "Tahun Akademik";
        $data["data_tahun_akademik"] = $this->Master_tahun_akademik_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('tahun_akademik/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
    	$TahunAkademik = $this->Master_tahun_akademik_m;
        $validation = $this->form_validation;
        $validation->set_rules($TahunAkademik->rules());
        if ($validation->run()) {
            $TahunAkademik->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tahun Akademik berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_tahun_akademik");
        }
        $data["title"] = "Tambah Data Master Tahun Akademik";
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('tahun_akademik/add');
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('master_tahun_akademik');

        $TahunAkademik = $this->Master_tahun_akademik_m;
        $validation = $this->form_validation;
        $validation->set_rules($TahunAkademik->rules());

        if ($validation->run()) {
            $TahunAkademik->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tahun Akademik berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("master_tahun_akademik");
        }
        $data["title"] = "Edit Data Tahun Akademik";
        $data["data_tahun_akademik"] = $TahunAkademik->getById($id);
        if (!$data["data_tahun_akademik"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('tahun_akademik/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tahun Akademik gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_tahun_akademik');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_tahun_akademik');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tahun Akademik berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_tahun_akademik');
        }
    }
}
