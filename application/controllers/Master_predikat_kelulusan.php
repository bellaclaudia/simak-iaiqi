<?php
defined('BASEPATH') or exit('No direct script access allowed');

class master_predikat_kelulusan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_predikat_kelulusan_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $pk = $this->Master_predikat_kelulusan_m;

        $validation = $this->form_validation;
        $validation->set_rules($pk->rules());
        if ($validation->run()) {
            $pk->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Predikat Kelulusan berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_predikat_kelulusan");
        }
        $data["title"] = "Predikat Kelulusan";

        $data['data_pk'] = $this->Master_predikat_kelulusan_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('predikat_kelulusan/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('nama_predikat', 'nama_predikat', 'required');
        $this->form_validation->set_rules('ipk_batas_awal', 'ipk_batas_awal', 'required');
        $this->form_validation->set_rules('ipk_batas_akhir', 'ipk_batas_akhir', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Predikat Kelulusan gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_predikat_kelulusan');
        } else {
            $data = array(
                //"id" => $_POST['id'],
                "nama_predikat" => $this->input->post('nama_predikat'),
                "ipk_batas_awal" => $this->input->post('ipk_batas_awal'),
                "ipk_batas_akhir" => $this->input->post('ipk_batas_akhir'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_predikat_kelulusan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Predikat Kelulusan berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_predikat_kelulusan');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Predikat Kelulusan gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_predikat_kelulusan');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_predikat_kelulusan');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Predikat Kelulusan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_predikat_kelulusan');
        }
    }
}
