<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_kuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("jadwal_kuliah_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_ta = $this->input->post('id_ta');
        $id_semester = $this->input->post('id_semester');
        $id_prodi = $this->input->post('id_prodi');
        $excel = $this->input->post('excel');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $data["title"] = "Jadwal Kuliah";
        $data["data_jadwal_kuliah"] = $this->jadwal_kuliah_m->getAll($id_ta, $id_semester, $id_prodi);
        $data["data_prodi"] = $this->jadwal_kuliah_m->getProgramStudi();
        $data["data_thn_akademik"] = $this->jadwal_kuliah_m->getTahunAkademik();
        $data["data_semester"] = $this->jadwal_kuliah_m->getSemester();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('jadwal_kuliah/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('jadwal_kuliah/excel', $data);
    }

    public function tambah()
    {
        $jadwal_kuliah = $this->jadwal_kuliah_m;
        $validation = $this->form_validation;
        $validation->set_rules($jadwal_kuliah->rules());
        if ($validation->run()) {
            $jadwal_kuliah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jadwal Kuliah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("jadwal_kuliah");
        }
        $data["title"] = "Tambah Data Master Jadwal Kuliah";
        $data["data_prodi"] = $this->jadwal_kuliah_m->getProgramStudi();
        $data['tahun_akademik'] = $this->jadwal_kuliah_m->getTahunAkademik();
        //$data['mata_kuliah'] = $this->jadwal_kuliah_m->getMataKuliah();
        $data['semester'] = $this->jadwal_kuliah_m->getSemester();
        $data['program_studi'] = $this->jadwal_kuliah_m->getProgramStudi();
        // 17-11-2021 nambah data kelas
        //$data["data_kelas"] = $this->jadwal_kuliah_m->get_kelas_by_semta();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('jadwal_kuliah/add', $data);
        $this->load->view('templates/footer');
    }

    // 03-11-2021
    public function get_mk_by_prodi()
    {
        $id_prodi     = $this->input->post('id_prodi', TRUE);
        $id_semester     = $this->input->post('id_semester', TRUE);
        $data['data_mk'] = $this->jadwal_kuliah_m->get_mk_by_prodi($id_prodi, $id_semester);
        $this->load->view('jadwal_kuliah/vlistmk', $data);
        return true;
    }

    // 17-11-2021
    public function get_kelas_by_prodisemta()
    {
        $id_prodi     = $this->input->post('id_prodi', TRUE);
        $id_semester     = $this->input->post('id_semester', TRUE);
        $id_ta     = $this->input->post('id_ta', TRUE);
        $data['data_kelas'] = $this->jadwal_kuliah_m->get_kelas_by_prodisemta($id_prodi); // ga dipake: $id_semester, $id_ta
        $this->load->view('jadwal_kuliah/vlistkelas', $data);
        return true;
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('jadwal_kuliah');

        $jadwal_kuliah = $this->jadwal_kuliah_m;
        $validation = $this->form_validation;
        $validation->set_rules($jadwal_kuliah->rules());

        if ($validation->run()) {
            $jadwal_kuliah->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jadwal Kuliah berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("jadwal_kuliah");
        }
        $data["title"] = "Edit Data Jadwal Kuliah";
        $data["data_jadwal_kuliah"] = $jadwal_kuliah->getById($id);
        $jadwal = $jadwal_kuliah->getById($id);

        $expjammulai = explode(":", $jadwal->jam_mulai);
        $jam1 = $expjammulai[0];
        $menit1 = $expjammulai[1];
        $expjamselesai = explode(":", $jadwal->jam_selesai);
        $jam2 = $expjamselesai[0];
        $menit2 = $expjamselesai[1];

        $data['jam1'] = $jam1;
        $data['menit1'] = $menit1;
        $data['jam2'] = $jam2;
        $data['menit2'] = $menit2;

        $data['tahun_akademik'] = $this->jadwal_kuliah_m->getTahunAkademik();
        $data["data_prodi"] = $this->jadwal_kuliah_m->getProgramStudi();
        //$data['mata_kuliah'] = $this->jadwal_kuliah_m->getMataKuliah();
        $data['mata_kuliah'] = $this->jadwal_kuliah_m->getMataKuliah($jadwal->id_program_studi, $jadwal->id_semester);
        $data['kelas'] = $this->jadwal_kuliah_m->get_kelas_by_prodisemta($jadwal->id_program_studi); // ga dipake: , $jadwal->id_semester, $jadwal->id_tahun_akademik
        $data['semester'] = $this->jadwal_kuliah_m->getSemester();
        $data['program_studi'] = $this->jadwal_kuliah_m->getProgramStudi();
        if (!$data["data_jadwal_kuliah"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('jadwal_kuliah/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jadwal Kuliah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('jadwal_kuliah');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('jadwal_kuliah');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Jadwal Kuliah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('jadwal_kuliah');
        }
    }

    // public function get_matkul_by_prodi()
    // {
    //     $id_program_studi = $this->input->post('id_program_studi', TRUE);
    //     $data['data_matkul'] = $this->jadwal_kuliah_m->get_matkul_by_prodi($id_program_studi);
    //     $this->load->view('jadwal_kuliah/vlistmatkul', $data);
    //     return true;
    // }
}
