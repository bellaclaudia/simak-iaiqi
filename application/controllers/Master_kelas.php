<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_kelas_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $data["title"] = "Kelas";
        $data["data_kelas"] = $this->Master_kelas_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('kelas/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $Kelas = $this->Master_kelas_m;
        $validation = $this->form_validation;
        $validation->set_rules($Kelas->rules());
        if ($validation->run()) {
            $Kelas->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kelas berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_kelas");
        }
        $data["title"] = "Tambah Data Master Kelas";
        // $data['tahun_akademik'] = $this->Master_kelas_m->getTahunAkademik();
        // $data['semester'] = $this->Master_kelas_m->getSemester();
        $data['program_studi'] = $this->Master_kelas_m->getProgramStudi();
        $data['kategori_kelas'] = $this->Master_kelas_m->getKategoriKelas();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('kelas/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('master_kelas');

        $Kelas = $this->Master_kelas_m;
        $validation = $this->form_validation;
        $validation->set_rules($Kelas->rules());

        if ($validation->run()) {
            $Kelas->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kelas berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
            redirect("master_kelas");
        }
        $data["title"] = "Edit Data Kelas";
        $data["data_kelas"] = $Kelas->getById($id);
        // $data['tahun_akademik'] = $this->Master_kelas_m->getTahunAkademik();
        // $data['semester'] = $this->Master_kelas_m->getSemester();
        $data['program_studi'] = $this->Master_kelas_m->getProgramStudi();
        $data['kategori_kelas'] = $this->Master_kelas_m->getKategoriKelas();
        if (!$data["data_kelas"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('kelas/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kelas gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kelas');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_kelas');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kelas berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_kelas');
        }
    }
}
