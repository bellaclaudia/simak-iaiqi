<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_bobot_nilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_bobot_nilai_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $bobnilai = $this->Master_bobot_nilai_m;

        $validation = $this->form_validation;
        $validation->set_rules($bobnilai->rules());
        if ($validation->run()) {
            $bobnilai->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Nilai berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_bobot_nilai");
        }
        $data["title"] = "Level Nilai";

        $data['data_bobot_nilai'] = $this->Master_bobot_nilai_m->getAll();
        $data['list_nilai_huruf'] = $this->Master_bobot_nilai_m->get_nilai_huruf();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('bobot_nilai/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $data['list_nilai_huruf'] = $this->Master_bobot_nilai_m->get_nilai_huruf();
        $this->form_validation->set_rules('id_nilai_huruf', 'id_nilai_huruf', 'required');
        $this->form_validation->set_rules('bobot', 'bobot', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Nilai gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_bobot_nilai');
        } else {
            $data = array(
                //"id" => $_POST['id'],
                "id_nilai_huruf" => $this->input->post('id_nilai_huruf'),
                "bobot" => $this->input->post('bobot'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_bobot_nilai', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Nilai berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_bobot_nilai');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Nilai gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_bobot_nilai');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_bobot_nilai');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Bobot Nilai berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_bobot_nilai');
        }
    }
}
