<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_kuliah_reguler extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("biaya_kuliah_reguler_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $id_ta = $this->input->post('id_ta2');
        $id_semester = $this->input->post('id_semester2');
        $id_prodi = $this->input->post('id_prodi2');
        $excel = $this->input->post('excel');

        if ($id_ta == '') $id_ta = 0;
        if ($id_semester == '') $id_semester = 0;
        if ($id_prodi == '') $id_prodi = 0;

        $biaya_kuliah_reguler = $this->biaya_kuliah_reguler_m;

        $validation = $this->form_validation;
        $validation->set_rules($biaya_kuliah_reguler->rules());
        if ($validation->run()) {
            $biaya_kuliah_reguler->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah Reguler berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("biaya_kuliah_reguler");
        }
        $data["title"] = "Biaya Kuliah Reguler";

        $data['data_biaya_kuliah_reguler'] = $this->biaya_kuliah_reguler_m->getAll($id_ta, $id_semester, $id_prodi);
        $data['semester'] = $this->biaya_kuliah_reguler_m->getSemester();
        $data['tahun_akademik'] = $this->biaya_kuliah_reguler_m->getTahunAkademik();
        $data["data_prodi"] = $this->biaya_kuliah_reguler_m->getProgramStudi();
        //$data['biaya_kuliah'] = $this->biaya_kuliah_reguler_m->getBiayaKuliah();

        $data["id_ta"] = $id_ta;
        $data["id_semester"] = $id_semester;
        $data["id_prodi"] = $id_prodi;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('biaya_kuliah_reguler/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('biaya_kuliah_reguler/excel', $data);
    }

    // 04-11-2021 dikomen, karena skrg pake generate biaya
    // public function edit($id = null)
    // {
    //     $data['semester'] = $this->biaya_kuliah_reguler_m->getSemester();
    //     $data['tahun_akademik'] = $this->biaya_kuliah_reguler_m->getTahunAkademik();
    //     $data['biaya_kuliah'] = $this->biaya_kuliah_reguler_m->getBiayaKuliah();
    //     $this->form_validation->set_rules('id_semester', 'id_semester', 'required');
    //     $this->form_validation->set_rules('id_tahun_akademik', 'id_tahun_akademik', 'required');
    //     $this->form_validation->set_rules('id_biaya_kuliah', 'id_biaya_kuliah', 'required');
    //     $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
    //     if ($this->form_validation->run() == FALSE) {
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Biaya Kuliah Reguler gagal diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('biaya_kuliah_reguler');
    //     } else {
    //         $data = array(
    //             //"id" => $this->input->post('id'),
    //             "id_semester" => $this->input->post('id_semester'),
    //             "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
    //             "id_program_studi" => $this->input->post('id_program_studi'),
    //             "id_biaya_kuliah" => $this->input->post('id_biaya_kuliah'),
    //             "jumlah" => $this->input->post('jumlah'),
    //             "tgl_update" => date('Y-m-d H:i:s'),
    //             "user_update_by" => $this->session->userdata['username']
    //         );
    //         $this->db->where('id', $this->input->post('id'));
    //         $this->db->update('biaya_kuliah_persemester', $data);
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Biaya Kuliah Reguler berhasil diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('biaya_kuliah_reguler');
    //     }
    // }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah Reguler gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_reguler');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('biaya_kuliah_persemester');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Biaya Kuliah Reguler berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('biaya_kuliah_reguler');
        }
    }
}
