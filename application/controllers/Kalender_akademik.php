<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kalender_akademik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("kalender_akademik_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $semester = $this->kalender_akademik_m;
        $validation = $this->form_validation;
        $validation->set_rules($semester->rules());
        if ($validation->run()) {
            $semester->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kalender Akademik berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("kalender_akademik");
        }
        $data["title"] = "Kalender Akademik";
        $data["data_kalender_akademik"] = $this->kalender_akademik_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('kalender_akademik/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('tgl_awal', 'tgl_awal', 'required');
        $this->form_validation->set_rules('tgl_akhir', 'tgl_akhir', 'required');
        $this->form_validation->set_rules('kegiatan', 'kegiatan', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kalender Akademik gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('kalender_akademik');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "tgl_awal" => $this->input->post('tgl_awal'),
                "tgl_akhir" => $this->input->post('tgl_akhir'),
                "kegiatan" => $this->input->post('kegiatan'),
                "deskripsi" => $this->input->post('deskripsi'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('kalender_akademik', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kalender Akademik berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('kalender_akademik');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kalender Akademik gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('kalender_akademik');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('kalender_akademik');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Kalender Akademik berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('kalender_akademik');
        }
    }
}
