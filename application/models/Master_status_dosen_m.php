<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_status_dosen_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_status_dosen';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'kode',  //samakan dengan atribute name pada tags input
                'label' => 'Kode',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Nama Status Dosen',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
