<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_prodi_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_program_studi';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_fakultas',  //samakan dengan atribute name pada tags input
                'label' => 'ID Fakultas',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_jenjang_studi',  //samakan dengan atribute name pada tags input
                'label' => 'ID Jenjang Studi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kode',  //samakan dengan atribute name pada tags input
                'label' => 'Kode',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Nama Prodi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.id, a.nama, a.kode, b.id as id_fakultas, b.nama as fakultas, c.id as id_jenjang_studi, c.nama as jenjang_studi');
        $this->db->from('master_program_studi a');
        $this->db->join('master_fakultas b', 'b.id = a.id_fakultas', 'left');
        $this->db->join('master_jenjang_studi c', 'c.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getFakultas()
    {
        return $this->db->get('master_fakultas')->result_array();
    }

    public function getJenjangStudi()
    {
        return $this->db->get('master_jenjang_studi')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_fakultas" => $this->input->post('id_fakultas'),
            "id_jenjang_studi" => $this->input->post('id_jenjang_studi'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
