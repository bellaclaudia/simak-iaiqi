<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Khs_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //private $table = 'calon_mahasiswa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_ta',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_semester',
                'label' => 'Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_prodi',
                'label' => 'Program Studi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mhs yg udah ada khs (login admin)
    public function getAll($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('khs_mhs a');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = c.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("c.id_program_studi", $id_prodi);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("c.id_program_studi", "asc");
        $this->db->order_by("c.nim", "asc");

        // $this->db->select('b.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
        //                 c.nim, c.nama as nama_mhs, g.nama as nilai_huruf, d.nama as nama_mk ');
        // $this->db->from('khs_mhs a');
        // $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        // $this->db->join('master_nilai_huruf g', 'g.id = b.id_nilai_huruf', 'left');
        // $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        // $this->db->join('master_mata_kuliah d', 'd.id = b.id_mata_kuliah', 'left');
        // $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        // $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');

        // if ($id_semester != '0')
        //     $this->db->where("a.id_semester", $id_semester);
        // if ($id_ta != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_ta);
        // if ($id_prodi != '0')
        //     $this->db->where("c.id_program_studi", $id_prodi);

        // $this->db->order_by("a.id_tahun_akademik", "asc");
        // $this->db->order_by("a.id_semester", "asc");
        // $this->db->order_by("c.id_program_studi", "asc");
        // $this->db->order_by("c.nim", "asc");
        // $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();

        // $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        // e.nama as nama_prodi, f.nama as nama_jenjang, g.nama as nama_dosen ');
        // $this->db->from('krs_mhs a');
        // $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        // $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        // $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        // $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        // $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        // $this->db->join('dosen g', 'g.id = a.id_dosen_verifikator', 'left');

        // if ($id_ta != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_ta);
        // if ($id_semester != '0')
        //     $this->db->where("a.id_semester", $id_semester);
        // if ($id_prodi != '0')
        //     $this->db->where("d.id_program_studi", $id_prodi);

        // $this->db->order_by("c.tahun", "desc");
        // $this->db->order_by("a.id_semester", "asc");
        // $query = $this->db->get();
        // return $query->result();
    }

    // 02-03-2022
    public function get_khs_pddikti_akm($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.jenis_semester, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, g.kode as kode_prodi, g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('khs_mhs a');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = c.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("c.id_program_studi", $id_prodi);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("c.id_program_studi", "asc");
        $this->db->order_by("c.nim", "asc");

        $query = $this->db->get();
        return $query->result();
    }

    // 09-04-2022
    public function get_khs_pddikti_nilai_mhs($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.jenis_semester, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, c.id_program_studi, g.kode as kode_prodi, g.nama as nama_prodi, h.nama as nama_jenjang,
                        b.id_mata_kuliah, b.nilai_akhir, b1.kode as kode_mk, b1.nama as nama_mk, b2.nama as nilai_huruf, b3.bobot ');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_mata_kuliah b1', 'b.id_mata_kuliah = b1.id', 'left');
        $this->db->join('master_nilai_huruf b2', 'b.id_nilai_huruf = b2.id', 'left');
        $this->db->join('master_bobot_nilai b3', 'b2.id = b3.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = c.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("c.id_program_studi", $id_prodi);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("c.id_program_studi", "asc");
        $this->db->order_by("c.nim", "asc");

        $query = $this->db->get();
        return $query->result();
    }

    // 26-11-2021
    public function get_komponen_nilai()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_komponen_nilai')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getKelas()
    {
        return $this->db->get('master_kelas')->result_array();
    }

    // 30-11-2021
    public function get_nilai_huruf()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_nilai_huruf')->result_array();
    }

    public function get_mhs_pindahan_by_prodi($id_prodi)
    {
        // $this->db->select('id, nim, nama ');
        // $this->db->from('mahasiswa');
        // $this->db->where("id_program_studi", $id_prodi);
        // $this->db->order_by("nama", "asc");
        // $query = $this->db->get();

        $sqlxx = " SELECT id, nim, nama FROM mahasiswa WHERE id_program_studi = '$id_prodi' AND jenis_mhs = '2' 
                AND id not in (select a.id_mhs FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs)
                ORDER BY nama asc ";
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 30-11-2021
    public function get_khs_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks, c.nama as nilai_huruf 
        FROM khs_mhs_pindahan_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id
        LEFT JOIN master_nilai_huruf c ON c.id = a.id_nilai_huruf  
        WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        ORDER BY a.id asc ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    public function get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select id_mata_kuliah FROM khs_mhs_pindahan_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
        AND id_semester = '$id_semester' AND id_program_studi = '$id_prodi')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 01-12-2021 detail khs by id_khs
    public function get_detail_khs($id_khs)
    {
        $this->db->select('a.id_semester, a.id_tahun_akademik, a.id_mhs, b.*, c.nama as nama_semester, d.tahun, d.jenis_semester, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, e.id_program_studi,
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah, h.sks, i.nama as nilai_huruf,
        (j.bobot*h.sks) as angka_kredit, h2.nama as nama_dosen ');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'b.id_khs_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf i', 'i.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai j', 'i.id = j.id_nilai_huruf', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->join('dosen_pembimbing_akademik_mhs h1', 'h1.id_kelas = e.id_kelas', 'left');
        $this->db->join('dosen h2', 'h2.id = h1.id_dosen', 'left');
        $this->db->where("a.id", $id_khs);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }

    // 01-12-2021 utk form edit
    public function get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        // WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        // AND a.id NOT IN (select b.id_mata_kuliah FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs
        // WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        // AND a.id_semester = '$id_semester')
        // ORDER BY a.kode ";

        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select b.id_mata_kuliah FROM khs_mhs a INNER JOIN khs_mhs_mk b ON a.id = b.id_khs_mhs 
        WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        AND a.id_semester = '$id_semester')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 29-11-2021
    public function getAll_nilai_mhs_pindahan($id_ta, $id_semester, $id_prodi)
    {
        $this->db->distinct();
        $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('khs_mhs a');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = c.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->where("c.jenis_mhs", '2');
        $this->db->where("a.is_konversi_nilai_mhs_pindahan", '1');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("c.id_program_studi", $id_prodi);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("c.id_program_studi", "asc");
        $this->db->order_by("c.nim", "asc");
        $query = $this->db->get();
        return $query->result();

        // $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        // e.nama as nama_prodi, f.nama as nama_jenjang, g.nama as nama_dosen ');
        // $this->db->from('krs_mhs a');
        // $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        // $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        // $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        // $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        // $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        // $this->db->join('dosen g', 'g.id = a.id_dosen_verifikator', 'left');

        // if ($id_ta != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_ta);
        // if ($id_semester != '0')
        //     $this->db->where("a.id_semester", $id_semester);
        // if ($id_prodi != '0')
        //     $this->db->where("d.id_program_studi", $id_prodi);

        // $this->db->order_by("c.tahun", "desc");
        // $this->db->order_by("a.id_semester", "asc");
        // $query = $this->db->get();
        // return $query->result();
    }

    // 04-02-2022
    public function get_data_mk($id_semester, $id_ta, $id_kelas)
    {
        $this->db->distinct();
        $this->db->select('a.id_kelas, a.id_mata_kuliah, a.jenis_kuliah, a.id_semester, a.id_tahun_akademik, 
                        e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        d.kode, d.nama as nama_mata_kuliah, d.sks, 
                        g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_kelas, b.id_dosen, c.nidn, c.nama as nama_dosen ');
        $this->db->from('jadwal_kuliah a');
        $this->db->join('dosen_pengajar_kuliah b', 'b.id_jadwal_kuliah = a.id', 'inner');
        $this->db->join('dosen c', 'c.id = b.id_dosen', 'inner');
        $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        // $this->db->join('master_komponen_nilai d', 'a.id_komponen_nilai = d.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->join('master_kelas i', 'i.id = a.id_kelas', 'left');
        //$this->db->where("b.id_dosen", $id_dosen);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_kelas != '0')
            $this->db->where("a.id_kelas", $id_kelas);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detail_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_kelas) //$id_komponen_nilai
    {
        // $queryxx    = $this->db->query(" SELECT nama FROM master_komponen_nilai WHERE id = '$id_komponen_nilai' ");
        // if ($queryxx->num_rows() > 0) {
        //     $hasilrowxx = $queryxx->row();
        //     $nama_komponen    = $hasilrowxx->nama;
        // } else
        //     $nama_komponen = '';

        // 28-12-2021 ALUR BARU:
        // query data dari KRS. di perulangan hasil query, ambil data nilai tiap2 komponen. jika blm ada maka kasih default 0
        $sql = " SELECT a1.id as id_mhs, a1.nim, a1.nama as nama_mhs, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik,
                    g.nama as nama_prodi, h.nama as nama_jenjang, i.kode as kode_mk, i.nama as nama_mk, j.nama as nama_kelas, b.id as id_krs_detail  
                    FROM krs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                    INNER JOIN krs_mhs_detail b ON b.id_krs_mhs = a.id
                    LEFT JOIN master_semester e ON e.id = a.id_semester
                    LEFT JOIN master_tahun_akademik f ON f.id = a.id_tahun_akademik
                    LEFT JOIN master_program_studi g ON g.id = a1.id_program_studi
                    LEFT JOIN master_jenjang_studi h ON h.id = g.id_jenjang_studi
                    LEFT JOIN master_mata_kuliah i ON i.id = b.id_mata_kuliah
                    LEFT JOIN master_kelas j ON j.id = a1.id_kelas
                    WHERE a.status_verifikasi = '1' AND a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                    AND a1.id_kelas = '$id_kelas' ORDER BY a1.nama ";

        $query = $this->db->query($sql);

        $data_mhs = array();
        if ($query->num_rows() > 0) {
            $hasil = $query->result();
            foreach ($hasil as $row) {
                $id_mhs = $row->id_mhs;
                $nim = $row->nim;
                $nama = $row->nama_mhs;
                $nama_semester = $row->nama_semester;
                $tahun = $row->tahun;
                $nama_tahun_akademik = $row->nama_tahun_akademik;
                $nama_prodi = $row->nama_prodi;
                $nama_jenjang = $row->nama_jenjang;
                $kode_mk = $row->kode_mk;
                $nama_mk = $row->nama_mk;
                $nama_kelas = $row->nama_kelas;
                $id_krs_detail = $row->id_krs_detail;

                // ambil data2 nilai berdasarkan komponen nilai
                $sqlxx = " SELECT id FROM master_komponen_nilai ORDER BY id ";
                $queryxx = $this->db->query($sqlxx);

                $data_nilai = array();
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->result();
                    foreach ($hasilxx as $rowxx) {
                        $sql2 = " SELECT a1.id, d.nilai_angka, d.id as id_nilai FROM master_komponen_nilai a1 LEFT JOIN khs_mhs_mk_detail d ON d.id_komponen_nilai = a1.id
                        LEFT JOIN khs_mhs_mk c ON c.id = d.id_khs_mk
                        LEFT JOIN khs_mhs a ON a.id = c.id_khs_mhs
                        LEFT JOIN mahasiswa b ON a.id_mhs = b.id
                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik' 
                        AND b.id_kelas = '$id_kelas'
                        AND c.id_mata_kuliah = '$id_mata_kuliah' AND b.id = '$id_mhs' AND a1.id = '$rowxx->id' ";

                        $query2 = $this->db->query($sql2);


                        if ($query2->num_rows() > 0) {
                            $hasil2 = $query2->result();
                            foreach ($hasil2 as $row2) {
                                $id_komponen_nilai = $row2->id;
                                $id_nilai = $row2->id_nilai;
                                $nilai_angka = $row2->nilai_angka;

                                $data_nilai[] = array(
                                    'id_komponen_nilai' => $id_komponen_nilai,
                                    'id_nilai' => $id_nilai,
                                    'nilai_angka' => $nilai_angka
                                );
                            }
                        } else {
                            $data_nilai[] = array(
                                'id_komponen_nilai' => $rowxx->id,
                                'id_nilai' => 0,
                                'nilai_angka' => 0
                            );
                        }
                    }
                }

                // ambil nilai akhir dan huruf mutu
                $sqlxx = " SELECT a.nilai_akhir, a.id_nilai_huruf, b.nama FROM khs_mhs_mk a LEFT JOIN master_nilai_huruf b ON a.id_nilai_huruf = b.id 
                          WHERE a.id_krs_mhs_detail = '$id_krs_detail' ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilrowxx = $queryxx->row();
                    $nilai_akhir    = $hasilrowxx->nilai_akhir;
                    $nilai_huruf    = $hasilrowxx->nama;
                } else {
                    $nilai_akhir = 0;
                    $nilai_huruf = '';
                }

                // header
                $data_mhs[] = array(
                    'id_mhs' => $id_mhs,
                    'nim' => $nim,
                    'nama' => $nama,
                    'nama_semester' => $nama_semester,
                    'tahun' => $tahun,
                    'nama_tahun_akademik' => $nama_tahun_akademik,
                    'nama_prodi' => $nama_prodi,
                    'nama_jenjang' => $nama_jenjang,
                    'kode_mk' => $kode_mk,
                    'nama_mk' => $nama_mk,
                    'nama_kelas' => $nama_kelas,
                    'id_krs_detail' => $id_krs_detail,
                    'nilai_akhir' => $nilai_akhir,
                    'nilai_huruf' => $nilai_huruf,
                    'data_nilai' => $data_nilai
                );

                //$data_nilai = array();
            }
        } else
            $data_mhs = '';

        return $data_mhs;

        // -------------------------- end alur baru ------------------------------------------------------------------------
    }


    // 23-10-2021
    // public function get_calon_mhs_by_id($id_calon_mhs)
    // {
    //     $this->db->select('a.*, b.nama as prodi, bb.nama as nama_jenjang, d.nama as agama, e.nama as jalur_pendaftaran, f.nama as provinsi, g.nama as kab_kota, 
    //     h.nama as goldar, j.nominal, j.tgl_bayar, i.nama_bank, i.no_rekening, i.atas_nama');
    //     $this->db->from('calon_mahasiswa a');
    //     $this->db->join('master_program_studi b', 'b.id = a.id_program_studi_pilihan', 'left');
    //     $this->db->join('master_jenjang_studi bb', 'b.id_jenjang_studi = bb.id', 'left');
    //     //$this->db->join('master_user c', 'c.id = a.id_user', 'left');
    //     $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
    //     $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
    //     $this->db->join('master_gelombang_calon_mhs e', 'e.id = a.id_gelombang_calon_mhs', 'left');
    //     $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
    //     $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
    //     $this->db->join('pembayaran_biaya_calon_mhs j', 'j.id_calon_mhs = a.id', 'left');
    //     $this->db->join('master_rekening i', 'i.id = j.id_rekening', 'left');
    //     $this->db->where("a.id", $id_calon_mhs);
    //     $query = $this->db->get();
    //     return $query->row();
    // }
}
