<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_komponen_nilai_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_komponen_nilai';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'kode',  //samakan dengan atribute name pada tags input
                'label' => 'Kode',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Nama komponen nilai',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'persentase',
                'label' => 'Persentase',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "persentase" => $this->input->post('persentase'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
