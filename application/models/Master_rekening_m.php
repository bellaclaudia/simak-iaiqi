<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_rekening_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_rekening';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'nama_bank',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Bank',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'no_rekening',
                'label' => 'No. Rekening',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'atas_nama',
                'label' => 'Atas Nama',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "nama_bank" => $this->input->post('nama_bank'),
            "no_rekening" => $this->input->post('no_rekening'),
            "atas_nama" => $this->input->post('atas_nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
