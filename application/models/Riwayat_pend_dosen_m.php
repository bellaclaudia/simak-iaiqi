<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_pend_dosen_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'riwayat_pendidikan_dosen';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_dosen',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Dosen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'pendidikan',
                'label' => 'Pendidikan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'gelar',
                'label' => 'Gelar',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'dari_tahun',
                'label' => 'Dari Tahun',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sampai_tahun',
                'label' => 'Sampai Tahun',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'asal_kampus',
                'label' => 'Asal Kampus',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.*, b.nama as nama_dosen');
        $this->db->from('riwayat_pendidikan_dosen a');
        $this->db->join('dosen b', 'b.id = a.id_dosen', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataDosen()
    {
        return $this->db->get('dosen')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_dosen" => $this->input->post('id_dosen'),
            "pendidikan" => $this->input->post('pendidikan'),
            "gelar" => $this->input->post('gelar'),
            "dari_tahun" => $this->input->post('dari_tahun'),
            "sampai_tahun" => $this->input->post('sampai_tahun'),
            "asal_kampus" => $this->input->post('asal_kampus'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            "id_dosen" => $this->input->post('id_dosen'),
            "pendidikan" => $this->input->post('pendidikan'),
            "gelar" => $this->input->post('gelar'),
            "dari_tahun" => $this->input->post('dari_tahun'),
            "sampai_tahun" => $this->input->post('sampai_tahun'),
            "asal_kampus" => $this->input->post('asal_kampus'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}
