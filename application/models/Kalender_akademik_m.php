<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kalender_akademik_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'kalender_akademik';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'tgl_awal',  //samakan dengan atribute name pada tags input
                'label' => 'Tanggal Awal',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tgl_akhir',  //samakan dengan atribute name pada tags input
                'label' => 'Tanggal Akhir',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kegiatan',  //samakan dengan atribute name pada tags input
                'label' => 'Kegiatan',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'deskripsi',
                'label' => 'Deskripsi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "tgl_awal" => $this->input->post('tgl_awal'),
            "tgl_akhir" => $this->input->post('tgl_akhir'),
            "kegiatan" => $this->input->post('kegiatan'),
            "deskripsi" => $this->input->post('deskripsi'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            "tgl_awal" => $this->input->post('tgl_awal'),
            "tgl_akhir" => $this->input->post('tgl_akhir'),
            "kegiatan" => $this->input->post('kegiatan'),
            "deskripsi" => $this->input->post('deskripsi'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }
}