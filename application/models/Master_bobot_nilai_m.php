<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_bobot_nilai_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_bobot_nilai';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_nilai_huruf',  //samakan dengan atribute name pada tags input
                'label' => 'ID Nilai Huruf',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'bobot',  //samakan dengan atribute name pada tags input
                'label' => 'Bobot',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.id, a.bobot, b.id as id_nilai_huruf, b.nama as nilai_huruf');
        $this->db->from('master_bobot_nilai a');
        $this->db->join('master_nilai_huruf b', 'b.id = a.id_nilai_huruf', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_nilai_huruf()
    {
        return $this->db->get('master_nilai_huruf')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_nilai_huruf" => $this->input->post('id_nilai_huruf'),
            "bobot" => $this->input->post('bobot'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
