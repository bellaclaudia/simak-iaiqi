<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semester_pendek_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //private $table = 'calon_mahasiswa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_ta',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_semester',
                'label' => 'Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_prodi',
                'label' => 'Program Studi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data sp (login admin)
    public function getAll($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        e.nama as nama_prodi, f.nama as nama_jenjang, g.nama as nama_dosen ');
        $this->db->from('sp_mhs a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->join('dosen g', 'g.id = a.id_dosen_verifikator', 'left');

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_prodi != '0')
            $this->db->where("d.id_program_studi", $id_prodi);

        $this->db->order_by("c.tahun", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function get_mhs_by_prodi_verifiedkeu($id_prodi, $id_ta, $id_sem)
    {
        $sqlxx = " SELECT id, nim, nama FROM mahasiswa WHERE id_program_studi = '$id_prodi' 
                AND id in (select id_mhs FROM pembayaran_biaya_kuliah WHERE id_semester = '$id_sem' 
                AND id_tahun_akademik = '$id_ta' AND jenis_biaya = '2' AND status_verifikasi = '1') 
                AND id not in (select a.id_mhs FROM sp_mhs a INNER JOIN sp_mhs_detail b ON a.id = b.id_sp_mhs 
                WHERE a.id_semester = '$id_sem' AND a.id_tahun_akademik = '$id_ta')
                ORDER BY nama asc ";
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    public function get_sp_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks, c.hari, c.jam_mulai, c.jam_selesai 
        // FROM krs_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        // INNER JOIN jadwal_kuliah c ON c.id = a.id_jadwal_kuliah
        // WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        // ORDER BY a.id asc ";

        $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks 
        FROM sp_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        ORDER BY a.id asc ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    public function get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // modif 17-11-2021 ga usah pake inner join ke jadwal kuliah
        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select id_mata_kuliah FROM sp_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
        AND id_semester = '$id_semester' AND id_program_studi = '$id_prodi')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // detail sp by id_sp
    public function get_detail_sp($id_sp)
    {
        $this->db->select('a.id_semester, a.id_tahun_akademik, a.id_mhs, b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, e.id_program_studi,
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah ');
        $this->db->from('sp_mhs a');
        $this->db->join('sp_mhs_detail b', 'b.id_sp_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->where("a.id", $id_sp);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }

    // 14-11-2021 utk form edit
    public function get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // 09-11-2021 modif tambahin relasi ke jadwal kuliah
        // modif 18-11-2021 ga pake relasi ke jadwal kuliah
        // $sqlxx = " SELECT a.id, a.kode, a.nama, b.id as id_jadwal, b.hari, b.jam_mulai, b.jam_selesai FROM master_mata_kuliah a INNER JOIN jadwal_kuliah b ON a.id = b.id_mata_kuliah 
        // WHERE b.id_semester = '$id_semester' AND b.id_tahun_akademik = '$id_ta' AND b.id_program_studi = '$id_prodi' 
        // AND a.id NOT IN (select b.id_mata_kuliah FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs
        // WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        // AND a.id_semester = '$id_semester')
        // ORDER BY a.kode ";

        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select b.id_mata_kuliah FROM sp_mhs a INNER JOIN sp_mhs_detail b ON a.id = b.id_sp_mhs
        WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        AND a.id_semester = '$id_semester')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }


    // 23-10-2021
    // public function get_calon_mhs_by_id($id_calon_mhs)
    // {
    //     $this->db->select('a.*, b.nama as prodi, bb.nama as nama_jenjang, d.nama as agama, e.nama as jalur_pendaftaran, f.nama as provinsi, g.nama as kab_kota, 
    //     h.nama as goldar, j.nominal, j.tgl_bayar, i.nama_bank, i.no_rekening, i.atas_nama');
    //     $this->db->from('calon_mahasiswa a');
    //     $this->db->join('master_program_studi b', 'b.id = a.id_program_studi_pilihan', 'left');
    //     $this->db->join('master_jenjang_studi bb', 'b.id_jenjang_studi = bb.id', 'left');
    //     //$this->db->join('master_user c', 'c.id = a.id_user', 'left');
    //     $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
    //     $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
    //     $this->db->join('master_gelombang_calon_mhs e', 'e.id = a.id_gelombang_calon_mhs', 'left');
    //     $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
    //     $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
    //     $this->db->join('pembayaran_biaya_calon_mhs j', 'j.id_calon_mhs = a.id', 'left');
    //     $this->db->join('master_rekening i', 'i.id = j.id_rekening', 'left');
    //     $this->db->where("a.id", $id_calon_mhs);
    //     $query = $this->db->get();
    //     return $query->row();
    // }
}
