<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transkrip_nilai_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //menampilkan semua data 
    public function getAll($id_tahun_akademik,$id_mhs)
    {
        $this->db->select('c.kode as kode_matkul, c.nama as matkul, c.sks, d.nama as nilai, h.bobot, f.*, g.nama as prodi');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_mata_kuliah c', 'c.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf d', 'd.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai h', 'd.id = h.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa f', 'f.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi g', 'g.id = f.id_program_studi', 'left');
        $this->db->order_by("a.id", "desc");

        if ($id_tahun_akademik != '0')
            $this->db->where("a.id_tahun_akademik", $id_tahun_akademik);
         if ($id_mhs != '0')
            $this->db->where("a.id_mhs", $id_mhs);
        $query = $this->db->get();
        return $query->result();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getPredikatKelulusan()
    {
        return $this->db->get('master_predikat_kelulusan')->result_array();
    }

    public function getMahasiswa()
    {
        return $this->db->get('mahasiswa')->result_array();
    }

    public function getPerMhs($id_tahun_akademik,$id)
    {
        $this->db->select('c.kode as kode_matkul, c.nama as matkul, c.sks, d.nama as nilai, h.bobot, f.*, g.nama as prodi');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_mata_kuliah c', 'c.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf d', 'd.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai h', 'd.id = h.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa f', 'f.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi g', 'g.id = f.id_program_studi', 'left');
        $this->db->order_by("a.id", "desc");
        $this->db->where('f.id', $id);

        if ($id_tahun_akademik != '0')
            $this->db->where("a.id_tahun_akademik", $id_tahun_akademik);
        $query = $this->db->get();
        return $query->result();
    }

}
