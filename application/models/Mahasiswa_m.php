<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'mahasiswa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_program_studi',  //samakan dengan atribute name pada tags input
                'label' => 'Program Studi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kewarganegaraan',
                'label' => 'Kewarganegaraan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis_mhs',  //samakan dengan atribute name pada tags input
                'label' => 'Jenis Mhs',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis_kelamin',
                'label' => 'Jenis Kelamin',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_agama',
                'label' => 'Agama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_golongan_darah',
                'label' => 'Golongan Darah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'mulai_semester',
                'label' => 'Mulai Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kelurahan',
                'label' => 'Kelurahan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kecamatan',
                'label' => 'Kecamatan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ayah',
                'label' => 'Nama Ayah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ibu',
                'label' => 'Nama Ibu',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jumlah_biaya_masuk',
                'label' => 'Jumlah Biaya Masuk',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kps',
                'label' => 'KPS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_lahir',
                'label' => 'Tanggal Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_masuk_kuliah',
                'label' => 'Tanggal Masuk Kuliah',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_angkatan, $id_prodi, $id_kelas)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, e.nama as jalur_pendaftaran, f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, i.nama as fakultas, j.nama as kelas, k.nama as nama_angkatan, c.nama as status_mhs, l.nama as jenis_pembiayaan, b.kode as kode_prodi');
        $this->db->from('mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_mhs c', 'c.id = a.id_status_mhs', 'left');
        $this->db->join('master_angkatan_mhs k', 'k.id = a.id_angkatan', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_jalur_pendaftaran e', 'e.id = a.id_jalur_pendaftaran', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');
        $this->db->join('master_kelas j', 'j.id = a.id_kelas', 'left');
        $this->db->join('master_jenis_pembiayaan l', 'l.id = a.id_jenis_pembiayaan', 'left');

        if ($id_angkatan != '0')
            $this->db->where("a.id_angkatan", $id_angkatan);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);
        if ($id_kelas != '0')
            $this->db->where("a.id_kelas", $id_kelas);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStatusMhs()
    {
        return $this->db->get('master_status_mhs')->result_array();
    }

    public function getAgama()
    {
        return $this->db->get('master_agama')->result_array();
    }

    public function getGoldar()
    {
        return $this->db->get('master_golongan_darah')->result_array();
    }

    public function getJalurPendaftaran()
    {
        return $this->db->get('master_jalur_pendaftaran')->result_array();
    }

    public function getProvinsi()
    {
        return $this->db->get('master_provinsi')->result_array();
    }

    public function getKabKota()
    {
        return $this->db->get('master_kabupaten_kota')->result_array();
    }

    public function getKelas()
    {
        return $this->db->get('master_kelas')->result_array();
    }

    public function get_jenis_pembiayaan()
    {
        return $this->db->get('master_jenis_pembiayaan')->result_array();
    }

    public function get_angkatan()
    {
        return $this->db->get('master_angkatan_mhs')->result_array();
    }

    public function save()
    {
        $this->db->trans_begin();

        // 15-12-2021 generate nim
        $id_program_studi = $this->input->post('id_program_studi');
        $sqlxx = " SELECT nim FROM mahasiswa WHERE id_program_studi = '$id_program_studi' ORDER BY nim DESC LIMIT 1 ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            //$nim_urut = substr($hasilxx->nim,-3);
            $nim_urut = substr($hasilxx->nim, 6, 3);
            $nim_urut++;
            if ($nim_urut < 100 && $nim_urut > 9)
                $nim_urut = '0' . $nim_urut;
            else if ($nim_urut < 100 && $nim_urut < 10)
                $nim_urut = '00' . $nim_urut;
        } else {
            $nim_urut = 1;
            $nim_urut = '00' . $nim_urut;
        }

        $sqlxx = " select a.kode as kode_fakultas, b.kode as kode_prodi FROM master_fakultas a 
                        INNER JOIN master_program_studi b ON a.id = b.id_fakultas
                        WHERE b.id = '$id_program_studi' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $kode_fakultas    = $hasilxx->kode_fakultas;
            $kode_prodi    = $hasilxx->kode_prodi;
        }

        // set nim
        $mulai_semester = $this->input->post('mulai_semester');
        $tahun = substr($mulai_semester, 2, 2);
        $nim = $tahun . $kode_fakultas . $kode_prodi . $nim_urut;

        $tgl_skrg = date('Y-m-d H:i:s');
        $data = array(
            'id_grup_user' => '4',
            'username' => $nim,
            'userpass' => md5($nim),
            'tgl_input' => $tgl_skrg,
            'user_update_by' => $this->session->userdata['username']
        );

        $this->db->insert('master_user', $data);

        $id_usernya = $this->db->insert_id();

        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        //$config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'MHSfoto_' . $id_usernya . '_' . $timestamp;

        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                $file_foto = $data['file_foto'];
            } else {
                $error = 1;
            }
        } else
            $file_foto = '';

        $data = array(
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_user" => $id_usernya,
            "file_foto" => $file_foto,
            "npwp" => $this->input->post('npwp'),
            "nim" => $nim,
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "nisn" => $this->input->post('nisn'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "id_jalur_pendaftaran" => $this->input->post('id_jalur_pendaftaran'),
            "tgl_masuk_kuliah" => $this->input->post('tgl_masuk_kuliah'),
            "mulai_semester" => $this->input->post('mulai_semester'),
            "kelurahan" => $this->input->post('kelurahan'),
            "kecamatan" => $this->input->post('kecamatan'),
            "kode_pos" => $this->input->post('kode_pos'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "no_telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "nama_ayah" => $this->input->post('nama_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "id_jenis_pembiayaan" => $this->input->post('id_jenis_pembiayaan'),
            "jumlah_biaya_masuk" => $this->input->post('jumlah_biaya_masuk'),
            "id_status_mhs" => $this->input->post('id_status_mhs'),
            "id_kelas" => $this->input->post('id_kelas'),
            "id_angkatan" => $this->input->post('id_angkatan'),
            "jenis_mhs" => $this->input->post('jenis_mhs'),
            "kewarganegaraan" => $this->input->post('kewarganegaraan'),
            "rt" => $this->input->post('rt'),
            "rw" => $this->input->post('rw'),
            "dusun" => $this->input->post('dusun'),
            "jenis_tinggal" => $this->input->post('jenis_tinggal'),
            "telp_rmh" => $this->input->post('telp_rmh'),
            "kps" => $this->input->post('kps'),
            "no_kps" => $this->input->post('no_kps'),
            "transportasi" => $this->input->post('transportasi'),
            "nik_ayah" => $this->input->post('nik_ayah'),
            "tgl_lahir_ayah" => $this->input->post('tgl_lahir_ayah'),
            "pend_ayah" => $this->input->post('pend_ayah'),
            "pek_ayah" => $this->input->post('pek_ayah'),
            "penghasilan_ayah" => $this->input->post('penghasilan_ayah'),
            "nik_ibu" => $this->input->post('nik_ibu'),
            "tgl_lahir_ibu" => $this->input->post('tgl_lahir_ibu'),
            "pend_ibu" => $this->input->post('pend_ibu'),
            "pek_ibu" => $this->input->post('pek_ibu'),
            "penghasilan_ibu" => $this->input->post('penghasilan_ibu'),
            "nama_wali" => $this->input->post('nama_wali'),
            "tgl_lahir_wali" => $this->input->post('tgl_lahir_wali'),
            "pend_wali" => $this->input->post('pend_wali'),
            "pek_wali" => $this->input->post('pek_wali'),
            "penghasilan_wali" => $this->input->post('penghasilan_wali'),
            "sks_diakui" => $this->input->post('sks_diakui'),
            "program_studi_asal" => $this->input->post('program_studi_asal'),
            "perguruan_tinggi_asal" => $this->input->post('perguruan_tinggi_asal'),
            "tgl_lulus" => $this->input->post('tgl_lulus'),
            "nomor_ijazah" => $this->input->post('nomor_ijazah'),
            "nomor_transkrip" => $this->input->post('nomor_transkrip'),
            "judul_skripsi" => $this->input->post('judul_skripsi'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return true;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function getView($id)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, e.nama as jalur_pendaftaran, f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, 
                        i.nama as fakultas, c.nama as status_mhs, j.nama as kelas, k.nama as jenis_pembiayaan, kk.nama as nama_angkatan');
        $this->db->from('mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_mhs c', 'c.id = a.id_status_mhs', 'left');
        $this->db->join('master_angkatan_mhs kk', 'kk.id = a.id_angkatan', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_jalur_pendaftaran e', 'e.id = a.id_jalur_pendaftaran', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');
        $this->db->join('master_kelas j', 'j.id = a.id_kelas', 'left');
        $this->db->join('master_jenis_pembiayaan k', 'k.id = a.id_jenis_pembiayaan', 'left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function update()
    {
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        //$config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');

        // ambil id user
        $sqlxx = " select id_user FROM mahasiswa WHERE id = '" . $this->input->post('id') . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_usernya    = $hasilxx->id_user;

            // 31-10-2021 update username pake nim yg ada. sementara ditutup dulu. nim ga boleh sembarangan diedit sendiri
            // $tgl_skrg = date('Y-m-d H:i:s');
            // $data = array(
            //     "username" => $this->input->post('nim'),
            //     "userpass" => $this->input->post('nim'),
            //     "tgl_update" => $tgl_skrg,
            //     "user_update_by" => $this->session->userdata['username']
            // );
            // $this->db->where('id', $id_usernya);
            // $this->db->update('master_user', $data);
        }

        $config['file_name'] = 'MHSfoto_' . $id_usernya . '_' . $timestamp;

        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                $file_foto = $data['file_foto'];

                //=========== hapus file lama
                $this->db->select('file_foto');
                $this->db->where('id', $this->input->post('id'));
                $q = $this->db->get('mahasiswa')->row_array();
                if (isset($q)) {
                    if (!empty($q['file_foto'])) {
                        unlink($dir . '/' . $q['file_foto']);
                    }
                }
            } else {
                // echo $this->upload->display_errors();
                $error = 1;
            }
        } else {
            $this->db->select('file_foto');
            $this->db->where('id', $this->input->post('id'));
            $q = $this->db->get('mahasiswa')->row_array();
            // if (isset($q)) {
            //     if (!empty($q['file_foto'])) {
            //         unlink($dir . '/' . $q['file_foto']);
            //     }
            // }
            $file_foto = $q['file_foto'];
        }

        $data = array(
            "id_program_studi" => $this->input->post('id_program_studi'),
            "file_foto" => $file_foto,
            "npwp" => $this->input->post('npwp'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "nisn" => $this->input->post('nisn'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "id_jalur_pendaftaran" => $this->input->post('id_jalur_pendaftaran'),
            "tgl_masuk_kuliah" => $this->input->post('tgl_masuk_kuliah'),
            "mulai_semester" => $this->input->post('mulai_semester'),
            "kelurahan" => $this->input->post('kelurahan'),
            "kecamatan" => $this->input->post('kecamatan'),
            "kode_pos" => $this->input->post('kode_pos'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "no_telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "nama_ayah" => $this->input->post('nama_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "id_jenis_pembiayaan" => $this->input->post('id_jenis_pembiayaan'),
            "jumlah_biaya_masuk" => $this->input->post('jumlah_biaya_masuk'),
            "id_status_mhs" => $this->input->post('id_status_mhs'),
            "id_kelas" => $this->input->post('id_kelas'),
            "id_angkatan" => $this->input->post('id_angkatan'),
            "jenis_mhs" => $this->input->post('jenis_mhs'),
            "kewarganegaraan" => $this->input->post('kewarganegaraan'),
            "rt" => $this->input->post('rt'),
            "rw" => $this->input->post('rw'),
            "dusun" => $this->input->post('dusun'),
            "jenis_tinggal" => $this->input->post('jenis_tinggal'),
            "telp_rmh" => $this->input->post('telp_rmh'),
            "kps" => $this->input->post('kps'),
            "no_kps" => $this->input->post('no_kps'),
            "transportasi" => $this->input->post('transportasi'),
            "nik_ayah" => $this->input->post('nik_ayah'),
            "tgl_lahir_ayah" => $this->input->post('tgl_lahir_ayah'),
            "pend_ayah" => $this->input->post('pend_ayah'),
            "pek_ayah" => $this->input->post('pek_ayah'),
            "penghasilan_ayah" => $this->input->post('penghasilan_ayah'),
            "nik_ibu" => $this->input->post('nik_ibu'),
            "tgl_lahir_ibu" => $this->input->post('tgl_lahir_ibu'),
            "pend_ibu" => $this->input->post('pend_ibu'),
            "pek_ibu" => $this->input->post('pek_ibu'),
            "penghasilan_ibu" => $this->input->post('penghasilan_ibu'),
            "nama_wali" => $this->input->post('nama_wali'),
            "tgl_lahir_wali" => $this->input->post('tgl_lahir_wali'),
            "pend_wali" => $this->input->post('pend_wali'),
            "pek_wali" => $this->input->post('pek_wali'),
            "penghasilan_wali" => $this->input->post('penghasilan_wali'),
            "sks_diakui" => $this->input->post('sks_diakui'),
            "program_studi_asal" => $this->input->post('program_studi_asal'),
            "perguruan_tinggi_asal" => $this->input->post('perguruan_tinggi_asal'),
            "tgl_lulus" => $this->input->post('tgl_lulus'),
            "nomor_ijazah" => $this->input->post('nomor_ijazah'),
            "nomor_transkrip" => $this->input->post('nomor_transkrip'),
            "judul_skripsi" => $this->input->post('judul_skripsi'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );

        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    // 06-11-2021
    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function get_data_pembayaran_biaya_reguler($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nama_bank, d.no_rekening, d.atas_nama,
                        e.nim, e.nama as nama_mhs ');
        $this->db->from('pembayaran_biaya_kuliah a');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->join('master_rekening d', 'd.id = a.id_rekening', 'left');
        $this->db->where("a.id_mhs", $id_mhs);
        $this->db->where("a.jenis_biaya", '1');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_pembayaran_biaya_sp($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nama_bank, d.no_rekening, d.atas_nama,
                        e.nim, e.nama as nama_mhs ');
        $this->db->from('pembayaran_biaya_kuliah a');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->join('master_rekening d', 'd.id = a.id_rekening', 'left');
        $this->db->where("a.id_mhs", $id_mhs);
        $this->db->where("a.jenis_biaya", '2');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    // 12-11-2021 status mhs
    public function get_data_status_mhs($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, 
                        e.nim, e.nama as nama_mhs, f.nama as nama_status ');
        $this->db->from('riwayat_status_mhs a');
        $this->db->join('master_status_mhs f', 'f.id = a.id_status_mhs', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->where("a.id_mhs", $id_mhs);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_dosen_pa($id_mhs)
    {
        $this->db->select('a.*, f.nama as nama_kelas, g.nidn, g.nama as nama_dosen, 
                        e.nim, e.nama as nama_mhs, f.nama as nama_status ');
        $this->db->from('dosen_pembimbing_akademik_mhs a');
        $this->db->join('master_kelas f', 'f.id = a.id_kelas', 'left');
        $this->db->join('mahasiswa e', 'e.id_kelas = f.id', 'left');
        $this->db->join('dosen g', 'g.id = a.id_dosen', 'left');
        // $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        // $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->where("e.id", $id_mhs);

        // if ($id_semester != '0')
        //     $this->db->where("a.id_semester", $id_semester);
        // if ($id_ta != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    // get data khs mhs
    public function get_data_khs_mhs($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('b.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, g.nama as nilai_huruf, d.nama as nama_mk ');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_nilai_huruf g', 'g.id = b.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_mata_kuliah d', 'd.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->where("a.id_mhs", $id_mhs);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // get data KRS mhs
    public function get_data_krs_mhs($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        e.nama as nama_prodi, f.nama as nama_jenjang, g.nama as nama_dosen ');
        $this->db->from('krs_mhs a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->join('dosen g', 'g.id = a.id_dosen_verifikator', 'left');
        $this->db->where("d.id", $id_mhs);

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);

        $this->db->order_by("c.tahun", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // detail krs by id_krs
    public function get_detail_krs($id_krs)
    {
        //$this->db->distinct();
        $this->db->select('a.id_semester, a.id_tahun_akademik, a.id_mhs, b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, e.id_program_studi,
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah, i.nama as nama_fakultas ');
        $this->db->from('krs_mhs a');
        $this->db->join('krs_mhs_detail b', 'b.id_krs_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->join('master_fakultas i', 'i.id = f.id_fakultas', 'left');
        //$this->db->join('dosen_pembimbing_akademik_mhs j', 'j.id_kelas = e.id_kelas', 'left');
        //$this->db->join('dosen k', 'k.id = j.id_dosen', 'left');
        $this->db->where("a.id", $id_krs);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        //print_r($this->db->last_query());

        return $query;
        // , k.nama as nama_dosen
        //return $query->result();
    }

    public function get_krs_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks, c.hari, c.jam_mulai, c.jam_selesai 
        // FROM krs_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        // INNER JOIN jadwal_kuliah c ON c.id = a.id_jadwal_kuliah
        // WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        // ORDER BY a.id asc ";
        // $queryxx = $this->db->query($sqlxx);

        $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks
        FROM krs_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        ORDER BY a.id asc ";
        $queryxx = $this->db->query($sqlxx);

        return $queryxx->result();
    }

    public function get_mk_by_prodi_semester($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // 09-11-2021 modif tambahin relasi ke jadwal kuliah
        // 18-11-2021 modif ga pake relasi ke jadwal
        // $sqlxx = " SELECT a.id, a.kode, a.nama, b.id as id_jadwal, b.hari, b.jam_mulai, b.jam_selesai FROM master_mata_kuliah a 
        // INNER JOIN jadwal_kuliah b ON a.id = b.id_mata_kuliah 
        // WHERE b.id_semester = '$id_semester' AND b.id_tahun_akademik = '$id_ta' AND b.id_program_studi = '$id_prodi' 
        // AND a.id NOT IN (select id_mata_kuliah FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
        // AND id_semester = '$id_semester' AND id_program_studi = '$id_prodi')
        // ORDER BY a.kode ";

        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a 
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select id_mata_kuliah FROM krs_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
        AND id_semester = '$id_semester' AND id_program_studi = '$id_prodi')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 14-11-2021 utk form edit
    public function get_mk_by_prodi_semester_formedit($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // 09-11-2021 modif tambahin relasi ke jadwal kuliah
        // 18-11-2021 modif ga pake relasi ke jadwal
        // $sqlxx = " SELECT a.id, a.kode, a.nama, b.id as id_jadwal, b.hari, b.jam_mulai, b.jam_selesai FROM master_mata_kuliah a INNER JOIN jadwal_kuliah b ON a.id = b.id_mata_kuliah 
        // WHERE b.id_semester = '$id_semester' AND b.id_tahun_akademik = '$id_ta' AND b.id_program_studi = '$id_prodi' 
        // AND a.id NOT IN (select b.id_mata_kuliah FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs
        // WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        // AND a.id_semester = '$id_semester')
        // ORDER BY a.kode ";

        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select b.id_mata_kuliah FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs
        WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        AND a.id_semester = '$id_semester')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 20-11-2021 get data SP
    public function get_data_sp_mhs($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        e.nama as nama_prodi, f.nama as nama_jenjang, g.nama as nama_dosen ');
        $this->db->from('sp_mhs a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->join('dosen g', 'g.id = a.id_dosen_verifikator', 'left');
        $this->db->where("d.id", $id_mhs);

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);

        $this->db->order_by("c.tahun", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // 20-11-2021
    // detail SP by id_sp
    public function get_detail_sp($id_sp)
    {
        $this->db->select('a.id_semester, a.id_tahun_akademik, a.id_mhs, b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, e.id_program_studi,
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah ');
        $this->db->from('sp_mhs a');
        $this->db->join('sp_mhs_detail b', 'b.id_sp_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->where("a.id", $id_sp);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }

    public function get_sp_temp_mk($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        // $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks, c.hari, c.jam_mulai, c.jam_selesai 
        // FROM krs_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        // INNER JOIN jadwal_kuliah c ON c.id = a.id_jadwal_kuliah
        // WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        // ORDER BY a.id asc ";
        // $queryxx = $this->db->query($sqlxx);

        $sqlxx = " SELECT a.id, a.id_mata_kuliah, b.kode, b.nama, a.sks
        FROM sp_mhs_temp_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id 
        WHERE a.id_tahun_akademik = '$id_ta' AND a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' AND a.id_mhs = '$id_mhs' 
        ORDER BY a.id asc ";
        $queryxx = $this->db->query($sqlxx);

        return $queryxx->result();
    }

    public function get_mk_by_prodi_semester_sp($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a 
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select id_mata_kuliah FROM sp_mhs_temp_mk WHERE id_mhs = '$id_mhs' AND id_tahun_akademik = '$id_ta' 
        AND id_semester = '$id_semester' AND id_program_studi = '$id_prodi')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    public function get_mk_by_prodi_semester_sp_formedit($id_ta, $id_semester, $id_prodi, $id_mhs)
    {
        $sqlxx = " SELECT a.id, a.kode, a.nama FROM master_mata_kuliah a  
        WHERE a.id_semester = '$id_semester' AND a.id_program_studi = '$id_prodi' 
        AND a.id NOT IN (select b.id_mata_kuliah FROM sp_mhs a INNER JOIN sp_mhs_detail b ON a.id = b.id_sp_mhs
        WHERE a.id_mhs = '$id_mhs' AND a.id_tahun_akademik = '$id_ta' 
        AND a.id_semester = '$id_semester')
        ORDER BY a.kode ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // jadwal kuliah mhs
    public function get_data_mk($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        d.kode, d.nama as nama_mata_kuliah, d.sks, g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_kelas ');
        $this->db->from('jadwal_kuliah a');
        $this->db->join('krs_mhs_detail b', 'b.id_mata_kuliah = a.id_mata_kuliah', 'inner');
        $this->db->join('krs_mhs c', 'b.id_krs_mhs = c.id', 'inner');
        $this->db->join('mahasiswa d1', 'd1.id = c.id_mhs AND d1.id_kelas = a.id_kelas', 'inner');
        $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'inner');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->join('master_kelas i', 'i.id = a.id_kelas', 'left');
        $this->db->where("c.id_mhs", $id_mhs);
        // -------------------------------------------------

        // $this->db->select('a.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
        //                 c.nidn, c.nama as nama_dosen, d.kode, d.nama as nama_mata_kuliah, d.sks, e.nama as nama_prodi, f.nama as nama_jenjang ');
        // $this->db->from('jadwal_kuliah a');
        // $this->db->join('dosen_pengajar_kuliah b', 'b.id_jadwal_kuliah = a.id', 'inner');
        // $this->db->join('dosen c', 'c.id = b.id_dosen', 'inner');
        // $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'left');
        // $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        // $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        // // $this->db->join('master_komponen_nilai d', 'a.id_komponen_nilai = d.id', 'left');
        // $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        // $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        // $this->db->where("b.id_dosen", $id_mhs);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // 26-11-2021
    public function get_komponen_nilai()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_komponen_nilai')->result_array();
    }
}
