<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getKalenderAkademik()
    {
        $this->db->select('a.*');
        $this->db->from('kalender_akademik a');
        $this->db->order_by("tgl_input", "desc");
        $this->db->order_by("tgl_update", "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    public function getPengumumanAkademik()
    {
        $this->db->select('a.*');
        $this->db->from('pengumuman_akademik a');
        $this->db->order_by("tgl_input", "desc");
        $this->db->order_by("tgl_update", "desc");
        $query = $this->db->get();
        return $query->result();
    }
}