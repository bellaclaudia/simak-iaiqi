<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_tahun_akademik_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_tahun_akademik';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'nama',  //samakan dengan atribute name pada tags input
                'label' => 'Periode Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tahun',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'jenis_semester',
                'label' => 'Jenis Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_semester',
                'label' => 'Tanggal Mulai Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_semester',
                'label' => 'Tanggal Selesai Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_krs',
                'label' => 'Tanggal Mulai KRS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_krs',
                'label' => 'Tanggal Selesai KRS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_kuliah',
                'label' => 'Tanggal Mulai Kuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_kuliah',
                'label' => 'Tanggal Selesai Kuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_uts',
                'label' => 'Tanggal Mulai UTS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_uts',
                'label' => 'Tanggal Selesai UTS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_uas',
                'label' => 'Tanggal Mulai UAS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_uas',
                'label' => 'Tanggal Selesai UAS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_mulai_isi_nilai',
                'label' => 'Tanggal Mulai Isi Nilai',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_selesai_isi_nilai',
                'label' => 'Tanggal Selesai Isi Nilai',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "tahun" => $this->input->post('tahun'),
            "jenis_semester" => $this->input->post('jenis_semester'),
            "tgl_mulai_semester" => $this->input->post('tgl_mulai_semester'),
            "tgl_selesai_semester" => $this->input->post('tgl_selesai_semester'),
            "tgl_mulai_krs" => $this->input->post('tgl_mulai_krs'),
            "tgl_selesai_krs" => $this->input->post('tgl_selesai_krs'),
            "tgl_mulai_kuliah" => $this->input->post('tgl_mulai_kuliah'),
            "tgl_selesai_kuliah" => $this->input->post('tgl_selesai_kuliah'),
            "tgl_mulai_uts" => $this->input->post('tgl_mulai_uts'),
            "tgl_selesai_uts" => $this->input->post('tgl_selesai_uts'),
            "tgl_mulai_uas" => $this->input->post('tgl_mulai_uas'),
            "tgl_selesai_uas" => $this->input->post('tgl_selesai_uas'),
            "tgl_mulai_isi_nilai" => $this->input->post('tgl_mulai_isi_nilai'),
            "tgl_selesai_isi_nilai" => $this->input->post('tgl_selesai_isi_nilai'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "tahun" => $this->input->post('tahun'),
            "jenis_semester" => $this->input->post('jenis_semester'),
            "tgl_mulai_semester" => $this->input->post('tgl_mulai_semester'),
            "tgl_selesai_semester" => $this->input->post('tgl_selesai_semester'),
            "tgl_mulai_krs" => $this->input->post('tgl_mulai_krs'),
            "tgl_selesai_krs" => $this->input->post('tgl_selesai_krs'),
            "tgl_mulai_kuliah" => $this->input->post('tgl_mulai_kuliah'),
            "tgl_selesai_kuliah" => $this->input->post('tgl_selesai_kuliah'),
            "tgl_mulai_uts" => $this->input->post('tgl_mulai_uts'),
            "tgl_selesai_uts" => $this->input->post('tgl_selesai_uts'),
            "tgl_mulai_uas" => $this->input->post('tgl_mulai_uas'),
            "tgl_selesai_uas" => $this->input->post('tgl_selesai_uas'),
            "tgl_mulai_isi_nilai" => $this->input->post('tgl_mulai_isi_nilai'),
            "tgl_selesai_isi_nilai" => $this->input->post('tgl_selesai_isi_nilai'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}
