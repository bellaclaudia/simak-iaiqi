<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_level_nilai_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_level_nilai';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_nilai_huruf',  //samakan dengan atribute name pada tags input
                'label' => 'ID Nilai Huruf',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nilai_batas_awal',  //samakan dengan atribute name pada tags input
                'label' => 'Nilai batas awal',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nilai_batas_akhir',
                'label' => 'Nilai batas akhir',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.id, a.nilai_batas_awal, a.nilai_batas_akhir, b.id as id_nilai_huruf, b.nama as nilai_huruf');
        $this->db->from('master_level_nilai a');
        $this->db->join('master_nilai_huruf b', 'b.id = a.id_nilai_huruf', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_nilai_huruf()
    {
        return $this->db->get('master_nilai_huruf')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_nilai_huruf" => $this->input->post('id_nilai_huruf'),
            "nilai_batas_awal" => $this->input->post('nilai_batas_awal'),
            "nilai_batas_akhir" => $this->input->post('nilai_batas_akhir'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
