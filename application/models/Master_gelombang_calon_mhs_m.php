<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_gelombang_calon_mhs_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_gelombang_calon_mhs';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_tahun_akademik',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Gelombang',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_awal',
                'label' => 'Tanggal Awal',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_akhir',
                'label' => 'Tanggal Akhir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'lokasi',
                'label' => 'Lokasi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.id, a.nama, a.tgl_awal, a.tgl_akhir, a.lokasi, b.nama as tahun_akademik');
        $this->db->from('master_gelombang_calon_mhs a');
        $this->db->join('master_tahun_akademik b', 'b.id = a.id_tahun_akademik', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_tahun_akademik" => $_POST['id_tahun_akademik'],
            "nama" => $_POST['nama'],
            "tgl_awal" => $_POST['tgl_awal'],
            "tgl_akhir" => $_POST['tgl_akhir'],
            "lokasi" => $_POST['lokasi'],
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
