<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pa_mhs_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'dosen_pembimbing_akademik_mhs';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_dosen',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Dosen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_kelas',  //samakan dengan atribute name pada tags input
                'label' => 'Kelas',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*, b.id as id_dosen, b.nama as nama_dosen, c.id as id_kelas, c.nama as kelas');
        $this->db->from('dosen_pembimbing_akademik_mhs a');
        $this->db->join('dosen b', 'b.id = a.id_dosen', 'left');
        $this->db->join('master_kelas c', 'c.id = a.id_kelas', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getDosen()
    {
        return $this->db->get('dosen')->result_array();
    }

    public function getKelas()
    {
        $this->db->select('a.*, b.nama as semester, c.nama as tahun_akademik, d.nama as program_studi, e.nama as nama_jenjang');
        $this->db->from('master_kelas a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('master_program_studi d', 'd.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi e', 'e.id = d.id_jenjang_studi', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_dosen" => $this->input->post('id_dosen'),
            "id_kelas" => $this->input->post('id_kelas'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
