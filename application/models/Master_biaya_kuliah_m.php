<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_biaya_kuliah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_biaya_kuliah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_tahun_akademik',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kode',
                'label' => 'Kode',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Deskripsi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis',
                'label' => 'Jenis',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nominal',
                'label' => 'Nominal',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_ta, $id_prodi)
    {
        $this->db->select('a.*, b.nama as tahun_akademik, 
                            e.nama as nama_prodi, f.nama as nama_jenjang');
        $this->db->from('master_biaya_kuliah a');
        $this->db->join('master_tahun_akademik b', 'b.id = a.id_tahun_akademik', 'left');
        $this->db->join('master_program_studi e', 'e.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "jenis" => $this->input->post('jenis'),
            "nominal" => $this->input->post('nominal'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
