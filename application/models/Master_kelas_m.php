<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_kelas_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_kelas';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            // [
            //     'field' => 'id_tahun_akademik',  //samakan dengan atribute name pada tags input
            //     'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
            //     'rules' => 'trim|required' //rules validasi
            // ],
            [
                'field' => 'kode',
                'label' => 'Kode',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Mata Kuliah',
                'rules' => 'trim|required'
            ],
            // [
            //     'field' => 'id_semester',
            //     'label' => 'Semester',
            //     'rules' => 'trim|required'
            // ],
            [
                'field' => 'id_program_studi',
                'label' => 'Program Studi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_kategori_kelas',
                'label' => 'Kategori Kelas',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        // $this->db->select('a.*, b.nama as tahun_akademik, d.nama as semester, e.nama as program_studi, f.nama as kategori_kelas');
        $this->db->select('a.*, e.nama as program_studi, f.nama as kategori_kelas');
        $this->db->from('master_kelas a');
        // $this->db->join('master_tahun_akademik b', 'b.id = a.id_tahun_akademik', 'left');
        // $this->db->join('master_semester d', 'd.id = a.id_semester', 'left');
        $this->db->join('master_program_studi e', 'e.id = a.id_program_studi', 'left');
        $this->db->join('master_kategori_kelas f', 'f.id = a.id_kategori_kelas', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    // public function getTahunAkademik()
    // {
    //     return $this->db->get('master_tahun_akademik')->result_array();
    // }

    // public function getSemester()
    // {
    //     return $this->db->get('master_semester')->result_array();
    // }

    public function getProgramStudi()
    {
        return $this->db->get('master_program_studi')->result_array();
    }

    public function getKategoriKelas()
    {
        return $this->db->get('master_kategori_kelas')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            //"id_semester" => $this->input->post('id_semester'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            //"id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            //"id_semester" => $this->input->post('id_semester'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            //"id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_kategori_kelas" => $this->input->post('id_kategori_kelas'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}
