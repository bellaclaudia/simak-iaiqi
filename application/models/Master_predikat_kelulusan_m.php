<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_predikat_kelulusan_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_predikat_kelulusan';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'nama_predikat',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Predikat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'ipk_batas_awal',  //samakan dengan atribute name pada tags input
                'label' => 'IPK batas awal',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'ipk_batas_akhir',
                'label' => 'IPK batas akhir',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('id, nama_predikat, ipk_batas_awal, ipk_batas_akhir');
        $this->db->from('master_predikat_kelulusan');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "nama_predikat" => $this->input->post('nama_predikat'),
            "ipk_batas_awal" => $this->input->post('ipk_batas_awal'),
            "ipk_batas_akhir" => $this->input->post('ipk_batas_akhir'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
