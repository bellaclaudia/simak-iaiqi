<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_mata_kuliah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_mata_kuliah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_jenis_mk',  //samakan dengan atribute name pada tags input
                'label' => 'Jenis Mata Kuliah',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kode',
                'label' => 'Kode',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Mata Kuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks',
                'label' => 'SKS',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks_tatap_muka',
                'label' => 'SKS Tatap Muka',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks_praktik',
                'label' => 'SKS Praktik',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks_praktik_lapangan',
                'label' => 'SKS Praktik Lapangan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'sks_simulasi',
                'label' => 'SKS Simulasi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_metode_pembelajaran',
                'label' => 'Metode Pembelajaran',
                'rules' => 'trim|required'
            ],
            // [
            //     'field' => 'id_tahun_akademik',
            //     'label' => 'Tahun Akademik',
            //     'rules' => 'trim|required'
            // ],
            [
                'field' => 'tgl_mulai_efektif',
                'label' => 'Tanggal Mulai Efektif',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_akhir_efektif',
                'label' => 'Tanggal Akhir Efektif',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_semester',
                'label' => 'Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_program_studi',
                'label' => 'Program Studi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll($id_semester, $id_prodi) // $id_ta, 
    {
        $this->db->select('a.*, b.nama as jenis_mk, c.nama as metode_pembelajaran, d.nama as semester, e.nama as program_studi '); // f.nama as tahun_akademik
        $this->db->from('master_mata_kuliah a');
        $this->db->join('master_jenis_mk b', 'b.id = a.id_jenis_mk', 'left');
        $this->db->join('master_metode_pembelajaran c', 'c.id = a.id_metode_pembelajaran', 'left');
        $this->db->join('master_semester d', 'd.id = a.id_semester', 'left');
        $this->db->join('master_program_studi e', 'e.id = a.id_program_studi', 'left');
        //$this->db->join('master_tahun_akademik f', 'f.id = a.id_tahun_akademik', 'left');

        // if ($id_ta != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getJenisMk()
    {
        return $this->db->get('master_jenis_mk')->result_array();
    }

    public function getMetodePembelajaran()
    {
        return $this->db->get('master_metode_pembelajaran')->result_array();
    }

    public function getSemester()
    {
        return $this->db->get('master_semester')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_jenis_mk" => $this->input->post('id_jenis_mk'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "sks" => $this->input->post('sks'),
            "sks_tatap_muka" => $this->input->post('sks_tatap_muka'),
            "sks_praktik" => $this->input->post('sks_praktik'),
            "sks_praktik_lapangan" => $this->input->post('sks_praktik_lapangan'),
            "sks_simulasi" => $this->input->post('sks_simulasi'),
            "id_metode_pembelajaran" => $this->input->post('id_metode_pembelajaran'),
            "tgl_mulai_efektif" => $this->input->post('tgl_mulai_efektif'),
            "tgl_akhir_efektif" => $this->input->post('tgl_akhir_efektif'),
            "id_semester" => $this->input->post('id_semester'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            "id_jenis_mk" => $this->input->post('id_jenis_mk'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "sks" => $this->input->post('sks'),
            "sks_tatap_muka" => $this->input->post('sks_tatap_muka'),
            "sks_praktik" => $this->input->post('sks_praktik'),
            "sks_praktik_lapangan" => $this->input->post('sks_praktik_lapangan'),
            "sks_simulasi" => $this->input->post('sks_simulasi'),
            "id_metode_pembelajaran" => $this->input->post('id_metode_pembelajaran'),
            "tgl_mulai_efektif" => $this->input->post('tgl_mulai_efektif'),
            "tgl_akhir_efektif" => $this->input->post('tgl_akhir_efektif'),
            "id_semester" => $this->input->post('id_semester'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }

    public function actUploadTemplate($input = array())
    {
        return $this->db->insert('template_mata_kuliah', $input);
    }

    public function getTemplateMataKuliah($id = '')
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        return $this->db->get('template_mata_kuliah');
    }

    public function getDataTemplate($kode, $id_tahun_akademik, $id_program_studi)
    {
        $this->db->select('a.*');
        $this->db->from('master_mata_kuliah a');
        if (!empty($kode)) {
            $this->db->where('a.kode', $kode);
            $this->db->where('a.id_tahun_akademik', $id_tahun_akademik);
            $this->db->where('a.id_program_studi', $id_program_studi);
        }
        return $this->db->get();
    }

    function updateData($act = "add", $input = array())
    {
        // return $this->db->insert('sdm_pph21', $input);
        if ($act == "add") {
            return $this->db->insert('master_mata_kuliah', $input);
        } else {
            return $this->db->where('kode', $input['kode'])->update('master_mata_kuliah', $input);
        }
    }

    function updateDataTemplate($data = array())
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('template_mata_kuliah', $data);
    }
}
