<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumni_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'mahasiswa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_program_studi',  //samakan dengan atribute name pada tags input
                'label' => 'Program Studi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nim',
                'label' => 'NIM',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_lahir',
                'label' => 'Tanggal Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis_kelamin',
                'label' => 'Jenis Kelamin',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_agama',
                'label' => 'Agama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_golongan_darah',
                'label' => 'Golongan Darah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'mulai_semester',
                'label' => 'Mulai Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kelurahan',
                'label' => 'Kelurahan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kecamatan',
                'label' => 'Kecamatan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kode_pos',
                'label' => 'Kode Pos',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_provinsi',
                'label' => 'Provinsi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_kabupaten_kota',
                'label' => 'Kabupaten / Kota',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No. Telp',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ayah',
                'label' => 'Nama Ayah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ibu',
                'label' => 'Nama Ibu',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_angkatan, $id_prodi)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, i.nama as fakultas, j.nama as nama_angkatan');
        $this->db->from('mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_mhs c', 'c.id = a.id_status_mhs', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');
        $this->db->join('master_angkatan_mhs j', 'j.id = a.id_angkatan', 'left');
        $this->db->order_by("id", "desc");
        $this->db->where("a.id_status_mhs", 4);

        if ($id_angkatan != '0')
            $this->db->where("a.id_angkatan", $id_angkatan);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();

        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStatusMhs()
    {
        return $this->db->get('master_status_mhs')->result_array();
    }

    public function getAgama()
    {
        return $this->db->get('master_agama')->result_array();
    }

    public function getGoldar()
    {
        return $this->db->get('master_golongan_darah')->result_array();
    }

    public function getProvinsi()
    {
        return $this->db->get('master_provinsi')->result_array();
    }

    public function getKabKota()
    {
        return $this->db->get('master_kabupaten_kota')->result_array();
    }

    public function get_angkatan()
    {
        return $this->db->get('master_angkatan_mhs')->result_array();
    }

    public function save()
    {
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        $config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
            } else {
                $error = 1;
            }
        }


        $this->db->trans_begin();

        $tgl_skrg = date('Y-m-d H:i:s');
        $data = array(
            'id_grup_user' => '4',
            'username' => $this->input->post('nim'),
            'userpass' => md5($this->input->post('nim')),
            'tgl_input' => $tgl_skrg,
            'user_update_by' => $this->session->userdata['username']
        );

        $this->db->insert('master_user', $data);

        $id_usernya = $this->db->insert_id();

        $data = array(
            "id_program_studi" => $this->input->post('id_program_studi'),
            "file_foto" => $this->upload->data("file_name"),
            "nim" => $this->input->post('nim'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "mulai_semester" => $this->input->post('mulai_semester'),
            "kelurahan" => $this->input->post('kelurahan'),
            "kecamatan" => $this->input->post('kecamatan'),
            "kode_pos" => $this->input->post('kode_pos'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "no_telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "nama_ayah" => $this->input->post('nama_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "id_status_mhs" => 4,
            "id_angkatan" => $this->input->post('id_angkatan'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return true;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function getView($id)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, i.nama as fakultas, c.nama as status_mhs, j.nama as nama_angkatan');
        $this->db->from('mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_mhs c', 'c.id = a.id_status_mhs', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');
        $this->db->join('master_angkatan_mhs j', 'j.id = a.id_angkatan', 'left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function update()
    {
        $data = array(
            'id' => $this->input->post('id'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "nim" => $this->input->post('nim'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "tgl_masuk_kuliah" => $this->input->post('tgl_masuk_kuliah'),
            "mulai_semester" => $this->input->post('mulai_semester'),
            "kelurahan" => $this->input->post('kelurahan'),
            "kecamatan" => $this->input->post('kecamatan'),
            "kode_pos" => $this->input->post('kode_pos'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "no_telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "nama_ayah" => $this->input->post('nama_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "id_angkatan" => $this->input->post('id_angkatan'),
            "id_status_mhs" => 4,
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        $config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                //=========== hapus file lama
                $this->db->select('file_foto');
                $this->db->where('id', $this->input->post('id'));
                $q = $this->db->get('mahasiswa')->row_array();
                if (isset($q)) {
                    if (!empty($q['file_foto'])) {
                        unlink($dir . '/' . $q['file_foto']);
                    }
                }
            } else {
                // echo $this->upload->display_errors();
                $error = 1;
            }
        }


        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }
}
