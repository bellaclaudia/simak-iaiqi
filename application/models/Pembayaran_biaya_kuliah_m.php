<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_biaya_kuliah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // 07-11-2021
    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_mhs_by_pembayaran()
    {
        $sqlxx = " SELECT distinct a.id, a.nim, a.nama FROM mahasiswa a inner join pembayaran_biaya_kuliah b ON a.id = b.id_mhs ORDER BY a.nama ";
        $query = $this->db->query($sqlxx);
        return $query->result_array();

        // $this->db->select('a.nim, a.nama ');
        // $this->db->from('mahasiswa a');
        // $this->db->join('pembayaran_biaya_kuliah b', 'b.id_mhs = a.id', 'left');
        // $this->db->order_by("a.nama", "asc");
        // $query = $this->db->get();
        // return $query->result_array();
    }

    public function get_data_pembayaran_biaya($id_semester, $id_ta, $id_prodi, $id_mhs, $jenis_biaya)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nama_bank, d.no_rekening, d.atas_nama,
                        e.nim, e.nama as nama_mhs, ee.nama as nama_prodi, f.nama as nama_jenjang ');
        $this->db->from('pembayaran_biaya_kuliah a');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->join('master_rekening d', 'd.id = a.id_rekening', 'left');
        $this->db->join('master_program_studi ee', 'ee.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = ee.id_jenjang_studi', 'left');
        //$this->db->where("a.id_mhs", $id_mhs);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_prodi != '0')
            $this->db->where("e.id_program_studi", $id_prodi);
        if ($id_mhs != '0')
            $this->db->where("a.id_mhs", $id_mhs);
        if ($jenis_biaya != '0')
            $this->db->where("a.jenis_biaya", $jenis_biaya);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_mhs_by_prodi($id_prodi)
    {
        $sqlxx = " SELECT id, nim, nama FROM mahasiswa WHERE true ";
        if ($id_prodi != '')
            $sqlxx .= " AND id_program_studi = '$id_prodi' ORDER BY nama asc ";

        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result_array();
    }
}
