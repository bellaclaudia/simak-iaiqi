<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calon_mhs_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'calon_mahasiswa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_program_studi_pilihan',  //samakan dengan atribute name pada tags input
                'label' => 'Program Studi Pilihan',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_kategori_kelas',  //samakan dengan atribute name pada tags input
                'label' => 'Kategori Kelas',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_gelombang_calon_mhs',
                'label' => 'Gelombang',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kelurahan',
                'label' => 'Kelurahan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kecamatan',
                'label' => 'Kecamatan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kode_pos',
                'label' => 'Kode Pos',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_lahir',
                'label' => 'Tanggal Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis_kelamin',
                'label' => 'Jenis Kelamin',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nisn',
                'label' => 'NISN',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_agama',
                'label' => 'Agama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_golongan_darah',
                'label' => 'Golongan Darah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_provinsi',
                'label' => 'Provinsi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_kabupaten_kota',
                'label' => 'Kabupaten / Kota',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No. Telp',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'ukuran_jas',
                'label' => 'Ukuran Jas',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ayah',
                'label' => 'Nama Ayah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ibu',
                'label' => 'Nama Ibu',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'ukuran_jas',
                'label' => 'Ukuran Jas',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_gelombang, $id_prodi)
    {
        $this->db->select('a.*, b.nama as prodi, bb.nama as nama_jenjang, d.nama as agama, e.nama as jalur_pendaftaran, f.nama as provinsi, g.nama as kab_kota, 
        h.nama as goldar, e.nama as gelombang, i.nama as nama_kategori_kelas');
        // j.nominal, j.tgl_bayar, i.nama_bank, i.no_rekening, i.atas_nama
        $this->db->from('calon_mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi_pilihan', 'left');
        $this->db->join('master_jenjang_studi bb', 'b.id_jenjang_studi = bb.id', 'left');
        //$this->db->join('master_user c', 'c.id = a.id_user', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_gelombang_calon_mhs e', 'e.id = a.id_gelombang_calon_mhs', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_kategori_kelas i', 'i.id = a.id_kategori_kelas', 'left');
        // $this->db->join('pembayaran_biaya_calon_mhs j', 'j.id_calon_mhs = a.id', 'left');
        // $this->db->join('master_rekening i', 'i.id = j.id_rekening', 'left');

        if ($id_gelombang != '0')
            $this->db->where("a.id_gelombang_calon_mhs", $id_gelombang);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi_pilihan", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    // public function getUser()
    // {
    //     $this->db->select('a.*');
    //     $this->db->from('master_user a');
    //     $this->db->join('master_grup_user b', 'b.id = a.id_grup_user', 'left');
    //     $this->db->where("a.id_grup_user", 4);
    //     $this->db->order_by("a.id", "desc");
    //     $query = $this->db->get();
    //     return $query->result_array();
    // }

    public function getAgama()
    {
        return $this->db->get('master_agama')->result_array();
    }

    public function getGoldar()
    {
        return $this->db->get('master_golongan_darah')->result_array();
    }

    public function getGelombang()
    {
        $this->db->select('a.*, b.tahun, b.nama as nama_tahun_akademik');
        $this->db->from('master_gelombang_calon_mhs a');
        $this->db->join('master_tahun_akademik b', 'b.id = a.id_tahun_akademik', 'left');
        $this->db->order_by("b.nama", "desc");
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    // public function getJalurPendaftaran()
    // {
    //     return $this->db->get('master_jalur_pendaftaran')->result_array();
    // }

    public function getProvinsi()
    {
        return $this->db->get('master_provinsi')->result_array();
    }

    public function getKabKota()
    {
        return $this->db->get('master_kabupaten_kota')->result_array();
    }

    // 27-10-2021
    public function get_master_dokumen()
    {
        return $this->db->get('master_dokumen_calon_mhs')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $id_gelombang = $this->input->post('id_gelombang_calon_mhs');

        // generate no_pendaftaran. format (sementara): pmb tahun + idgelombang + no urut. contoh: pmb202111
        // cek no urut terakhir berdasarkan id gelombang
        $sqlxx = " SELECT no_urut_daftar FROM calon_mahasiswa WHERE id_gelombang_calon_mhs = '$id_gelombang' ORDER BY no_urut_daftar DESC LIMIT 1 ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $no_urut_daftar    = $hasilxx->no_urut_daftar;
            $no_urut_daftar++;
        } else
            $no_urut_daftar = 1;

        // ambil tahun dari gelombang_calon_mhs
        $sqlxx = " select b.tahun FROM master_gelombang_calon_mhs a 
                    INNER JOIN master_tahun_akademik b ON b.id = a.id_tahun_akademik
                    WHERE a.id = '$id_gelombang' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $tahun    = $hasilxx->tahun;
        }

        // set no pendaftaran
        $no_pendaftaran = "pmb" . $tahun . $id_gelombang . $no_urut_daftar;

        // ambil nama grup user
        $grup_user = $this->session->userdata['id_grup_user'];
        $sqlxx = " select nama_grup FROM master_grup_user WHERE id = '$grup_user' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $nama_grup_user    = $hasilxx->nama_grup;
        }

        $this->db->trans_begin();

        // inset ke tabel master_user, usernamenya pake dari no_pendaftaran, passwordnya juga samain
        $tgl_skrg = date('Y-m-d H:i:s');
        $data = array(
            'id_grup_user' => '3',
            'username' => $no_pendaftaran,
            'userpass' => md5($no_pendaftaran),
            'tgl_input' => $tgl_skrg,
            'user_update_by' => $this->session->userdata['username']
        );

        $this->db->insert('master_user', $data);

        $id_usernya = $this->db->insert_id();

        // 23-10-2021 get total biaya calon mhs, ini utk di field total_biaya
        $sqlxx = " select sum(nominal) as jum FROM master_biaya_calon_mhs ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $total_biaya    = $hasilxx->jum;

            if ($total_biaya == '')
                $total_biaya = 0;
        } else
            $total_biaya = 0;

        // file foto UPDATE 23-10-2021
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size'] = 5120;
        //$config['encrypt_name'] = FALSE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'PMBfoto_' . $id_usernya . '_' . $timestamp;
        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $file_foto = $upl['file_name'];

                unset($this->upload);
            } else {
                $error = 1;
            }
        }

        // file dokumen
        $config['allowed_types'] = 'pdf';
        $config['overwrite'] = false;
        $config['max_size'] = 5120;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'PMBdoc_' . $id_usernya . '_' . $timestamp;

        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_dokumen']['tmp_name'])) {
            $this->load->library('upload', $config);
            $up = $this->upload->do_upload('file_dokumen');
            if ($up) {
                $upl = $this->upload->data();
                $file_dokumen = $upl['file_name'];
            } else {
                $error = 1;
            }
        }

        $data = array(
            "id_program_studi_pilihan" => $this->input->post('id_program_studi_pilihan'),
            "no_pendaftaran" => $no_pendaftaran,
            "id_user" => $id_usernya,
            "no_urut_daftar" => $no_urut_daftar,
            "id_gelombang_calon_mhs" => $this->input->post('id_gelombang_calon_mhs'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "no_telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "nama_ayah" => $this->input->post('nama_ayah'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "registered_by" => $nama_grup_user,
            "user_update_by" => $this->session->userdata['username'],
            "total_biaya" => $total_biaya,
            "file_foto" => $file_foto,
            "file_dokumen" => $file_dokumen
        );
        $this->db->insert($this->table, $data);

        //$idnya = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        return true;
    }

    // 23-10-2021
    public function get_calon_mhs_by_id($id_calon_mhs)
    {
        $this->db->select('a.*, b.nama as prodi, bb.nama as nama_jenjang, d.nama as agama, e.nama as gelombang, f.nama as provinsi, g.nama as kab_kota, 
        h.nama as goldar, j.nominal, j.tgl_bayar, i.nama_bank, i.no_rekening, i.atas_nama');
        $this->db->from('calon_mahasiswa a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi_pilihan', 'left');
        $this->db->join('master_jenjang_studi bb', 'b.id_jenjang_studi = bb.id', 'left');
        //$this->db->join('master_user c', 'c.id = a.id_user', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_gelombang_calon_mhs e', 'e.id = a.id_gelombang_calon_mhs', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('pembayaran_biaya_calon_mhs j', 'j.id_calon_mhs = a.id', 'left');
        $this->db->join('master_rekening i', 'i.id = j.id_rekening', 'left');
        $this->db->where("a.id", $id_calon_mhs);
        $query = $this->db->get();
        return $query->row();
    }

    // 01-06-2022
    public function get_kategori_kelas()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_kategori_kelas')->result_array();
    }
}
