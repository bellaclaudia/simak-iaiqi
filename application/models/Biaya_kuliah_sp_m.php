<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_kuliah_sp_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'biaya_kuliah_sp';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_semester',  //samakan dengan atribute name pada tags input
                'label' => 'Semester',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_tahun_akademik',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_biaya_kuliah',  //samakan dengan atribute name pada tags input
                'label' => 'Biaya Kuliah',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_mhs',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Mahasiswa',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*, b.nama as semester, c.nama as tahun_akademik, e.nama as nama_prodi, f.nama as nama_jenjang, d.nama as biaya_kuliah, g.nama as nama_mhs');
        $this->db->from('biaya_kuliah_sp a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('master_program_studi e', 'e.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->join('master_biaya_kuliah d', 'd.id = a.id_biaya_kuliah', 'left');
        $this->db->join('mahasiswa g', 'g.id = a.id_mhs', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getSemester()
    {
        return $this->db->get('master_semester')->result_array();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMahasiswa()
    {
        return $this->db->get('mahasiswa')->result_array();
    }

    public function getBiayaKuliah()
    {
        $this->db->select('a.*');
        $this->db->from('master_biaya_kuliah a');
        $this->db->where('a.jenis', 'SP');
        return $this->db->get()->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_semester" => $this->input->post('id_semester'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_biaya_kuliah" => $this->input->post('id_biaya_kuliah'),
            "id_mhs" => $this->input->post('id_mhs'),
            "jumlah" => $this->input->post('jumlah'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function get_mhs_by_prodi($id_program_studi)
    {
        $sqlxx = " SELECT id, nim, nama FROM mahasiswa WHERE id_program_studi = '$id_program_studi' ORDER BY nama asc ";
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }
}
