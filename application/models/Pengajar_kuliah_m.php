<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajar_kuliah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'dosen_pengajar_kuliah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_dosen',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Dosen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_jadwal_kuliah',
                'label' => 'Jadwal Kuliah',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, c.hari, c.jam_mulai, c.jam_selesai, b.nama as nama_dosen, b.nidn, d.nama as semester, 
                        e.nama as tahun_akademik, f.nama as mata_kuliah, g.nama as nama_prodi, h.nama as nama_jenjang');
        $this->db->from('dosen_pengajar_kuliah a');
        $this->db->join('dosen b', 'b.id = a.id_dosen', 'left');
        $this->db->join('jadwal_kuliah c', 'c.id = a.id_jadwal_kuliah', 'left');
        $this->db->join('master_semester d', 'd.id = c.id_semester', 'left');
        $this->db->join('master_tahun_akademik e', 'e.id = c.id_tahun_akademik', 'left');
        $this->db->join('master_mata_kuliah f', 'f.id = c.id_mata_kuliah', 'left');
        $this->db->join('master_program_studi g', 'g.id = b.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');

        if ($id_ta != '0')
            $this->db->where("c.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("c.id_semester", $id_semester);
        if ($id_prodi != '0')
            $this->db->where("b.id_program_studi", $id_prodi);

        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataDosen()
    {
        return $this->db->get('dosen')->result_array();
    }

    // modif 18-11-2021 pake parameter id_dosen
    public function getJadwalKuliah()
    {
        // ambil prodi dosen. 10-12-2021 ini ga pake filter prodi. sementara dikomen dulu
        // $sqlxx = " select id_program_studi FROM dosen where id = '$id_dosen' ";
        // $queryxx = $this->db->query($sqlxx);
        // //if ($queryxx->num_rows() > 0) {
        // $hasilxx = $queryxx->row();
        // $id_prodi    = $hasilxx->id_program_studi;

        $this->db->select('c.id, c.hari, c.jam_mulai, c.jam_selesai, d.nama as semester, e.nama as tahun_akademik, f.nama as mata_kuliah, 
        g.nama as nama_prodi, h.nama as nama_jenjang');
        $this->db->from('jadwal_kuliah c');
        $this->db->join('master_semester d', 'd.id = c.id_semester', 'left');
        $this->db->join('master_tahun_akademik e', 'e.id = c.id_tahun_akademik', 'left');
        $this->db->join('master_mata_kuliah f', 'f.id = c.id_mata_kuliah', 'left');
        $this->db->join('master_program_studi g', 'g.id = c.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        //$this->db->where("c.id_program_studi", $id_prodi);
        $this->db->order_by("d.nama", "desc");
        $query = $this->db->get();
        return $query->result_array();

        // return $this->db->get('jadwal_kuliah')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_dosen" => $this->input->post('id_dosen'),
            "id_jadwal_kuliah" => $this->input->post('id_jadwal_kuliah'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
            "id_dosen" => $this->input->post('id_dosen'),
            "id_jadwal_kuliah" => $this->input->post('id_jadwal_kuliah'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}
