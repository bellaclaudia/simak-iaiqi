<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_kuliah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'jadwal_kuliah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_semester',  //samakan dengan atribute name pada tags input
                'label' => 'Semester',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_tahun_akademik',
                'label' => 'Tahun Akademik',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_prodi',
                'label' => 'Program Studi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_kelas',
                'label' => 'Kelas',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_mata_kuliah',
                'label' => 'Mata Kuliah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'max_kuota',
                'label' => 'Maksimal Kuota per Kelas',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'hari',
                'label' => 'Hari',
                'rules' => 'trim|required'
            ]
            // [
            //     'field' => 'jam_mulai',
            //     'label' => 'Jam Mulai',
            //     'rules' => 'trim|required'
            // ],
            // [
            //     'field' => 'jam_selesai',
            //     'label' => 'Jam Selesai',
            //     'rules' => 'trim|required'
            // ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, b.nama as semester, c.nama as tahun_akademik, d.nama as mata_kuliah, a1.nama as nama_kelas,
                        e.nama as nama_prodi, f.nama as nama_jenjang');
        $this->db->from('jadwal_kuliah a');
        $this->db->join('master_kelas a1', 'a1.id = a.id_kelas', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getMataKuliah($id_program_studi, $id_semester)
    {
        return $this->db->get_where('master_mata_kuliah', ["id_program_studi" => $id_program_studi, "id_semester" => $id_semester])->result_array();
    }

    public function getSemester()
    {
        return $this->db->get('master_semester')->result_array();
    }

    // 03-11-2021
    public function get_mk_by_prodi($id_prodi, $id_semester)
    {
        $sqlxx = " SELECT id, kode, nama FROM master_mata_kuliah WHERE id_program_studi = '$id_prodi' AND id_semester = '$id_semester' ORDER BY kode asc ";
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    // 17-11-2021
    public function get_kelas_by_prodisemta($id_prodi) // 18-11-2021 ga dipake: $id_semester, $id_ta
    {
        $sqlxx = " SELECT id, kode, nama FROM master_kelas WHERE id_program_studi = '$id_prodi' ORDER BY kode asc ";
        // AND id_semester = '$id_semester' AND id_tahun_akademik = '$id_ta'
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result();
    }

    public function save()
    {
        $jam1 = $this->input->post('jam1');
        $menit1 = $this->input->post('menit1');
        $jam_mulai = $jam1 . ":" . $menit1;

        $jam2 = $this->input->post('jam2');
        $menit2 = $this->input->post('menit2');
        $jam_selesai = $jam2 . ":" . $menit2;

        $data = array(
            "id_semester" => $this->input->post('id_semester'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_program_studi" => $this->input->post('id_prodi'),
            "id_mata_kuliah" => $this->input->post('id_mata_kuliah'),
            "id_kelas" => $this->input->post('id_kelas'),
            "id_program_studi" => $this->input->post('id_prodi'),
            "max_kuota" => $this->input->post('max_kuota'),
            "jenis_kuliah" => $this->input->post('jenis_kuliah'),
            "hari" => $this->input->post('hari'),
            "jam_mulai" => $jam_mulai,
            "jam_selesai" => $jam_selesai,
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $jam1 = $this->input->post('jam1');
        $menit1 = $this->input->post('menit1');
        $jam_mulai = $jam1 . ":" . $menit1;

        $jam2 = $this->input->post('jam2');
        $menit2 = $this->input->post('menit2');
        $jam_selesai = $jam2 . ":" . $menit2;

        $data = array(
            "id_semester" => $this->input->post('id_semester'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_mata_kuliah" => $this->input->post('id_mata_kuliah'),
            "id_kelas" => $this->input->post('id_kelas'),
            "id_program_studi" => $this->input->post('id_prodi'),
            "max_kuota" => $this->input->post('max_kuota'),
            "jenis_kuliah" => $this->input->post('jenis_kuliah'),
            "hari" => $this->input->post('hari'),
            "jam_mulai" => $jam_mulai,
            "jam_selesai" => $jam_selesai,
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }

    // public function get_matkul_by_prodi($id_program_studi)
    // {
    //     $sqlxx = " SELECT id, kode, nama FROM master_mata_kuliah WHERE id_program_studi = '$id_program_studi' ORDER BY nama asc ";
    //     $queryxx = $this->db->query($sqlxx);
    //     return $queryxx->result();
    // }
}
