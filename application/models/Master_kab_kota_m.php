<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_kab_kota_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_kabupaten_kota';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'kode',  //samakan dengan atribute name pada tags input
                'label' => 'Kode',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Nama Kab / Kota',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll()
    {
        $this->db->select('a.*, b.nama as provinsi');
        $this->db->from('master_kabupaten_kota a');
        $this->db->join('master_provinsi b', 'b.id = a.id_provinsi', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_provinsi" => $this->input->post('id_provinsi'),
            "kode" => $this->input->post('kode'),
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getProvinsi()
    {
        return $this->db->get('master_provinsi')->result_array();
    }
}
