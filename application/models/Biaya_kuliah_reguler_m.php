<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_kuliah_reguler_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'biaya_kuliah_persemester';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_semester',  //samakan dengan atribute name pada tags input
                'label' => 'Semester',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_program_studi',  //samakan dengan atribute name pada tags input
                'label' => 'Program Studi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_tahun_akademik',  //samakan dengan atribute name pada tags input
                'label' => 'Tahun Akademik',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
            // [
            //     'field' => 'id_biaya_kuliah',  //samakan dengan atribute name pada tags input
            //     'label' => 'Biaya Kuliah',  // label yang kan ditampilkan pada pesan error
            //     'rules' => 'trim|required' //rules validasi
            // ],
            // [
            //     'field' => 'jumlah',
            //     'label' => 'Jumlah',
            //     'rules' => 'trim|required'
            // ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_ta, $id_semester, $id_prodi)
    {
        $this->db->select('a.*, b.nama as semester, c.nama as tahun_akademik, 
                        e.nama as nama_prodi, f.nama as nama_jenjang, d.nama as biaya_kuliah');
        $this->db->from('biaya_kuliah_persemester a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('master_program_studi e', 'e.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->join('master_biaya_kuliah d', 'd.id = a.id_biaya_kuliah', 'left');

        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);
        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getSemester()
    {
        return $this->db->get('master_semester')->result_array();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBiayaKuliah()
    {
        $this->db->select('a.*');
        $this->db->from('master_biaya_kuliah a');
        $this->db->where('a.jenis', 'REGULER');
        return $this->db->get()->result_array();
    }

    //menyimpan data
    public function save()
    {
        $id_tahun_akademik = $this->input->post('id_tahun_akademik');
        $id_program_studi = $this->input->post('id_program_studi');
        $id_sem = $this->input->post('id_semester');

        // 04-11-2021 query get data dari master_biaya_kuliah dgn jenis REGULER, trus didalam perulangan hasil query bikin insert
        $sqlxx = " select id, nominal FROM master_biaya_kuliah WHERE jenis = 'REGULER' AND id_tahun_akademik = '$id_tahun_akademik' 
        AND id_program_studi = '$id_program_studi' ORDER BY id ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->result();
            foreach ($hasilxx as $rowxx) {
                $id_master_biaya   = $rowxx->id;
                $nominal    = $rowxx->nominal;

                $data = array(
                    "id_semester" => $id_sem,
                    "id_tahun_akademik" => $id_tahun_akademik,
                    "id_program_studi" => $id_program_studi,
                    "id_biaya_kuliah" => $id_master_biaya,
                    "jumlah" => $nominal,
                    "tgl_input" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
                return $this->db->insert($this->table, $data);

                // $data = array(
                //     'id_krs_mhs' => $id_krs,
                //     'id_mata_kuliah' => $id_mk_temp,
                //     'sks' => $sks_temp,
                //     'status' => '1'
                // );

                // $this->db->insert('krs_mhs_detail', $data);
            }
        }
    }
}
