<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'dosen';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_program_studi',  //samakan dengan atribute name pada tags input
                'label' => 'Program Studi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jenis_kelamin',
                'label' => 'Jenis Kelamin',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_agama',
                'label' => 'Agama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_jabatan_akademik_dosen',
                'label' => 'Jabatan Akademik Dosen',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll($id_status_dosen, $id_prodi)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, c.nama as status_dosen, e.nama as jabatan_akademik_dosen, f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, i.nama as fakultas');
        $this->db->from('dosen a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_dosen c', 'c.id = a.id_status_dosen', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_jabatan_akademik_dosen e', 'e.id = a.id_jabatan_akademik_dosen', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');

        if ($id_status_dosen != '0')
            $this->db->where("a.id_status_dosen", $id_status_dosen);
        if ($id_prodi != '0')
            $this->db->where("a.id_program_studi", $id_prodi);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProgramStudi()
    {
        //return $this->db->get('master_program_studi')->result_array();
        $this->db->select('a.*, b.nama as nama_jenjang');
        $this->db->from('master_program_studi a');
        $this->db->join('master_jenjang_studi b', 'b.id = a.id_jenjang_studi', 'left');
        $this->db->order_by("b.id", "asc");
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStatusDosen()
    {
        return $this->db->get('master_status_dosen')->result_array();
    }

    public function getAgama()
    {
        return $this->db->get('master_agama')->result_array();
    }

    public function getGoldar()
    {
        return $this->db->get('master_golongan_darah')->result_array();
    }

    public function getJabatanAkademikDosen()
    {
        return $this->db->get('master_jabatan_akademik_dosen')->result_array();
    }

    public function getProvinsi()
    {
        return $this->db->get('master_provinsi')->result_array();
    }

    public function getKabKota()
    {
        return $this->db->get('master_kabupaten_kota')->result_array();
    }

    public function save()
    {
        $this->db->trans_begin();

        $tgl_skrg = date('Y-m-d H:i:s');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $no = date('Ymd', strtotime($tgl_lahir));
        $username = "dosen" . $no;
        if ($this->input->post('nidn') == NULL) {
            $data = array(
                'id_grup_user' => '5',
                'username' => $username,
                'userpass' => md5($username),
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
        } else {
            $data = array(
                'id_grup_user' => '5',
                'username' => $this->input->post('nidn'),
                'userpass' => md5($this->input->post('nidn')),
                'tgl_input' => $tgl_skrg,
                'user_update_by' => $this->session->userdata['username']
            );
        }


        $this->db->insert('master_user', $data);

        $id_usernya = $this->db->insert_id();

        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        //$config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'DOSfoto_' . $id_usernya . '_' . $timestamp;

        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                $file_foto = $data['file_foto'];
            } else {
                $error = 1;
            }
        } else
            $file_foto = '';

        $data = array(
            "id_program_studi" => $this->input->post('id_program_studi'),
            "id_user" => $id_usernya,
            "nidn" => $this->input->post('nidn'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "id_jabatan_akademik_dosen" => $this->input->post('id_jabatan_akademik_dosen'),
            "id_status_dosen" => $this->input->post('id_status_dosen'),
            "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
            "email" => $this->input->post('email'),
            "no_telp" => $this->input->post('no_telp'),
            "file_foto" => $file_foto,
            "pangkat_golongan" => $this->input->post('pangkat_golongan'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return true;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function getView($id)
    {
        $this->db->select('a.*, b.nama as prodi, d.nama as agama, c.nama as status_dosen, e.nama as jabatan_akademik_dosen, 
        f.nama as provinsi, g.nama as kab_kota, h.nama as goldar, i.nama as fakultas');
        $this->db->from('dosen a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_status_dosen c', 'c.id = a.id_status_dosen', 'left');
        $this->db->join('master_agama d', 'd.id = a.id_agama', 'left');
        $this->db->join('master_golongan_darah h', 'h.id = a.id_golongan_darah', 'left');
        $this->db->join('master_jabatan_akademik_dosen e', 'e.id = a.id_jabatan_akademik_dosen', 'left');
        $this->db->join('master_provinsi f', 'f.id = a.id_provinsi', 'left');
        $this->db->join('master_kabupaten_kota g', 'g.id = a.id_kabupaten_kota', 'left');
        $this->db->join('master_fakultas i', 'i.id = b.id_fakultas', 'left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function update()
    {

        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        //$config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');

        $this->db->trans_begin();

        // ambil id user
        $sqlxx = " select id_user FROM dosen WHERE id = '" . $this->input->post('id') . "' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $id_usernya    = $hasilxx->id_user;

            // 31-10-2021 update username pake nidn yg ada. sementara ditutup dulu. nidn ga boleh sembarangan diedit sendiri
            // $tgl_skrg = date('Y-m-d H:i:s');
            // $data = array(
            //     "username" => $this->input->post('nidn'),
            //     "userpass" => $this->input->post('nidn'),
            //     "tgl_update" => $tgl_skrg,
            //     "user_update_by" => $this->session->userdata['username']
            // );
            // $this->db->where('id', $id_usernya);
            // $this->db->update('master_user', $data);
        }

        $config['file_name'] = 'DOSfoto_' . $id_usernya . '_' . $timestamp;
        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                $file_foto = $data['file_foto'];

                //=========== hapus file lama
                $this->db->select('file_foto');
                $this->db->where('id', $this->input->post('id'));
                $q = $this->db->get('dosen')->row_array();
                if (isset($q)) {
                    if (!empty($q['file_foto'])) {
                        unlink($dir . '/' . $q['file_foto']);
                    }
                }
            } else {
                // echo $this->upload->display_errors();
                $error = 1;
            }
        } else {
            $this->db->select('file_foto');
            $this->db->where('id', $this->input->post('id'));
            $q = $this->db->get('dosen')->row_array();
            // if (isset($q)) {
            //     if (!empty($q['file_foto'])) {
            //         unlink($dir . '/' . $q['file_foto']);
            //     }
            // }
            $file_foto = $q['file_foto'];
        }

        $data = array(
            //'id' => $this->input->post('id'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "nidn" => $this->input->post('nidn'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "id_provinsi" => $this->input->post('id_provinsi'),
            "id_kabupaten_kota" => $this->input->post('id_kabupaten_kota'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin'),
            "nik" => $this->input->post('nik'),
            "tempat_lahir" => $this->input->post('tempat_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "id_agama" => $this->input->post('id_agama'),
            "id_golongan_darah" => $this->input->post('id_golongan_darah'),
            "id_jabatan_akademik_dosen" => $this->input->post('id_jabatan_akademik_dosen'),
            "id_status_dosen" => $this->input->post('id_status_dosen'),
            "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
            "email" => $this->input->post('email'),
            "no_telp" => $this->input->post('no_telp'),
            "pangkat_golongan" => $this->input->post('pangkat_golongan'),
            "file_foto" => $file_foto,
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );

        $this->db->update($this->table, $data, array('id' => $this->input->post('id')));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return true;
    }

    // 31-10-2021
    public function get_riwayat_pendidikan_dosen($id_dosen)
    {
        $this->db->select('a.*, b.nama as nama_dosen');
        $this->db->from('riwayat_pendidikan_dosen a');
        $this->db->join('dosen b', 'b.id = a.id_dosen', 'left');
        $this->db->where("a.id_dosen", $id_dosen);
        $this->db->order_by("a.id", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // 09-11-2021
    public function get_semester()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_semester')->result_array();
    }

    public function get_tahun_akademik()
    {
        $this->db->order_by("tahun", "desc");
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function get_komponen_nilai()
    {
        $this->db->order_by("id", "asc");
        return $this->db->get('master_komponen_nilai')->result_array();
    }

    public function get_mata_kuliah($id_prodi)
    {
        //$this->db->order_by("tahun", "desc");
        //return $this->db->get('master_mata_kuliah')->result_array();

        // 13-12-2021 ini munculin semua data mk semua prodi

        $this->db->select('a.id, a.kode, a.nama, g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('master_mata_kuliah a');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        //$this->db->where("id_program_studi", $id_prodi);
        $this->db->order_by("a.id_program_studi", "asc");
        $this->db->order_by("a.kode", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    // 13-12-2021 by semester
    public function get_mata_kuliah2($id_semester)
    {
        //$this->db->order_by("tahun", "desc");
        //return $this->db->get('master_mata_kuliah')->result_array();

        // 13-12-2021 ini munculin semua data mk semua prodi by filter semester
        $this->db->select('a.id, a.kode, a.nama, g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('master_mata_kuliah a');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->where("a.id_semester", $id_semester);
        $this->db->order_by("a.id_program_studi", "asc");
        $this->db->order_by("a.kode", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_data_bobot_komponen_nilai($id_semester, $id_ta, $id_dosen)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, 
                        e.nidn, e.nama as nama_dosen, d.nama as nama_komponen_nilai, f.kode as kode_mata_kuliah, f.nama as nama_mata_kuliah,
                        g.nama as nama_prodi, h.nama as nama_jenjang ');
        $this->db->from('bobot_komponen_nilai_dosen a');
        $this->db->join('dosen e', 'e.id = a.id_dosen', 'left');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'a.id_tahun_akademik = c.id', 'left');
        $this->db->join('master_komponen_nilai d', 'a.id_komponen_nilai = d.id', 'left');
        $this->db->join('master_mata_kuliah f', 'a.id_mata_kuliah = f.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = f.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->where("a.id_dosen", $id_dosen);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_bobot_komponen_nilai_byid($id)
    {
        $this->db->select('a.* ');
        $this->db->from('bobot_komponen_nilai_dosen a');
        $this->db->where("a.id", $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function get_data_mk($id_semester, $id_ta, $id_dosen)
    {
        $this->db->distinct();
        $this->db->select('a.id_kelas, a.id_mata_kuliah, a.jenis_kuliah, a.id_semester, a.id_tahun_akademik, 
                        e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nidn, c.nama as nama_dosen, d.kode, d.nama as nama_mata_kuliah, d.sks, 
                        g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_kelas ');
        $this->db->from('jadwal_kuliah a');
        $this->db->join('dosen_pengajar_kuliah b', 'b.id_jadwal_kuliah = a.id', 'inner');
        $this->db->join('dosen c', 'c.id = b.id_dosen', 'inner');
        $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        // $this->db->join('master_komponen_nilai d', 'a.id_komponen_nilai = d.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->join('master_kelas i', 'i.id = a.id_kelas', 'left');
        $this->db->where("b.id_dosen", $id_dosen);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // 24-11-2021 jadwal mengajar kuliah
    public function get_jadwal_mengajar($id_semester, $id_ta, $id_dosen)
    {
        //$this->db->distinct();
        $this->db->select('a.id, a.hari, a.jam_mulai, a.jam_selesai, a.id_kelas, a.id_mata_kuliah, a.jenis_kuliah, a.id_semester, a.id_tahun_akademik, 
                        e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nidn, c.nama as nama_dosen, d.kode, d.nama as nama_mata_kuliah, d.sks, 
                        g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_kelas ');
        $this->db->from('jadwal_kuliah a');
        $this->db->join('dosen_pengajar_kuliah b', 'b.id_jadwal_kuliah = a.id', 'inner');
        $this->db->join('dosen c', 'c.id = b.id_dosen', 'inner');
        $this->db->join('master_mata_kuliah d', 'd.id = a.id_mata_kuliah', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        // $this->db->join('master_komponen_nilai d', 'a.id_komponen_nilai = d.id', 'left');
        $this->db->join('master_program_studi g', 'g.id = a.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi h', 'h.id = g.id_jenjang_studi', 'left');
        $this->db->join('master_kelas i', 'i.id = a.id_kelas', 'left');
        $this->db->where("b.id_dosen", $id_dosen);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // 13-11-2021 get data krs waiting
    public function get_data_krs_waiting($id_semester, $id_ta, $id_dosen)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        e.nama as nama_prodi, f.nama as nama_jenjang ');
        $this->db->from('krs_mhs a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        $this->db->join('master_kelas h', 'h.id = d.id_kelas', 'left');
        $this->db->join('dosen_pembimbing_akademik_mhs g', 'g.id_kelas = h.id', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->where("g.id_dosen", $id_dosen);
        $this->db->where("a.status_verifikasi", '0');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.nama", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // detail krs by id_krs
    public function get_detail_krs_waiting($id_krs)
    {
        $this->db->select('b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, 
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah ');
        $this->db->from('krs_mhs a');
        $this->db->join('krs_mhs_detail b', 'b.id_krs_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->where("a.id", $id_krs);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }

    // 16-11-2021 get data nilai
    public function get_detail_nilai($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_prodi, $id_kelas, $id_komponen_nilai)
    {
        // // ambil id_semester dan id_tahun_akademik dari id jadwal
        // $queryxx = $this->db->query(" SELECT id_semester, id_tahun_akademik, id_program_studi FROM jadwal_kuliah WHERE id = '$id_jadwal' ");
        // $hasilxx = $queryxx->row();
        // $id_semester    = $hasilxx->id_semester;
        // $id_tahun_akademik    = $hasilxx->id_tahun_akademik;
        // $id_program_studi = $hasilxx->id_program_studi;

        $queryxx    = $this->db->query(" SELECT nama FROM master_komponen_nilai WHERE id = '$id_komponen_nilai' ");
        if ($queryxx->num_rows() > 0) {
            $hasilrowxx = $queryxx->row();
            $nama_komponen    = $hasilrowxx->nama;
        } else
            $nama_komponen = '';

        //  cek apakah sudah ada data di khs
        $sqlxx = " SELECT a.id FROM khs_mhs a INNER JOIN mahasiswa b ON a.id_mhs = b.id 
                INNER JOIN khs_mhs_mk c ON c.id_khs_mhs = a.id
                INNER JOIN khs_mhs_mk_detail d ON d.id_khs_mk = c.id
                WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik' 
                 AND b.id_kelas = '$id_kelas'
                AND c.id_mata_kuliah = '$id_mata_kuliah' AND d.id_komponen_nilai = '$id_komponen_nilai' "; // AND b.id_program_studi = '$id_prodi'
        // echo $sqlxx;
        // die();
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            // jika sudah ada, maka query ke tabel khs utk get data nilai angka tiap2 mhs berdasarkan komponen nilainya
            // 02-12-2021 perlu ditambahin query get data lagi utk mhs yg menyusul masuk ke kelas. BARU MAU DIBIKIN

            $sql = " SELECT a1.id as id_mhs, a1.nim, a1.nama as nama_mhs, c.id, c.nilai_angka, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                    g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_mk, j.nama as nama_kelas, d.id as id_krs_detail 
                    FROM khs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                    INNER JOIN khs_mhs_mk b ON b.id_khs_mhs = a.id
                    INNER JOIN khs_mhs_mk_detail c ON c.id_khs_mk = b.id
                    INNER JOIN krs_mhs_detail d ON d.id = b.id_krs_mhs_detail
                    LEFT JOIN master_semester e ON e.id = a.id_semester
                    LEFT JOIN master_tahun_akademik f ON f.id = a.id_tahun_akademik
                    LEFT JOIN master_program_studi g ON g.id = a1.id_program_studi
                    LEFT JOIN master_jenjang_studi h ON h.id = g.id_jenjang_studi
                    LEFT JOIN master_mata_kuliah i ON i.id = b.id_mata_kuliah
                    LEFT JOIN master_kelas j ON j.id = a1.id_kelas
                    WHERE c.id_komponen_nilai = '$id_komponen_nilai' AND a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                    AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi'

            // outputnya nanti dlm bentuk array, samain variabel2nya
            $query = $this->db->query($sql);

            $data_nilai = array();
            if ($query->num_rows() > 0) {
                $hasil = $query->result();
                foreach ($hasil as $row) {
                    $id_mhs = $row->id_mhs;
                    $nim = $row->nim;
                    $nama = $row->nama_mhs;
                    $id_nilai = $row->id;
                    $nilai_angka = $row->nilai_angka;
                    $nama_semester = $row->nama_semester;
                    $tahun = $row->tahun;
                    $nama_tahun_akademik = $row->nama_tahun_akademik;
                    $nama_prodi = $row->nama_prodi;
                    $nama_jenjang = $row->nama_jenjang;
                    $nama_mk = $row->nama_mk;
                    $nama_kelas = $row->nama_kelas;
                    $id_krs_detail = $row->id_krs_detail;

                    $data_nilai[] = array(
                        'id_nilai' => $id_nilai,
                        'id_mhs' => $id_mhs,
                        'nim' => $nim,
                        'nama' => $nama,
                        'nilai_angka' => $nilai_angka,
                        'nama_semester' => $nama_semester,
                        'tahun' => $tahun,
                        'nama_tahun_akademik' => $nama_tahun_akademik,
                        'nama_prodi' => $nama_prodi,
                        'nama_jenjang' => $nama_jenjang,
                        'nama_mk' => $nama_mk,
                        'nama_kelas' => $nama_kelas,
                        'nama_komponen' => $nama_komponen,
                        'id_krs_detail' => $id_krs_detail,
                        'baru' => 0
                    );
                }
            } else
                $data_nilai = '';
        } else {
            // jika belum ada, maka query ke tabel krs detail utk dapetin mhs2 yg ambil mata kuliah berdasarkan semester, thn, prodi
            $sql = " SELECT a1.id as id_mhs, a1.nim, a1.nama as nama_mhs, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik,
                    g.nama as nama_prodi, h.nama as nama_jenjang, i.nama as nama_mk, j.nama as nama_kelas, b.id as id_krs_detail  
                    FROM krs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                    INNER JOIN krs_mhs_detail b ON b.id_krs_mhs = a.id
                    LEFT JOIN master_semester e ON e.id = a.id_semester
                    LEFT JOIN master_tahun_akademik f ON f.id = a.id_tahun_akademik
                    LEFT JOIN master_program_studi g ON g.id = a1.id_program_studi
                    LEFT JOIN master_jenjang_studi h ON h.id = g.id_jenjang_studi
                    LEFT JOIN master_mata_kuliah i ON i.id = b.id_mata_kuliah
                    LEFT JOIN master_kelas j ON j.id = a1.id_kelas
                    WHERE a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                    AND a1.id_kelas = '$id_kelas' "; // AND a1.id_program_studi = '$id_prodi'
            // echo $sql;
            // die();

            // outputnya nanti dlm bentuk array, samain variabel2nya
            $query = $this->db->query($sql);

            $data_nilai = array();
            if ($query->num_rows() > 0) {
                $hasil = $query->result();
                foreach ($hasil as $row) {
                    $id_mhs = $row->id_mhs;
                    $nim = $row->nim;
                    $nama = $row->nama_mhs;
                    $nama_semester = $row->nama_semester;
                    $tahun = $row->tahun;
                    $nama_tahun_akademik = $row->nama_tahun_akademik;
                    $nama_prodi = $row->nama_prodi;
                    $nama_jenjang = $row->nama_jenjang;
                    $nama_mk = $row->nama_mk;
                    $nama_kelas = $row->nama_kelas;
                    $id_krs_detail = $row->id_krs_detail;

                    $data_nilai[] = array(
                        'id_nilai' => 0,
                        'id_mhs' => $id_mhs,
                        'nim' => $nim,
                        'nama' => $nama,
                        'nilai_angka' => 0,
                        'nama_semester' => $nama_semester,
                        'tahun' => $tahun,
                        'nama_tahun_akademik' => $nama_tahun_akademik,
                        'nama_prodi' => $nama_prodi,
                        'nama_jenjang' => $nama_jenjang,
                        'nama_mk' => $nama_mk,
                        'nama_kelas' => $nama_kelas,
                        'nama_komponen' => $nama_komponen,
                        'id_krs_detail' => $id_krs_detail,
                        'baru' => 1
                    );
                }
            } else
                $data_nilai = '';
        }

        return $data_nilai;
    }

    // 20-11-2021
    public function get_data_sp_waiting($id_semester, $id_ta, $id_dosen)
    {
        $this->db->select('a.*, b.nama as nama_semester, c.tahun, c.nama as nama_tahun_akademik, d.nim, d.nama as nama_mhs, 
        e.nama as nama_prodi, f.nama as nama_jenjang ');
        $this->db->from('sp_mhs a');
        $this->db->join('master_semester b', 'b.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik c', 'c.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa d', 'd.id = a.id_mhs', 'left');
        $this->db->join('master_kelas h', 'h.id = d.id_kelas', 'left');
        $this->db->join('dosen_pembimbing_akademik_mhs g', 'g.id_kelas = h.id', 'left');
        $this->db->join('master_program_studi e', 'e.id = d.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi f', 'f.id = e.id_jenjang_studi', 'left');
        $this->db->where("g.id_dosen", $id_dosen);
        $this->db->where("a.status_verifikasi", '0');

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "desc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.nama", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detail_sp_waiting($id_sp)
    {
        $this->db->select('b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, e.nim, e.nama as nama_mhs, 
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah ');
        $this->db->from('sp_mhs a');
        $this->db->join('sp_mhs_detail b', 'b.id_sp_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->where("a.id", $id_sp);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }

    // 28-12-2021 nilai gabungan
    public function get_detail_nilai_gabungan($id_mata_kuliah, $id_semester, $id_tahun_akademik, $id_prodi, $id_kelas) //$id_komponen_nilai
    {
        // $queryxx    = $this->db->query(" SELECT nama FROM master_komponen_nilai WHERE id = '$id_komponen_nilai' ");
        // if ($queryxx->num_rows() > 0) {
        //     $hasilrowxx = $queryxx->row();
        //     $nama_komponen    = $hasilrowxx->nama;
        // } else
        //     $nama_komponen = '';

        // 28-12-2021 ALUR BARU:
        // query data dari KRS. di perulangan hasil query, ambil data nilai tiap2 komponen. jika blm ada maka kasih default 0
        $sql = " SELECT a1.id as id_mhs, a1.nim, a1.nama as nama_mhs, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik,
                    g.nama as nama_prodi, h.nama as nama_jenjang, i.kode as kode_mk, i.nama as nama_mk, j.nama as nama_kelas, b.id as id_krs_detail  
                    FROM krs_mhs a INNER JOIN mahasiswa a1 ON a1.id = a.id_mhs
                    INNER JOIN krs_mhs_detail b ON b.id_krs_mhs = a.id
                    LEFT JOIN master_semester e ON e.id = a.id_semester
                    LEFT JOIN master_tahun_akademik f ON f.id = a.id_tahun_akademik
                    LEFT JOIN master_program_studi g ON g.id = a1.id_program_studi
                    LEFT JOIN master_jenjang_studi h ON h.id = g.id_jenjang_studi
                    LEFT JOIN master_mata_kuliah i ON i.id = b.id_mata_kuliah
                    LEFT JOIN master_kelas j ON j.id = a1.id_kelas
                    WHERE a.status_verifikasi = '1' AND a.id_semester = '$id_semester' 
                    AND a.id_tahun_akademik = '$id_tahun_akademik' AND b.id_mata_kuliah = '$id_mata_kuliah' 
                    AND a1.id_kelas = '$id_kelas' ORDER BY a1.nama ";

        $query = $this->db->query($sql);

        $data_mhs = array();
        if ($query->num_rows() > 0) {
            $hasil = $query->result();
            foreach ($hasil as $row) {
                $id_mhs = $row->id_mhs;
                $nim = $row->nim;
                $nama = $row->nama_mhs;
                $nama_semester = $row->nama_semester;
                $tahun = $row->tahun;
                $nama_tahun_akademik = $row->nama_tahun_akademik;
                $nama_prodi = $row->nama_prodi;
                $nama_jenjang = $row->nama_jenjang;
                $kode_mk = $row->kode_mk;
                $nama_mk = $row->nama_mk;
                $nama_kelas = $row->nama_kelas;
                $id_krs_detail = $row->id_krs_detail;

                // ambil data2 nilai berdasarkan komponen nilai
                $sqlxx = " SELECT id FROM master_komponen_nilai ORDER BY id ";
                $queryxx = $this->db->query($sqlxx);

                $data_nilai = array();
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->result();
                    foreach ($hasilxx as $rowxx) {
                        $sql2 = " SELECT a1.id, d.nilai_angka, d.id as id_nilai FROM master_komponen_nilai a1 LEFT JOIN khs_mhs_mk_detail d ON d.id_komponen_nilai = a1.id
                        LEFT JOIN khs_mhs_mk c ON c.id = d.id_khs_mk
                        LEFT JOIN khs_mhs a ON a.id = c.id_khs_mhs
                        LEFT JOIN mahasiswa b ON a.id_mhs = b.id
                        WHERE a.id_semester = '$id_semester' AND a.id_tahun_akademik = '$id_tahun_akademik' 
                        AND b.id_kelas = '$id_kelas'
                        AND c.id_mata_kuliah = '$id_mata_kuliah' AND b.id = '$id_mhs' AND a1.id = '$rowxx->id' ";

                        $query2 = $this->db->query($sql2);


                        if ($query2->num_rows() > 0) {
                            $hasil2 = $query2->result();
                            foreach ($hasil2 as $row2) {
                                $id_komponen_nilai = $row2->id;
                                $id_nilai = $row2->id_nilai;
                                $nilai_angka = $row2->nilai_angka;

                                $data_nilai[] = array(
                                    'id_komponen_nilai' => $id_komponen_nilai,
                                    'id_nilai' => $id_nilai,
                                    'nilai_angka' => $nilai_angka
                                );
                            }
                        } else {
                            $data_nilai[] = array(
                                'id_komponen_nilai' => $rowxx->id,
                                'id_nilai' => 0,
                                'nilai_angka' => 0
                            );
                        }
                    }
                }

                // ambil nilai akhir dan huruf mutu
                $sqlxx = " SELECT a.nilai_akhir, a.id_nilai_huruf, b.nama FROM khs_mhs_mk a LEFT JOIN master_nilai_huruf b ON a.id_nilai_huruf = b.id 
                          WHERE a.id_krs_mhs_detail = '$id_krs_detail' ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilrowxx = $queryxx->row();
                    $nilai_akhir    = $hasilrowxx->nilai_akhir;
                    $nilai_huruf    = $hasilrowxx->nama;
                } else {
                    $nilai_akhir = 0;
                    $nilai_huruf = '';
                }

                // header
                $data_mhs[] = array(
                    'id_mhs' => $id_mhs,
                    'nim' => $nim,
                    'nama' => $nama,
                    'nama_semester' => $nama_semester,
                    'tahun' => $tahun,
                    'nama_tahun_akademik' => $nama_tahun_akademik,
                    'nama_prodi' => $nama_prodi,
                    'nama_jenjang' => $nama_jenjang,
                    'kode_mk' => $kode_mk,
                    'nama_mk' => $nama_mk,
                    'nama_kelas' => $nama_kelas,
                    'id_krs_detail' => $id_krs_detail,
                    'nilai_akhir' => $nilai_akhir,
                    'nilai_huruf' => $nilai_huruf,
                    'data_nilai' => $data_nilai
                );

                //$data_nilai = array();
            }
        } else
            $data_mhs = '';

        return $data_mhs;

        // -------------------------- end alur baru ------------------------------------------------------------------------
    }

    // 29-12-2021
    // get data khs mhs
    public function get_data_khs_mhs($id_semester, $id_ta, $id_mhs)
    {
        $this->db->select('b.*, e.nama as nama_semester, f.tahun, f.nama as nama_tahun_akademik, 
                        c.nim, c.nama as nama_mhs, g.nama as nilai_huruf, d.nama as nama_mk ');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_nilai_huruf g', 'g.id = b.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa c', 'c.id = a.id_mhs', 'left');
        $this->db->join('master_mata_kuliah d', 'd.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_semester e', 'e.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik f', 'a.id_tahun_akademik = f.id', 'left');
        $this->db->where("a.id_mhs", $id_mhs);

        if ($id_semester != '0')
            $this->db->where("a.id_semester", $id_semester);
        if ($id_ta != '0')
            $this->db->where("a.id_tahun_akademik", $id_ta);

        $this->db->order_by("a.id_tahun_akademik", "asc");
        $this->db->order_by("a.id_semester", "asc");
        $this->db->order_by("d.kode", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_mhs_by_dosen_pa($id_dosen)
    {
        $sqlxx = " SELECT a.id, a.nim, a.nama FROM mahasiswa a INNER JOIN master_kelas b ON a.id_kelas = b.id
                   INNER JOIN dosen_pembimbing_akademik_mhs c ON c.id_kelas = b.id
                   INNER JOIN dosen d ON d.id = c.id_dosen WHERE d.id = '$id_dosen' ORDER BY a.nama ";
        $queryxx = $this->db->query($sqlxx);
        return $queryxx->result_array();
    }
}
