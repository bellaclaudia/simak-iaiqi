<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_user';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_grup_user',  //samakan dengan atribute name pada tags input
                'label' => 'Grup User',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            // [
            //     'field' => 'id_program_studi',  //samakan dengan atribute name pada tags input
            //     'label' => 'Program Studi',  // label yang kan ditampilkan pada pesan error
            //     'rules' => 'trim|required' //rules validasi
            // ],
            [
                'field' => 'username',  //samakan dengan atribute name pada tags input
                'label' => 'username',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'userpass',
                'label' => 'Password',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*, b.nama_grup, c.nama as prodi');
        $this->db->from('master_user a');
        $this->db->join('master_grup_user b', 'b.id = a.id_grup_user', 'left');
        $this->db->join('master_program_studi c', 'c.id = a.id_program_studi', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getGrupUser()
    {
        return $this->db->get('master_grup_user')->result_array();
    }

    public function getProgramStudi()
    {
        return $this->db->get('master_program_studi')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_grup_user" => $this->input->post('id_grup_user'),
            "id_program_studi" => $this->input->post('id_program_studi'),
            "username" => $this->input->post('username'),
            "userpass" => md5($this->input->post('userpass')),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);

        // modif 08-06-2022
        $id_usernya = $this->db->insert_id();

        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['overwrite'] = false;
        $config['max_size']             = 5120;
        //$config['encrypt_name']         = TRUE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'USERfoto_' . $id_usernya . '_' . $timestamp;

        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file_foto']['tmp_name'])) {

            $up = $this->upload->do_upload('file_foto');
            if ($up) {
                $upl = $this->upload->data();
                $data['file_foto'] = $upl['file_name'];
                $file_foto = $data['file_foto'];
            } else {
                $error = 1;
            }
        } else
            $file_foto = '';

        //
        $data = array(
            "file_foto" => $file_foto
        );

        return $this->db->update($this->table, $data, array('id' => $id_usernya));
    }

    // 14-06-2022
    public function get_user_by_id($id)
    {
        $this->db->select('a.id_grup_user, a.id_program_studi, a.username, a.file_foto, b.nama as prodi, c.nama_grup');
        $this->db->from('master_user a');
        $this->db->join('master_program_studi b', 'b.id = a.id_program_studi', 'left');
        $this->db->join('master_grup_user c', 'c.id = a.id_grup_user', 'left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    // function jumlah_data(){

    //     return $this->db->get($this->table)->num_rows();
    // }

    // function data($number,$offset){
    //     $this->db->select('a.*, b.nama_grup, c.nama as prodi');
    //     $this->db->from('master_user a');
    //     $this->db->join('master_grup_user b', 'b.id = a.id_grup_user', 'left');
    //     $this->db->join('master_program_studi c', 'c.id = a.id_program_studi', 'left');
    //     $this->db->order_by("id", "desc");
    //     $query = $this->db->get($this->table,$number,$offset);   
    //     return $query->result();    
    // }
}
