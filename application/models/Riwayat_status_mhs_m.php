<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_status_mhs_m extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }

    private $table = 'riwayat_status_mhs';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_mhs',  //samakan dengan atribute name pada tags input
                'label' => 'Jenis Mata Kuliah',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'id_semester',
                'label' => 'Semester',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_tahun_akademik',
                'label' => 'Tahun Akademik',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_status_mhs',
                'label' => 'Status Mahasiswa',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.*, b.nama as nama_mhs, c.nama as semester, d.nama as tahun_akademik, e.nama as status_mhs');
        $this->db->from('riwayat_status_mhs a');
        $this->db->join('mahasiswa b','b.id = a.id_mhs','left');
        $this->db->join('master_semester c','c.id = a.id_semester','left');
        $this->db->join('master_tahun_akademik d','d.id = a.id_tahun_akademik','left');
        $this->db->join('master_status_mhs e','e.id = a.id_status_mhs','left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getMahasiswa()
    {
        return $this->db->get('mahasiswa')->result_array();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getSemester()
    {
        return $this->db->get('master_semester')->result_array();
    }

    public function getStatusMhs()
    {
        return $this->db->get('master_status_mhs')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_mhs" => $this->input->post('id_mhs'),
            "id_semester" => $this->input->post('id_semester'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_status_mhs" => $this->input->post('id_status_mhs'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $data = array(
           "id_mhs" => $this->input->post('id_mhs'),
            "id_semester" => $this->input->post('id_semester'),
            "id_tahun_akademik" => $this->input->post('id_tahun_akademik'),
            "id_status_mhs" => $this->input->post('id_status_mhs'),
            "tgl_update" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}