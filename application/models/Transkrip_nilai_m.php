<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transkrip_nilai_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //menampilkan semua data 
    public function getAll($id_mhs)
    {
        $this->db->select('c.kode as kode_matkul, c.nama as matkul, c.sks, d.nama as nilai, h.bobot, f.*, g.nama as prodi');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_mata_kuliah c', 'c.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf d', 'd.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai h', 'd.id = h.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa f', 'f.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi g', 'g.id = f.id_program_studi', 'left');
        $this->db->where("a.id_mhs", $id_mhs);
        $this->db->order_by("a.id", "desc");

        $query = $this->db->get();
        return $query->result();
    }

    public function getTahunAkademik()
    {
        return $this->db->get('master_tahun_akademik')->result_array();
    }

    public function getPredikatKelulusan()
    {
        return $this->db->get('master_predikat_kelulusan')->result_array();
    }

    public function getMahasiswa()
    {
        return $this->db->get('mahasiswa')->result_array();
    }

    public function getPerMhs($id)
    {
        $this->db->select('c.kode as kode_matkul, c.nama as matkul, c.sks, d.nama as nilai, h.bobot, f.*, g.nama as prodi');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'a.id = b.id_khs_mhs', 'left');
        $this->db->join('master_mata_kuliah c', 'c.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf d', 'd.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai h', 'd.id = h.id_nilai_huruf', 'left');
        $this->db->join('mahasiswa f', 'f.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi g', 'g.id = f.id_program_studi', 'left');
        $this->db->order_by("a.id", "desc");
        $this->db->where('f.id', $id);

        // if ($id_tahun_akademik != '0')
        //     $this->db->where("a.id_tahun_akademik", $id_tahun_akademik);
        $query = $this->db->get();
        return $query->result();
    }

    // 16-01-2022
    public function get_detail_transkrip($id_mhs)
    {
        $this->db->select('a.id_semester, a.id_tahun_akademik, a.id_mhs, b.*, c.nama as nama_semester, d.tahun, d.nama as nama_tahun_akademik, 
        e.nim, e.nama as nama_mhs, e.id_program_studi, e.tempat_lahir, e.tgl_lahir, e.nomor_ijazah, e.tgl_lulus, e.nomor_transkrip, e.judul_skripsi,
        f.nama as nama_prodi, g.nama as nama_jenjang, h.kode, h.nama as nama_mata_kuliah, h.sks, i.nama as nilai_huruf, j.bobot ');
        $this->db->from('khs_mhs a');
        $this->db->join('khs_mhs_mk b', 'b.id_khs_mhs = a.id', 'left');
        $this->db->join('master_mata_kuliah h', 'h.id = b.id_mata_kuliah', 'left');
        $this->db->join('master_nilai_huruf i', 'i.id = b.id_nilai_huruf', 'left');
        $this->db->join('master_bobot_nilai j', 'i.id = j.id_nilai_huruf', 'left');
        $this->db->join('master_semester c', 'c.id = a.id_semester', 'left');
        $this->db->join('master_tahun_akademik d', 'd.id = a.id_tahun_akademik', 'left');
        $this->db->join('mahasiswa e', 'e.id = a.id_mhs', 'left');
        $this->db->join('master_program_studi f', 'f.id = e.id_program_studi', 'left');
        $this->db->join('master_jenjang_studi g', 'g.id = f.id_jenjang_studi', 'left');
        $this->db->where("e.id", $id_mhs);

        $this->db->order_by("b.id", "asc");
        $query = $this->db->get();
        return $query;
        //return $query->result();
    }
}
