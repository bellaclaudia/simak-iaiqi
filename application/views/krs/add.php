<script>
  function get_mhs(id_prodi) {
    var id_ta = $("#select2-1").val();
    var id_sem = $("#select2-2").val();
    $.ajax({
      type: 'post',
      url: '<?= base_url('krs/get_mhs_by_prodi_verifiedkeu'); ?>',
      data: 'id_prodi=' + id_prodi + '&id_ta=' + id_ta + '&id_sem=' + id_sem,
      success: function(response) {
        $("#mhsnya").html(response);
      }
    });
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('krs'); ?>">Data KRS</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <form id="FrmAddKrs" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta" onchange="get_mhs($('#select2-4').val());">
                  <option value="0" selected disabled>Pilih Tahun Akademik</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester" onchange="get_mhs($('#select2-4').val());">
                  <option value="0" selected disabled>Pilih Semester</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_semester') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_prodi" onchange="get_mhs(this.value);">
                  <option value="0" selected disabled>Pilih Prodi</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_prodi') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mahasiswa</label>
              <div class="col-md-4">
                <label id="mhsnya">
                  <select class="form-control select2-single" id="select2-5" name="id_mhs">
                    <option value="0" selected disabled>Pilih Mahasiswa</option>
                    <?php foreach ($data_mhs as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['nim'] . " - " . $k['nama'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </label>
                <small class="text-danger">
                  <?php echo form_error('id_mhs') ?>
                </small>
              </div>
            </div>

            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Lanjut Pilih Mata Kuliah</button>&nbsp;
              <a href="<?= base_url('krs'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>