<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('krs'); ?>">Data KRS</a></li>
    <li class="breadcrumb-item active">Detail Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Detail KRS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">

            Tahun Akademik : <b><?= $thn_akademiknya ?></b> <br>
            Semester : <b><?= $nama_semester ?></b> <br>
            Program Studi : <b><?= $prodinya ?></b> <br>
            Mahasiswa : <b><?= $mhsnya ?></b> <br><br>
            <a href="<?= base_url('krs'); ?>" class="btn btn-success btn-circle">Kembali Ke Halaman List KRS</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Mata Kuliah</th>
                  <th>Nama Mata Kuliah</th>
                  <th>SKS</th>
                  <th>Status (Baru/Mengulang)</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($detail_krs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->kode ?></td>
                    <td><?= $row->nama_mata_kuliah ?></td>
                    <td style="text-align: right;"><?= $row->sks ?></td>
                    <td>
                      <?php
                      if ($row->status == '1')
                        echo "Baru";
                      else
                        echo "Mengulang";
                      ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>