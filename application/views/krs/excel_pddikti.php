<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=pddikti_krs.xls");
header("Cache-control: public");
?>
<style>
  .allcen {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .allcen2 {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .str {
    mso-number-format: \@;
  }
</style>

<head>
  <meta charset="utf-8" />
  <title>SIMAK IAIQI | Data KRS</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <style>
    body {
      font-family: verdana, arial, sans-serif;
      font-size: 14px;
      line-height: 20px;
      font-weight: 400;
      -webkit-font-smoothing: antialiased;
      font-smoothing: antialiased;
    }

    table.gridtable {
      font-family: verdana, arial, sans-serif;
      font-size: 11px;
      width: 100%;
      color: #333333;
      border-width: 1px;
      border-color: #e9e9e9;
      border-collapse: collapse;
    }

    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      font-size: 12px;
      border-style: solid;
      font-weight: 900;
      color: #ffffff;
      border-color: #e9e9e9;
      background: #ea6153;
    }

    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #e9e9e9;
      background-color: #ffffff;
    }
  </style>
</head>

<body>
  <table border="1" width="100%" class="gridtable">
    <thead style="background-color: blue">
      <tr>
        <th class="allcen center bold">NIM</th>
        <th class="allcen center bold">Nama</th>
        <th class="allcen center bold">Semester</th>
        <th class="allcen center bold">Kode Matakuliah</th>
        <th class="allcen center bold">Nama Matakuliah</th>
        <th class="allcen center bold">Kelas</th>
        <th class="allcen center bold">Kode Prodi</th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1; ?>
      <?php foreach ($data_krs_excel as $row) :

        $kode_prodinya = (string)$row->kode_prodi;
      ?>
        <tr>
          <td><?= $row->nim ?></td>
          <td><?= $row->nama_mhs ?></td>
          <td><?= $row->tahun . $row->jenis_semester ?></td>
          <td><?= $row->kode_mk ?></td>
          <td><?= $row->nama_mk ?></td>
          <td><?= "[" . $row->kode_kelas . "] " . $row->nama_kelas ?></td>
          <?php
          echo    "<td>=\"$kode_prodinya\"</td>";
          ?>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>