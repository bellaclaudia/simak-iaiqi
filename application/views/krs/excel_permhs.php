<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=krs_" . $nim . ".xls");
header("Cache-control: public");
?>
<h4 style="text-align: center">INSTITUT AGAMA ISLAM AL QUR'AN AL ITTIFAQIAH<br>INDRALAYA OGAN ILIR SUMATERA SELATAN</h4>

<h4 style="text-align: center">FORMULIR RENCANA STUDI (FRS)</h4>
<table border="0" width="100%">
  <tr>
    <td>Nama Mahasiswa</td>
    <td>: <?= $nama_mhs ?></td>
    <td>&nbsp;</td>
    <td>NIM</td>
    <td>: <?= $nim ?></td>
  </tr>
  <tr>
    <td>Fak / Prodi</td>
    <td>: <?= $nama_fakultas ?></td>
    <td>&nbsp;</td>
    <td>Program</td>
    <td>: <?= $prodinya ?></td>
  </tr>
  <tr>
    <td>SMT / TH</td>
    <td>: <?= $nama_semester . " / " . $thn_akademiknya ?></td>
    <td>&nbsp;</td>
    <td>Pemb. Akademik</td>
    <td>: <?= $nama_dosen ?></td>
  </tr>
</table><br>

<table border="1" width="100%">
  <tr>
    <th rowspan="2">No</th>
    <th rowspan="2">Kode MK</th>
    <th rowspan="2">Mata Kuliah</th>
    <th colspan="3">Status</th>
    <th colspan="2">Kode</th>
  </tr>
  <tr>
    <th>Baru</th>
    <th>Ulang</th>
    <th>Nilai</th>
    <th>SKS</th>
    <th>Ket</th>
  </tr>
  <?php $i = 1;
  $jumsks = 0; ?>
  <?php foreach ($detail_krs as $row) : ?>
    <tr>
      <td style="text-align: right;"><?= $i++; ?></td>
      <td><?= $row->kode ?></td>
      <td><?= $row->nama_mata_kuliah ?></td>
      <td style="text-align: center;"><?php if ($row->status == '1') echo "V";
                                      else echo "&nbsp;"; ?></td>
      <td style="text-align: center;"><?php if ($row->status == '2') echo "V";
                                      else echo "&nbsp;"; ?></td>
      <td>&nbsp;</td>
      <td style="text-align: right;"><?= $row->sks ?></td>
      <td>&nbsp;</td>

    </tr>
  <?php
    $jumsks += $row->sks;
  endforeach; ?>
  <tr>
    <td colspan="3">Jumlah kredit yang didaftar</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td style="text-align: right;"><?= $jumsks ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">Jumlah kredit yang diperoleh</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td style="text-align: right;"><?= $jumsks ?></td>
    <td>&nbsp;</td>
  </tr>
</table><br><br>
<!-- <p style="text-align: right;">Indralaya, <?= date("d-m-Y") ?><br> -->
<table border="0" align="right">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Indralaya, <?= date("d-m-Y") ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <!-- <td><b>Catatan:</b>
      <br>Dibuat rangkap 4 (empat)<br>1. Mahasiswa (Putih)<br>2. Penasihat Akademik (Merah)<br>3. Ketua Jurusan (Biru)<br>4. Kepala Tata Usaha (Hijau)
      <br><br>Mahasiswa bertanggung jawab atas ketelitian pengisian ini
    </td> -->
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>
      Mengetahui/menyetujui,<br><br><br><br><br>(....................................)
    </td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>
      Tanda tangan Mahasiswa<br><br><br><br><br>(....................................)
    </td>
  </tr>

</table>