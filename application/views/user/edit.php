<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('master_user'); ?>">Data User</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <form id="FrmEditUser" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="is_simpan" value="1">
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Grup User</label>
              <div class="col-md-9">
                <input class="form-control" type="text" name="grup_user" readonly value="<?= $data_user->nama_grup; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Program Studi</label>
              <div class="col-md-9">
                <input class="form-control" type="text" name="prodi" readonly value="<?= $data_user->prodi; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Username</label>
              <div class="col-md-9">
                <input class="form-control" type="text" name="uname" readonly value="<?= $data_user->username; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Password</label>
              <div class="col-md-9">
                <input class="form-control" type="password" name="passwdnya" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Foto</label>
              <div class="col-md-9">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span><br>
                <img src="<?= base_url('uploads/' . $data_user->file_foto) ?>" alt="" width="200" class="img-thumbnail rounded-circle">
              </div>
            </div>


            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('master_user'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>