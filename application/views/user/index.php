<script>
  window.onload = function() {
    document.getElementById("username").value = "";
    document.getElementById("userpass").value = "";
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data User</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data User
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th>No</th>
                <th>Grup User</th>
                <th>Program Studi</th>
                <th>Username</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_user as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama_grup ?></td>
                  <td><?= $row->prodi ?></td>
                  <td><?= $row->username ?></td>
                  <td style="text-align: center;">
                    <a href="<?= base_url('master_user/edit/' . $row->id); ?>" class="btn btn-success btn-circle" title="Edit Data"><i class="fa fa-edit"></i></a>
                    <?php if ($row->id_grup_user != 3 && $row->id_grup_user != 4 && $row->id_grup_user != 5) { ?>
                      <a href="<?php echo site_url('master_user/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data User <?= $row->username; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    <?php } ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddUser', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Grup User</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_grup_user" required>
                        <option value="0" selected disabled>Pilih Data Grup User</option>
                        <?php foreach ($grup_user as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama_grup']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Program Studi</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-2" name="id_program_studi" required>
                        <option value="0" selected disabled>Pilih Data Program Studi</option>
                        <?php foreach ($prodi as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Username</label>
                    <div class="col-md-9">
                      <input autocomplete="off" class="form-control" type="text" name="username" id="username" value="" placeholder="Isi Username" required>
                      <small class="text-danger">
                        <?php echo form_error('username') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Password</label>
                    <div class="col-md-9">
                      <input class="form-control" type="password" name="userpass" id="userpass" value="" placeholder="Isi Password" required>
                      <small class="text-danger">
                        <?php echo form_error('userpass') ?>
                      </small>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Foto</label>
                    <div class="col-md-9">
                      <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                      <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</main>
</div>