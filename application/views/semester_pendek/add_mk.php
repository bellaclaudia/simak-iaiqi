<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('krs'); ?>">Data Semester Pendek</a></li>
    <li class="breadcrumb-item active">Tambah Data Detail Mata Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Detail Mata Kuliah
        </div>
        <div class="card-body">

          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tahun Akademik</label>
            <div class="col-md-4">
              <b><?= $nama_ta ?></b>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Semester</label>
            <div class="col-md-4">
              <b><?= $nama_semester ?></b>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Program Studi</label>
            <div class="col-md-4">
              <b><?= $nama_prodi ?></b>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Mahasiswa</label>
            <div class="col-md-4">
              <b><?= $mhs ?></b>
            </div>
          </div>
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Mata Kuliah</button><br><br>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode MK</th>
                <th>Nama MK</th>
                <th>SKS</th>
                <!-- <th>Jadwal Kuliah</th> -->
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_temp_mk as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->kode ?></td>
                  <td><?= $row->nama ?></td>
                  <td style="text-align: right;"><?= $row->sks ?></td>
                  <!-- <td><?php //$row->hari . ", " . $row->jam_mulai . " - " . $row->jam_selesai 
                            ?></td> -->
                  <td>
                    <a href="<?php echo site_url('semester_pendek/hapus_temp_mk/' . $row->id . '/' . $id_ta . '/' . $id_semester . '/' . $id_prodi . '/' . $id_mhs); ?>" onclick="return confirm('Apakah Anda Yakin Menghapus Data Mata Kuliah <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddTempMk', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Mata Kuliah</label>
                    <div class="col-md-9">
                      <input type="hidden" name="id_ta" value="<?= $id_ta ?>">
                      <input type="hidden" name="id_semester" value="<?= $id_semester ?>">
                      <input type="hidden" name="id_prodi" value="<?= $id_prodi ?>">
                      <input type="hidden" name="id_mhs" value="<?= $id_mhs ?>">
                      <input type="hidden" name="is_simpan_mk" value="1">

                      <select class="form-control select2-single" id="select2-1" name="id_mk_jadwal">
                        <option value="0" selected disabled>Pilih Mata Kuliah</option>
                        <?php
                        foreach ($data_mk as $row1) { ?>
                          <!-- <option value="<?php //echo $row1->id . "," . $row1->id_jadwal 
                                              ?>"><?php //echo "[" . $row1->kode . "] " . $row1->nama . " (Jadwal Kuliah: " . $row1->hari . ", " . $row1->jam_mulai . " - " . $row1->jam_selesai . ")"  
                                                  ?></option> -->
                          <option value="<?php echo $row1->id ?>"><?php echo "[" . $row1->kode . "] " . $row1->nama  ?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button name="submit_krs" value="1" class="btn btn-sm btn-success btn-ladda" data-style="expand-right" onclick="return confirm('Data SP akan disimpan dan selanjutnya menunggu persetujuan dosen PA. Anda yakin akan melanjutkan?')">Simpan Data SP</button>&nbsp;
          <a href="<?= base_url('semester_pendek'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>