<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Biaya Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Biaya Kuliah
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>

          <b>Filter Pencarian</b>
          <?php
          $attributes = array('id' => 'FrmPencarianBiaya', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tahun Akademik</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_ta">
                <option value="0">- All -</option>
                <?php foreach ($tahun_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Program Studi</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-2" name="id_prodi">
                <option value="0">- All -</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
          </form>
          <hr>

          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Tahun Akademik</th>
                <th>Program Studi</th>
                <th>Kode</th>
                <th>Deskripsi</th>
                <th>Jenis</th>
                <th>Nominal (Rp.)</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_biaya_kuliah as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->tahun_akademik ?></td>
                  <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                  <td><?= $row->kode ?></td>
                  <td><?= $row->nama ?></td>
                  <td><?= $row->jenis ?></td>
                  <td align="right"><?= number_format($row->nominal) ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('master_biaya_kuliah/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Biaya Kuliah <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBiayaKuliah', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
                    <div class="col-md-9">
                      <select class="form-control" name="id_tahun_akademik" required>
                        <option value="0" selected disabled>Pilih Data Tahun Akademik</option>
                        <?php foreach ($tahun_akademik as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Program Studi</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-4" name="id_program_studi">
                        <option value="" selected disabled>Pilih Program Studi</option>
                        <?php foreach ($data_prodi as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Kode</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="kode" placeholder="Isi Kode" required>
                      <small class="text-danger">
                        <?php echo form_error('kode') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Deskripsi</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama" placeholder="Isi Deskripsi" required>
                      <small class="text-danger">
                        <?php echo form_error('nama') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Jenis</label>
                    <div class="col-md-9">
                      <select class="form-control" name="jenis" required>
                        <option value="0" selected disabled>Pilih Data Jenis</option>
                        <option value="REGULER">REGULER</option>
                        <option value="SP">SP</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nominal</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="nominal" placeholder="Isi Nominal" required>
                      <small class="text-danger">
                        <?php echo form_error('nominal') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_biaya_kuliah as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('master_biaya_kuliah/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control" name="id_tahun_akademik" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_tahun_akademik)) {
                              $lv = $row->id_tahun_akademik;
                            }
                            foreach ($tahun_akademik as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Program Studi</label>
                        <div class="col-md-9">
                          <select class="form-control" name="id_program_studi">
                            <option value="" selected disabled>Pilih Program Studi</option>
                            <?php foreach ($data_prodi as $k) : ?>
                              <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $row->id_program_studi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Kode</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="kode" placeholder="Isi Kode" required value="<?= $row->kode; ?>">
                          <small class="text-danger">
                            <?php echo form_error('kode') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Deskripsi</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama" placeholder="Isi Deskripsi" required value="<?= $row->nama; ?>">
                          <small class="text-danger">
                            <?php echo form_error('nama') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Jenis</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-4" name="jenis" required>
                            <?php
                            $lv = '';
                            if (isset($row->jenis)) {
                              $lv = $row->jenis;
                            }
                            ?>
                            <option <?php if ($row->nama == "REGULER") {
                                      echo "selected";
                                    } ?>>REGULER</option>
                            <option <?php if ($row->nama == "SP") {
                                      echo "selected";
                                    } ?>>SP</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nominal</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="nominal" placeholder="Isi Nominal" required value="<?= $row->nominal; ?>">
                          <small class="text-danger">
                            <?php echo form_error('nominal') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>