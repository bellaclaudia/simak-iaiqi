<script type="text/javascript">
  window.print();
</script>

<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 12px;
    border-collapse: collapse;
  }

  .judulnya {
    background-color: #DDD;
  }

  h3 {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 14px;
  }

  body {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 12px;
  }
</style>
<body>
<center>
  <img src="<?= base_url(); ?>assets/img/kop.png" style="width: 100%">
  <h3><u>TRANSKRIP AKADEMIK</u></h3>
  <b>Nomor: <?= $nomor_transkrip ?></b>
</center>
<br>

<table border="0" width="80%" align="center">
  <tr>
    <td>Nama</td>
    <td>: <?= $nama_mhs ?></td>
  </tr>
  <tr>
    <td>NIM/NIRM/NIRL</td>
    <td>: <?= $nim ?></td>
  </tr>
  <tr>
    <td>Tempat Dan Tanggal Lahir</td>
    <td>: <?= $tempat_lahir ?>, <?= $tgl_lahir ?></td>
  </tr>
  <tr>
    <td>Nomor Ijazah</td>
    <td>: <?= $nomor_ijazah ?></td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>: <?= $prodinya ?></td>
  </tr>
  <tr>
    <td>Tanggal, Bulan, Dan Tahun Lulus</td>
    <td>: <?= $tgl_lulus ?></td>
  </tr>
  <tr>
    <td>Nomor Keputusan Pendirian PTKI</td>
    <td>: &nbsp;</td>
  </tr>
  <tr>
    <td>Nomor SK BAN-PT (Akreditasi Perguruan Tinggi)</td>
    <td>: &nbsp;</td>
  </tr>
  <tr>
    <td>Nomor SK BAN-PT (Akreditasi Program Studi)</td>
    <td>: &nbsp;</td>
  </tr>
</table><br><br>

<table border="1" width="100%">
  <thead>
    <tr>
      <th>NO</th>
      <!-- <th>Tahun Akademik</th>
      <th>Semester</th>
      <th>Mahasiswa</th> -->
      <th>KODE MATA KULIAH</th>
      <th>NAMA MATA KULIAH</th>
      <th>SKS</th>
      <th>NILAI</th>
      <th>ANGKA KREDIT</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1;
    $total_sks = 0;
    $total_angka_bobot = 0; ?>
    <?php foreach ($detail_transkrip as $row) :
      $total_sks += $row->sks;
      $angka_bobot = $row->bobot * $row->sks;
      $total_angka_bobot += $angka_bobot;
    ?>
      <tr>
        <td style="text-align: center;"><?= $i++; ?></td>
        <!-- <td><?php //echo $row->nama_tahun_akademik 
                  ?></td>
        <td><?php //echo $row->nama_semester 
            ?></td>
        <td><?php //echo $row->nim . " - " . $row->nama_mhs 
            ?></td> -->
        <td width="10%"><?= $row->kode ?></td>
        <td width="60%"><?= $row->nama_mata_kuliah ?></td>
        <td width="10%" style="text-align: center;"><?= $row->sks ?></td>
        <td width="10%" style="text-align: center;"><?= $row->nilai_huruf ?></td>
        <td width="10%" style="text-align: center;"><?= $row->bobot * $row->sks ?></td>
      </tr>
    <?php endforeach;
    $ipk = round($total_angka_bobot / $total_sks, 2);

    ?>
    <tr>
      <td colspan="3">Jumlah</td>
      <td style="text-align: center;"><?= $total_sks ?></td>
      <td>&nbsp;</td>
      <td style="text-align: center;"><?= $total_angka_bobot ?></td>
    </tr>
    <tr>
      <td colspan="6">Index Prestasi Kumulatif&nbsp;&nbsp; : <?= $ipk ?></td>
    </tr>
    <tr>
      <td colspan="6">Yudisium &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </td>
    </tr>
    <tr>
      <td colspan="2">Judul Skripsi :</td>
      <td colspan="4"><?= $judul_skripsi ?></td>
    </tr>
  </tbody>
</table><br><br>

<table border="0" width="100%">
  <tr>
    <td width="30%">Ketua</td>
    <td width="40%">&nbsp;</td>
    <td width="30%">Indralaya, <?= date('d-m-Y') ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Wakil Ketua I,</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><b>H. Mukhyidin, M.A.</b></td>
    <td>&nbsp;</td>
    <td><b>Dr. Muyasaroh, M.Pd.I</b></td>
  </tr>
</table><br><br><br>
<img src="<?= base_url(); ?>assets/img/kopbwh.png" style="width: 100%;">
</body>