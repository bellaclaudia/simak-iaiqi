<script>
  function cek_input() {
    //var id_tahun_akademik = $("#select2-1").val();
    var id_mhs = $("#select2-2").val();

    // if (id_tahun_akademik == '0') {
    //   alert("Tahun akademik harus dipilih..!");
    //   return false;
    // }
    if (id_mhs == '0') {
      alert("Mahasiswa harus dipilih..!");
      return false;
    }
  }

  function popup_cetak() {
    //var id_ta = $('#select2-1').val();
    var id_mhs = $('#select2-2').val();

    // if (id_ta == '0') {
    //   alert("Tahun akademik harus dipilih..!");
    //   return false;
    // } else if (id_mhs == '0') {
    if (id_mhs == '0') {
      alert("Mahasiswa harus dipilih..!");
      return false;
    } else {
      window.open("<?= base_url(); ?>transkrip_nilai/cetak_transkrip_html/" + id_mhs, "PopupWindow", "width=950,height=600,scrollbars=yes,resizable=no");
      return false;
    }

  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Transkrip Nilai</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Transkrip Nilai
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianAlumni', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Mahasiswa</label>
              <div class="col-md-4">
                <input type="hidden" name="is_cari" value="1">
                <select class="form-control select2-single" id="select2-2" name="id_mhs">
                  <option value="0">- Pilih -</option>
                  <?php foreach ($mhs as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_mhs) { ?>selected<?php } ?>><?= $k['nim']; ?> | <?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_mhs') ?>
                </small>
              </div>
            </div>
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" onclick="return cek_input();">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="button" name="cetak_html" onclick="popup_cetak();">Cetak Transkrip</button>&nbsp;
            </form>

            <?php if ($is_cari == '1') { ?>
              <hr>
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Mata Kuliah</th>
                    <th>Nama Mata Kuliah</th>
                    <th>SKS</th>
                    <th>Nilai</th>
                    <th>Angka Kredit</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php $i = 1; ?>
                    <?php foreach ($data_transkrip_nilai as $row) : ?>
                      <td><?= $i++; ?></td>
                      <td><?= $row->kode_matkul ?></td>
                      <td><?= $row->matkul ?></td>
                      <td style="text-align: right;"><?= $row->sks ?></td>
                      <td><?= $row->nilai ?></td>
                      <td style="text-align: right;"><?= $row->bobot * $row->sks ?></td>
                  </tr>
                <?php endforeach; ?>
                </tr>
                </tbody>
              </table>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>