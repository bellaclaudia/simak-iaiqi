<!DOCTYPE html>
<html lang="en">
<head>
    <title>SIMAK IAIQI | Transkrip Nilai</title>
</head>
<style type="text/css">
    body {
        font-family: "Arial";
        font-size: 12px;
        padding: 1%;
    }
    table {
      border-collapse: collapse;
    }
    td, th {
      border: 1px solid black;
    }
    .no-border td {
      border: 1px solid white !important;
      padding-bottom: 10px;
    }
    th {
        padding: 2px;
    }
    td {
        padding-left: 10px;
        padding-top: 2px;
        padding-bottom: 2px;
    }
    h2 {
        letter-spacing: 5px;
    }
    .bold {
        font-weight: 700;
    }
    .center {
        padding-left: 0;
        text-align: center;
    }
    .mb5 {
        margin-bottom: 5px;
    }
    .m0 {
        margin: 0;
    }
    .mb10 {
        margin-bottom: 10px;
    }
    .ml10 {
        margin-left: 10px;
    }
    .mb20 {
        margin-bottom: 20px;
    }
    .mr20 {
        margin-right: 20px;
    }
    .mr50 {
        margin-right: 50px;
    }
</style>
<body>
<table class="no-border" border="1">
	<tr>
		<td width="50%" align="center"><h3>YAYASAN ISLAM AL ITTIFAQIAH</h3><h2>INSTITUT AGAMA ISLAM AL ITTIFAQIAH (IAIQI)</h2><h4>INDRALAYA OGAN ILIR SUMATERA SELATANJL. LINTAS TIMUR KM. 36 INDRALAYA OGAN ILIR SUMATERA SELATAN TELP. 0711-580793</h4></td>
		<td width="100%" align="center"><img src="<?= base_url(); ?>assets/img/logo.jpeg" style="width: 100px"></td>
	</tr>
</table>
<h2 style="text-align: center">
    <b><u>TRANSKRIP AKADEMIK</u></b>
</h2>
<table class="no-border mt10" border="1">
    <?php foreach ($data_transkrip_nilai as $row) : ?>
    <tr>
        <td width="40%">Nama</td>
        <td width="1%">: </td>
        <td width="50%"><?= $row->nama ?></td>
    </tr>
    <tr>
        <td>NIM/NIRM/NIRL</td>
        <td>: </td>
        <td><?= $row->nim ?></td>
    </tr>
    <tr>
        <td>Tempat dan Tanggal Lahir</td>
        <td>:</td>
        <td><?= $row->tempat_lahir ?>, <?= $row->tgl_lahir ?></td>
    </tr>
    <tr>
        <td>Nomor Ijasah</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td><?= $row->prodi ?></td>
    </tr>
    <tr>
        <td>Tanggal, Bulan dan Tahun Lulus</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td>Nomor Keputusan Pendirian PTKI</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td>Nomor SK-BAN-PT (Akreditasi Perguruan Tinggi)</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td>Nomor SK-BAN-PT (Akreditasi Program Studi)</td>
        <td>:</td>
        <td></td>
    </tr>
    <?php endforeach; ?>
</table><br><br>
<table border="1" width="100%" class="gridtable">
  	<thead>
		<tr>
          <th>No.</th>
          <th>Kode Mata Kuliah</th>
          <th>Nama Mata Kuliah</th>
          <th>SKS</th>
          <th>Nilai</th>
          <th>Angka Kredit</th>
        </tr>
        <tr>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
        </tr>
	</thead>
	<tbody>
		<tr>
			<?php $i = 1; ?>
            <?php foreach ($data_transkrip_nilai as $row) : ?>
            <?php $sks= 0; $sks += $row->sks; $bobot = 0; $bobot += $row->bobot*$row->sks; ?>
            
    		<td><?= $i++; ?></td>
            <td><?= $row->kode_matkul ?></td>
            <td><?= $row->matkul ?></td>
            <td align="center"><?= $row->sks ?></td>
            <td align="center"><?= $row->nilai ?></td>
            <td align="center"><?= $row->bobot*$row->sks ?></td>
	    </tr>
        <tr>
            <td colspan="3">Total</td>
            <td align="center"><?= $sks; ?></td>
            <td></td>
            <td align="center"><?= $bobot; ?></td>
        </tr>
        <?php
            function terbilang($i){
                $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
                if ($i < 12) return " " . $huruf[$i];
                  elseif ($i < 20) return terbilang($i - 10) . " belas";
                  elseif ($i < 100) return terbilang($i / 10) . " puluh" . terbilang($i % 10);
                  elseif ($i < 200) return " seratus" . terbilang($i - 100);
                  elseif ($i < 1000) return terbilang($i / 100) . " ratus" . terbilang($i % 100);
                  elseif ($i < 2000) return " seribu" . terbilang($i - 1000);
                  elseif ($i < 1000000) return terbilang($i / 1000) . " ribu" . terbilang($i % 1000);
                  elseif ($i < 1000000000) return terbilang($i / 1000000) . " juta" . terbilang($i % 1000000); }
                  $format = number_format($bobot/$sks,0,".", ",");
                    $terbilang = ucwords(terbilang($bobot/$sks));
            ?>
        <tr>
            <td colspan="2">Indeks Prestasi Kumulatif </td>
            <td colspan="4"><?= $format ?> ( <?= $terbilang; ?> )</td>
        </tr>
        <tr>
            <td colspan="2">Yudisium </td>
            
            <td colspan="4">
                <?php foreach ($predikat as $pr) : ?>
                <?php
                if($format >= $pr['ipk_batas_awal'] AND $format <= $pr['ipk_batas_akhir']){
                    echo $pr['nama_predikat'];
                }
                ?>
                <?php endforeach; ?>
            </td>
            
        </tr>
        <?php endforeach; ?>
		</tr>
	</tbody>
</table><br><br><br><br>
<table class="no-border" border="1">
    <tr>
        <td></td>
        <td width="50%"></td>
        <td width="50%"><center>Indralaya, <?php echo date('d M Y') ?></center></td>
    </tr>
    <tr>
        <td></td>
        <td><center>Ketua,<br><br><br><br><br></center></td>
        <td><center>Wakil Ketua I,<br><br><br><br><br></center></td>
    </tr>
    <tr>
        <td></td>
        <td><center><b>H. Mukhyidin, M.A.</b></center></td>
        <td><center><b>Dr. Hj. Muyasaroh, M.Pd.I.</b></center></td>
    </tr>
    <tr>
        <td></td>
        <td><center>NIY. 2008 09 11 018</center></td>
        <td><center>NIDN. 2107056601</center></td>
    </tr>
</table>
<script type="text/javascript">
        window.print();
    </script>
</body>
</html>
