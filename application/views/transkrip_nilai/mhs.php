<script>
  function popup_cetak() {
    var id_mhs = $('#id_mhs').val();
    window.open("<?= base_url(); ?>transkrip_nilai/cetak_transkrip_html/" + id_mhs, "PopupWindow", "width=950,height=600,scrollbars=yes,resizable=no");
    return false;


  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Mahasiswa</a>
    </li>
    <li class="breadcrumb-item active">Data Transkrip Nilai</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Transkrip Nilai
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php //if ($this->session->flashdata('message')) :
            if ($blm_bayar == '1') :
              echo $this->session->flashdata('message');
            endif;
            ?>
            <!-- 

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianAlumni', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                  <option value="0">- All -</option>
                  <?php //foreach ($tahun_akademik as $k) : 
                  ?>
                    <option value="<?php //echo $k['id']; 
                                    ?>" <?php //if ($k['id'] == $id_tahun_akademik) { 
                                        ?>selected<?php //} 
                                                  ?>><?php //echo $k['nama']; 
                                                      ?></option>
                  <?php //endforeach; 
                  ?>
                </select>
                <small class="text-danger">
                  <?php //echo form_error('id_tahun_akademik') 
                  ?>
                </small>
              </div>
            </div>
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success" type="submit" name="pdf" value="1" target="_blank">Export PDF</button>&nbsp;
            </form>
             -->

            <?php if ($blm_bayar == '0') { ?>
              <input type="hidden" name="id_mhs" id="id_mhs" value="<?= $id_mhs ?>">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="button" name="cetak_html" onclick="popup_cetak();">Cetak Transkrip</button>
              <hr>

              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Mata Kuliah</th>
                    <th>Nama Mata Kuliah</th>
                    <th>SKS</th>
                    <th>Nilai</th>
                    <th>Angka Kredit</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php $i = 1; ?>
                    <?php foreach ($data_transkrip_nilai as $row) : ?>
                      <td><?= $i++; ?></td>
                      <td><?= $row->kode_matkul ?></td>
                      <td><?= $row->matkul ?></td>
                      <td style="text-align: right;"><?= $row->sks ?></td>
                      <td><?= $row->nilai ?></td>
                      <td style="text-align: right;"><?= $row->bobot * $row->sks ?></td>
                  </tr>
                <?php endforeach; ?>
                </tr>
                </tbody>
              </table>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>