<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=pddikti_akm.xls");
header("Cache-control: public");
?>
<style>
  .allcen {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .allcen2 {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .str {
    mso-number-format: \@;
  }
</style>

<head>
  <meta charset="utf-8" />
  <title>SIMAK IAIQI | Data AKM</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <style>
    body {
      font-family: verdana, arial, sans-serif;
      font-size: 14px;
      line-height: 20px;
      font-weight: 400;
      -webkit-font-smoothing: antialiased;
      font-smoothing: antialiased;
    }

    table.gridtable {
      font-family: verdana, arial, sans-serif;
      font-size: 11px;
      width: 100%;
      color: #333333;
      border-width: 1px;
      border-color: #e9e9e9;
      border-collapse: collapse;
    }

    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      font-size: 12px;
      border-style: solid;
      font-weight: 900;
      color: #ffffff;
      border-color: #e9e9e9;
      background: #ea6153;
    }

    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #e9e9e9;
      background-color: #ffffff;
    }
  </style>
</head>

<body>
  <table border="1" width="100%" class="gridtable">
    <thead style="background-color: blue">
      <tr>
        <th class="allcen center bold">Nama</th>
        <th class="allcen center bold">Semester</th>
        <th class="allcen center bold">SKS</th>
        <th class="allcen center bold">IP Semester</th>
        <th class="allcen center bold">SKS Kumulatif</th>
        <th class="allcen center bold">Status</th>
        <th class="allcen center bold">Kode Prodi</th>
        <th class="allcen center bold">Biaya Kuliah Semester</th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1; ?>
      <?php foreach ($data_khs_excel as $row) :

        // sks semester
        $sqlsks = " SELECT sum(b.sks) as jumsks FROM khs_mhs_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id WHERE a.id_khs_mhs = '$row->id' ";
        $querysks = $this->db->query($sqlsks);
        if ($querysks->num_rows() > 0) {
          $hasilsks = $querysks->row();
          $jumsks    = $hasilsks->jumsks;
        } else
          $jumsks = 0;

        // hitung ip semester
        // 1. ambil total grade * bobot
        $sqlsks = " SELECT sum(c.sks*b.bobot) as gradebobot FROM khs_mhs_mk a INNER JOIN master_bobot_nilai b ON a.id_nilai_huruf = b.id_nilai_huruf
                    INNER JOIN master_mata_kuliah c ON c.id = a.id_mata_kuliah 
                    WHERE a.id_khs_mhs = '$row->id' ";
        //echo $sqlsks . "<br>";
        $querysks = $this->db->query($sqlsks);
        if ($querysks->num_rows() > 0) {
          $hasilsks = $querysks->row();
          $gradebobot    = $hasilsks->gradebobot;
        } else
          $gradebobot = 0;

        $ip_semester = $gradebobot / $jumsks;

        // sks kumulatif
        $sqlsks = " SELECT sum(b.sks) as jumsks FROM khs_mhs a1 INNER JOIN khs_mhs_mk a ON a1.id = a.id_khs_mhs
                    INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id WHERE a1.id_mhs = '$row->id_mhs' ";
        $querysks = $this->db->query($sqlsks);
        if ($querysks->num_rows() > 0) {
          $hasilsks = $querysks->row();
          $jumskskum    = $hasilsks->jumsks;
        } else
          $jumskskum = 0;

        // biaya kuliah semester yg sudah dibayar
        $sqlsks = " SELECT nominal FROM pembayaran_biaya_kuliah WHERE id_tahun_akademik = '$row->id_tahun_akademik' AND id_mhs = '$row->id_mhs' ";
        $querysks = $this->db->query($sqlsks);
        if ($querysks->num_rows() > 0) {
          $hasilsks = $querysks->row();
          $biayakuliah    = $hasilsks->nominal;
        } else
          $biayakuliah = 0;

        $kode_prodinya = (string)$row->kode_prodi;
      ?>
        <tr>
          <td><?= $row->nama_mhs ?></td>
          <td><?= $row->tahun . $row->jenis_semester ?></td>
          <td><?= $jumsks ?></td>
          <td><?= $ip_semester ?></td>
          <td><?= $jumskskum ?></td>
          <td>A</td>
          <?php
          echo    "<td>=\"$kode_prodinya\"</td>";
          ?>
          <td><?= $biayakuliah ?></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>