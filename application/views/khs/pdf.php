<!DOCTYPE html>
<html lang="en">
<head>
    <title>SIMAK IAIQI | KHS</title>
</head>
<style type="text/css">
    body {
        font-family: "Arial";
        font-size: 12px;
        padding: 1%;
    }
    table {
      border-collapse: collapse;
    }
    td, th {
      border: 1px solid black;
    }
    .no-border td {
      border: 1px solid white !important;
      padding-bottom: 10px;
    }
    th {
        padding: 2px;
    }
    td {
        padding-left: 10px;
        padding-top: 2px;
        padding-bottom: 2px;
    }
    h2 {
        letter-spacing: 5px;
    }
    .bold {
        font-weight: 700;
    }
    .center {
        padding-left: 0;
        text-align: center;
    }
    .mb5 {
        margin-bottom: 5px;
    }
    .m0 {
        margin: 0;
    }
    .mb10 {
        margin-bottom: 10px;
    }
    .ml10 {
        margin-left: 10px;
    }
    .mb20 {
        margin-bottom: 20px;
    }
    .mr20 {
        margin-right: 20px;
    }
    .mr50 {
        margin-right: 50px;
    }
</style>
<body>
<center><h3>KARTU HASIL STUDI MAHASISWA<br>INSTITUT AGAMA ISLAM AL ITTIFAQIAH (IAIQI) INDRALAYA OGAN ILIR SUMATERA SELATAN</h3></center><br>
<table class="no-border mt10" border="1">
	<?php foreach ($data_khs as $row) ?>
    <tr>
        <td width="65%">Nama : <?= $row->nama_mhs ?></td>
        <td width="35%">Tahun Akademik : <?= $row->nama_tahun_akademik ?></td>
    </tr>
    <tr>
        <td>NIM : <?= $row->nim ?></td>
        <td>Program Studi : <?= $row->nama_prodi ?></td>
    </tr>
    <tr>
        <td>Semester : <?= $row->nama_semester ?></td>
        <td>Pembimbing Akademik : <?= $row->nama_dosen ?></td>
    </tr>
</table><br><br>
<table border="1" width="100%" class="gridtable">
  	<thead>
		<tr>
          <th>No.</th>
          <th>Kode Mata Kuliah</th>
          <th>Nama Mata Kuliah</th>
          <th>SKS</th>
          <th>Nilai Angka</th>
          <th>Nilai Huruf</th>
          <th>Angka Kredit</th>
          <th>KET.</th>
        </tr>
	</thead>
	<tbody>
		<tr>
			<?php $i = 1; ?>
            <?php foreach ($data_khs as $row) : ?>
            <?php $sks= 0; $sks += $row->sks; $bobot = 0; $bobot += $row->bobot*$row->sks; ?>
			<td><?= $i++; ?></td>
            <td align="center"><?= $row->kode_matkul ?></td>
            <td><?= $row->matkul ?></td>
            <td align="center"><?= $row->sks ?></td>
            <td align="center"><?= $row->nilai_akhir ?></td>
            <td align="center"><?= $row->nilai ?></td>
            <td align="center"><?= $row->bobot*$row->sks ?></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3" align="center"><b>JUMLAH</b></td>
			<td align="center"><?= $sks; ?></td>
            <td></td>
            <td></td>
            <td align="center"><?= $bobot; ?></td>
            <td></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table><br><br><br><br>
<table class="no-border" border="1">
    <tr>
        <td></td>
        <td width="100%"><center>Indralaya, <?php echo date('d M Y') ?></center></td>
    </tr>
    <tr>
        <td></td>
        <td><center>a.n. Ketua,<br>Wakil Ketua I,<br><br><br><br><br></center></td>
    </tr>
    <tr>
        <td></td>
        <td><center><b>Dr. Hj. Muyasaroh, M.Pd.I.</b></center></td>
    </tr>
</table>
<script type="text/javascript">
        window.print();
    </script>
</body>
</html>
