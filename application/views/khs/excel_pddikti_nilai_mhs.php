<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=pddikti_nilai_mhs.xls");
header("Cache-control: public");
?>
<style>
  .allcen {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .allcen2 {
    text-align: center !important;
    vertical-align: middle !important;
    position: relative !important;
  }

  .str {
    mso-number-format: \@;
  }
</style>

<head>
  <meta charset="utf-8" />
  <title>SIMAK IAIQI | Data AKM</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <style>
    body {
      font-family: verdana, arial, sans-serif;
      font-size: 14px;
      line-height: 20px;
      font-weight: 400;
      -webkit-font-smoothing: antialiased;
      font-smoothing: antialiased;
    }

    table.gridtable {
      font-family: verdana, arial, sans-serif;
      font-size: 11px;
      width: 100%;
      color: #333333;
      border-width: 1px;
      border-color: #e9e9e9;
      border-collapse: collapse;
    }

    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      font-size: 12px;
      border-style: solid;
      font-weight: 900;
      color: #ffffff;
      border-color: #e9e9e9;
      background: #ea6153;
    }

    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #e9e9e9;
      background-color: #ffffff;
    }
  </style>
</head>

<body>
  <table border="1" width="100%" class="gridtable">
    <thead style="background-color: blue">
      <tr>
        <th class="allcen center bold">NIM</th>
        <th class="allcen center bold">Nama Mahasiswa</th>
        <th class="allcen center bold">Kode MK</th>
        <th class="allcen center bold">Mata Kuliah</th>
        <th class="allcen center bold">Semester</th>
        <th class="allcen center bold">Kelas</th>
        <th class="allcen center bold">Nilai Huruf</th>
        <th class="allcen center bold">Nilai Indeks</th>
        <th class="allcen center bold">Nilai Angka</th>
        <th class="allcen center bold">Kode Prodi</th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1; ?>
      <?php foreach ($data_khs_excel as $row) :

        $kode_prodinya = (string)$row->kode_prodi;

        // ambil kelas dari tabel jadwal
        $sqlkelas = " SELECT b.kode, b.nama FROM jadwal_kuliah a LEFT JOIN master_kelas b ON a.id_kelas = b.id 
                      WHERE a.id_semester = '$row->id_semester' AND a.id_tahun_akademik = '$row->id_tahun_akademik'
                      AND a.id_mata_kuliah = '$row->id_mata_kuliah' AND a.id_program_studi = '$row->id_program_studi' ";
        $querykelas = $this->db->query($sqlkelas);
        if ($querykelas->num_rows() > 0) {
          $hasilkelas = $querykelas->row();
          $kode_kelas    = $hasilkelas->kode;
          $nama_kelas    = $hasilkelas->nama;
          $kelasnya = "[" . $kode_kelas . "] " . $nama_kelas;
        } else {
          $kode_kelas    = '';
          $nama_kelas    = '';
          $kelasnya = '';
        }
      ?>
        <tr>
          <td><?= $row->nim ?></td>
          <td><?= $row->nama_mhs ?></td>
          <td><?= $row->kode_mk ?></td>
          <td><?= $row->nama_mk ?></td>
          <td><?= $row->tahun . $row->jenis_semester ?></td>
          <td><?= $kelasnya ?></td>
          <td><?= $row->nilai_huruf ?></td>
          <td><?= $row->bobot ?></td>
          <td><?= $row->nilai_akhir ?></td>
          <?php
          echo    "<td>=\"$kode_prodinya\"</td>";
          ?>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>