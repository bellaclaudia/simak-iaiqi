<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('krs'); ?>">Data KHS</a></li>
    <li class="breadcrumb-item active">Detail Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Detail KHS
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">

            Tahun Akademik : <b><?= $thn_akademiknya ?></b> <br>
            Semester : <b><?= $nama_semester ?></b> <br>
            Program Studi : <b><?= $prodinya ?></b> <br>
            Mahasiswa : <b><?= $mhsnya ?></b> <br><br>
            <a href="<?= base_url('khs'); ?>" class="btn btn-success btn-circle">Kembali</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Mata Kuliah</th>
                  <th>Nama Mata Kuliah</th>
                  <th>SKS</th>
                  <?php foreach ($data_komponen as $k) :
                  ?>
                    <th><?php echo $k['nama']
                        ?></th>
                  <?php endforeach;
                  ?>
                  <th>Nilai Akhir</th>
                  <th>Huruf Mutu</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($detail_khs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->kode ?></td>
                    <td><?= $row->nama_mata_kuliah ?></td>
                    <td style="text-align: right;"><?= $row->sks ?></td>

                    <?php foreach ($data_komponen as $k) :
                    ?>
                      <td style="text-align: right;">
                        <?php
                        $sqlxx = " SELECT nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id' AND id_komponen_nilai = '" . $k['id'] . "' ";
                        $queryxx = $this->db->query($sqlxx);
                        if ($queryxx->num_rows() > 0) {
                          $hasilxx = $queryxx->row();
                          $nilai_angka    = $hasilxx->nilai_angka;
                        } else
                          $nilai_angka = "-";

                        echo $nilai_angka;
                        ?>
                      </td>
                    <?php endforeach;
                    ?>
                    <td style="text-align: right;"><?php echo $row->nilai_akhir
                                                    ?></td>
                    <td style="text-align: center;"><?php echo $row->nilai_huruf
                                                    ?></td>
                  </tr>
                <?php endforeach; ?>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>