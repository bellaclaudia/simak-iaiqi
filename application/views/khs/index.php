<script type="text/javascript">
  function popup_cetak(id_khs) {
    window.open("<?= base_url(); ?>khs/cetak_khs_html/" + id_khs, "PopupWindow", "width=950,height=600,scrollbars=yes,resizable=no");
    return false;
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data KHS</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data KHS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianKHS', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_prodi">
                  <option value="0">- All -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel_pddikti_akm" value="1">Export Excel PDDIKTI (AKM)</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel_pddikti_nilai_mhs" value="1">Export Excel PDDIKTI (Nilai Mhs)</button>
            </form>
            <hr>

            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Mahasiswa</th>
                  <th>Aksi</th>

                  <!-- <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Mahasiswa</th>
                  <th>Mata Kuliah</th>
                  <?php //foreach ($data_komponen as $k) : 
                  ?>
                    <th><?php //echo $k['nama'] 
                        ?></th>
                  <?php //endforeach; 
                  ?>
                  <th>Nilai Akhir</th>
                  <th>Huruf Mutu</th> -->
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_khs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_tahun_akademik ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <?php //foreach ($data_komponen as $k) : 
                    ?>
                    <!-- <td style="text-align: center;"> -->
                    <?php
                    // $sqlxx = " SELECT nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id' AND id_komponen_nilai = '" . $k['id'] . "' ";
                    // $queryxx = $this->db->query($sqlxx);
                    // if ($queryxx->num_rows() > 0) {
                    //   $hasilxx = $queryxx->row();
                    //   $nilai_angka    = $hasilxx->nilai_angka;
                    // } else
                    //   $nilai_angka = "-";

                    // echo $nilai_angka;
                    ?>
                    <!-- </td> -->
                    <?php //endforeach; 
                    ?>
                    <td>
                      <!-- <a href="<?= base_url('khs/edit_nilai/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <a href="<?php echo site_url('khs/hapus_nilai/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data nilai mhs ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a> -->
                      <a href="<?= base_url('khs/view_nilai/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a>
                      <a href="javascript:void(0)" class="btn btn-success btn-circle" title="Cetak KHS" onclick="popup_cetak(<?= $row->id ?>);"><i class="fa fa-print"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>