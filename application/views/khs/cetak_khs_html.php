<script type="text/javascript">
  window.print();
</script>

<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 12px;
    border-collapse: collapse;
  }

  .judulnya {
    background-color: #DDD;
  }

  h3 {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 14px;
  }

  body {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 12px;
  }
</style>

<center>
  <h3>KARTU HASIL STUDI MAHASISWA<br>
    INSTITUT AGAMA ISLAM AL-QUR'AN AL ITTIFAQIAH (IAIQI)</h3>
</center>
<br>

<table width="100%" border="1" align="center">
  <tr>
    <td colspan="2">&nbsp;Nama</td>
    <td>&nbsp;<?= $nama_mhs ?></td>
    <td colspan="2">&nbsp;Tahun Akademik</td>
    <td colspan="3">&nbsp;<?= $thn_akademiknya ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;NIM</td>
    <td>&nbsp;<?= $nim ?></td>
    <td colspan="2">&nbsp;Program Studi</td>
    <td colspan="3">&nbsp;<?= $prodinya ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;Semester</td>
    <td>&nbsp;<?= $nama_semester ?></td>
    <td colspan="2">&nbsp;Pemb. Akademik</td>
    <td colspan="3">&nbsp;<?= $nama_dosen_pa ?></td>
  </tr>

  <tr>
    <th>NO</th>
    <th>KODE MATA KULIAH</th>
    <th>NAMA MATA KULIAH</th>
    <th>SKS</th>
    <th>NILAI ANGKA</th>
    <th>NILAI HURUF</th>
    <th>ANGKA KREDIT</th>
    <th>KET</th>
  </tr>
  <?php $i = 1;
  $jum_sks = 0;
  $jum_angka_kredit = 0; ?>
  <?php foreach ($detail_khs as $row) :
    $jum_sks += $row->sks;
    $jum_angka_kredit += $row->angka_kredit;
  ?>
    <tr>
      <td style="text-align: center;"><?= $i++ ?></td>
      <td style="text-align: center;"><?= $row->kode ?></td>
      <td>&nbsp;<?= $row->nama_mata_kuliah ?></td>
      <td style="text-align: center;"><?= $row->sks ?></td>
      <td style="text-align: center;"><?= $row->nilai_akhir ?></td>
      <td style="text-align: center;"><?= $row->nilai_huruf ?></td>
      <td style="text-align: center;"><?= $row->angka_kredit ?></td>
      <td>&nbsp;</td>
    </tr>
  <?php endforeach;

  // sks semester
  $sqlsks = " SELECT sum(b.sks) as jumsks FROM khs_mhs_mk a INNER JOIN master_mata_kuliah b ON a.id_mata_kuliah = b.id WHERE a.id_khs_mhs = '$id_khs' ";
  $querysks = $this->db->query($sqlsks);
  if ($querysks->num_rows() > 0) {
    $hasilsks = $querysks->row();
    $jumsks    = $hasilsks->jumsks;
  } else
    $jumsks = 0;

  // hitung ip semester
  // 1. ambil total grade * bobot
  $sqlsks = " SELECT sum(c.sks*b.bobot) as gradebobot FROM khs_mhs_mk a INNER JOIN master_bobot_nilai b ON a.id_nilai_huruf = b.id_nilai_huruf
              INNER JOIN master_mata_kuliah c ON c.id = a.id_mata_kuliah 
              WHERE a.id_khs_mhs = '$id_khs' ";
  //echo $sqlsks . "<br>";
  $querysks = $this->db->query($sqlsks);
  if ($querysks->num_rows() > 0) {
    $hasilsks = $querysks->row();
    $gradebobot    = $hasilsks->gradebobot;
  } else
    $gradebobot = 0;

  $ip_semester = round($gradebobot / $jumsks, 2);

  ?>
  <tr>
    <td colspan="3" style="text-align: center;">JUMLAH</td>
    <td style="text-align: center;"><?= $jum_sks ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td style="text-align: center;"><?= $jum_angka_kredit ?></td>
  </tr>
</table><br>

<table border="0" width="90%" align="center">
  <tr>
    <td>IP Semester</td>
    <td>:</td>
    <td width="15%"><?= $ip_semester ?></td>
    <td rowspan="7" style="vertical-align:top;"><i>Keterangan Nilai:<br>
        1. 80 - 100 = A<br>
        2. 70 - 79,99 = B<br>
        3. 60 - 69,99 = C<br>
        4. 56 - 59,99 = D<br>
        5. < - 55,99=E</i><br>
    </td>
    <td>&nbsp;</td>
    <td>Indralaya, <?= date('d-m-Y') ?></td>
  </tr>
  <tr>
    <td style="width: 15%;">IP Semester Lalu</td>
    <td>:</td>
    <td><?= $ip_semester_lalu ?></td>
    <td width="20%">&nbsp;</td>
    <td>a.n. Ketua</td>
  </tr>
  <tr>
    <td>IPK</td>
    <td>:</td>
    <td><?= $ipk ?></td>
    <td>&nbsp;</td>
    <td>Wakil Ketua I</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Dr. Muyasaroh, M.Pd.I</td>
  </tr>
</table>
<br><br>