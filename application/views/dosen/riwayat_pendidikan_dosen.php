<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Riwayat Pendidikan Dosen
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Dosen</th>
                  <th>Pendidikan</th>
                  <th>Gelar</th>
                  <th>Asal Kampus</th>
                  <th>Periode</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_riwayat_pend_dosen as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_dosen ?></td>
                    <td><?= $row->pendidikan ?></td>
                    <td><?= $row->gelar ?></td>
                    <td><?= $row->asal_kampus ?></td>
                    <td><?= $row->dari_tahun ?> - <?= $row->sampai_tahun ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>