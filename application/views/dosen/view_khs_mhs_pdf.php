<script type="text/javascript">
  window.print();
</script>

<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 12px;
    border-collapse: collapse;
  }

  .judulnya {
    background-color: #DDD;
  }

  h3 {
    font-family: Helvetica, Geneva, Arial,
      SunSans-Regular, sans-serif;
    font-size: 14px;
  }
</style>

<h3>Kartu Hasil Studi Mahasiswa</h3>
<table border="1" width="100%">
  <thead>
    <tr>
      <th>No.</th>
      <th>Tahun Akademik</th>
      <th>Semester</th>
      <th>Mahasiswa</th>
      <th>Mata Kuliah</th>
      <?php foreach ($data_komponen as $k) : ?>
        <th><?= $k['nama'] ?></th>
      <?php endforeach; ?>
      <th>Nilai Akhir</th>
      <th>Huruf Mutu</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_khs_mhs as $row) : ?>
      <tr>
        <td style="text-align: center;"><?= $i++; ?></td>
        <td><?= $row->nama_tahun_akademik ?></td>
        <td><?= $row->nama_semester ?></td>
        <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
        <td><?= $row->nama_mk ?></td>
        <?php foreach ($data_komponen as $k) : ?>
          <td style="text-align: center;">
            <?php
            $sqlxx = " SELECT nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id' AND id_komponen_nilai = '" . $k['id'] . "' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
              $hasilxx = $queryxx->row();
              $nilai_angka    = $hasilxx->nilai_angka;
            } else
              $nilai_angka = "-";

            echo $nilai_angka;
            ?>
          </td>
        <?php endforeach; ?>
        <td style="text-align: center;"><?= $row->nilai_akhir ?></td>
        <td style="text-align: center;"><?= $row->nilai_huruf ?></td>
      </tr>
    <?php endforeach; ?>

  </tbody>
</table>