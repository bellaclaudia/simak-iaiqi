<script type="text/javascript">
  // $(function() {
  //   var id_semester = $('#select2-2').val();
  //   $.ajax({
  //     type: 'post',
  //     url: '<?php //base_url('dosen/get_mata_kuliah'); 
                ?>',
  //     data: 'id_semester=' + id_semester,
  //     success: function(response) {
  //       $("#jadwalnya").html(response);
  //     }
  //   });
  // });

  function get_mata_kuliah(id_semester) {
    //alert('disini');
    $.ajax({
      type: 'post',
      url: '<?= base_url('dosen/get_mata_kuliah'); ?>',
      data: 'id_semester=' + id_semester,
      success: function(response) {
        $("#mknya").html(response);
      }
    });
  }
</script>

<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data Bobot Komponen Nilai
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?><br>
          <form id="FrmAddBobot" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="is_cek" value="1">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <!-- <option value="0" selected disabled>Pilih Tahun Akademik</option> -->
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester" onchange="get_mata_kuliah(this.value);">
                  <!-- <option value="0" selected disabled>Pilih Semester</option> -->
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mata Kuliah</label>
              <div class="col-md-4">
                <label id="mknya">
                  <select class="form-control select2-single" id="select2-4" name="id_mata_kuliah">
                    <!-- <option value="0">Global (Default)</option> -->
                    <?php foreach ($data_mk as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_mata_kuliah) { ?>selected<?php } ?>><?= "[" . $k['kode'] . "] " . $k['nama'] . " (Prodi: " . $k['nama_prodi'] . " " . $k['nama_jenjang'] . ")" ?></option>
                    <?php endforeach; ?>
                  </select>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Komponen Nilai</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-5" name="id_komponen_nilai">
                  <?php foreach ($data_komponen as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_komponen_nilai) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Persentase</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="persentase" value="0" style="text-align:right;">
              </div>
            </div>

            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('dosen/set_bobot_komponen_nilai'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>