<script type="text/javascript">
  // $(function() {

  //   $(document).on("click", ".btn_export", function() {
  //     alert('didieu');
  //     return false;
  //     //var this_id = $(this).attr('id');
  //     var id_ta = $('#select2-1').val();
  //     var id_semester = $('#select2-2').val();
  //     var id_mhs = $('#select2-4').val();

  //     window.open(site_url + "dosen/view_khs_mhs_pdf/" + id_ta + "/" + id_semester + "/" + id_mhs, "PopupWindow", "width=800,height=600,scrollbars=yes,resizable=no");
  //     return false;

  //   });
  // });

  function popup_export() {
    //var this_id = $(this).attr('id');
    var id_ta = $('#select2-1').val();
    var id_semester = $('#select2-2').val();
    var id_mhs = $('#select2-4').val();

    window.open("<?= base_url(); ?>dosen/view_khs_mhs_pdf/" + id_ta + "/" + id_semester + "/" + id_mhs, "PopupWindow", "width=950,height=600,scrollbars=yes,resizable=no");
    return false;
  }
</script>

<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data KHS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianKHS', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <input type="hidden" name="is_cari" value="1">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <!-- <option value="0">- All -</option> -->
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <!-- <option value="0">- All -</option> -->
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_semester') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mahasiswa</label>
              <div class="col-md-4">
                <label id="mhsnya">
                  <select class="form-control select2-single" id="select2-4" name="id_mhs">
                    <!-- <option value="0" selected disabled>Pilih Mahasiswa</option> -->
                    <?php foreach ($data_mhs as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_mhs) { ?>selected<?php } ?>><?= $k['nim'] . " - " . $k['nama'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </label>
                <small class="text-danger">
                  <?php echo form_error('id_mhs') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="button" name="pdf" onclick="popup_export();">Export PDF</button>&nbsp;
            <!-- <a href=" javascript:void(0)" class="btn btn-success btn-circle" onclick="popup_export();">Export PDF</a> -->
            </form>
            <hr>
            <?php if ($is_cari != '') { ?>
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tahun Akademik</th>
                    <th>Semester</th>
                    <th>Mahasiswa</th>
                    <th>Mata Kuliah</th>
                    <?php foreach ($data_komponen as $k) : ?>
                      <th><?= $k['nama'] ?></th>
                    <?php endforeach; ?>
                    <th>Nilai Akhir</th>
                    <th>Huruf Mutu</th>
                    <!-- <th>Nilai Per Komponen</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($data_khs_mhs as $row) : ?>
                    <tr>
                      <td style="text-align: right;"><?= $i++; ?></td>
                      <td><?= $row->nama_tahun_akademik ?></td>
                      <td><?= $row->nama_semester ?></td>
                      <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                      <td><?= $row->nama_mk ?></td>
                      <?php foreach ($data_komponen as $k) : ?>
                        <td style="text-align: right;">
                          <?php
                          $sqlxx = " SELECT nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id' AND id_komponen_nilai = '" . $k['id'] . "' ";
                          $queryxx = $this->db->query($sqlxx);
                          if ($queryxx->num_rows() > 0) {
                            $hasilxx = $queryxx->row();
                            $nilai_angka    = $hasilxx->nilai_angka;
                          } else
                            $nilai_angka = "-";

                          echo $nilai_angka;
                          ?>
                        </td>
                      <?php endforeach; ?>
                      <td style="text-align: right;"><?= $row->nilai_akhir ?></td>
                      <td style="text-align: center;"><?= $row->nilai_huruf ?></td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>