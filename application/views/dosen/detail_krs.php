<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Detail KRS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">

            Tahun Akademik : <b><?= $thn_akademiknya ?></b> <br>
            Semester : <b><?= $nama_semester ?></b> <br>
            Program Studi : <b><?= $prodinya ?></b> <br>
            Mahasiswa : <b><?= $mhsnya ?></b> <br><br>
            <a href="<?= base_url('dosen/view_krs_waiting_mhs/'); ?>" class="btn btn-success btn-circle">Kembali Ke Halaman List KRS</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Mata Kuliah</th>
                  <th>Nama Mata Kuliah</th>
                  <th>SKS</th>
                  <th>Status (Baru/Mengulang)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php $i = 1; ?>
                  <?php foreach ($detail_krs as $row) : ?>
                    <td><?= $i++; ?></td>
                    <td><?= $row->kode ?></td>
                    <td><?= $row->nama_mata_kuliah ?></td>
                    <td style="text-align: right;"><?= $row->sks ?></td>
                    <td>
                      <?php
                      if ($row->status == '1')
                        echo "Baru";
                      else
                        echo "Mengulang";
                      ?>
                    </td>
                </tr>
              <?php endforeach; ?>
              </tr>
              </tbody>
            </table>

            <div class="modal fade" id="alasanreject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Isi Alasan Tolak KRS</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php
                    $attributes = array('id' => 'FrmAddAlasan', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                    echo form_open('', $attributes);
                    ?>
                    <div class="form-group row">
                      <label class="col-md-3 col-form-label">Alasan Tolak</label>
                      <div class="col-md-9">
                        <input type="hidden" name="is_reject" value="1">
                        <textarea class="form-control" name="alasan_ditolak" id="" cols="20" rows="5"></textarea>
                        <!-- <input class="form-control" type="text" name="nama" placeholder="Isi Alasan Tolak" required> -->
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                      <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Konfirmasi Tolak KRS</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <a href="<?= base_url('dosen/approve_krs/' . $id_krs); ?>" class="btn btn-success btn-circle" onclick="return confirm('Apakah Anda yakin akan setujui KRS mahasiswa <?= $nama_mhs; ?> ?');">Setujui</button></a>&nbsp; <a href="#" class="btn btn-success btn-circle" data-toggle="modal" data-target="#alasanreject">Tolak (Isi Alasan Tolak)</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>