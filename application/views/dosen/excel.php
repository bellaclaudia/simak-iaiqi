<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=dosen.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>SIMAK IAIQI | Dosen</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Dosen pada IAIQI Indralaya</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
      	<th class="allcen center bold">Nama</th>
      	<th class="allcen center bold">Alamat</th>
      	<th class="allcen center bold">Jenis Kelamin</th>
      	<th class="allcen center bold">Tempat/Tgl Lahir</th>
      	<th class="allcen center bold">NIK</th>
      	<th class="allcen center bold">NIDN</th>
      	<th class="allcen center bold">Agama</th>
      	<th class="allcen center bold">Golongan Darah</th>
      	<th class="allcen center bold">Fakultas</th>
      	<th class="allcen center bold">Prodi</th>
      	<th class="allcen center bold">Pangkat/Golongan</th>
      	<th class="allcen center bold">No. Telepon</th>
      	<th class="allcen center bold">Email</th>
      	<th class="allcen center bold">Status Dosen</th>
      	<th class="allcen center bold">Pendidikan Terakhir</th>
      	<th class="allcen center bold">Jabatan Akademik Dosen</th>
      	<th class="allcen center bold">Tgl. Input</th>
      	<th class="allcen center bold">Tgl. Update</th>
      	<th class="allcen center bold">User Update By</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_dosen as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->alamat ?>, <?= $row->kab_kota ?>, <?= $row->provinsi ?></td>
        <td><?php if ($row->jenis_kelamin == 'L') echo "Laki-Laki";
            else echo "Perempuan"; ?></td>
        <td><?= $row->tempat_lahir ?>, <?= $row->tgl_lahir ?></td>
        <td>'<?= $row->nik ?></td>
        <td>'<?= $row->nidn ?></td>
        <td><?= $row->agama ?></td>
        <td><?= $row->goldar ?></td>
        <td><?= $row->fakultas ?></td>
        <td><?= $row->prodi ?></td>
        <td><?= $row->pangkat_golongan ?></td>
        <td><?= $row->no_telp ?></td>
        <td><?= $row->email ?></td>
        <td><?= $row->status_dosen ?></td>
        <td><?= $row->pendidikan_terakhir ?></td>
        <td><?= $row->jabatan_akademik_dosen ?></td>
        <td><?= $row->tgl_input ?></td>
        <td><?= $row->tgl_update ?></td>
        <td><?= $row->user_update_by ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>