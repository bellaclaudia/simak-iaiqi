<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Set Bobot Komponen Nilai
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianBobot', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <!-- <a href="<?= base_url('calon_mhs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
            </form>
            <hr>

            <a href="<?= base_url('dosen/add_bobot_komponen_nilai'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Mata Kuliah</th>
                  <th>Komponen Nilai</th>
                  <th>Persentase</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php $i = 1; ?>
                  <?php foreach ($data_bobot as $row) : ?>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_tahun_akademik ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?php if ($row->id_mata_kuliah == 0) echo "Global (Default)";
                        else echo "[" . $row->kode_mata_kuliah . "] " . $row->nama_mata_kuliah ?></td>
                    <td><?= $row->nama_komponen_nilai ?></td>
                    <td style="text-align: right;"><?= $row->persentase ?></td>
                    <td style="text-align:center">
                      <a href="<?= base_url('dosen/edit_bobot_komponen_nilai/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>&nbsp;
                      <a href="<?php echo site_url('dosen/hapus_bobot_komponen_nilai/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Bobot Komponen Nilai ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
              <?php endforeach; ?>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>