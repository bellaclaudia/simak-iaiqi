<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('dosen'); ?>">Data Dosen</a></li>
    <li class="breadcrumb-item active">View Data Dosen</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-eye"></i> View Data Dosen
        </div>
        <div class="card-body">
          <?php if($data_dosen->file_foto != NULL) : ?>
          <a href="<?= base_url('uploads/'.$data_dosen->file_foto); ?>" target="_blank"><img src="<?= base_url('uploads/'.$data_dosen->file_foto); ?>" alt="<?= $data_dosen->file_foto ?>" width="150" class="img-thumbnail rounded-circle"></a><br>
          <center><a href="<?= base_url('uploads/'.$data_dosen->file_foto); ?>" download>Download</a></center>
          <?php endif ?>
          <?php if($data_dosen->file_foto == NULL and $data_dosen->jenis_kelamin == 'P') : ?>
          <a href="<?= base_url('uploads/icon/dosen cewek.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/dosen cewek.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?>
          <?php if($data_dosen->file_foto == NULL and $data_dosen->jenis_kelamin == 'L') : ?>
          <a href="<?= base_url('uploads/icon/dosen cowok.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/dosen cowok.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?><br><br>
          <table class="table table-responsive-sm table-striped">
            <input type="hidden" name="id" value="<?= $data_dosen->id; ?>">
            <tr>
              <td>Nama Lengkap : <?= $data_dosen->nama; ?></td>
              <td colspan="2">Alamat : <?= $data_dosen->alamat; ?>, <?= $data_dosen->kab_kota; ?>, <?= $data_dosen->provinsi; ?></td>
            </tr>
            <tr>
              <td>NIDN : <?= $data_dosen->nidn; ?></td>
              <td>NIK : <?= $data_dosen->nik; ?></td>
              <td>Program Studi : <?= $data_dosen->prodi; ?></td>
            </tr>
            <tr>
              <td>Tempat Lahir : <?= $data_dosen->tempat_lahir; ?></td>
              <td>Tanggal Lahir : <?= $data_dosen->tgl_lahir; ?></td>
              <td>Jenis Kelamin : <?php if ($data_dosen->jenis_kelamin == 'L') echo "Laki-Laki";
                                  else echo "Perempuan"; ?></td>
            </tr>
            <tr>
              <td>Agama : <?= $data_dosen->agama; ?></td>
              <td>Golongan Darah : <?= $data_dosen->goldar; ?></td>
              <td>Jabatan Akademik Dosen : <?= $data_dosen->jabatan_akademik_dosen; ?></td>
            </tr>
            <tr>
              <td>Provinsi : <?= $data_dosen->provinsi; ?></td>
              <td>Kabupaten / Kota : <?= $data_dosen->kab_kota; ?></td>
              <td>Pangkat / Golongan : <?= $data_dosen->pangkat_golongan; ?></td>
            </tr>
            <tr>
              <td>No. Telepon : <?= $data_dosen->no_telp; ?></td>
              <td>Email : <?= $data_dosen->email; ?></td>
              <td>Status Dosen : <?= $data_dosen->status_dosen; ?></td>
            </tr>
            <tr>
              <td>Pendidikan Terakhir : <?= $data_dosen->pendidikan_terakhir; ?></td>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
          <div class="modal-footer">
            <a href="<?= base_url('dosen'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Kembali</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>