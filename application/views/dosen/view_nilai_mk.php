<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Nilai Mata Kuliah
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianNilai', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            </form>
            <hr>

            <!-- <a href="<?= base_url('dosen/pengisian_nilai'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Form Pengisian Nilai</button></a><br><br> -->
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Kelas</th>
                  <th>Mata Kuliah</th>
                  <th>SKS</th>
                  <th>Jenis Kuliah</th>
                  <!-- <?php //foreach ($data_komponen as $k) : 
                        ?>
                    <th><?php //echo $k['nama'] 
                        ?></th>
                  <?php //endforeach; 
                  ?> -->
                  <th>Pengisian Nilai</th>
                  <th>Proses Nilai Akhir</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php $i = 1; ?>
                  <?php foreach ($data_mk as $row) : ?>
                    <td><?= $i++; ?></td>
                    <td><?= $row->tahun . " (" . $row->nama_tahun_akademik . ")" ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?= $row->nama_kelas ?></td>
                    <td><?= "[" . $row->kode . "] " . $row->nama_mata_kuliah ?></td>
                    <td style="text-align: right;"><?= $row->sks ?></td>
                    <td><?php if ($row->jenis_kuliah == 1) echo "Reguler";
                        else echo "SP"; ?></td>
                    <!-- <?php //foreach ($data_komponen as $k) : 
                          ?>
                      <td style="text-align: center;"><a href="<?php //echo base_url('dosen/input_nilai/' . $row->id_mata_kuliah . '/' . $row->id_semester . '/' . $row->id_tahun_akademik . '/' . $row->id_kelas . '/' . $k['id']); 
                                                                ?>" title="Isi Nilai" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a></td>
                    <?php //endforeach; 
                    ?> -->
                    <td><a href="<?= base_url('dosen/input_nilai_gabungan/' . $row->id_mata_kuliah . '/' . $row->id_semester . '/' . $row->id_tahun_akademik . '/' . $row->id_kelas); ?>" title="Isi Nilai" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a></td>
                    <td>
                      <a href="<?= base_url('dosen/proses_nilai_akhir/' . $row->id_mata_kuliah . '/' . $row->id_semester . '/' . $row->id_tahun_akademik . '/' . $row->id_kelas); ?>" class="btn btn-success btn-circle" onclick="return confirm('Apakah Anda yakin akan melakukan proses nilai akhir untuk data ini ? Proses akan dilakukan secara otomatis');">Proses</button></a>
                      <br>

                      <?php
                      // get last proses nilai akhir 
                      $sqlxx = " select id, tgl_proses, user_update_by FROM history_proses_nilai_akhir 
                                WHERE id_kelas = '$row->id_kelas' AND id_semester = '$row->id_semester' 
                                AND id_tahun_akademik = '$row->id_tahun_akademik' AND id_mata_kuliah = '$row->id_mata_kuliah'
                                ORDER BY id DESC LIMIT 1 ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $hasilxx = $queryxx->row();
                        $tgl_proses = $hasilxx->tgl_proses;
                        $user_update_by = $hasilxx->user_update_by;

                        $proses_terakhir = $tgl_proses . " (Oleh: " . $user_update_by . ")";
                      } else
                        $proses_terakhir = "-";
                      ?>

                      Proses terakhir : <?= $proses_terakhir ?>
                    </td>
                </tr>
              <?php endforeach; ?>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>