<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Biodata Dosen
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <form id="FrmEditDosen" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lengkap</label>
              <div class="col-md-4">
                <input class="form-control" type="hidden" name="id" value="<?= $data_dosen->id; ?>">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= $data_dosen->nama; ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIDN</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nidn" placeholder="Isi NIDN" value="<?= $data_dosen->nidn; ?>" readonly="true">
                <small class="text-danger">
                  <?php echo form_error('nidn') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_dosen->id; ?>">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_program_studi)) {
                    $lv = $data_dosen->id_program_studi;
                  }
                  foreach ($prodi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Status Dosen</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_status_dosen">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_status_dosen)) {
                    $lv = $data_dosen->id_status_dosen;
                  }
                  foreach ($status_dosen as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_status_dosen') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <!-- <a href="<?= base_url('uploads/' . $data_dosen->file_foto); ?>" download><?= $data_dosen->file_foto; ?></a> -->
                <img src="<?= base_url('uploads/' . $data_dosen->file_foto) ?>" alt="" width="200" class="img-thumbnail rounded-circle">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>
              </div>
              <label class="col-md-2 col-form-label">NIK</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= $data_dosen->nik; ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pendidikan Terakhir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="pendidikan_terakhir" placeholder="Isi Pendidikan Terakhir" value="<?= $data_dosen->pendidikan_terakhir; ?>">
                <small class="text-danger">
                  <?php echo form_error('pendidikan_terakhir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jabatan Akademik Dosen</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jabatan_akademik_dosen">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_jabatan_akademik_dosen)) {
                    $lv = $data_dosen->id_jabatan_akademik_dosen;
                  }
                  foreach ($jabatan_akademik_dosen as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_jabatan_akademik_dosen') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">&nbsp;</label>
              <div class="col-md-4">
                &nbsp;
              </div>

              <label class="col-md-2 col-form-label">Pangkat / Golongan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="pangkat_golongan" placeholder="Isi Pangkat/Golongan" value="<?= $data_dosen->pangkat_golongan; ?>">
                <small class="text-danger">
                  <?php echo form_error('pangkat_golongan') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Alamat</label>
              <div class="col-md-4">
                <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= $data_dosen->alamat; ?></textarea>
                <small class="text-danger">
                  <?php echo form_error('alamat') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input type="radio" name="jenis_kelamin" value="Perempuan" <?php if ($data_dosen->jenis_kelamin == "P") {
                                                                              echo "checked";
                                                                            } ?> /> Perempuan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="jenis_kelamin" value="Laki-laki" <?php if ($data_dosen->jenis_kelamin == "L") {
                                                                              echo "checked";
                                                                            } ?> /> Laki-laki
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= $data_dosen->tempat_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= $data_dosen->tgl_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama</label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_agama)) {
                    $lv = $data_dosen->id_agama;
                  }
                  foreach ($agama as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_golongan_darah)) {
                    $lv = $data_dosen->id_golongan_darah;
                  }
                  foreach ($goldar as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_golongan_darah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_provinsi">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_provinsi)) {
                    $lv = $data_dosen->id_provinsi;
                  }
                  foreach ($provinsi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_provinsi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <?php
                  $lv = '';
                  if (isset($data_dosen->id_kabupaten_kota)) {
                    $lv = $data_dosen->id_kabupaten_kota;
                  }
                  foreach ($kab_kota as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kabupaten_kota') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= $data_dosen->email; ?>">
                <small class="text-danger">
                  <?php echo form_error('email') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. Telepon</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Telepon" value="<?= $data_dosen->no_telp; ?>">
                <small class="text-danger">
                  <?php echo form_error('no_telp') ?>
                </small>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>