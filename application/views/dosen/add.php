<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('dosen'); ?>">Data Dosen</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <form id="FrmAddDosen" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lengkap</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= set_value('nama'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIDN</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nidn" placeholder="Isi NIDN" value="<?= set_value('nidn'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nidn') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <option value="0" selected disabled>Pilih Data Program Studi</option>
                  <?php foreach ($prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Status Dosen</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_status_dosen">
                  <option value="0" selected disabled>Set Status Dosen</option>
                  <?php foreach ($status_dosen as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_status_dosen') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>

              </div>
              <label class="col-md-2 col-form-label">NIK</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= set_value('nik'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pendidikan Terakhir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="pendidikan_terakhir" placeholder="Isi Pendidikan Terakhir" value="<?= set_value('pendidikan_terakhir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('pendidikan_terakhir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jabatan Akademik Dosen</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jabatan_akademik_dosen">
                  <option value="0" selected disabled>Pilih Data Jabatan Akademik Dosen</option>
                  <?php foreach ($jabatan_akademik_dosen as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_jabatan_akademik_dosen') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">&nbsp;</label>
              <div class="col-md-4">
                &nbsp;
              </div>

              <label class="col-md-2 col-form-label">Pangkat / Golongan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="pangkat_golongan" placeholder="Isi Pangkat/Golongan" value="<?= set_value('pangkat_golongan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('pangkat_golongan') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Alamat</label>
              <div class="col-md-4">
                <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= set_value('alamat'); ?></textarea>
                <small class="text-danger">
                  <?php echo form_error('alamat') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input class="uniform" type="radio" value="L" name="jenis_kelamin"> Laki-laki &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="uniform" type="radio" value="P" name="jenis_kelamin"> Perempuan
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= set_value('tempat_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama</label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <option value="0" selected disabled>Pilih Data Agama</option>
                  <?php foreach ($agama as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <option value="0" selected disabled>Pilih Data Golongan Darah</option>
                  <?php foreach ($goldar as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_golongan_darah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_provinsi">
                  <option value="0" selected disabled>Pilih Data Provinsi</option>
                  <?php foreach ($provinsi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_provinsi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <option value="0" selected disabled>Pilih Data Kabupaten / Kota</option>
                  <?php foreach ($kab_kota as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kabupaten_kota') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= set_value('email'); ?>">
                <small class="text-danger">
                  <?php echo form_error('email') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. Telepon</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Telepon" value="<?= set_value('no_telp'); ?>">
                <small class="text-danger">
                  <?php echo form_error('no_telp') ?>
                </small>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('dosen'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>