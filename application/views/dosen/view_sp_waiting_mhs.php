<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data SP Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianSP', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            </form>
            <hr>

            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Mahasiswa</th>
                  <th>Total SKS</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_sp as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->tahun . " (" . $row->nama_tahun_akademik . ")" ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <td style="text-align: right;"><?= $row->total_sks ?></td>
                    <td style="text-align: center;"><a href="<?= base_url('dosen/detail_sp/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></button></a>&nbsp;
                      <!-- <a href="<?= base_url('dosen/approve_krs/' . $row->id); ?>" class="btn btn-success btn-circle" onclick="return confirm('Apakah Anda yakin akan setujui KRS mahasiswa <?= $row->nama_mhs; ?> ?');">Setujui</button></a>&nbsp; <a href="<?= base_url('dosen/alasan_reject_krs/' . $row->id); ?>" class="btn btn-success btn-circle">Tolak (Isi Alasan Tolak)</button></a> -->
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>