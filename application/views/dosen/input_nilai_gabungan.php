<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Form Pengisian Nilai Gabungan
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if (is_array($detail_nilai)) { ?>
              Tahun Akademik : <b><?= $thn_akademiknya ?></b> <br>
              Semester : <b><?= $nama_semester ?></b> <br>
              Program Studi : <b><?= $prodinya ?></b> <br>
              Kelas : <b><?= $nama_kelas ?></b> <br>
              Mata Kuliah : <b><?= "[" . $kode_mk . "] " . $nama_mk ?></b> <br><br>

              <form id="FrmAddNilai" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="jumdata" value="<?= $jumdata ?>">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>NIM</th>
                      <th>Nama Mhs</th>
                      <!-- <th>Nilai</th> -->
                      <?php foreach ($data_komponen as $k) : ?>
                        <th><?= $k['nama'] ?></th>
                      <?php endforeach; ?>
                      <th>Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php
                    if (is_array($detail_nilai)) {
                      $jumnya = count($detail_nilai);

                      for ($j = 0; $j < $jumnya; $j++) {
                    ?>
                        <tr>
                          <td><?= $i++; ?></td>
                          <td><?= $detail_nilai[$j]['nim'] ?>
                            <!-- <input type="hidden" name="id_nilai[]" value="<?php //echo $detail_nilai[$j]['id_nilai'] 
                                                                                ?>"> -->

                            <!-- <input type="hidden" name="baru[]" value="<?php //echo $detail_nilai[$j]['baru'] 
                                                                            ?>"> -->

                          </td>
                          <td><?= $detail_nilai[$j]['nama'] ?></td>
                          <!-- <td><input type="text" style="text-align: right;" name="nilai_angka[]" value="<?php //echo $detail_nilai[$j]['nilai_angka'] 
                                                                                                              ?>"></td> -->

                          <?php //foreach ($data_komponen as $k) : 
                          $perkomponen = $detail_nilai[$j]['data_nilai'];
                          $jumdetail = count($perkomponen);
                          for ($xx = 0; $xx < $jumdetail; $xx++) {
                          ?>
                            <td><input type="text" style="text-align: right;" name="nilai_angka[]" value="<?php echo $perkomponen[$xx]['nilai_angka'] ?>">
                              <input type="hidden" name="id_komponen_nilai[]" value="<?php echo $perkomponen[$xx]['id_komponen_nilai'] ?>">
                              <input type="hidden" name="id_nilai[]" value="<?php echo $perkomponen[$xx]['id_nilai'] ?>">
                              <input type="hidden" name="id_mhs[]" value="<?= $detail_nilai[$j]['id_mhs'] ?>">
                              <input type="hidden" name="id_krs_detail[]" value="<?= $detail_nilai[$j]['id_krs_detail'] ?>">
                            </td>
                          <?php
                          }
                          //endforeach; 
                          ?>
                          <td><?= $detail_nilai[$j]['nilai_akhir'] . " (" . $detail_nilai[$j]['nilai_huruf'] . ")" ?></td>
                        </tr>
                    <?php }
                    }
                    ?>
                    </tr>
                  </tbody>
                </table>

                <div class="modal-footer">
                  <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="simpan" value="1"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
                  <a href="<?= base_url('dosen/view_nilai_mk'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
                </div>
              </form>
            <?php } else { ?>
              <?php if ($this->session->flashdata('message')) :
                echo $this->session->flashdata('message');
              endif; ?>
            <?php } ?>
          </div>


        </div>
      </div>
    </div>
  </div>
</main>
</div>