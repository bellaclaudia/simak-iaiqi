<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Dosen</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Dosen
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianDosen', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_prodi">
                  <option value="0">- All -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_prodi') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Status Dosen</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_status_dosen">
                  <option value="0">- All -</option>
                  <?php foreach ($data_status_dosen as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_status_dosen) { ?>selected<?php } ?>><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_status_dosen') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>

            <a href="<?= base_url('dosen/tambah'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Nama</th>
                  <th>Status Dosen</th>
                  <th>Jabatan Akademik</th>
                  <th>Jenis Kelamin</th>
                  <th>Fakultas</th>
                  <th>Prodi</th>
                  <th>Foto Profil</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php $i = 1; ?>
                  <?php foreach ($data_dosen as $row) : ?>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nidn ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->status_dosen ?></td>
                    <td><?= $row->jabatan_akademik_dosen ?></td>
                    <td><?php if ($row->jenis_kelamin == 'L') echo "Laki-Laki";
                        else echo "Perempuan"; ?></td>
                    <td><?= $row->fakultas ?></td>
                    <td><?= $row->prodi ?></td>
                    <td>
                      <?php if ($row->file_foto != NULL) : ?>
                        <a href="<?= base_url('uploads/' . $row->file_foto); ?>" target="_blank"><img src="<?= base_url('uploads/' . $row->file_foto); ?>" alt="<?= $row->file_foto ?>" width="150" class="img-thumbnail rounded-circle"></a><br>
                        <center><a href="<?= base_url('uploads/' . $row->file_foto); ?>" download>Download</a></center>
                      <?php endif ?>
                      <?php if ($row->file_foto == NULL and $row->jenis_kelamin == 'P') : ?>
                        <a href="<?= base_url('uploads/icon/dosen cewek.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/dosen cewek.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
                      <?php endif ?>
                      <?php if ($row->file_foto == NULL and $row->jenis_kelamin == 'L') : ?>
                        <a href="<?= base_url('uploads/icon/dosen cowok.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/dosen cowok.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
                      <?php endif ?>
                    </td>
                    <td>
                      <a href="<?= base_url('dosen/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></a><br><br>
                      <a href="<?= base_url('dosen/view/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a><br><br>
                      <a href="<?php echo site_url('dosen/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Dosen <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
              <?php endforeach; ?>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>