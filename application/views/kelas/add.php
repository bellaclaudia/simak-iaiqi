<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('master_kelas'); ?>">Data Master Kelas</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddKelas', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Kode Kelas</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="kode" placeholder="Isi Kode" value="<?= set_value('kode'); ?>">
              <small class="text-danger">
                <?php echo form_error('kode') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Nama Kelas</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="nama" placeholder="Isi Kelas" value="<?= set_value('nama'); ?>">
              <small class="text-danger">
                <?php echo form_error('nama') ?>
              </small>
            </div>
          </div>
          <!-- <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                <option value="0" selected disabled>Pilih Data Tahun Akademik</option>
                <?php //foreach ($tahun_akademik as $k) : 
                ?>
                  <option value="<?php //echo $k['id']; 
                                  ?>"><?php //echo $k['nama']; 
                                      ?></option>
                <?php //endforeach; 
                ?>
              </select>
              <small class="text-danger">
                <?php //echo form_error('id_tahun_akademik') 
                ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Data Semester</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-3" name="id_semester">
                <option value="0" selected disabled>Pilih Data Semester</option>
                <?php //foreach ($semester as $k) : 
                ?>
                  <option value="<?php //echo $k['id']; 
                                  ?>"><?php //echo $k['nama']; 
                                      ?></option>
                <?php //endforeach; 
                ?>
              </select>
              <small class="text-danger">
                <?php //echo form_error('id_semester') 
                ?>
              </small>
            </div>
          </div> -->
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Program Studi</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-4" name="id_program_studi">
                <option value="0" selected disabled>Pilih Data Program Studi</option>
                <?php foreach ($program_studi as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_program_studi') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Kategori Kelas</label>
            <div class="col-md-3">
              <select class="form-control" name="id_kategori_kelas">
                <option value="0" selected disabled>Pilih Data Kategori Kelas</option>
                <?php foreach ($kategori_kelas as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_kategori_kelas') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('master_kelas'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>