<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Master Kelas</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Master Kelas
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <a href="<?= base_url('master_kelas/tambah/'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kategori Kelas</th>
                  <th>Kode Kelas</th>
                  <th>Nama Kelas</th>
                  <!-- <th>Tahun akademik</th>
                  <th>Semester</th> -->
                  <th>Program Studi</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_kelas as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->kategori_kelas ?></td>
                    <td><?= $row->kode ?></td>
                    <td><?= $row->nama ?></td>
                    <!-- <td><?php //echo $row->tahun_akademik 
                              ?></td>
                    <td><?php //echo $row->semester 
                        ?></td> -->
                    <td><?= $row->program_studi ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td>
                      <a href="<?= base_url('master_kelas/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <a href="<?php echo site_url('master_kelas/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Kelas <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>