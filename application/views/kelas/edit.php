<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('master_kelas'); ?>">Data Master Kelas</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddMataKuliah', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Kode Kelas</label>
            <div class="col-md-3">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_kelas->id; ?>">
              <input class="form-control" type="text" name="kode" placeholder="Isi Kode" value="<?= $data_kelas->kode; ?>">
              <small class="text-danger">
                <?php echo form_error('kode') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Nama Kelas</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="nama" placeholder="Isi Kelas" value="<?= $data_kelas->nama; ?>">
              <small class="text-danger">
                <?php echo form_error('nama') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <!-- <label class="col-md-3 col-form-label">Data Semester</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-3" name="id_semester">
                <?php
                // $lv = '';
                // if (isset($row->id_semester)) {
                //   $lv = $row->id_semester;
                // }
                // foreach ($semester as $r => $v) {
                ?>
                  <option value="<?php //echo $v['id'] 
                                  ?>" <?php //$v['id'] == $lv ? 'selected' : '' 
                                      ?>><?php //echo $v['nama'] 
                                          ?></option>
                <?php
                //}
                ?>
              </select>
              <small class="text-danger">
                <?php //echo form_error('id_semester') 
                ?>
              </small>
            </div> -->
            <label class="col-md-3 col-form-label">Program Studi</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-4" name="id_program_studi">
                <?php
                $lv = '';
                if (isset($data_kelas->id_program_studi)) {
                  $lv = $data_kelas->id_program_studi;
                }
                foreach ($program_studi as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_program_studi') ?>
              </small>
            </div>

            <label class="col-md-3 col-form-label">Kategori Kelas</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-1" name="id_kategori_kelas">
                <?php
                $lv = '';
                if (isset($data_kelas->id_kategori_kelas)) {
                  $lv = $data_kelas->id_kategori_kelas;
                }
                foreach ($kategori_kelas as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_kategori_kelas') ?>
              </small>
            </div>

          </div>
          <!-- <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                <?php
                // $lv = '';
                // if (isset($row->id_tahun_akademik)) {
                //   $lv = $row->id_tahun_akademik;
                // }
                // foreach ($tahun_akademik as $r => $v) {
                ?>
                  <option value="<?php //echo $v['id'] 
                                  ?>" <?php //$v['id'] == $lv ? 'selected' : '' 
                                      ?>><?php //echo $v['nama'] 
                                          ?></option>
                <?php
                //}
                ?>
              </select>
              <small class="text-danger">
                <?php //echo form_error('id_tahun_akademik') 
                ?>
              </small>
            </div>
          </div> -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('master_kelas'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>