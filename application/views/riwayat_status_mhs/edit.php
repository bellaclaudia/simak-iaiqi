<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('riwayat_status_mhs'); ?>">Data Riwayat Status Mahasiswa</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddRiwayatStatusMhs', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Mahasiswa</label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_riwayat_status_mhs->id; ?>">
              <select class="form-control select2-single" id="select2-1" name="id_mhs">
                <?php
                $lv = '';
                if (isset($row->id_mhs)) {
                  $lv = $row->id_mhs;
                }
                foreach ($mahasiswa as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_mhs') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-2" name="id_tahun_akademik">
                <?php
                $lv = '';
                if (isset($row->id_tahun_akademik)) {
                  $lv = $row->id_tahun_akademik;
                }
                foreach ($tahun_akademik as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_tahun_akademik') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Semester</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-3" name="id_semester">
                <?php
                $lv = '';
                if (isset($row->id_semester)) {
                  $lv = $row->id_semester;
                }
                foreach ($semester as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Status Mahasiswa</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-4" name="id_status_mhs">
                <?php
                $lv = '';
                if (isset($row->id_status_mhs)) {
                  $lv = $row->id_status_mhs;
                }
                foreach ($status_mhs as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_status_mhs') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('riwayat_status_mhs'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>