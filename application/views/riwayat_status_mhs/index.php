<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Riwayat Status Mahasiswa</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Riwayat Status Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <a href="<?= base_url('riwayat_status_mhs/tambah/'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Mahasiswa</th>
                  <th>Semester</th>
                  <th>Tahun Akademik</th>
                  <th>Status Mahasiswa</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_riwayat_status_mhs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_mhs ?></td>
                    <td><?= $row->semester ?></td>
                    <td><?= $row->tahun_akademik ?></td>
                    <td><?= $row->status_mhs ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td>
                      <a href="<?= base_url('riwayat_status_mhs/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <a href="<?php echo site_url('riwayat_status_mhs/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Riwayat Status Mahasiswa <?= $row->nama_mhs; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>