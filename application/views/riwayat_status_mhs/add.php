<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('riwayat_status_mhs'); ?>">Data Riwayat Status Mahasiswa</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddRiwayatStatusMhs', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Mahasiswa</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-1" name="id_mhs">
                <option value="0" selected disabled>Pilih Data Mahasiswa</option>
                <?php foreach ($mahasiswa as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_mhs') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-2" name="id_tahun_akademik">
                <option value="0" selected disabled>Pilih Data Tahun Akademik</option>
                <?php foreach ($tahun_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_tahun_akademik') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Semester</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-3" name="id_semester">
                <option value="0" selected disabled>Pilih Data Semester</option>
                <?php foreach ($semester as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Status Mahasiswa</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-4" name="id_status_mhs">
                <option value="0" selected disabled>Pilih Data Status Mahasiswa</option>
                <?php foreach ($status_mhs as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_status_mhs') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('riwayat_status_mhs'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>