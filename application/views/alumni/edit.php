<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('alumni'); ?>">Data Alumni</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <form id="FrmAddAlumni" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_alumni->id; ?>">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <?php
                  $lv = '';
                  if (isset($data_alumni->id_program_studi)) {
                    $lv = $data_alumni->id_program_studi;
                  }
                  foreach ($prodi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Angkatan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_angkatan">
                  <option value="0" selected disabled>Pilih Data Angkatan</option>
                  <?php foreach ($data_angkatan as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $data_alumni->id_angkatan) { ?>selected<?php } ?>><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_angkatan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <a href="<?= base_url('uploads/' . $data_alumni->file_foto); ?>" download><?= $data_alumni->file_foto; ?></a>
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>
              </div>
              <label class="col-md-2 col-form-label">NIK</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= $data_alumni->nik; ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIM</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nim" placeholder="Isi NIM" value="<?= $data_alumni->nim; ?>">
                <small class="text-danger">
                  <?php echo form_error('nim') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Lengkap</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= $data_alumni->nama; ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Alamat</label>
              <div class="col-md-4">
                <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= $data_alumni->alamat; ?></textarea>
                <small class="text-danger">
                  <?php echo form_error('alamat') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input type="radio" name="jenis_kelamin" value="Perempuan" <?php if ($data_alumni->jenis_kelamin == "P") {
                                                                              echo "checked";
                                                                            } ?> /> Perempuan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="jenis_kelamin" value="Laki-laki" <?php if ($data_alumni->jenis_kelamin == "L") {
                                                                              echo "checked";
                                                                            } ?> /> Laki-laki
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= $data_alumni->tempat_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= $data_alumni->tgl_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama</label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <?php
                  $lv = '';
                  if (isset($data_alumni->id_agama)) {
                    $lv = $data_alumni->id_agama;
                  }
                  foreach ($agama as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <?php
                  $lv = '';
                  if (isset($data_alumni->id_golongan_darah)) {
                    $lv = $data_alumni->id_golongan_darah;
                  }
                  foreach ($goldar as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_golongan_darah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mulai Semester</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="mulai_semester" placeholder="Isi Mulai Semester" value="<?= $data_alumni->mulai_semester; ?>">
                <small class="text-danger">
                  <?php echo form_error('mulai_semester') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kelurahan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kelurahan" placeholder="Isi Kelurahan" value="<?= $data_alumni->kelurahan; ?>">
                <small class="text-danger">
                  <?php echo form_error('kelurahan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kecamatan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kecamatan" placeholder="Isi Kecamatan" value="<?= $data_alumni->kecamatan; ?>">
                <small class="text-danger">
                  <?php echo form_error('kecamatan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kode Pos</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kode_pos" placeholder="Isi Kode Pos" value="<?= $data_alumni->kode_pos; ?>">
                <small class="text-danger">
                  <?php echo form_error('kode_pos') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_provinsi">
                  <?php
                  $lv = '';
                  if (isset($data_alumni->id_provinsi)) {
                    $lv = $data_alumni->id_provinsi;
                  }
                  foreach ($provinsi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_provinsi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <?php
                  $lv = '';
                  if (isset($data_alumni->id_kabupaten_kota)) {
                    $lv = $data_alumni->id_kabupaten_kota;
                  }
                  foreach ($kab_kota as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kabupaten_kota') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= $data_alumni->email; ?>">
                <small class="text-danger">
                  <?php echo form_error('email') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. Telepon</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Telepon" value="<?= $data_alumni->no_telp; ?>">
                <small class="text-danger">
                  <?php echo form_error('no_telp') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ayah" placeholder="Isi Nama Ayah" value="<?= $data_alumni->nama_ayah; ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ayah') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= $data_alumni->nama_ibu; ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ibu') ?>
                </small>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('alumni'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>