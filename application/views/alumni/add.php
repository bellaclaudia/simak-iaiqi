<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('alumni'); ?>">Data Alumni</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <!-- <?php
                $attributes = array('id' => 'FrmAddAlumni', 'method' => "post", "autocomplete" => "off", "enctype" => "multipart/form-data");
                echo form_open('', $attributes);

                if (isset($error)) {
                  echo "ERROR UPLOAD : <br/>";
                  print_r($error);
                  echo "<hr/>";
                }
                ?> -->
          <form id="FrmAddMahasiswa" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <option value="0" selected disabled>Pilih Data Program Studi</option>
                  <?php foreach ($prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>

              <label class="col-md-2 col-form-label">Angkatan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_angkatan">
                  <option value="0" selected disabled>Pilih Data Angkatan</option>
                  <?php foreach ($data_angkatan as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_angkatan') ?>
                </small>
              </div>

            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>

              </div>
              <label class="col-md-2 col-form-label">NIK</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= set_value('nik'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIM</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nim" placeholder="Isi NIM" value="<?= set_value('nim'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nim') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Lengkap</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= set_value('nama'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Alamat</label>
              <div class="col-md-4">
                <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= set_value('alamat'); ?></textarea>
                <small class="text-danger">
                  <?php echo form_error('alamat') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input class="uniform" type="radio" value="L" name="jenis_kelamin"> Laki-laki &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="uniform" type="radio" value="P" name="jenis_kelamin"> Perempuan
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= set_value('tempat_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama</label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <option value="0" selected disabled>Pilih Data Agama</option>
                  <?php foreach ($agama as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <option value="0" selected disabled>Pilih Data Golongan Darah</option>
                  <?php foreach ($goldar as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_golongan_darah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mulai Semester</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="mulai_semester" placeholder="Isi Mulai Semester" value="<?= set_value('mulai_semester'); ?>">
                <small class="text-danger">
                  <?php echo form_error('mulai_semester') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kelurahan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kelurahan" placeholder="Isi Kelurahan" value="<?= set_value('kelurahan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kelurahan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kecamatan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kecamatan" placeholder="Isi Kecamatan" value="<?= set_value('kecamatan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kecamatan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kode Pos</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kode_pos" placeholder="Isi Kode Pos" value="<?= set_value('kode_pos'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kode_pos') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_provinsi">
                  <option value="0" selected disabled>Pilih Data Provinsi</option>
                  <?php foreach ($provinsi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_provinsi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <option value="0" selected disabled>Pilih Data Kabupaten / Kota</option>
                  <?php foreach ($kab_kota as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kabupaten_kota') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= set_value('email'); ?>">
                <small class="text-danger">
                  <?php echo form_error('email') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. Telepon</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Telepon" value="<?= set_value('no_telp'); ?>">
                <small class="text-danger">
                  <?php echo form_error('no_telp') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ayah" placeholder="Isi Nama Ayah" value="<?= set_value('nama_ayah'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ayah') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= set_value('nama_ibu'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ibu') ?>
                </small>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('alumni'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>