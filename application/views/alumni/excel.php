<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=alumni.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>SIMAK IAIQI | Alumni</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Alumni pada IAIQI Indralaya</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
      	<th class="allcen center bold">NIM</th>
      	<th class="allcen center bold">Nama</th>
      	<th class="allcen center bold">Alamat</th>
      	<th class="allcen center bold">Jenis Kelamin</th>
      	<th class="allcen center bold">Tempat/Tgl Lahir</th>
      	<th class="allcen center bold">NIK</th>
      	<th class="allcen center bold">NISN</th>
      	<th class="allcen center bold">Agama</th>
      	<th class="allcen center bold">Golongan Darah</th>
      	<th class="allcen center bold">Mulai Semester</th>
      	<th class="allcen center bold">Fakultas</th>
      	<th class="allcen center bold">Prodi</th>
      	<th class="allcen center bold">Angkatan</th>
      	<th class="allcen center bold">No. Telepon</th>
      	<th class="allcen center bold">Email</th>
      	<th class="allcen center bold">Nama Ayah</th>
      	<th class="allcen center bold">Nama Ibu</th>
      	<th class="allcen center bold">Status Mahasiswa</th>
      	<th class="allcen center bold">Tgl. Input</th>
      	<th class="allcen center bold">Tgl. Update</th>
      	<th class="allcen center bold">User Update By</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_alumni as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
      	<td><?= $row->nim ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->alamat ?>, <?= $row->kelurahan ?>, <?= $row->kecamatan ?>, <?= $row->kab_kota ?>, <?= $row->provinsi ?>, <?= $row->kode_pos ?></td>
        <td><?php if ($row->jenis_kelamin == 'L') echo "Laki-Laki";
            else echo "Perempuan"; ?></td>
        <td><?= $row->tempat_lahir ?>, <?= $row->tgl_lahir ?></td>
        <td>'<?= $row->nik ?></td>
        <td>'<?= $row->nisn ?></td>
        <td><?= $row->agama ?></td>
        <td><?= $row->goldar ?></td>
        <td><?= $row->mulai_semester ?></td>
        <td><?= $row->fakultas ?></td>
        <td><?= $row->prodi ?></td>
        <td><?= $row->nama_angkatan ?></td>
        <td><?= $row->no_telp ?></td>
        <td><?= $row->email ?></td>
        <td><?= $row->nama_ayah ?></td>
        <td><?= $row->nama_ibu ?></td>
        <td><?= $row->status_mhs ?></td>
        <td><?= $row->tgl_input ?></td>
        <td><?= $row->tgl_update ?></td>
        <td><?= $row->user_update_by ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>