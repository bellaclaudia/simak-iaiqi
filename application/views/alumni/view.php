<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('mahasiswa'); ?>">Data Alumni</a></li>
    <li class="breadcrumb-item active">View Data Alumni</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-eye"></i> View Data Alumni
        </div>
        <div class="card-body">
          <img src="<?= base_url('uploads/' . $data_alumni->file_foto); ?>" style="width: 100px"><br><br>
          <table class="table table-responsive-sm table-striped">
            <input type="hidden" name="id" value="<?= $data_alumni->id; ?>">
            <tr>
              <td>Nama Lengkap : <?= $data_alumni->nama; ?></td>
              <td colspan="2">Alamat : <?= $data_alumni->alamat; ?>, <?= $data_alumni->kelurahan; ?>, <?= $data_alumni->kecamatan; ?>, <?= $data_alumni->kab_kota; ?>, <?= $data_alumni->provinsi; ?>, <?= $data_alumni->kode_pos; ?></td>
            </tr>
            <tr>
              <td>NIM : <?= $data_alumni->nim; ?></td>
              <td>NIK : <?= $data_alumni->nik; ?></td>
              <td>Program Studi : <?= $data_alumni->prodi; ?></td>
            </tr>
            <tr>
              <td>Tempat Lahir : <?= $data_alumni->tempat_lahir; ?></td>
              <td>Tanggal Lahir : <?= $data_alumni->tgl_lahir; ?></td>
              <td>Jenis Kelamin : <?php if ($data_alumni->jenis_kelamin == 'L') echo "Laki-Laki";
                                  else echo "Perempuan"; ?></td>
            </tr>
            <tr>
              <td>Mulai Semester : <?= $data_alumni->mulai_semester; ?></td>
              <td>No. Telepon : <?= $data_alumni->no_telp; ?></td>
              <td>Status Mahasiswa : <?= $data_alumni->status_mhs; ?></td>
            </tr>
            <tr>
              <td>Email : <?= $data_alumni->email; ?></td>
              <td>Nama Ayah : <?= $data_alumni->nama_ayah; ?></td>
              <td>Nama Ibu : <?= $data_alumni->nama_ibu; ?></td>
            </tr>
            <tr>
              <td>Agama : <?= $data_alumni->agama; ?></td>
              <td>Golongan Darah : <?= $data_alumni->goldar; ?></td>
              <td>Angkatan : <?= $data_alumni->nama_angkatan; ?></td>
            </tr>
          </table>
          <div class="modal-footer">
            <a href="<?= base_url('alumni'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Kembali</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>