<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Riwayat Pendidikan Dosen</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Riwayat Pendidikan Dosen
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <a href="<?= base_url('riwayat_pend_dosen/tambah/'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Dosen</th>
                  <th>Pendidikan</th>
                  <th>Gelar</th>
                  <th>Asal Kampus</th>
                  <th>Periode</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_riwayat_pend_dosen as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_dosen ?></td>
                    <td><?= $row->pendidikan ?></td>
                    <td><?= $row->gelar ?></td>
                    <td><?= $row->asal_kampus ?></td>
                    <td><?= $row->dari_tahun ?> - <?= $row->sampai_tahun ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td>
                      <a href="<?= base_url('riwayat_pend_dosen/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <a href="<?php echo site_url('riwayat_pend_dosen/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Riwayat Pendidikan Dosen <?= $row->nama_dosen; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>