<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('riwayat_pend_dosen'); ?>">Data Riwayat Pendidikan Dosen</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddRiwayatPendDosen', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Dosen</label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_riwayat_pend_dosen->id; ?>">
              <select class="form-control select2-single" id="select2-1" name="id_dosen">
                <?php
                $lv = '';
                if (isset($row->id_dosen)) {
                  $lv = $row->id_dosen;
                }
                foreach ($dosen as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_dosen') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Pendidikan</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="pendidikan" placeholder="Isi Pendidikan" value="<?= $data_riwayat_pend_dosen->pendidikan; ?>">
              <small class="text-danger">
                <?php echo form_error('pendidikan') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Gelar</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="gelar" placeholder="Isi Gelar" value="<?= $data_riwayat_pend_dosen->gelar; ?>">
              <small class="text-danger">
                <?php echo form_error('gelar') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Asal Kampus</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="asal_kampus" placeholder="Isi Asal Kampus" value="<?= $data_riwayat_pend_dosen->asal_kampus; ?>">
              <small class="text-danger">
                <?php echo form_error('asal_kampus') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Dari Tahun</label>
            <div class="col-md-3">
              <input class="form-control" type="number" name="dari_tahun" value="<?= $data_riwayat_pend_dosen->dari_tahun; ?>">
              <small class="text-danger">
                <?php echo form_error('dari_tahun') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Sampai Tahun</label>
            <div class="col-md-3">
              <input class="form-control" type="number" name="sampai_tahun" value="<?= $data_riwayat_pend_dosen->sampai_tahun; ?>">
              <small class="text-danger">
                <?php echo form_error('sampai_tahun') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('riwayat_pend_dosen'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>