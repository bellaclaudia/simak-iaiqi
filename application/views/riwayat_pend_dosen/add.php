<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('riwayat_pend_dosen'); ?>">Data Riwayat Pendidikan Dosen</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddRiwayatPendDosen', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Dosen</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-1" name="id_dosen">
                <option value="0" selected disabled>Pilih Data Dosen</option>
                <?php foreach ($dosen as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_dosen') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Pendidikan</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="pendidikan" placeholder="Isi Pendidikan" value="<?= set_value('pendidikan'); ?>">
              <small class="text-danger">
                <?php echo form_error('pendidikan') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Gelar</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="gelar" placeholder="Isi Gelar" value="<?= set_value('gelar'); ?>">
              <small class="text-danger">
                <?php echo form_error('gelar') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Asal Kampus</label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="asal_kampus" placeholder="Isi Asal Kampus" value="<?= set_value('asal_kampus'); ?>">
              <small class="text-danger">
                <?php echo form_error('asal_kampus') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Dari Tahun</label>
            <div class="col-md-3">
              <input class="form-control" type="number" name="dari_tahun" value="<?= set_value('dari_tahun'); ?>">
              <small class="text-danger">
                <?php echo form_error('dari_tahun') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Sampai Tahun</label>
            <div class="col-md-3">
              <input class="form-control" type="number" name="sampai_tahun" value="<?= set_value('sampai_tahun'); ?>">
              <small class="text-danger">
                <?php echo form_error('sampai_tahun') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('riwayat_pend_dosen'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>