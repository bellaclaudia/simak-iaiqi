<script type="text/javascript">
  var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,',
      template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
      base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
      },
      format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
          return c[p];
        })
      }
    return function(table, name) {
      if (!table.nodeType) table = document.getElementById(table)
      var ctx = {
        worksheet: name || 'Worksheet',
        table: table.innerHTML
      }
      window.location.href = uri + base64(format(template, ctx))
    }
  })()
</script>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Mata Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Mata Kuliah
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?><br>

            <b>Filter Pencarian</b>
            <!-- <?php
                  $attributes = array('id' => 'FrmPencarianMk', 'method' => "post", "autocomplete" => "off");
                  echo form_open('', $attributes);
                  ?> -->
            <form method="post" autocomplete="off" id="form_cari">
              <div class="form-group row">
                <label class="col-md-2 col-form-label">Semester</label>
                <div class="col-md-4">
                  <select class="form-control select2-single" id="select2-1" name="id_semester">
                    <option value="0">- All -</option>
                    <?php foreach ($data_semester as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('id_semester') ?>
                  </small>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-2 col-form-label">Program Studi</label>
                <div class="col-md-4">
                  <select class="form-control select2-single" id="select2-2" name="id_prodi">
                    <option value="0">- All -</option>
                    <?php foreach ($data_prodi as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('id_prodi') ?>
                  </small>
                </div>
              </div>
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="cari" value="1">Cari</button>&nbsp;
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>
            <a href="<?= base_url('master_mata_kuliah/tambah/'); ?>"><button style="margin-left: 78%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a>&nbsp;&nbsp;
            <a href="<?= base_url('uploads/template_master_matkul.xls'); ?>" download><button class="btn btn-sm btn-success mb-1" type="button">Download Template</button></a>&nbsp;&nbsp;
            <a href="<?= base_url('master_mata_kuliah/template/'); ?>"><button class="btn btn-sm btn-success mb-1" type="button">Data Template</button></a><br><br>

            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Jenis Mata Kuliah</th>
                  <th>Kode</th>
                  <th>Mata Kuliah</th>
                  <th>SKS</th>
                  <th>SKS Tatap Muka</th>
                  <th>SKS Praktik</th>
                  <th>SKS Praktik Lapangan</th>
                  <th>SKS Simulasi</th>
                  <th>Metode Pembelajaran</th>
                  <th>Tanggal Mulai Efektif</th>
                  <th>Tanggal Akhir Efektif</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_mata_kuliah as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->jenis_mk ?></td>
                    <td><?= $row->kode ?></td>
                    <td><?= $row->nama ?></td>
                    <td style="text-align: right;"><?= $row->sks ?></td>
                    <td style="text-align: right;"><?= $row->sks_tatap_muka ?></td>
                    <td style="text-align: right;"><?= $row->sks_praktik ?></td>
                    <td style="text-align: right;"><?= $row->sks_praktik_lapangan ?></td>
                    <td style="text-align: right;"><?= $row->sks_simulasi ?></td>
                    <td><?= $row->metode_pembelajaran ?></td>
                    <td><?= $row->tgl_mulai_efektif ?></td>
                    <td><?= $row->tgl_akhir_efektif ?></td>
                    <td><?= $row->semester ?></td>
                    <td><?= $row->program_studi ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td>
                      <?php
                      $boleh_delete = 1;

                      // cek krs_mhs_detail
                      $sqlxx = " SELECT id FROM krs_mhs_detail WHERE id_mata_kuliah = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }

                      // cek khs_mhs_mk
                      $sqlxx = " SELECT id FROM khs_mhs_mk WHERE id_mata_kuliah = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }
                      ?>

                      <a href="<?= base_url('master_mata_kuliah/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <?php if ($boleh_delete == 1) { ?>
                        <br><br>
                        <a href="<?php echo site_url('master_mata_kuliah/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Mata Kuliah <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <script>
              function exportData() {
                form_data = $("#form_cari").serialize();

                window.location = 'master_mata_kuliah/export?' + form_data;
              }
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>