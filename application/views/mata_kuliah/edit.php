<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('master_mata_kuliah'); ?>">Data Mata Kuliah</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddMataKuliah', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Jenis Mata Kuliah</label>
            <div class="col-md-3">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_mata_kuliah->id; ?>">
              <select class="form-control" name="id_jenis_mk">
                <?php
                $lv = '';
                if (isset($row->id_jenis_mk)) {
                  $lv = $row->id_jenis_mk;
                }
                foreach ($jenis_mk as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_jenis_mk') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">SKS</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="sks" placeholder="Isi SKS" value="<?= $data_mata_kuliah->sks; ?>">
              <small class="text-danger">
                <?php echo form_error('sks') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Kode</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="kode" placeholder="Isi Kode" value="<?= $data_mata_kuliah->kode; ?>">
              <small class="text-danger">
                <?php echo form_error('kode') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Mata Kuliah</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="nama" placeholder="Isi Mata Kuliah" value="<?= $data_mata_kuliah->nama; ?>">
              <small class="text-danger">
                <?php echo form_error('nama') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">SKS Tatap Muka</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="sks_tatap_muka" placeholder="Isi SKS Tatap Muka" value="<?= $data_mata_kuliah->sks_tatap_muka; ?>">
              <small class="text-danger">
                <?php echo form_error('sks_tatap_muka') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">SKS Praktik</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="sks_praktik" placeholder="Isi SKS Praktik" value="<?= $data_mata_kuliah->sks_praktik; ?>">
              <small class="text-danger">
                <?php echo form_error('sks_praktik') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">SKS Praktik Lapangan</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="sks_praktik_lapangan" placeholder="Isi SKS Praktik Lapangan" value="<?= $data_mata_kuliah->sks_praktik_lapangan; ?>">
              <small class="text-danger">
                <?php echo form_error('sks_praktik_lapangan') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">SKS Simulasi</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="sks_simulasi" placeholder="Isi SKS Simulasi" value="<?= $data_mata_kuliah->sks_simulasi; ?>">
              <small class="text-danger">
                <?php echo form_error('sks_simulasi') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Metode Pembelajaran</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-1" name="id_metode_pembelajaran">
                <?php
                $lv = '';
                if (isset($row->id_metode_pembelajaran)) {
                  $lv = $row->id_metode_pembelajaran;
                }
                foreach ($metode_pembelajaran as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_metode_pembelajaran') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-2" name="id_tahun_akademik">
                <?php
                $lv = '';
                if (isset($row->id_tahun_akademik)) {
                  $lv = $row->id_tahun_akademik;
                }
                foreach ($tahun_akademik as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_tahun_akademik') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai Efektif</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_efektif" value="<?= $data_mata_kuliah->tgl_mulai_efektif; ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_efektif') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Akhir Efektif</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_akhir_efektif" value="<?= $data_mata_kuliah->tgl_akhir_efektif; ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_akhir_efektif') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Data Semester</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-3" name="id_semester">
                <?php
                $lv = '';
                if (isset($row->id_semester)) {
                  $lv = $row->id_semester;
                }
                foreach ($semester as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_semester') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Data Program Studi</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-4" name="id_program_studi">
                <?php
                $lv = '';
                if (isset($row->id_program_studi)) {
                  $lv = $row->id_program_studi;
                }
                foreach ($program_studi as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_program_studi') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('master_mata_kuliah'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>