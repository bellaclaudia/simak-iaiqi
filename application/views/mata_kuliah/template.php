<main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item">
            <a href="#">Admin</a>
          </li>
          <li class="breadcrumb-item active">Data Template Master Mata Kuliah</li>
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-list"></i> Data Template Master Mata Kuliah
              </div>
              <div style="overflow-x:auto;">
              <div class="card-body" id="dataarea">
                <a href="<?= base_url('master_mata_kuliah'); ?>"><button class="btn btn-sm btn-success mb-1" type="button">Kembali</button></a>&nbsp;&nbsp;
                <button style="margin-left: 88%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Upload Template</button><br><br>
                <?php if ($this->session->flashdata('message')) :
                    echo $this->session->flashdata('message');
                endif; ?>
                <table class="table table-striped table-bordered datatable">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>File</th>
                      <th>Tgl. Input</th>
                      <th>Tgl. Proses / User Proses By</th>
                      <th>Status Proses</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php $i=1; ?>
                    <?php foreach ($data_template_matkul as $row) : ?>
                    <tr>
                      <td><?= $i++; ?></td>
                      <td><a class="fa fa-download" href="<?=base_url('uploads/biodata/template_matkul/'.$row['nama_file'])?>" title="Download File" download> <?=$row['nama_file']?></td>
                      <td><?= $row['tgl_input'] ?></td>
                      <?php if(!empty($row['process_time'])){ ?>
			                <td><?=$row['process_time'].' / '.$row['process_by']?></td>
			                <td>Sudah Diproses</td>
			            <?php }
			            else{
			                ?>
			                <td>Belum Diproses</td>
			                <td><a style="color: white" class="btn btn-sm btn-success" title="Proses" href="<?= base_url('master_mata_kuliah/proses_data/'.$row['id']); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Memproses Template Data Mata Kuliah ini ?');"> Proses </a></td>
			                <?php
			            }
			            ?>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Tambah Data</h4>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form id="FrmAddTemplateMatkul" action="<?php echo site_url('master_mata_kuliah/submit_template'); ?>" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            <div class="form-group row">
                                  <label class="col-md-3 col-form-label">File CSV</label>
                                  <div class="col-md-9">
                                    <input class="form-control" type="file" name="file_csv" accept=".csv" required>
                                    <span style="color: red">*File type = .csv</span>
                                  </div>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                              </div>
                            </form>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </main>
    </div>
<div class="modal fade" id="basic"  role="basic" aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
        </div>


        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>