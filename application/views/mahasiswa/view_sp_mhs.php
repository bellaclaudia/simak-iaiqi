<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data SP Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianSP', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <!-- <a href="<?= base_url('calon_mhs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
            </form>
            <hr>

            <a href="<?= base_url('mahasiswa/input_sp'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>

            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Mahasiswa</th>
                  <th>Program Studi</th>
                  <th>Total SKS</th>
                  <th>Status Verifikasi Dosen</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_sp_mhs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->tahun . " (" . $row->nama_tahun_akademik . ")" ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td style="text-align: right;"><?= $row->total_sks ?></td>
                    <td><?php
                        if ($row->status_verifikasi == '1') {
                          $boleh_edit = 0;

                          $exptgl = explode('-', $row->tgl_verifikasi);
                          $thn = $exptgl[0];
                          $tgl = $exptgl[2];
                          $bln = $exptgl[1];
                          $row->tgl_verifikasi = $tgl . "-" . $bln . "-" . $thn;

                          $desc_status_verifikasi = "Disetujui (Tanggal: " . $row->tgl_verifikasi . ", Oleh: " . $row->nama_dosen . ")";
                        } else if ($row->status_verifikasi == '2') {
                          $boleh_edit = 1;

                          $exptgl = explode('-', $row->tgl_verifikasi);
                          $thn = $exptgl[0];
                          $tgl = $exptgl[2];
                          $bln = $exptgl[1];
                          $row->tgl_verifikasi = $tgl . "-" . $bln . "-" . $thn;

                          $desc_status_verifikasi = "Ditolak (Tanggal: " . $row->tgl_verifikasi . ", Oleh: " . $row->nama_dosen . "<br>Alasan Ditolak: " . $row->alasan_ditolak . ")";
                        } else {
                          $boleh_edit = 1;
                          $desc_status_verifikasi = "Waiting";
                        }
                        echo $desc_status_verifikasi ?></td>
                    <td>
                      <?php
                      if ($boleh_edit == '1') { ?>
                        <a href="<?= base_url('mahasiswa/edit_sp/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                        <a href="<?php echo site_url('mahasiswa/hapus_sp/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data SP ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                      <?php } ?>
                      <a href="<?= base_url('mahasiswa/detail_sp/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></button></a>
                    </td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>