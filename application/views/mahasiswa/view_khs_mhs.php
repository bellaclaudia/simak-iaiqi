<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data KHS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php //if ($this->session->flashdata('message')) :
            if ($blm_bayar == '1') :
              echo $this->session->flashdata('message');
            endif;
            ?>

            <?php if ($blm_bayar == '0') { ?>
              <b>Filter Pencarian</b>
              <?php
              $attributes = array('id' => 'FrmPencarianKHS', 'method' => "post", "autocomplete" => "off");
              echo form_open('', $attributes);
              ?>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Tahun Akademik</label>
                <div class="col-md-4">
                  <input type="hidden" name="is_cari" value="1">
                  <select class="form-control select2-single" id="select2-1" name="id_ta">
                    <option value="0">- All -</option>
                    <?php foreach ($data_thn_akademik as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('id_ta') ?>
                  </small>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Semester</label>
                <div class="col-md-4">
                  <select class="form-control select2-single" id="select2-2" name="id_semester">
                    <option value="0">- All -</option>
                    <?php foreach ($data_semester as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('id_semester') ?>
                  </small>
                </div>
              </div>

              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
              <!-- <a href="<?= base_url('calon_mhs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
              </form>

              <?php //if ($is_cari == '1') { 
              ?>
              <hr>

              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tahun Akademik</th>
                    <th>Semester</th>
                    <th>Mahasiswa</th>
                    <th>Mata Kuliah</th>
                    <?php foreach ($data_komponen as $k) : ?>
                      <th><?= $k['nama'] ?></th>
                    <?php endforeach; ?>
                    <th>Nilai Akhir</th>
                    <th>Huruf Mutu</th>
                    <!-- <th>Nilai Per Komponen</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($data_khs_mhs as $row) : ?>
                    <tr>
                      <td><?= $i++; ?></td>
                      <td><?= $row->nama_tahun_akademik ?></td>
                      <td><?= $row->nama_semester ?></td>
                      <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                      <td><?= $row->nama_mk ?></td>
                      <?php foreach ($data_komponen as $k) : ?>
                        <td style="text-align: center;">
                          <?php
                          $sqlxx = " SELECT nilai_angka FROM khs_mhs_mk_detail WHERE id_khs_mk = '$row->id' AND id_komponen_nilai = '" . $k['id'] . "' ";
                          $queryxx = $this->db->query($sqlxx);
                          if ($queryxx->num_rows() > 0) {
                            $hasilxx = $queryxx->row();
                            $nilai_angka    = $hasilxx->nilai_angka;
                          } else
                            $nilai_angka = "-";

                          echo $nilai_angka;
                          ?>
                        </td>
                      <?php endforeach; ?>
                      <td style="text-align: center;"><?= $row->nilai_akhir ?></td>
                      <td><?= $row->nilai_huruf ?></td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            <?php }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>