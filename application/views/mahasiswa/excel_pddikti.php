<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=pddikti_mahasiswa.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>SIMAK IAIQI | Data Mahasiswa</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
        <th class="allcen center bold">NO.</th>
      	<th class="allcen center bold">NIM</th>
      	<th class="allcen center bold">Nama</th>
      	<th class="allcen center bold">Tempat Lahir</th>
      	<th class="allcen center bold">Tanggal Lahir</th>
      	<th class="allcen center bold">Jenis Kelamin</th>
        <th class="allcen center bold">NIK</th>
        <th class="allcen center bold">Agama</th>
        <th class="allcen center bold">NISN</th>
        <th class="allcen center bold">Jalur Pendaftaran</th>
        <th class="allcen center bold">NPWP</th>
        <th class="allcen center bold">Kewarganegaraan</th>
        <th class="allcen center bold">Jenis Pendaftaran</th>
        <th class="allcen center bold">Tgl Masuk Kuliah</th>
      	<th class="allcen center bold">Mulai Semester</th>
        <th class="allcen center bold">Jalan</th>
        <th class="allcen center bold">RT</th>
        <th class="allcen center bold">RW</th>
        <th class="allcen center bold">Dusun</th>
      	<th class="allcen center bold">Kelurahan</th>
      	<th class="allcen center bold">Kecamatan</th>
      	<th class="allcen center bold">Kode Pos</th>
        <th class="allcen center bold">Jenis Tinggal</th>
        <th class="allcen center bold">Alat Transportasi</th>
        <th class="allcen center bold">Telp Rumah</th>
        <th class="allcen center bold">No HP</th>
        <th class="allcen center bold">Email</th>
      	<th class="allcen center bold">Terima KPS</th>
      	<th class="allcen center bold">No KPS</th>
        <th class="allcen center bold">NIK Ayah</th>
      	<th class="allcen center bold">Nama Ayah</th>
      	<th class="allcen center bold">Tgl Lahir Ayah</th>
      	<th class="allcen center bold">Pendidikan Ayah</th>
      	<th class="allcen center bold">Pekerjaan Ayah</th>
      	<th class="allcen center bold">Penghasilan Ayah</th>
      	<th class="allcen center bold">NIK Ibu</th>
      	<th class="allcen center bold">Nama Ibu</th>
      	<th class="allcen center bold">Tanggal Lahir Ibu</th>
      	<th class="allcen center bold">Pendidikan Ibu</th>
      	<th class="allcen center bold">Pekerjaan Ibu</th>
      	<th class="allcen center bold">Penghasilan Ibu</th>
      	<th class="allcen center bold">Nama Wali</th>
      	<th class="allcen center bold">Tanggal Lahir wali</th>
      	<th class="allcen center bold">Pendidikan Wali</th>
      	<th class="allcen center bold">Pekerjaan Wali</th>
      	<th class="allcen center bold">Penghasilan Wali</th>
      	<th class="allcen center bold">Kode Prodi</th>
      	<th class="allcen center bold">Jenis Pembiayaan</th>
      	<th class="allcen center bold">Jumlah Biaya Masuk</th>
      	<th class="allcen center bold">SKS Diakui</th>
      	<th class="allcen center bold">Asal Perguruan Tinggi</th>
      	<th class="allcen center bold">Asal Program Studi</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_mahasiswa as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
      	<td><?= $row->nim ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->tempat_lahir ?></td>
        <td>
        	<?php
	          if ($row->tgl_lahir != '') {
	            $exptgl = explode('-', $row->tgl_lahir);
	            $thn = $exptgl[0];
	            $tgl = $exptgl[2];
	            $bln = $exptgl[1];
	            $row->tgl_lahir = $tgl . "-" . $bln . "-" . $thn;
	          }
	        ?>
	        <?= $row->tgl_lahir ?>
        </td>
        <td><?= $row->jenis_kelamin ?></td>
        <td>'<?= $row->nik ?></td>
        <td><?= $row->agama ?></td>
        <td>'<?= $row->nisn ?></td>
        <td><?= $row->jalur_pendaftaran ?></td>
        <td><?= $row->npwp ?></td>
        <td><?= $row->kewarganegaraan ?></td>
        <td><?= $row->jenis_mhs ?></td>
        <td><?= $row->tgl_masuk_kuliah ?></td>
        <td><?= $row->mulai_semester ?></td>
        <td><?= $row->alamat ?></td>
        <td><?= $row->rt ?></td>
        <td><?= $row->rw ?></td>
        <td><?= $row->dusun ?></td>
        <td><?= $row->kelurahan ?></td>
        <td><?= $row->kecamatan ?></td>
        <td><?= $row->kode_pos ?></td>
        <td><?= $row->jenis_tinggal ?></td>
        <td><?= $row->transportasi ?></td>
        <td><?= $row->telp_rmh ?></td>
        <td><?= $row->no_telp ?></td>
        <td><?= $row->email ?></td>
        <td><?= $row->kps ?></td>
        <td><?= $row->no_kps ?></td>
        <td><?= $row->nik_ayah ?></td>
        <td><?= $row->nama_ayah ?></td>
        <td><?= $row->tgl_lahir_ayah ?></td>
        <td><?= $row->pend_ayah ?></td>
        <td><?= $row->pek_ayah ?></td>
        <td><?= $row->penghasilan_ayah ?></td>
        <td><?= $row->nik_ibu ?></td>
        <td><?= $row->nama_ibu ?></td>
        <td><?= $row->tgl_lahir_ibu ?></td>
        <td><?= $row->pend_ibu ?></td>
        <td><?= $row->pek_ibu ?></td>
        <td><?= $row->penghasilan_ibu ?></td>
        <td><?= $row->nama_wali ?></td>
        <td><?= $row->tgl_lahir_wali ?></td>
        <td><?= $row->pend_wali ?></td>
        <td><?= $row->pek_wali ?></td>
        <td><?= $row->penghasilan_wali ?></td>
        <td><?= $row->kode_prodi ?></td>
        <td><?= $row->jenis_pembiayaan ?></td>
        <td><?= $row->jumlah_biaya_masuk ?></td>
        <td><?= $row->sks_diakui ?></td>
        <td><?= $row->program_studi_asal ?></td>
        <td><?= $row->perguruan_tinggi_asal ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>