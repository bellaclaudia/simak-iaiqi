<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Status Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianStatus', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <!-- <a href="<?= base_url('calon_mhs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
            </form>
            <hr>

            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Mahasiswa</th>
                  <th>Status Mahasiswa</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_status_mhs as $row) : ?>
                  <tr>

                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_tahun_akademik ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <td><?= $row->nama_status ?></td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>