<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('mahasiswa'); ?>">Data Mahasiswa</a></li>
    <li class="breadcrumb-item active">View Data Mahasiswa</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-eye"></i> View Data Mahasiswa
        </div>
        <div class="card-body">
          <?php if ($data_mahasiswa->file_foto != NULL) : ?>
            <a href="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" target="_blank"><img src="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" alt="<?= $data_mahasiswa->file_foto ?>" width="150" class="img-thumbnail rounded-circle"></a><br>
            <center><a href="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" download>Download</a></center>
          <?php endif ?>
          <?php if ($data_mahasiswa->file_foto == NULL and $data_mahasiswa->jenis_kelamin == 'P') : ?>
            <a href="<?= base_url('uploads/icon/santriwati.png'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/santriwati.png'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?>
          <?php if ($data_mahasiswa->file_foto == NULL and $data_mahasiswa->jenis_kelamin == 'L') : ?>
            <a href="<?= base_url('uploads/icon/santri.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/santri.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?><br><br>
          <table class="table table-responsive-sm table-striped">
            <input type="hidden" name="id" value="<?= $data_mahasiswa->id; ?>">
            <tr>
              <td>Nama Lengkap : <?= $data_mahasiswa->nama; ?></td>
              <td>NISN : <?= $data_mahasiswa->nisn; ?></td>
              <td>Angkatan : <?= $data_mahasiswa->nama_angkatan ?></td>
            </tr>
            <tr>
              <td>Kelas : <?= $data_mahasiswa->kelas; ?></td>
              <td>Alamat : <?= $data_mahasiswa->alamat; ?>, <?= $data_mahasiswa->rt; ?>, <?= $data_mahasiswa->rw; ?>, <?= $data_mahasiswa->dusun; ?>, <?= $data_mahasiswa->kelurahan; ?>, <?= $data_mahasiswa->kecamatan; ?>, <?= $data_mahasiswa->kab_kota; ?>, <?= $data_mahasiswa->provinsi; ?>, <?= $data_mahasiswa->kode_pos; ?></td>
              <td>Jenis Tempat Tinggal : <?= $data_mahasiswa->jenis_tinggal; ?></td>
            </tr>
            <tr>
              <td>NIM : <?= $data_mahasiswa->nim; ?></td>
              <td>NIK : <?= $data_mahasiswa->nik; ?></td>
              <td>Program Studi : <?= $data_mahasiswa->prodi; ?></td>
            </tr>
            <tr>
              <td>No. Telepon Rumah : <?= $data_mahasiswa->telp_rmh; ?></td>
              <td>KPS : <?= $data_mahasiswa->kps; ?></td>
              <td>No. KPS (Jika Ada) : <?= $data_mahasiswa->no_kps; ?></td>
            </tr>
            <tr>
              <td>NPWP : <?= $data_mahasiswa->npwp; ?></td>
              <td>Kewarganegaraan : <?= $data_mahasiswa->kewarganegaraan; ?></td>
              <td>Alat Transportasi : <?= $data_mahasiswa->transportasi; ?></td>
            </tr>
            <tr>
              <td>Tempat Lahir : <?= $data_mahasiswa->tempat_lahir; ?></td>
              <td>Tanggal Lahir : <?= $data_mahasiswa->tgl_lahir; ?></td>
              <td>Jenis Kelamin : <?php if ($data_mahasiswa->jenis_kelamin == 'L') echo "Laki-Laki";
                                  else echo "Perempuan"; ?></td>
            </tr>
            <tr>
              <td>Agama : <?= $data_mahasiswa->agama; ?></td>
              <td>Golongan Darah : <?= $data_mahasiswa->goldar; ?></td>
              <td>Jalur Pendaftaran : <?= $data_mahasiswa->jalur_pendaftaran; ?></td>
            </tr>
            <tr>
              <td>Tgl. Masuk Kuliah : <?= $data_mahasiswa->tgl_masuk_kuliah; ?></td>
              <td>Mulai Semester : <?= $data_mahasiswa->mulai_semester; ?></td>
              <td>No. Telepon : <?= $data_mahasiswa->no_telp; ?></td>
            </tr>
            <tr>
              <td>NIK Ayah : <?= $data_mahasiswa->nik_ayah; ?></td>
              <td>Nama Ayah : <?= $data_mahasiswa->nama_ayah; ?></td>
              <td>Tgl. Lahir Ayah : <?= $data_mahasiswa->tgl_lahir_ayah; ?></td>
            </tr>
            <tr>
              <td>Pendidikan Ayah : <?= $data_mahasiswa->pend_ayah; ?></td>
              <td>Pekerjaan Ayah : <?= $data_mahasiswa->pek_ayah; ?></td>
              <td>Penghasilan Ayah : <?= $data_mahasiswa->penghasilan_ayah; ?></td>
            </tr>
            <tr>
              <td>NIK Ibu : <?= $data_mahasiswa->nik_ibu; ?></td>
              <td>Nama Ibu : <?= $data_mahasiswa->nama_ibu; ?></td>
              <td>Tgl. Lahir Ibu : <?= $data_mahasiswa->tgl_lahir_ibu; ?></td>
            </tr>
            <tr>
              <td>Pendidikan Ibu : <?= $data_mahasiswa->pend_ibu; ?></td>
              <td>Pekerjaan Ibu : <?= $data_mahasiswa->pek_ibu; ?></td>
              <td>Penghasilan Ibu : <?= $data_mahasiswa->penghasilan_ibu; ?></td>
            </tr>
            <tr>
              <td>Nama Wali : <?= $data_mahasiswa->nama_wali; ?></td>
              <td>Tgl. Lahir Wali : <?= $data_mahasiswa->tgl_lahir_wali; ?></td>
              <td>Pendidikan Wali : <?= $data_mahasiswa->pend_wali; ?></td>
            </tr>
            <tr>
              <td>Pekerjaan Wali : <?= $data_mahasiswa->pek_wali; ?></td>
              <td>Penghasilan Wali : <?= $data_mahasiswa->penghasilan_wali; ?></td>
              <td>Email : <?= $data_mahasiswa->email; ?></td>
            </tr>
            <tr>
              <td>SKS Diakui : <?= $data_mahasiswa->sks_diakui; ?></td>
              <td>Program Studi Asal : <?= $data_mahasiswa->program_studi_asal; ?></td>
              <td>Perguruan Tinggi Asal : <?= $data_mahasiswa->perguruan_tinggi_asal; ?></td>
            </tr>
            <tr>
              <td>Status Mahasiswa : <?= $data_mahasiswa->status_mhs; ?></td>
              <td>Jenis Pembiayaan : <?= $data_mahasiswa->jenis_pembiayaan; ?></td>
              <td>Jumlah Biaya Masuk : <?= $data_mahasiswa->jumlah_biaya_masuk; ?></td>
            </tr>
            <tr>
              <td>Tanggal Lulus : <?= $data_mahasiswa->tgl_lulus; ?></td>
              <td colspan="2">Nomor Ijazah : <?= $data_mahasiswa->nomor_ijazah; ?></td>
            </tr>
            <tr>
              <td>Nomor Transkrip : <?= $data_mahasiswa->nomor_transkrip; ?></td>
              <td colspan="2">Judul Skripsi : <?= $data_mahasiswa->judul_skripsi; ?></td>
            </tr>
          </table>
          <div class="modal-footer">
            <a href="<?= base_url('mahasiswa'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Kembali</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>