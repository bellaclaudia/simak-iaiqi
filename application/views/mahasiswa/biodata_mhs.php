<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Biodata Mahasiswa
        </div>
        <div class="card-body">
          <form id="FrmEditMahasiswa" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lengkap <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_mahasiswa->id; ?>">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= $data_mahasiswa->nama; ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIM</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nim" value="<?= $data_mahasiswa->nim; ?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NISN</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nisn" placeholder="Isi NISN" value="<?= $data_mahasiswa->nisn; ?>">
              </div>
              <label class="col-md-2 col-form-label">NPWP</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="npwp" placeholder="Isi NPWP" value="<?= $data_mahasiswa->npwp; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kewarganegaraan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kewarganegaraan" placeholder="Isi Kewarganegaraan" value="<?= $data_mahasiswa->kewarganegaraan; ?>">
                <small class="text-danger">
                  <?php echo form_error('kewarganegaraan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIK <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= $data_mahasiswa->nik; ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kelas <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_kelas" disabled>
                  <option value="0" selected>Pilih Data Kelas</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_kelas)) {
                    $lv = $data_mahasiswa->id_kelas;
                  }
                  foreach ($kelas as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <span style="color: red">*Kelas hanya dapat di update oleh Admin</span>
                <small class="text-danger">
                  <?php echo form_error('id_kelas') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Status Mahasiswa</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-7" name="id_status_mhs">
                  <option value="0" selected>Pilih Status Mahasiswa</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_status_mhs)) {
                    $lv = $data_mahasiswa->id_status_mhs;
                  }
                  foreach ($status_mhs as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <?php if ($data_mahasiswa->file_foto != NULL) : ?>
                  <a href="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" target="_blank"><img src="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" alt="<?= $data_mahasiswa->file_foto ?>" width="150" class="img-thumbnail rounded-circle"></a><br>
                  <a href="<?= base_url('uploads/' . $data_mahasiswa->file_foto); ?>" download>Download</a>
                <?php endif ?>
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>
              </div>
              <label class="col-md-2 col-form-label">Jenis Pendaftaran <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control" name="jenis_mhs">
                  <option value="0" <?= $data_mahasiswa->jenis_mhs == 0 ? 'selected' : '' ?>>Pilih Jenis Mahasiswa</option>
                  <option value="Lainnya" <?= $data_mahasiswa->jenis_mhs == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                  <option value="Peserta Didik Baru" <?= $data_mahasiswa->jenis_mhs == 'Peserta Didik Baru' ? 'selected' : '' ?>>Peserta Didik Baru</option>
                  <option value="Pindahan" <?= $data_mahasiswa->jenis_mhs == 'Pindahan' ? 'selected' : '' ?>>Pindahan</option>
                  <option value="Naik Kelas" <?= $data_mahasiswa->jenis_mhs == 'Naik Kelas' ? 'selected' : '' ?>>Naik Kelas</option>
                  <option value="Akselerasi" <?= $data_mahasiswa->jenis_mhs == 'Akselerasi' ? 'selected' : '' ?>>Akselerasi</option>
                  <option value="Mengulang" <?= $data_mahasiswa->jenis_mhs == 'Mengulang' ? 'selected' : '' ?>>Mengulang</option>
                  <option value="Lanjutan Semester" <?= $data_mahasiswa->jenis_mhs == 'Lanjutan Semester' ? 'selected' : '' ?>>Lanjutan Semester</option>
                  <option value="Pindahan Alih Bentuk" <?= $data_mahasiswa->jenis_mhs == 'Pindahan Alih Bentuk' ? 'selected' : '' ?>>Pindahan Alih Bentuk</option>
                  <option value="Putus Sekolah" <?= $data_mahasiswa->jenis_mhs == 'Putus Sekolah' ? 'selected' : '' ?>>Putus Sekolah</option>
                  <option value="Kelas Ekstensi" <?= $data_mahasiswa->jenis_mhs == 'Kelas Ekstensi' ? 'selected' : '' ?>>Kelas Ekstensi</option>
                  <option value="Alih Jenjang" <?= $data_mahasiswa->jenis_mhs == 'Alih Jenjang' ? 'selected' : '' ?>>Alih Jenjang</option>
                  <option value="Lintas Jalur" <?= $data_mahasiswa->jenis_mhs == 'Lintas Jalur' ? 'selected' : '' ?>>Lintas Jalur</option>
                  <option value="Rekognisi Pembelajaran Lampau (RPL)" <?= $data_mahasiswa->jenis_mhs == 'Rekognisi Pembelajaran Lampau (RPL)' ? 'selected' : '' ?>>Rekognisi Pembelajaran Lampau (RPL)</option>
                </select>
                <small class="text-danger">
                  <?php echo form_error('jenis_mhs') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Angkatan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-8" name="id_angkatan">
                  <option value="0" selected>Pilih Data Angkatan</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_angkatan)) {
                    $lv = $data_mahasiswa->id_angkatan;
                  }
                  foreach ($angkatan as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Program Studi <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <option value="0" selected>Pilih Data Program Studi</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_program_studi)) {
                    $lv = $data_mahasiswa->id_program_studi;
                  }
                  foreach ($prodi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jalan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="alamat" value="<?= $data_mahasiswa->alamat; ?>" placeholder="Isi Jalan">
              </div>
              <label class="col-md-2 col-form-label">RT</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="rt" value="<?= $data_mahasiswa->rt; ?>" placeholder="Isi RT">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">RW</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="rw" value="<?= $data_mahasiswa->rw; ?>" placeholder="Isi RW">
              </div>
              <label class="col-md-2 col-form-label">Nama Dusun</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="dusun" value="<?= $data_mahasiswa->dusun; ?>" placeholder="Isi Nama Dusun">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kecamatan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kecamatan" placeholder="Isi Kecamatan" value="<?= $data_mahasiswa->kecamatan; ?>">
                <small class="text-danger">
                  <?php echo form_error('kecamatan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kelurahan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kelurahan" placeholder="Isi Kelurahan" value="<?= $data_mahasiswa->kelurahan; ?>">
                <small class="text-danger">
                  <?php echo form_error('kelurahan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-10" name="id_provinsi">
                  <option value="0" selected>Pilih Data Provinsi</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_provinsi)) {
                    $lv = $data_mahasiswa->id_provinsi;
                  }
                  foreach ($provinsi as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <option value="0" selected>Pilih Data Kabupaten / Kota</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_kabupaten_kota)) {
                    $lv = $data_mahasiswa->id_kabupaten_kota;
                  }
                  foreach ($kab_kota as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kode Pos</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="kode_pos" placeholder="Isi Kode Pos" value="<?= $data_mahasiswa->kode_pos; ?>">
              </div>
              <label class="col-md-2 col-form-label">Jenis Tinggal</label>
              <div class="col-md-4">
                <select class="form-control" name="jenis_tinggal">
                  <option value="0" <?= $data_mahasiswa->jenis_tinggal == 0 ? 'selected' : '' ?>>Pilih Jenis Tinggal</option>
                  <option value="Bersama orangtua" <?= $data_mahasiswa->jenis_tinggal == 'Bersama orangtua' ? 'selected' : '' ?>>Bersama orangtua</option>
                  <option value="Wali" <?= $data_mahasiswa->jenis_tinggal == 'Wali' ? 'selected' : '' ?>>Wali</option>
                  <option value="Kost" <?= $data_mahasiswa->jenis_tinggal == 'Kost' ? 'selected' : '' ?>>Kost</option>
                  <option value="Asrama" <?= $data_mahasiswa->jenis_tinggal == 'Asrama' ? 'selected' : '' ?>>Asrama</option>
                  <option value="Panti Asuhan" <?= $data_mahasiswa->jenis_tinggal == 'Panti Asuhan' ? 'selected' : '' ?>>Panti Asuhan</option>
                  <option value="Lainnya" <?= $data_mahasiswa->jenis_tinggal == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
              
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Telp. Rumah</label>
              <div class="col-md-4">
                <input class="form-control" type="number" placeholder="Isi Telp. Rumah" name="telp_rmh" value="<?= $data_mahasiswa->telp_rmh; ?>">
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input type="radio" name="jenis_kelamin" value="L" <?php if ($data_mahasiswa->jenis_kelamin == "L") {
                                                                      echo "checked";
                                                                    } ?> /> Laki-laki

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="jenis_kelamin" value="P" <?php if ($data_mahasiswa->jenis_kelamin == "P") {
                                                                      echo "checked";
                                                                    } ?> /> Perempuan
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= $data_mahasiswa->tempat_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= $data_mahasiswa->tgl_lahir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Terima KPS <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input type="radio" name="kps" value="Ya" <?php if ($data_mahasiswa->kps == "Ya") {
                                                                      echo "checked";
                                                                    } ?> /> Ya

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="kps" value="Tidak" <?php if ($data_mahasiswa->kps == "Tidak") {
                                                                      echo "checked";
                                                                    } ?> /> Tidak
                <small class="text-danger">
                  <?php echo form_error('kps') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. KPS</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="no_kps" placeholder="Isi No. KPS (Jika Ada)" value="<?= $data_mahasiswa->no_kps; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <option value="0" selected>Pilih Data Agama</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_agama)) {
                    $lv = $data_mahasiswa->id_agama;
                  }
                  foreach ($agama as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <option value="0" selected>Pilih Data Golongan Darah</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_golongan_darah)) {
                    $lv = $data_mahasiswa->id_golongan_darah;
                  }
                  foreach ($goldar as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jalur Pendaftaran</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jalur_pendaftaran">
                  <option value="0" selected>Pilih Data Jalur Pendaftaran</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_jalur_pendaftaran)) {
                    $lv = $data_mahasiswa->id_jalur_pendaftaran;
                  }
                  foreach ($jalur_pendaftaran as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Tanggal Masuk Kuliah <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_masuk_kuliah" value="<?= $data_mahasiswa->tgl_masuk_kuliah; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_masuk_kuliah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mulai Semester <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="mulai_semester" maxlength="5" placeholder="Isi Mulai Semester" value="<?= $data_mahasiswa->mulai_semester; ?>">
                <i>* 4 digit tahun, 1 digit jenis semester. Contoh: 20211</i>
                <small class="text-danger">
                  <?php echo form_error('mulai_semester') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Alat Transportasi</label>
              <div class="col-md-4">
                <select class="form-control" name="transportasi">
                  <option value="0" <?= $data_mahasiswa->transportasi == 0 ? 'selected' : '' ?>>Pilih Alat Transportasi</option>
                  <option value="Jalan Kaki" <?= $data_mahasiswa->transportasi == 'Jalan Kaki' ? 'selected' : '' ?>>Jalan Kaki</option>
                  <option value="Kendaraan Pribadi" <?= $data_mahasiswa->transportasi == 'Kendaraan Pribadi' ? 'selected' : '' ?>>Kendaraan Pribadi</option>
                  <option value="Angkutan Umum/bus/pete-pete" <?= $data_mahasiswa->transportasi == 'Angkutan Umum/bus/pete-pete' ? 'selected' : '' ?>>Angkutan Umum/bus/pete-pete</option>
                  <option value="Mobil/Bus Antar-jemput" <?= $data_mahasiswa->transportasi == 'Mobil/Bus Antar-jemput' ? 'selected' : '' ?>>Mobil/Bus Antar-jemput</option>
                  <option value="Kereta Api" <?= $data_mahasiswa->transportasi == 'Kereta Api' ? 'selected' : '' ?>>Kereta Api</option>
                  <option value="Ojek" <?= $data_mahasiswa->transportasi == 'Ojek' ? 'selected' : '' ?>>Ojek</option>
                  <option value="Andong/bendi/sado/dokar/delman/becak" <?= $data_mahasiswa->transportasi == 'Andong/bendi/sado/dokar/delman/becak' ? 'selected' : '' ?>>Andong/bendi/sado/dokar/delman/becak</option>
                  <option value="Perahu penyeberangan/perahu/getek" <?= $data_mahasiswa->transportasi == 'Perahu penyeberangan/perahu/getek' ? 'selected' : '' ?>>Perahu penyeberangan/perahu/getek</option>
                  <option value="Kuda" <?= $data_mahasiswa->transportasi == 'Kuda' ? 'selected' : '' ?>>Kuda</option>
                  <option value="Sepeda" <?= $data_mahasiswa->transportasi == 'Sepeda' ? 'selected' : '' ?>>Sepeda</option>
                  <option value="Sepeda Motor" <?= $data_mahasiswa->transportasi == 'Sepeda Motor' ? 'selected' : '' ?>>Sepeda Motor</option>
                  <option value="Mobil Pribadi" <?= $data_mahasiswa->transportasi == 'Mobil Pribadi' ? 'selected' : '' ?>>Mobil Pribadi</option>
                  <option value="Lainnya" <?= $data_mahasiswa->transportasi == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= $data_mahasiswa->email; ?>">
              </div>
              <label class="col-md-2 col-form-label">No. Handphone</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Handphone" value="<?= $data_mahasiswa->no_telp; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIK Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik_ayah" placeholder="Isi NIK Ayah" value="<?= $data_mahasiswa->nik_ayah; ?>">
              </div>
              <label class="col-md-2 col-form-label">Nama Ayah <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ayah" placeholder="Isi Nama Ayah" value="<?= $data_mahasiswa->nama_ayah; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Lahir Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_ayah" value="<?= $data_mahasiswa->tgl_lahir_ayah; ?>">
              </div>
              <label class="col-md-2 col-form-label">Pendidikan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_ayah">
                  <option value="0" <?= $data_mahasiswa->pend_ayah == 0 ? 'selected' : '' ?>>Pilih Pendidikan Ayah</option>
                  <option value="Tidak Sekolah" <?= $data_mahasiswa->pend_ayah == 'Tidak Sekolah' ? 'selected' : '' ?>>Tidak Sekolah</option>
                  <option value="PAUD" <?= $data_mahasiswa->pend_ayah == 'PAUD' ? 'selected' : '' ?>>PAUD</option>
                  <option value="TK / Sederajat" <?= $data_mahasiswa->pend_ayah == 'TK / Sederajat' ? 'selected' : '' ?>>TK / Sederajat</option>
                  <option value="Putus SD" <?= $data_mahasiswa->pend_ayah == 'Putus SD' ? 'selected' : '' ?>>Putus SD</option>
                  <option value="SD / Sederajat" <?= $data_mahasiswa->pend_ayah == 'SD / Sederajat' ? 'selected' : '' ?>>SD / Sederajat</option>
                  <option value="SMP / Sederajat" <?= $data_mahasiswa->pend_ayah == 'SMP / Sederajat' ? 'selected' : '' ?>>SMP / Sederajat</option>
                  <option value="SMA / Sederajat" <?= $data_mahasiswa->pend_ayah == 'SMA / Sederajat' ? 'selected' : '' ?>>SMA / Sederajat</option>
                  <option value="Paket A" <?= $data_mahasiswa->pend_ayah == 'Paket A' ? 'selected' : '' ?>>Paket A</option>
                  <option value="Paket B" <?= $data_mahasiswa->pend_ayah == 'Paket B' ? 'selected' : '' ?>>Paket B</option>
                  <option value="Paket C" <?= $data_mahasiswa->pend_ayah == 'Paket C' ? 'selected' : '' ?>>Paket C</option>
                  <option value="D1" <?= $data_mahasiswa->pend_ayah == 'D1' ? 'selected' : '' ?>>D1</option>
                  <option value="D2" <?= $data_mahasiswa->pend_ayah == 'D2' ? 'selected' : '' ?>>D2</option>
                  <option value="D3" <?= $data_mahasiswa->pend_ayah == 'D3' ? 'selected' : '' ?>>D3</option>
                  <option value="D4" <?= $data_mahasiswa->pend_ayah == 'D4' ? 'selected' : '' ?>>D4</option>
                  <option value="S1" <?= $data_mahasiswa->pend_ayah == 'S1' ? 'selected' : '' ?>>S1</option>
                  <option value="S1 Terapan" <?= $data_mahasiswa->pend_ayah == 'S1 Terapan' ? 'selected' : '' ?>>S1 Terapan</option>
                  <option value="S2" <?= $data_mahasiswa->pend_ayah == 'S2' ? 'selected' : '' ?>>S2</option>
                  <option value="S2 Terapan" <?= $data_mahasiswa->pend_ayah == 'S2 Terapan' ? 'selected' : '' ?>>S2 Terapan</option>
                  <option value="S3" <?= $data_mahasiswa->pend_ayah == 'S3' ? 'selected' : '' ?>>S3</option>
                  <option value="S3 Terapan" <?= $data_mahasiswa->pend_ayah == 'S3 Terapan' ? 'selected' : '' ?>>S3 Terapan</option>
                  <option value="Non Formal" <?= $data_mahasiswa->pend_ayah == 'Non Formal' ? 'selected' : '' ?>>Non Formal</option>
                  <option value="Informal" <?= $data_mahasiswa->pend_ayah == 'Informal' ? 'selected' : '' ?>>Informal</option>
                  <option value="Tidak Diisi" <?= $data_mahasiswa->pend_ayah == 'Tidak Diisi' ? 'selected' : '' ?>>Tidak Diisi</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pend_ayah == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pekerjaan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_ayah">
                  <option value="0" <?= $data_mahasiswa->pek_ayah == 0 ? 'selected' : '' ?>>Pilih Pekerjaan Ayah</option>
                  <option value="Tidak Bekerja" <?= $data_mahasiswa->pek_ayah == 'Tidak Bekerja' ? 'selected' : '' ?>>Tidak Bekerja</option>
                  <option value="Nelayan" <?= $data_mahasiswa->pek_ayah == 'Nelayan' ? 'selected' : '' ?>>Nelayan</option>
                  <option value="Petani" <?= $data_mahasiswa->pek_ayah == 'Petani' ? 'selected' : '' ?>>Petani</option>
                  <option value="Peternak" <?= $data_mahasiswa->pek_ayah == 'Peternak' ? 'selected' : '' ?>>Peternak</option>
                  <option value="PNS/TNI/Polri" <?= $data_mahasiswa->pek_ayah == 'PNS/TNI/Polri' ? 'selected' : '' ?>>PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta" <?= $data_mahasiswa->pek_ayah == 'Karyawan Swasta' ? 'selected' : '' ?>>Karyawan Swasta</option>
                  <option value="Pedagang Kecil" <?= $data_mahasiswa->pek_ayah == 'Pedagang Kecil' ? 'selected' : '' ?>>Pedagang Kecil</option>
                  <option value="Pedagang Besar" <?= $data_mahasiswa->pek_ayah == 'Pedagang Besar' ? 'selected' : '' ?>>Pedagang Besar</option>
                  <option value="Wiraswasta" <?= $data_mahasiswa->pek_ayah == 'Wiraswasta' ? 'selected' : '' ?>>Wiraswasta</option>
                  <option value="Wirausaha" <?= $data_mahasiswa->pek_ayah == 'Wirausaha' ? 'selected' : '' ?>>Wirausaha</option>
                  <option value="Buruh" <?= $data_mahasiswa->pek_ayah == 'Buruh' ? 'selected' : '' ?>>Buruh</option>
                  <option value="Pensiunan" <?= $data_mahasiswa->pek_ayah == 'Pensiunan' ? 'selected' : '' ?>>Pensiunan</option>
                  <option value="Sudah Meninggal" <?= $data_mahasiswa->pek_ayah == 'Sudah Meninggal' ? 'selected' : '' ?>>Sudah Meninggal</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pek_ayah == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Penghasilan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_ayah">
                  <option value="0" <?= $data_mahasiswa->penghasilan_ayah == 0 ? 'selected' : '' ?>>Pilih Penghasilan Ayah</option>
                  <option value="Kurang dari Rp. 500.000" <?= $data_mahasiswa->penghasilan_ayah == 'Kurang dari Rp. 500.000' ? 'selected' : '' ?>>Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000" <?= $data_mahasiswa->penghasilan_ayah == 'Rp. 500.000 - Rp. 1.000.000' ? 'selected' : '' ?>>Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000" <?= $data_mahasiswa->penghasilan_ayah == 'Rp. 1.000.000 - Rp. 2.000.000' ? 'selected' : '' ?>>Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000" <?= $data_mahasiswa->penghasilan_ayah == 'Rp. 2.000.000 - Rp. 5.000.000' ? 'selected' : '' ?>>Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_ayah == 'Rp. 5.000.000 - Rp. 20.000.000' ? 'selected' : '' ?>>Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_ayah == 'Lebih dari Rp. 20.000.000' ? 'selected' : '' ?>>Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya" <?= $data_mahasiswa->penghasilan_ayah == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIK Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik_ibu" placeholder="Isi NIK Ibu" value="<?= $data_mahasiswa->nik_ibu; ?>">
              </div>
              <label class="col-md-2 col-form-label">Nama Ibu <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= $data_mahasiswa->nama_ibu; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Lahir Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_ibu" value="<?= $data_mahasiswa->tgl_lahir_ibu; ?>">
              </div>
              <label class="col-md-2 col-form-label">Pendidikan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_ibu">
                  <option value="0" <?= $data_mahasiswa->pend_ibu == 0 ? 'selected' : '' ?>>Pilih Pendidikan Ayah</option>
                  <option value="Tidak Sekolah" <?= $data_mahasiswa->pend_ibu == 'Tidak Sekolah' ? 'selected' : '' ?>>Tidak Sekolah</option>
                  <option value="PAUD" <?= $data_mahasiswa->pend_ibu == 'PAUD' ? 'selected' : '' ?>>PAUD</option>
                  <option value="TK / Sederajat" <?= $data_mahasiswa->pend_ibu == 'TK / Sederajat' ? 'selected' : '' ?>>TK / Sederajat</option>
                  <option value="Putus SD" <?= $data_mahasiswa->pend_ibu == 'Putus SD' ? 'selected' : '' ?>>Putus SD</option>
                  <option value="SD / Sederajat" <?= $data_mahasiswa->pend_ibu == 'SD / Sederajat' ? 'selected' : '' ?>>SD / Sederajat</option>
                  <option value="SMP / Sederajat" <?= $data_mahasiswa->pend_ibu == 'SMP / Sederajat' ? 'selected' : '' ?>>SMP / Sederajat</option>
                  <option value="SMA / Sederajat" <?= $data_mahasiswa->pend_ibu == 'SMA / Sederajat' ? 'selected' : '' ?>>SMA / Sederajat</option>
                  <option value="Paket A" <?= $data_mahasiswa->pend_ibu == 'Paket A' ? 'selected' : '' ?>>Paket A</option>
                  <option value="Paket B" <?= $data_mahasiswa->pend_ibu == 'Paket B' ? 'selected' : '' ?>>Paket B</option>
                  <option value="Paket C" <?= $data_mahasiswa->pend_ibu == 'Paket C' ? 'selected' : '' ?>>Paket C</option>
                  <option value="D1" <?= $data_mahasiswa->pend_ibu == 'D1' ? 'selected' : '' ?>>D1</option>
                  <option value="D2" <?= $data_mahasiswa->pend_ibu == 'D2' ? 'selected' : '' ?>>D2</option>
                  <option value="D3" <?= $data_mahasiswa->pend_ibu == 'D3' ? 'selected' : '' ?>>D3</option>
                  <option value="D4" <?= $data_mahasiswa->pend_ibu == 'D4' ? 'selected' : '' ?>>D4</option>
                  <option value="S1" <?= $data_mahasiswa->pend_ibu == 'S1' ? 'selected' : '' ?>>S1</option>
                  <option value="S1 Terapan" <?= $data_mahasiswa->pend_ibu == 'S1 Terapan' ? 'selected' : '' ?>>S1 Terapan</option>
                  <option value="S2" <?= $data_mahasiswa->pend_ibu == 'S2' ? 'selected' : '' ?>>S2</option>
                  <option value="S2 Terapan" <?= $data_mahasiswa->pend_ibu == 'S2 Terapan' ? 'selected' : '' ?>>S2 Terapan</option>
                  <option value="S3" <?= $data_mahasiswa->pend_ibu == 'S3' ? 'selected' : '' ?>>S3</option>
                  <option value="S3 Terapan" <?= $data_mahasiswa->pend_ibu == 'S3 Terapan' ? 'selected' : '' ?>>S3 Terapan</option>
                  <option value="Non Formal" <?= $data_mahasiswa->pend_ibu == 'Non Formal' ? 'selected' : '' ?>>Non Formal</option>
                  <option value="Informal" <?= $data_mahasiswa->pend_ibu == 'Informal' ? 'selected' : '' ?>>Informal</option>
                  <option value="Tidak Diisi" <?= $data_mahasiswa->pend_ibu == 'Tidak Diisi' ? 'selected' : '' ?>>Tidak Diisi</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pend_ibu == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pekerjaan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_ibu">
                  <option value="0" <?= $data_mahasiswa->pek_ibu == 0 ? 'selected' : '' ?>>Pilih Pekerjaan Ayah</option>
                  <option value="Tidak Bekerja" <?= $data_mahasiswa->pek_ibu == 'Tidak Bekerja' ? 'selected' : '' ?>>Tidak Bekerja</option>
                  <option value="Nelayan" <?= $data_mahasiswa->pek_ibu == 'Nelayan' ? 'selected' : '' ?>>Nelayan</option>
                  <option value="Petani" <?= $data_mahasiswa->pek_ibu == 'Petani' ? 'selected' : '' ?>>Petani</option>
                  <option value="Peternak" <?= $data_mahasiswa->pek_ibu == 'Peternak' ? 'selected' : '' ?>>Peternak</option>
                  <option value="PNS/TNI/Polri" <?= $data_mahasiswa->pek_ibu == 'PNS/TNI/Polri' ? 'selected' : '' ?>>PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta" <?= $data_mahasiswa->pek_ibu == 'Karyawan Swasta' ? 'selected' : '' ?>>Karyawan Swasta</option>
                  <option value="Pedagang Kecil" <?= $data_mahasiswa->pek_ibu == 'Pedagang Kecil' ? 'selected' : '' ?>>Pedagang Kecil</option>
                  <option value="Pedagang Besar" <?= $data_mahasiswa->pek_ibu == 'Pedagang Besar' ? 'selected' : '' ?>>Pedagang Besar</option>
                  <option value="Wiraswasta" <?= $data_mahasiswa->pek_ibu == 'Wiraswasta' ? 'selected' : '' ?>>Wiraswasta</option>
                  <option value="Wirausaha" <?= $data_mahasiswa->pek_ibu == 'Wirausaha' ? 'selected' : '' ?>>Wirausaha</option>
                  <option value="Buruh" <?= $data_mahasiswa->pek_ibu == 'Buruh' ? 'selected' : '' ?>>Buruh</option>
                  <option value="Pensiunan" <?= $data_mahasiswa->pek_ibu == 'Pensiunan' ? 'selected' : '' ?>>Pensiunan</option>
                  <option value="Sudah Meninggal" <?= $data_mahasiswa->pek_ibu == 'Sudah Meninggal' ? 'selected' : '' ?>>Sudah Meninggal</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pek_ibu == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Penghasilan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_ibu">
                  <option value="0" <?= $data_mahasiswa->penghasilan_ibu == 0 ? 'selected' : '' ?>>Pilih Penghasilan Ayah</option>
                  <option value="Kurang dari Rp. 500.000" <?= $data_mahasiswa->penghasilan_ibu == 'Kurang dari Rp. 500.000' ? 'selected' : '' ?>>Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000" <?= $data_mahasiswa->penghasilan_ibu == 'Rp. 500.000 - Rp. 1.000.000' ? 'selected' : '' ?>>Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000" <?= $data_mahasiswa->penghasilan_ibu == 'Rp. 1.000.000 - Rp. 2.000.000' ? 'selected' : '' ?>>Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000" <?= $data_mahasiswa->penghasilan_ibu == 'Rp. 2.000.000 - Rp. 5.000.000' ? 'selected' : '' ?>>Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_ibu == 'Rp. 5.000.000 - Rp. 20.000.000' ? 'selected' : '' ?>>Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_ibu == 'Lebih dari Rp. 20.000.000' ? 'selected' : '' ?>>Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya" <?= $data_mahasiswa->penghasilan_ibu == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Wali</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_wali" placeholder="Isi Nama Wali" value="<?= $data_mahasiswa->nama_wali; ?>">
              </div>
              <label class="col-md-2 col-form-label">Tanggal Lahir Wali</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_wali" value="<?= $data_mahasiswa->tgl_lahir_wali; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pendidikan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_wali">
                  <option value="0" <?= $data_mahasiswa->pend_wali == 0 ? 'selected' : '' ?>>Pilih Pendidikan Ayah</option>
                  <option value="Tidak Sekolah" <?= $data_mahasiswa->pend_wali == 'Tidak Sekolah' ? 'selected' : '' ?>>Tidak Sekolah</option>
                  <option value="PAUD" <?= $data_mahasiswa->pend_wali == 'PAUD' ? 'selected' : '' ?>>PAUD</option>
                  <option value="TK / Sederajat" <?= $data_mahasiswa->pend_wali == 'TK / Sederajat' ? 'selected' : '' ?>>TK / Sederajat</option>
                  <option value="Putus SD" <?= $data_mahasiswa->pend_wali == 'Putus SD' ? 'selected' : '' ?>>Putus SD</option>
                  <option value="SD / Sederajat" <?= $data_mahasiswa->pend_wali == 'SD / Sederajat' ? 'selected' : '' ?>>SD / Sederajat</option>
                  <option value="SMP / Sederajat" <?= $data_mahasiswa->pend_wali == 'SMP / Sederajat' ? 'selected' : '' ?>>SMP / Sederajat</option>
                  <option value="SMA / Sederajat" <?= $data_mahasiswa->pend_wali == 'SMA / Sederajat' ? 'selected' : '' ?>>SMA / Sederajat</option>
                  <option value="Paket A" <?= $data_mahasiswa->pend_wali == 'Paket A' ? 'selected' : '' ?>>Paket A</option>
                  <option value="Paket B" <?= $data_mahasiswa->pend_wali == 'Paket B' ? 'selected' : '' ?>>Paket B</option>
                  <option value="Paket C" <?= $data_mahasiswa->pend_wali == 'Paket C' ? 'selected' : '' ?>>Paket C</option>
                  <option value="D1" <?= $data_mahasiswa->pend_wali == 'D1' ? 'selected' : '' ?>>D1</option>
                  <option value="D2" <?= $data_mahasiswa->pend_wali == 'D2' ? 'selected' : '' ?>>D2</option>
                  <option value="D3" <?= $data_mahasiswa->pend_wali == 'D3' ? 'selected' : '' ?>>D3</option>
                  <option value="D4" <?= $data_mahasiswa->pend_wali == 'D4' ? 'selected' : '' ?>>D4</option>
                  <option value="S1" <?= $data_mahasiswa->pend_wali == 'S1' ? 'selected' : '' ?>>S1</option>
                  <option value="S1 Terapan" <?= $data_mahasiswa->pend_wali == 'S1 Terapan' ? 'selected' : '' ?>>S1 Terapan</option>
                  <option value="S2" <?= $data_mahasiswa->pend_wali == 'S2' ? 'selected' : '' ?>>S2</option>
                  <option value="S2 Terapan" <?= $data_mahasiswa->pend_wali == 'S2 Terapan' ? 'selected' : '' ?>>S2 Terapan</option>
                  <option value="S3" <?= $data_mahasiswa->pend_wali == 'S3' ? 'selected' : '' ?>>S3</option>
                  <option value="S3 Terapan" <?= $data_mahasiswa->pend_wali == 'S3 Terapan' ? 'selected' : '' ?>>S3 Terapan</option>
                  <option value="Non Formal" <?= $data_mahasiswa->pend_wali == 'Non Formal' ? 'selected' : '' ?>>Non Formal</option>
                  <option value="Informal" <?= $data_mahasiswa->pend_wali == 'Informal' ? 'selected' : '' ?>>Informal</option>
                  <option value="Tidak Diisi" <?= $data_mahasiswa->pend_wali == 'Tidak Diisi' ? 'selected' : '' ?>>Tidak Diisi</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pend_wali == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Pekerjaan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_wali">
                 <option value="0" <?= $data_mahasiswa->pek_wali == 0 ? 'selected' : '' ?>>Pilih Pekerjaan Ayah</option>
                  <option value="Tidak Bekerja" <?= $data_mahasiswa->pek_wali == 'Tidak Bekerja' ? 'selected' : '' ?>>Tidak Bekerja</option>
                  <option value="Nelayan" <?= $data_mahasiswa->pek_wali == 'Nelayan' ? 'selected' : '' ?>>Nelayan</option>
                  <option value="Petani" <?= $data_mahasiswa->pek_wali == 'Petani' ? 'selected' : '' ?>>Petani</option>
                  <option value="Peternak" <?= $data_mahasiswa->pek_wali == 'Peternak' ? 'selected' : '' ?>>Peternak</option>
                  <option value="PNS/TNI/Polri" <?= $data_mahasiswa->pek_wali == 'PNS/TNI/Polri' ? 'selected' : '' ?>>PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta" <?= $data_mahasiswa->pek_wali == 'Karyawan Swasta' ? 'selected' : '' ?>>Karyawan Swasta</option>
                  <option value="Pedagang Kecil" <?= $data_mahasiswa->pek_wali == 'Pedagang Kecil' ? 'selected' : '' ?>>Pedagang Kecil</option>
                  <option value="Pedagang Besar" <?= $data_mahasiswa->pek_wali == 'Pedagang Besar' ? 'selected' : '' ?>>Pedagang Besar</option>
                  <option value="Wiraswasta" <?= $data_mahasiswa->pek_wali == 'Wiraswasta' ? 'selected' : '' ?>>Wiraswasta</option>
                  <option value="Wirausaha" <?= $data_mahasiswa->pek_wali == 'Wirausaha' ? 'selected' : '' ?>>Wirausaha</option>
                  <option value="Buruh" <?= $data_mahasiswa->pek_wali == 'Buruh' ? 'selected' : '' ?>>Buruh</option>
                  <option value="Pensiunan" <?= $data_mahasiswa->pek_wali == 'Pensiunan' ? 'selected' : '' ?>>Pensiunan</option>
                  <option value="Sudah Meninggal" <?= $data_mahasiswa->pek_wali == 'Sudah Meninggal' ? 'selected' : '' ?>>Sudah Meninggal</option>
                  <option value="Lainnya" <?= $data_mahasiswa->pek_wali == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Penghasilan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_wali">
                  <option value="0" <?= $data_mahasiswa->penghasilan_wali == 0 ? 'selected' : '' ?>>Pilih Penghasilan Ayah</option>
                  <option value="Kurang dari Rp. 500.000" <?= $data_mahasiswa->penghasilan_wali == 'Kurang dari Rp. 500.000' ? 'selected' : '' ?>>Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000" <?= $data_mahasiswa->penghasilan_wali == 'Rp. 500.000 - Rp. 1.000.000' ? 'selected' : '' ?>>Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000" <?= $data_mahasiswa->penghasilan_wali == 'Rp. 1.000.000 - Rp. 2.000.000' ? 'selected' : '' ?>>Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000" <?= $data_mahasiswa->penghasilan_wali == 'Rp. 2.000.000 - Rp. 5.000.000' ? 'selected' : '' ?>>Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_wali == 'Rp. 5.000.000 - Rp. 20.000.000' ? 'selected' : '' ?>>Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000" <?= $data_mahasiswa->penghasilan_wali == 'Lebih dari Rp. 20.000.000' ? 'selected' : '' ?>>Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya" <?= $data_mahasiswa->penghasilan_wali == 'Lainnya' ? 'selected' : '' ?>>Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Jenis Pembiayaan</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jenis_pembiayaan">
                  <option value="0" selected>Pilih Data Jenis Pembiayaan</option>
                  <?php
                  $lv = '';
                  if (isset($data_mahasiswa->id_jenis_pembiayaan)) {
                    $lv = $data_mahasiswa->id_jenis_pembiayaan;
                  }
                  foreach ($jenis_pembiayaan as $r => $v) {
                  ?>
                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jumlah Biaya Masuk <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="jumlah_biaya_masuk" placeholder="Isi Jumlah Biaya Masuk" value="<?= $data_mahasiswa->jumlah_biaya_masuk; ?>">
                <small class="text-danger">
                  <?php echo form_error('jumlah_biaya_masuk') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">SKS Diakui</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="sks_diakui" placeholder="Isi SKS Diakui (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= $data_mahasiswa->sks_diakui; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi Asal</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="program_studi_asal" placeholder="Isi Program Studi Asal (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= $data_mahasiswa->program_studi_asal; ?>">
              </div>
              <label class="col-md-2 col-form-label">Perguruan Tinggi Asal</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="perguruan_tinggi_asal" placeholder="Isi Perguruan Tinggi Asal (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= $data_mahasiswa->perguruan_tinggi_asal; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tgl. Lulus</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lulus" value="<?= $data_mahasiswa->tgl_lulus; ?>">
              </div>
              <label class="col-md-2 col-form-label">Nomor Ijazah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nomor_ijazah" placeholder="Isi Nomor Ijazah" value="<?= $data_mahasiswa->nomor_ijazah; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nomor Transkrip</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nomor_transkrip" placeholder="Isi Nomor Transkrip" value="<?= $data_mahasiswa->nomor_transkrip; ?>">
              </div>
              <label class="col-md-2 col-form-label">Judul Skripsi</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="judul_skripsi" placeholder="Isi Judul Skripsi" value="<?= $data_mahasiswa->judul_skripsi; ?>">
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('mahasiswa'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>