<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Jadwal Kuliah
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianJadwal', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            </form>
            <hr>

            <!-- <a href="<?= base_url('dosen/pengisian_nilai'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Form Pengisian Nilai</button></a><br><br> -->
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Program Studi</th>
                  <th>Kelas</th>
                  <th>Mata Kuliah</th>
                  <th>Jadwal Kuliah</th>
                  <th>Jumlah Mhs Peserta</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php $i = 1; ?>
                  <?php foreach ($data_jadwal as $row) : ?>
                    <td><?= $i++; ?></td>
                    <td><?= $row->tahun . " (" . $row->nama_tahun_akademik . ")" ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?= $row->nama_kelas ?></td>
                    <td><?= "[" . $row->kode . "] " . $row->nama_mata_kuliah ?></td>
                    <td><?= $row->hari . " (" . $row->jam_mulai . " - " . $row->jam_selesai . ")" ?></td>
                    <td style="text-align: right;">
                      <?php
                      // get jumlah mhs dari tabel krs_mhs, krs_mhs_detail
                      $sqlxx = " select count(a.id) as jum_mhs FROM krs_mhs a INNER JOIN krs_mhs_detail b ON a.id = b.id_krs_mhs 
                                INNER JOIN mahasiswa c ON c.id = a.id_mhs
                                WHERE c.id_kelas = '$row->id_kelas' AND a.id_semester = '$row->id_semester' 
                                AND a.id_tahun_akademik = '$row->id_tahun_akademik' AND b.id_mata_kuliah = '$row->id_mata_kuliah' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $hasilxx = $queryxx->row();
                        $jum_mhs = $hasilxx->jum_mhs;

                        echo $jum_mhs;
                      }

                      ?>

                    </td>
                </tr>
              <?php endforeach; ?>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>