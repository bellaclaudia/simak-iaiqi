<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('mahasiswa'); ?>">Data Mahasiswa</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <!-- <?php
                $attributes = array('id' => 'FrmAddMahasiswa', 'method' => "post", "autocomplete" => "off", "enctype" => "multipart/form-data");
                echo form_open('', $attributes);

                if (isset($error)) {
                  echo "ERROR UPLOAD : <br/>";
                  print_r($error);
                  echo "<hr/>";
                }
                ?> -->
          <form id="FrmAddMahasiswa" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lengkap <span style="color: red">*</span></label>
              <div class="col-md-10">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= set_value('nama'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NISN</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nisn" placeholder="Isi NISN" value="<?= set_value('nisn'); ?>">
              </div>
              <label class="col-md-2 col-form-label">NPWP</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="npwp" placeholder="Isi NPWP" value="<?= set_value('npwp'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kewarganegaraan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kewarganegaraan" placeholder="Isi Kewarganegaraan" value="<?= set_value('kewarganegaraan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kewarganegaraan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIK <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= set_value('nik'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kelas <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_kelas">
                  <option value="0" selected>Pilih Data Kelas</option>
                  <?php foreach ($kelas as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kelas') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Status Mahasiswa</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-7" name="id_status_mhs">
                  <option value="0" selected>Pilih Status Mahasiswa</option>
                  <?php foreach ($status_mhs as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg | Maks. 5MB</span>
              </div>
              <label class="col-md-2 col-form-label">Jenis Pendaftaran <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control" name="jenis_mhs">
                  <option value="0"  selected>Pilih Jenis Pendaftaran</option>
                  <option value="Lainnya">Lainnya</option>
                  <option value="Peserta Didik Baru">Peserta Didik Baru</option>
                  <option value="Pindahan">Pindahan</option>
                  <option value="Naik Kelas">Naik Kelas</option>
                  <option value="Akselerasi">Akselerasi</option>
                  <option value="Mengulang">Mengulang</option>
                  <option value="Lanjutan Semester">Lanjutan Semester</option>
                  <option value="Pindahan Alih Bentuk">Pindahan Alih Bentuk</option>
                  <option value="Putus Sekolah">Putus Sekolah</option>
                  <option value="Kelas Ekstensi">Kelas Ekstensi</option>
                  <option value="Alih Jenjang">Alih Jenjang</option>
                  <option value="Lintas Jalur">Lintas Jalur</option>
                  <option value="Rekognisi Pembelajaran Lampau (RPL)">Rekognisi Pembelajaran Lampau (RPL)</option>
                </select>
                <small class="text-danger">
                  <?php echo form_error('jenis_mhs') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Angkatan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-8" name="id_angkatan">
                  <option value="0" selected>Pilih Data Angkatan</option>
                  <?php foreach ($angkatan as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Program Studi <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                  <option value="0" selected>Pilih Data Program Studi</option>
                  <?php foreach ($prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jalan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="alamat" value="<?= set_value('alamat'); ?>" placeholder="Isi Jalan">
              </div>
              <label class="col-md-2 col-form-label">RT</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="rt" value="<?= set_value('rt'); ?>" placeholder="Isi RT">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">RW</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="rw" value="<?= set_value('rw'); ?>" placeholder="Isi RW">
              </div>
              <label class="col-md-2 col-form-label">Nama Dusun</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="dusun" value="<?= set_value('dusun'); ?>" placeholder="Isi Nama Dusun">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kecamatan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kecamatan" placeholder="Isi Kecamatan" value="<?= set_value('kecamatan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kecamatan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kelurahan <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kelurahan" placeholder="Isi Kelurahan" value="<?= set_value('kelurahan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kelurahan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-10" name="id_provinsi">
                  <option value="0" selected>Pilih Data Provinsi</option>
                  <?php foreach ($provinsi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <option value="0" selected>Pilih Data Kabupaten / Kota</option>
                  <?php foreach ($kab_kota as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kode Pos</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="kode_pos" placeholder="Isi Kode Pos" value="<?= set_value('kode_pos'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Jenis Tinggal</label>
              <div class="col-md-4">
                <select class="form-control" name="jenis_tinggal">
                  <option value="0" selected>Pilih Jenis Tinggal</option>
                  <option value="Bersama orangtua">Bersama orangtua</option>
                  <option value="Wali">Wali</option>
                  <option value="Kost">Kost</option>
                  <option value="Asrama">Asrama</option>
                  <option value="Panti Asuhan">Panti Asuhan</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
              
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Telp. Rumah</label>
              <div class="col-md-4">
                <input class="form-control" type="number" placeholder="Isi Telp. Rumah" name="telp_rmh" value="<?= set_value('telp_rmh'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Jenis Kelamin <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="uniform" type="radio" value="L" name="jenis_kelamin"> Laki-laki &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="uniform" type="radio" value="P" name="jenis_kelamin"> Perempuan
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= set_value('tempat_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Terima KPS <span style="color: red">*</span></label>
              <div class="col-md-4">
                 <input class="uniform" type="radio" value="Ya" name="kps"> Ya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="uniform" type="radio" value="Tidak" name="kps"> Tidak
                <small class="text-danger">
                  <?php echo form_error('kps') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. KPS</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="no_kps" placeholder="Isi No. KPS (Jika Ada)" value="<?= set_value('no_kps'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama <span style="color: red">*</span></label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <option value="0" selected>Pilih Data Agama</option>
                  <?php foreach ($agama as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <option value="0" selected>Pilih Data Golongan Darah</option>
                  <?php foreach ($goldar as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jalur Pendaftaran</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jalur_pendaftaran">
                  <option value="0" selected>Pilih Data Jalur Pendaftaran</option>
                  <?php foreach ($jalur_pendaftaran as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Tanggal Masuk Kuliah <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_masuk_kuliah" value="<?= set_value('tgl_masuk_kuliah'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_masuk_kuliah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mulai Semester <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="mulai_semester" maxlength="5" placeholder="Isi Mulai Semester" value="<?= set_value('mulai_semester'); ?>">
                <i>* 4 digit tahun, 1 digit jenis semester. Contoh: 20211</i>
                <small class="text-danger">
                  <?php echo form_error('mulai_semester') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Alat Transportasi</label>
              <div class="col-md-4">
                <select class="form-control" name="transportasi">
                  <option value="0" selected>Pilih Alat Transportasi</option>
                  <option value="Jalan Kaki">Jalan Kaki</option>
                  <option value="Kendaraan Pribadi">Kendaraan Pribadi</option>
                  <option value="Angkutan Umum/bus/pete-pete">Angkutan Umum/bus/pete-pete</option>
                  <option value="Mobil/Bus Antar-jemput">Mobil/Bus Antar-jemput</option>
                  <option value="Kereta Api">Kereta Api</option>
                  <option value="Ojek">Ojek</option>
                  <option value="Andong/bendi/sado/dokar/delman/becak">Andong/bendi/sado/dokar/delman/becak</option>
                  <option value="Perahu penyeberangan/perahu/getek">Perahu penyeberangan/perahu/getek</option>
                  <option value="Kuda">Kuda</option>
                  <option value="Sepeda">Sepeda</option>
                  <option value="Sepeda Motor">Sepeda Motor</option>
                  <option value="Mobil Pribadi">Mobil Pribadi</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= set_value('email'); ?>">
              </div>
              <label class="col-md-2 col-form-label">No. Handphone</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Handphone" value="<?= set_value('no_telp'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIK Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik_ayah" placeholder="Isi NIK Ayah" value="<?= set_value('nik_ayah'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Nama Ayah <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ayah" placeholder="Isi Nama Ayah" value="<?= set_value('nama_ayah'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Lahir Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_ayah" value="<?= set_value('tgl_lahir_ayah'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Pendidikan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_ayah">
                  <option value="0" selected>Pilih Pendidikan Ayah</option>
                  <option value="Tidak Sekolah">Tidak Sekolah</option>
                  <option value="PAUD">PAUD</option>
                  <option value="TK / Sederajat">TK / Sederajat</option>
                  <option value="Putus SD">Putus SD</option>
                  <option value="SD / Sederajat">SD / Sederajat</option>
                  <option value="SMP / Sederajat">SMP / Sederajat</option>
                  <option value="SMA / Sederajat">SMA / Sederajat</option>
                  <option value="Paket A">Paket A</option>
                  <option value="Paket B">Paket B</option>
                  <option value="Paket C">Paket C</option>
                  <option value="D1">D1</option>
                  <option value="D2">D2</option>
                  <option value="D3">D3</option>
                  <option value="D4">D4</option>
                  <option value="Profesi">Profesi</option>
                  <option value="S1">S1</option>
                  <option value="S1">S1</option>
                  <option value="S2 Terapan">S2 Terapan</option>
                  <option value="S3">S3</option>
                  <option value="S3 Terapan">S3 Terapan</option>
                  <option value="Non Formal">Non Formal</option>
                  <option value="Informal">Informal</option>
                  <option value="Tidak Diisi">Tidak Diisi</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pekerjaan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_ayah">
                  <option value="0" selected>Pilih Pekerjaan Ayah</option>
                  <option value="Tidak Bekerja">Tidak Bekerja</option>
                  <option value="Nelayan">Nelayan</option>
                  <option value="Petani">Petani</option>
                  <option value="Peternak">Peternak</option>
                  <option value="PNS/TNI/Polri">PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta">Karyawan Swasta</option>
                  <option value="Pedagang Kecil">Pedagang Kecil</option>
                  <option value="Pedagang Besar">Pedagang Besar</option>
                  <option value="Wiraswasta">Wiraswasta</option>
                  <option value="Wirausaha">Wirausaha</option>
                  <option value="Buruh">Buruh</option>
                  <option value="Pensiunan">Pensiunan</option>
                  <option value="Sudah Meninggal">Sudah Meninggal</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Penghasilan Ayah</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_ayah">
                  <option value="0" selected>Pilih Penghasilan Ayah</option>
                  <option value="Kurang dari Rp. 500.000">Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000">Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000">Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000">Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000">Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000">Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">NIK Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik_ibu" placeholder="Isi NIK Ibu" value="<?= set_value('nik_ibu'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Nama Ibu <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= set_value('nama_ibu'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Lahir Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_ibu" value="<?= set_value('tgl_lahir_ibu'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Pendidikan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_ibu">
                  <option value="0" selected>Pilih Pendidikan Ibu</option>
                  <option value="Tidak Sekolah">Tidak Sekolah</option>
                  <option value="PAUD">PAUD</option>
                  <option value="TK / Sederajat">TK / Sederajat</option>
                  <option value="Putus SD">Putus SD</option>
                  <option value="SD / Sederajat">SD / Sederajat</option>
                  <option value="SMP / Sederajat">SMP / Sederajat</option>
                  <option value="SMA / Sederajat">SMA / Sederajat</option>
                  <option value="Paket A">Paket A</option>
                  <option value="Paket B">Paket B</option>
                  <option value="Paket C">Paket C</option>
                  <option value="D1">D1</option>
                  <option value="D2">D2</option>
                  <option value="D3">D3</option>
                  <option value="D4">D4</option>
                  <option value="Profesi">Profesi</option>
                  <option value="S1">S1</option>
                  <option value="S1">S1</option>
                  <option value="S2 Terapan">S2 Terapan</option>
                  <option value="S3">S3</option>
                  <option value="S3 Terapan">S3 Terapan</option>
                  <option value="Non Formal">Non Formal</option>
                  <option value="Informal">Informal</option>
                  <option value="Tidak Diisi">Tidak Diisi</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pekerjaan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_ibu">
                  <option value="0" selected>Pilih Pekerjaan Ibu</option>
                  <option value="Tidak Bekerja">Tidak Bekerja</option>
                  <option value="Nelayan">Nelayan</option>
                  <option value="Petani">Petani</option>
                  <option value="Peternak">Peternak</option>
                  <option value="PNS/TNI/Polri">PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta">Karyawan Swasta</option>
                  <option value="Pedagang Kecil">Pedagang Kecil</option>
                  <option value="Pedagang Besar">Pedagang Besar</option>
                  <option value="Wiraswasta">Wiraswasta</option>
                  <option value="Wirausaha">Wirausaha</option>
                  <option value="Buruh">Buruh</option>
                  <option value="Pensiunan">Pensiunan</option>
                  <option value="Sudah Meninggal">Sudah Meninggal</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Penghasilan Ibu</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_ibu">
                  <option value="0" selected>Pilih Penghasilan Ibu</option>
                  <option value="Kurang dari Rp. 500.000">Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000">Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000">Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000">Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000">Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000">Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Wali</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_wali" placeholder="Isi Nama Wali" value="<?= set_value('nama_wali'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Tanggal Lahir Wali</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir_wali" value="<?= set_value('tgl_lahir_wali'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Pendidikan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="pend_wali">
                  <option value="0" selected>Pilih Pendidikan Wali</option>
                  <option value="Tidak Sekolah">Tidak Sekolah</option>
                  <option value="PAUD">PAUD</option>
                  <option value="TK / Sederajat">TK / Sederajat</option>
                  <option value="Putus SD">Putus SD</option>
                  <option value="SD / Sederajat">SD / Sederajat</option>
                  <option value="SMP / Sederajat">SMP / Sederajat</option>
                  <option value="SMA / Sederajat">SMA / Sederajat</option>
                  <option value="Paket A">Paket A</option>
                  <option value="Paket B">Paket B</option>
                  <option value="Paket C">Paket C</option>
                  <option value="D1">D1</option>
                  <option value="D2">D2</option>
                  <option value="D3">D3</option>
                  <option value="D4">D4</option>
                  <option value="Profesi">Profesi</option>
                  <option value="S1">S1</option>
                  <option value="S1">S1</option>
                  <option value="S2 Terapan">S2 Terapan</option>
                  <option value="S3">S3</option>
                  <option value="S3 Terapan">S3 Terapan</option>
                  <option value="Non Formal">Non Formal</option>
                  <option value="Informal">Informal</option>
                  <option value="Tidak Diisi">Tidak Diisi</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Pekerjaan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="pek_wali">
                  <option value="0" selected>Pilih Pekerjaan Wali</option>
                  <option value="Tidak Bekerja">Tidak Bekerja</option>
                  <option value="Nelayan">Nelayan</option>
                  <option value="Petani">Petani</option>
                  <option value="Peternak">Peternak</option>
                  <option value="PNS/TNI/Polri">PNS/TNI/Polri</option>
                  <option value="Karyawan Swasta">Karyawan Swasta</option>
                  <option value="Pedagang Kecil">Pedagang Kecil</option>
                  <option value="Pedagang Besar">Pedagang Besar</option>
                  <option value="Wiraswasta">Wiraswasta</option>
                  <option value="Wirausaha">Wirausaha</option>
                  <option value="Buruh">Buruh</option>
                  <option value="Pensiunan">Pensiunan</option>
                  <option value="Sudah Meninggal">Sudah Meninggal</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Penghasilan Wali</label>
              <div class="col-md-4">
                <select class="form-control" name="penghasilan_wali">
                  <option value="0" selected>Pilih Penghasilan Wali</option>
                  <option value="Kurang dari Rp. 500.000">Kurang dari Rp. 500.000</option>
                  <option value="Rp. 500.000 - Rp. 1.000.000">Rp. 500.000 - Rp. 1.000.000</option>
                  <option value="Rp. 1.000.000 - Rp. 2.000.000">Rp. 1.000.000 - Rp. 2.000.000</option>
                  <option value="Rp. 2.000.000 - Rp. 5.000.000">Rp. 2.000.000 - Rp. 5.000.000</option>
                  <option value="Rp. 5.000.000 - Rp. 20.000.000">Rp. 5.000.000 - Rp. 20.000.000</option>
                  <option value="Lebih dari Rp. 20.000.000">Lebih dari Rp. 20.000.000</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
              </div>
              <label class="col-md-2 col-form-label">Jenis Pembiayaan</label>
              <div class="col-md-4">
                <select class="form-control" name="id_jenis_pembiayaan">
                  <option value="0" selected>Pilih Data Jenis Pembiayaan</option>
                  <?php foreach ($jenis_pembiayaan as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jumlah Biaya Masuk <span style="color: red">*</span></label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="jumlah_biaya_masuk" placeholder="Isi Jumlah Biaya Masuk" value="<?= set_value('jumlah_biaya_masuk'); ?>">
                <small class="text-danger">
                  <?php echo form_error('jumlah_biaya_masuk') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">SKS Diakui</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="sks_diakui" placeholder="Isi SKS Diakui (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= set_value('sks_diakui'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi Asal</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="program_studi_asal" placeholder="Isi Program Studi Asal (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= set_value('program_studi_asal'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Perguruan Tinggi Asal</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="perguruan_tinggi_asal" placeholder="Isi Perguruan Tinggi Asal (Jika Jenis Pendaftaran Selain Peserta Didik)" value="<?= set_value('perguruan_tinggi_asal'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tgl. Lulus</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lulus" value="<?= set_value('tgl_lulus'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Nomor Ijazah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nomor_ijazah" placeholder="Isi Nomor Ijazah" value="<?= set_value('nomor_ijazah'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nomor Transkrip</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nomor_transkrip" placeholder="Isi Nomor Transkrip" value="<?= set_value('nomor_transkrip'); ?>">
              </div>
              <label class="col-md-2 col-form-label">Judul Skripsi</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="judul_skripsi" placeholder="Isi Judul Skripsi" value="<?= set_value('judul_skripsi'); ?>">
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('mahasiswa'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>