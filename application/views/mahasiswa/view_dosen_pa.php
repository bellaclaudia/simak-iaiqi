<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Dosen Pembimbing Akademik (PA)
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kelas</th>
                  <th>Mahasiswa</th>
                  <th>Dosen Pembimbing</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_dosen_pa as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_kelas ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <td><?= $row->nidn . " - " . $row->nama_dosen ?></td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>