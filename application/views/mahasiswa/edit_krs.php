<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Edit KRS Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            Tahun Akademik : <b><?= $thn_akademiknya ?></b> <br>
            Semester : <b><?= $nama_semester ?></b> <br>
            Program Studi : <b><?= $prodinya ?></b> <br>
            Mahasiswa : <b><?= $mhsnya ?></b> <br><br>
            <a href="<?= base_url('mahasiswa/view_krs_mhs'); ?>" class="btn btn-success btn-circle">Kembali Ke Halaman List KRS</button></a>

            <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Mata Kuliah</button><br><br>
            <br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Mata Kuliah</th>
                  <th>Nama Mata Kuliah</th>
                  <th>SKS</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($detail_krs as $row) :
                  if ($row->id != '') {
                ?>
                    <tr>
                      <td><?= $i++; ?></td>
                      <td><?= $row->kode ?></td>
                      <td><?= $row->nama_mata_kuliah ?></td>
                      <td style="text-align: right;"><?= $row->sks ?></td>
                      <td>
                        <a href="<?php echo site_url('mahasiswa/hapus_detail_mk/' . $row->id . '/' . $id_krs); ?>" onclick="return confirm('Apakah Anda Yakin Menghapus Data Mata Kuliah <?= $row->nama_mata_kuliah; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                <?php
                  }
                endforeach; ?>
                </tr>
              </tbody>
            </table>

            <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php
                    $attributes = array('id' => 'FrmAddMk', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                    echo form_open('', $attributes);
                    ?>
                    <div class="form-group row">
                      <label class="col-md-3 col-form-label">Mata Kuliah</label>
                      <div class="col-md-9">
                        <input type="hidden" name="id_ta" value="<?= $id_ta ?>">
                        <input type="hidden" name="id_semester" value="<?= $id_semester ?>">
                        <input type="hidden" name="id_prodi" value="<?= $id_prodi ?>">
                        <input type="hidden" name="id_mhs" value="<?= $id_mhs ?>">
                        <input type="hidden" name="is_simpan_mk" value="1">

                        <select class="form-control select2-single" id="select2-1" name="id_mk_jadwal">
                          <option value="0" selected disabled>Pilih Mata Kuliah</option>
                          <?php
                          foreach ($data_mk as $row1) { ?>
                            <!-- <option value="<?php //echo $row1->id . "," . $row1->id_jadwal 
                                                ?>"><?php //echo "[" . $row1->kode . "] " . $row1->nama . " (Jadwal Kuliah: " . $row1->hari . ", " . $row1->jam_mulai . " - " . $row1->jam_selesai . ")"  
                                                    ?></option> -->
                            <option value="<?php echo $row1->id ?>"><?php echo "[" . $row1->kode . "] " . $row1->nama  ?></option>
                          <?php }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                      <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>