<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Menu</li>
        <?php if (isset($this->session->userdata['logged_in'])) { // jika udh lgin 
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
          </li>
          <?php if ($this->session->userdata['id_grup_user'] == '1') { // administrator 
          ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Master Umum</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('agama'); ?>">
                    <i class="nav-icon fa fa-list"></i> Agama
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_goldar'); ?>">
                    <i class="nav-icon fa fa-list"></i> Golongan Darah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_provinsi'); ?>">
                    <i class="nav-icon fa fa-list"></i> Provinsi
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_kab_kota'); ?>">
                    <i class="nav-icon fa fa-list"></i> Kabupaten/Kota
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Master Akademik</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_fakultas'); ?>">
                    <i class="nav-icon fa fa-list"></i> Fakultas
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_jenjang_studi'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jenjang Studi
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_prodi'); ?>">
                    <i class="nav-icon fa fa-list"></i> Program Studi
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_kategori_kelas'); ?>">
                    <i class="nav-icon fa fa-list"></i> Kategori Kelas
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_kelas'); ?>">
                    <i class="nav-icon fa fa-list"></i> Kelas
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_status_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Status Mahasiswa
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_status_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Status Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_jab_ak_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jabatan Akademik Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_semester'); ?>">
                    <i class="nav-icon fa fa-list"></i> Semester
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_tahun_akademik'); ?>">
                    <i class="nav-icon fa fa-list"></i> Tahun Akademik
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_jenis_mk'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jenis Mata Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_mata_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Mata Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_metode_pembelajaran'); ?>">
                    <i class="nav-icon fa fa-list"></i> Metode Pembelajaran
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_angkatan_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Angkatan
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_gelombang_calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Gelombang Pendaftaran Calon Mhs
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_dokumen_calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Dokumen Persyaratan Calon Mhs
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_jalur_pendaftaran'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jalur Pendaftaran
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_rekening'); ?>">
                    <i class="nav-icon fa fa-list"></i> Rekening Kampus
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Master SKS - Nilai</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_nilai_huruf'); ?>">
                    <i class="nav-icon fa fa-list"></i> Nilai Huruf
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_level_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Level Nilai
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_bobot_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Bobot Nilai
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_komponen_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Komponen Nilai
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_maksimum_sks'); ?>">
                    <i class="nav-icon fa fa-list"></i> Maksimum SKS
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_predikat_kelulusan'); ?>">
                    <i class="nav-icon fa fa-list"></i> Predikat Kelulusan
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Master Biaya Kuliah</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_jenis_pembiayaan'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jenis Pembiayaan
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_biaya_calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya Calon Mahasiswa
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_biaya_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya Kuliah
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Calon Mahasiswa</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Calon Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Mahasiswa</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Mahasiswa
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('riwayat_status_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Riwayat Status Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Alumni</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('alumni'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Alumni
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Dosen</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('riwayat_pend_dosen'); ?>">
                    <i class=" nav-icon fa fa-list"></i> Riwayat Pendidikan Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('set_pa_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Set PA Mahasiswa
                  </a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="">
                  <i class="nav-icon fa fa-list"></i> Set Pembimbing TA Mahasiswa
                </a>
              </li> -->
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> KRS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('krs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KRS
                  </a>
                </li>
                <!-- <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('verifikasi_krs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Persetujuan KRS
                  </a>
                </li> -->
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Nilai / KHS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('khs/view_nilai_mk'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Nilai Mata Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('khs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KHS
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('khs/mhs_pindahan'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Nilai Mhs Pindahan
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('transkrip_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Transkrip Nilai
                  </a>
                </li>
                <!-- <li class="nav-item">
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Master Umum</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('agama'); ?>">
                <i class="nav-icon fa fa-list"></i> Agama
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_goldar'); ?>">
                <i class="nav-icon fa fa-list"></i> Golongan Darah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_provinsi'); ?>">
                <i class="nav-icon fa fa-list"></i> Provinsi
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_kab_kota'); ?>">
                <i class="nav-icon fa fa-list"></i> Kabupaten/Kota
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Master Akademik</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_fakultas'); ?>">
                <i class="nav-icon fa fa-list"></i> Fakultas
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_jenjang_studi'); ?>">
                <i class="nav-icon fa fa-list"></i> Jenjang Studi
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_prodi'); ?>">
                <i class="nav-icon fa fa-list"></i> Program Studi
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_status_mhs'); ?>">
                <i class="nav-icon fa fa-list"></i> Status Mahasiswa
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_status_dosen'); ?>">
                <i class="nav-icon fa fa-list"></i> Status Dosen
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_jab_ak_dosen'); ?>">
                <i class="nav-icon fa fa-list"></i> Jabatan Akademik Dosen
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_semester'); ?>">
                <i class="nav-icon fa fa-list"></i> Semester
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_tahun_akademik'); ?>">
                <i class="nav-icon fa fa-list"></i> Tahun Akademik
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_jenis_mk'); ?>">
                <i class="nav-icon fa fa-list"></i> Jenis Mata Kuliah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_mata_kuliah'); ?>">
                <i class="nav-icon fa fa-list"></i> Mata Kuliah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_metode_pembelajaran'); ?>">
                <i class="nav-icon fa fa-list"></i> Metode Pembelajaran
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('master_angkatan_mhs'); ?>">
                <i class="nav-icon fa fa-list"></i> Angkatan
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Gelombang Pendaftaran Calon Mhs
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Jalur Pendaftaran
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Rekening Kampus
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Master SKS - Nilai</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Nilai Huruf
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Level Nilai
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Bobot Nilai
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Komponen Nilai
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Maksimum SKS
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Predikat Kelulusan
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Master Biaya Kuliah</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Biaya Calon Mahasiswa
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Biaya Kuliah
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Calon Mahasiswa</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Data Calon Mahasiswa
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Mahasiswa</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('mahasiswa'); ?>">
                <i class="nav-icon fa fa-list"></i> Data Mahasiswa
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Riwayat Status Mahasiswa
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Alumni</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Data Alumni
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Dosen</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Data Dosen
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Riwayat Pendidikan Dosen
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Set PA Mahasiswa
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Set Pembimbing TA Mahasiswa
              </a>
            </li> -->
              </ul>
            </li>
            <!-- <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> KRS</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Data KRS
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Persetujuan KRS
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-list"></i> Penilaian / KHS</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Data KHS
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="nav-icon fa fa-list"></i> Pengisian KHS
              </a>
            </li>
            </ul>
          </li> -->
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Perkuliahan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('jadwal_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jadwal Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pengajar_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Set Pengajar Kuliah
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Semester Pendek</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('semester_pendek'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data SP Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-money"></i> Pembayaran Biaya</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('biaya_kuliah_reguler'); ?>">
                    <i class="nav-icon fa fa-money"></i> Biaya Kuliah Reguler
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('biaya_kuliah_sp'); ?>">
                    <i class="nav-icon fa fa-money"></i> Biaya Kuliah SP
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pembayaran_biaya_kuliah'); ?>">
                    <i class="nav-icon fa fa-money"></i> Pembayaran Biaya Kuliah
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-info"></i> Informasi Kampus</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pengumuman_akademik'); ?>">
                    <i class="nav-icon fa fa-info"></i> Pengumuman Akademik
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('kalender_akademik'); ?>">
                    <i class="nav-icon fa fa-info"></i> Kalender Akademik
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_grup_user'); ?>">
                    <i class="nav-icon fa fa-users"></i> Grup User
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_user'); ?>">
                    <i class="nav-icon fa fa-user"></i> Data User
                  </a>
                </li>

                <!-- <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('format_nim'); ?>">
                    <i class="nav-icon fa fa-user"></i> Format NIM
                  </a>
                </li> -->

                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>

          <?php } else if ($this->session->userdata['id_grup_user'] == '2') { // staf akademik 
          ?>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Calon Mahasiswa</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Calon Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Mahasiswa</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Mahasiswa
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('riwayat_status_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Riwayat Status Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Alumni</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('alumni'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Alumni
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Dosen</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('riwayat_pendidikan_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Riwayat Pendidikan Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('set_pa_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Set PA Mahasiswa
                  </a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="">
                  <i class="nav-icon fa fa-list"></i> Set Pembimbing TA Mahasiswa
                </a>
              </li> -->
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> KRS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('krs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KRS
                  </a>
                </li>
                <!-- <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('krs/verifikasi_krs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Persetujuan KRS
                  </a>
                </li> -->
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Nilai / KHS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('khs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KHS
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('transkrip_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Transkrip Nilai
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Perkuliahan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('jadwal_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jadwal Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('set_pengajar_kuliah'); ?>">
                    <i class="nav-icon fa fa-list"></i> Set Pengajar Kuliah
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Semester Pendek</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('semester_pendek'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data SP Mahasiswa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-money"></i> Pembayaran Biaya</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('biaya_kuliah_reguler'); ?>">
                    <i class="nav-icon fa fa-money"></i> Biaya Kuliah Reguler
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('biaya_kuliah_sp'); ?>">
                    <i class="nav-icon fa fa-money"></i> Biaya Kuliah SP
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pembayaran_biaya_kuliah'); ?>">
                    <i class="nav-icon fa fa-money"></i> Pembayaran Biaya Kuliah
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-info"></i> Informasi Kampus</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pengumuman_akademik'); ?>">
                    <i class="nav-icon fa fa-info"></i> Pengumuman Akademik
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('kalender_akademik'); ?>">
                    <i class="nav-icon fa fa-info"></i> Kalender Akademik
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
          <?php } else if ($this->session->userdata['id_grup_user'] == '3') { // calon mhs
          ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Profil</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('calon_mhs/biodata_calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biodata Calon Mhs
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Pembayaran Biaya</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('calon_mhs/view_biaya_calon_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya Pendaftaran
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('calon_mhs/view_biaya_upp'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya UPP
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
          <?php } else if ($this->session->userdata['id_grup_user'] == '4') { // mhs
          ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Profil</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/biodata_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biodata Mhs
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_status_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Status Mhs
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> KRS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_krs_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KRS
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Semester Pendek</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_sp_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data SP Mahasiswa
                  </a>
                </li>

              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Nilai / KHS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_khs_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KHS
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/transkrip_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Transkrip Nilai
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Perkuliahan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_jadwal_kuliah_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jadwal Kuliah
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Pembayaran Biaya</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_biaya_kuliah_reguler_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya Kuliah Reguler
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_biaya_sp_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biaya Kuliah SP
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Dosen Pembimbing</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('mahasiswa/view_dosen_pa'); ?>">
                    <i class="nav-icon fa fa-list"></i> Pembimbing Akademik (PA)
                  </a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="<?= base_url('view_dosen_ta'); ?>">
                  <i class="nav-icon fa fa-list"></i> Pembimbing Tugas Akhir / Skripsi
                </a>
              </li> -->
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
          <?php
          } else if ($this->session->userdata['id_grup_user'] == '5') { // dosen
          ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Profil</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/biodata_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Biodata Dosen
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/riwayat_pendidikan_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Riwayat Pendidikan Dosen
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> KRS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/view_krs_waiting_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Persetujuan KRS
                  </a>
                </li>

              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Semester Pendek</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/view_sp_waiting_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Persetujuan SP
                  </a>
                </li>

              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Nilai / KHS</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/set_bobot_komponen_nilai'); ?>">
                    <i class="nav-icon fa fa-list"></i> Set Bobot Komponen Nilai
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/view_nilai_mk'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data Nilai Mata Kuliah
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/view_khs_mhs'); ?>">
                    <i class="nav-icon fa fa-list"></i> Data KHS Mhs
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Perkuliahan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('dosen/view_jadwal_kuliah_dosen'); ?>">
                    <i class="nav-icon fa fa-list"></i> Jadwal Mengajar Kuliah
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>

        <?php }
        }
        ?>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>