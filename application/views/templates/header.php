<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="SIMAK IAIQI">
  <meta name="author" content="Bella Claudia">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>.:: SIMAK IAIQI ::.</title>
  <!-- Icons-->
  <link href="<?= base_url(); ?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application-->
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url(); ?>assets/vendors/select2/css/select2.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda-themeless.min.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= base_url(); ?>">
      SIMAK IAIQI
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <?php
          // 08-06-2022 ambil data foto dari file_foto di tabel master_user. jika ga ada, maka cek jika jenisnya mhs/dosen, maka ambil dari tabel mhs/dosen
          if (isset($this->session->userdata['username'])) {
            $sqlxx = " select id, id_grup_user, file_foto FROM master_user WHERE username = '" . $this->session->userdata['username'] . "'";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
              $hasilxx = $queryxx->row();
              $file_foto    = $hasilxx->file_foto;
              $id_grup_user    = $hasilxx->id_grup_user;
              $id_user    = $hasilxx->id;

              if ($file_foto == '') {
                // cek apakah mhs/dosen
                if ($id_grup_user == '4') {
                  // mhs
                  $sqlxx2 = " SELECT file_foto FROM mahasiswa WHERE id_user = '$id_user' ";
                  $queryxx2 = $this->db->query($sqlxx2);
                  if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $file_foto    = "uploads/" . $hasilxx2->file_foto;
                    if ($file_foto == '') {
                      $file_foto = "assets/img/user.png";
                    }
                  }
                } else if ($id_grup_user == '5') {
                  // dosen
                  $sqlxx2 = " SELECT file_foto FROM dosen WHERE id_user = '$id_user' ";
                  $queryxx2 = $this->db->query($sqlxx2);
                  if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $file_foto    = "uploads/" . $hasilxx2->file_foto;
                    if ($file_foto == '')
                      $file_foto = "assets/img/user.png";
                  } else {
                    $file_foto = "assets/img/user.png";
                  }
                } else if ($id_grup_user == '3') {
                  // calon mhs
                  $sqlxx2 = " SELECT file_foto FROM calon_mahasiswa WHERE id_user = '$id_user' ";
                  $queryxx2 = $this->db->query($sqlxx2);
                  if ($queryxx2->num_rows() > 0) {
                    $hasilxx2 = $queryxx2->row();
                    $file_foto    = "uploads/" . $hasilxx2->file_foto;
                    if ($file_foto == '')
                      $file_foto = "assets/img/user.png";
                  } else {
                    $file_foto = "assets/img/user.png";
                  }
                } else {
                  $file_foto = "assets/img/user.png";
                }
              } else {
                $file_foto    = "uploads/" . $file_foto;
              }
            }
          } else {
            $file_foto = "assets/img/user.png";
          }

          ?>
          <img class="img-avatar" src="<?= base_url() . $file_foto; ?>"> <!-- assets/img/user.png -->
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <div class="dropdown-header text-center">
            <strong>Hi, <?php if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) echo "anonymous";
                        else echo $this->session->userdata['username']; ?> </strong>
          </div>

          <?php if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) { ?>
            <a class="dropdown-item" href="<?= base_url('login') ?>">
              <i class="fa fa-lock"></i> Login
            </a>
          <?php } else { ?>
            <a class="dropdown-item" href="<?= base_url('login/logout') ?>">
              <i class="fa fa-lock"></i> Logout
            </a>
          <?php } ?>
        </div>
      </li>
    </ul>
  </header>