<script>
  function get_mk(id_prodi, id_semester) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('jadwal_kuliah/get_mk_by_prodi'); ?>',
      data: 'id_prodi=' + id_prodi + '&id_semester=' + id_semester,
      success: function(response) {
        $("#mknya").html(response);
      }
    });
  }

  function get_kelas(id_prodi, id_semester, id_ta) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('jadwal_kuliah/get_kelas_by_prodisemta'); ?>',
      data: 'id_prodi=' + id_prodi + '&id_semester=' + id_semester + '&id_ta=' + id_ta,
      success: function(response) {
        $("#kelasnya").html(response);
      }
    });
  }
</script>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('jadwal_kuliah'); ?>">Data Jadwal Kuliah</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddMataKuliah', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Semester</label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_jadwal_kuliah->id; ?>">
              <select class="form-control select2-single" id="id_semester" name="id_semester" onchange="get_mk($('#id_prodi').val(), this.value);">
                <?php
                $lv = '';
                if (isset($data_jadwal_kuliah->id_semester)) {
                  $lv = $data_jadwal_kuliah->id_semester;
                }
                foreach ($semester as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tahun Akademik</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                <?php
                $lv = '';
                if (isset($data_jadwal_kuliah->id_tahun_akademik)) {
                  $lv = $data_jadwal_kuliah->id_tahun_akademik;
                }
                foreach ($tahun_akademik as $r => $v) {
                ?>
                  <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                <?php
                }
                ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_tahun_akademik') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Program Studi</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="id_prodi" name="id_prodi" onchange="get_mk(this.value, $('#id_semester').val()); get_kelas(this.value, $('#id_semester').val(), $('#select2-1').val());">
                <option value="0" selected disabled>Pilih Data Program Studi</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $data_jadwal_kuliah->id_program_studi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Kelas</label>
            <div class="col-md-9">
              <label id="kelasnya">
                <select class="form-control select2-single" id="id_kelas" name="id_kelas">
                  <?php
                  foreach ($kelas as $row1) { ?>
                    <option value="<?php echo $row1->id ?>" <?php if ($row1->id == $data_jadwal_kuliah->id_kelas) { ?>selected <?php } ?>><?php echo $row1->nama  ?></option>
                  <?php }
                  ?>
                </select></label>
              <small class="text-danger">
                <?php echo form_error('id_kelas') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Mata Kuliah</label>
            <div class="col-md-9">
              <label id="mknya">
                <select class="form-control select2-single" id="select2-2" name="id_mata_kuliah">
                  <?php foreach ($mata_kuliah as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $data_jadwal_kuliah->id_mata_kuliah) { ?>selected<?php } ?>><?= "[" . $k['kode'] . "] " . $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select></label>
              <small class="text-danger">
                <?php echo form_error('id_mata_kuliah') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Maksimal Kuota Mhs</label>
            <div class="col-md-3">
              <input class="form-control" type="text" style="text-align: right;" name="max_kuota" value="<?= $data_jadwal_kuliah->max_kuota; ?>">
              <small class="text-danger">
                <?php echo form_error('max_kuota') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jenis Kuliah</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="jenis_kuliah" name="jenis_kuliah">
                <option value="1" <?php if ($data_jadwal_kuliah->jenis_kuliah == 1) { ?>selected<?php } ?>>Reguler</option>
                <option value="2" <?php if ($data_jadwal_kuliah->jenis_kuliah == 2) { ?>selected<?php } ?>>SP</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Hari</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-4" name="hari">
                <option <?php if ($data_jadwal_kuliah->hari == "Senin") {
                          echo "selected";
                        } ?>>Senin</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Selasa") {
                          echo "selected";
                        } ?>>Selasa</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Rabu") {
                          echo "selected";
                        } ?>>Rabu</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Kamis") {
                          echo "selected";
                        } ?>>Kamis</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Jumat") {
                          echo "selected";
                        } ?>>Jumat</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Sabtu") {
                          echo "selected";
                        } ?>>Sabtu</option>
                <option <?php if ($data_jadwal_kuliah->hari == "Minggu") {
                          echo "selected";
                        } ?>>Minggu</option>
              </select>
              <small class="text-danger">
                <?php echo form_error('hari') ?>
              </small>
            </div>

          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jam Mulai</label>
            <div class="col-md-2">
              <select class="form-control select2-single" id="jam1" name="jam1">
                <?php for ($i = 0; $i <= 23; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    $jamnya =  "0" . $i;
                                  } else $jamnya = $i;
                                  echo $jamnya ?>" <?php if ($jamnya == $jam1) { ?>selected<?php } ?>><?php if ($i < 10) {
                                                                                                        echo "0" . $i;
                                                                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-control select2-single" id="menit1" name="menit1">
                <?php for ($i = 0; $i <= 59; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    $menitnya = "0" . $i;
                                  } else $menitnya = $i;
                                  echo $menitnya ?>" <?php if ($menitnya == $menit1) { ?>selected<?php } ?>><?php if ($i < 10) {
                                                                                                              echo "0" . $i;
                                                                                                            } else echo $i ?></option>
                <?php } ?>
              </select>
            </div> * hh:mm
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jam Selesai</label>
            <div class="col-md-2">
              <select class="form-control select2-single" id="jam2" name="jam2">
                <?php for ($i = 0; $i <= 23; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    $jamnya = "0" . $i;
                                  } else $jamnya = $i;
                                  echo $jamnya ?>" <?php if ($jamnya == $jam2) { ?>selected<?php } ?>><?php if ($i < 10) {
                                                                                                        echo "0" . $i;
                                                                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-control select2-single" id="menit2" name="menit2">
                <?php for ($i = 0; $i <= 59; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    $menitnya = "0" . $i;
                                  } else $menitnya = $i;
                                  echo $menitnya ?>" <?php if ($menitnya == $menit2) { ?>selected<?php } ?>><?php if ($i < 10) {
                                                                                                              echo "0" . $i;
                                                                                                            } else echo $i ?></option>
                <?php } ?>
              </select>
            </div> * hh:mm
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('jadwal_kuliah'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>