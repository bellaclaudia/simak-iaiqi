<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Jadwal Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Jadwal Kuliah
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">

            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?><br>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianKrs', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_prodi">
                  <option value="0">- All -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_prodi') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>

            <a href="<?= base_url('jadwal_kuliah/tambah/'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Semester</th>
                  <th>Tahun Akademik</th>
                  <th>Program Studi</th>
                  <th>Kelas</th>
                  <th>Mata Kuliah</th>
                  <th>Max Kuota Mhs</th>
                  <th>Jenis Kuliah</th>
                  <th>Hari</th>
                  <th>Jam</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_jadwal_kuliah as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->semester ?></td>
                    <td><?= $row->tahun_akademik ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?= $row->nama_kelas ?></td>
                    <td><?= $row->mata_kuliah ?></td>
                    <td style="text-align:right;"><?= $row->max_kuota ?></td>
                    <td><?php if ($row->jenis_kuliah == 1) echo "Reguler";
                        else echo "SP"; ?></td>
                    <td><?= $row->hari ?></td>
                    <td><?= $row->jam_mulai . " s/d " . $row->jam_selesai ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td>
                      <a href="<?= base_url('jadwal_kuliah/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a><br><br>
                      <a href="<?php echo site_url('jadwal_kuliah/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Jadwal Mata Kuliah <?= $row->mata_kuliah; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>