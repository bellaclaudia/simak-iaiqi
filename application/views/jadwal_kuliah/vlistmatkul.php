<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="SIMAK IAIQI">
  <meta name="author" content="Bella Claudia">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <link href="<?= base_url(); ?>assets/vendors/select2/css/select2.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda-themeless.min.css" rel="stylesheet">
</head>
<select class="form-control select2-single" id="select2-4" name="id_mata_kuliah">
	<option value="0" selected disabled>Pilih Mata Kuliah</option>
	<?php
	foreach ($data_matkul as $row1) { ?>
		<option value="<?php echo $row1->id ?>"><?php echo $row1->kode . " - " . $row1->nama  ?></option>
	<?php }
	?>
</select>
<script src="<?= base_url(); ?>assets/node_modules/select2/dist/js/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url(); ?>assets/js/advanced-forms.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/ladda/dist/spin.min.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda.min.js"></script>