<script>
  function get_mk(id_prodi, id_semester) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('jadwal_kuliah/get_mk_by_prodi'); ?>',
      data: 'id_prodi=' + id_prodi + '&id_semester=' + id_semester,
      success: function(response) {
        $("#mknya").html(response);
      }
    });
  }

  function get_kelas(id_prodi, id_semester, id_ta) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('jadwal_kuliah/get_kelas_by_prodisemta'); ?>',
      data: 'id_prodi=' + id_prodi + '&id_semester=' + id_semester + '&id_ta=' + id_ta,
      success: function(response) {
        $("#kelasnya").html(response);
      }
    });
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('jadwal_kuliah'); ?>">Data Jadwal Kuliah</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddJadwalKuliah', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Semester</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="id_semester" name="id_semester" onchange="get_mk($('#id_prodi').val(), this.value);">
                <option value="0" selected disabled>Pilih Data Semester</option>
                <?php foreach ($semester as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tahun Akademik</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                <option value="0" selected disabled>Pilih Data Tahun Akademik</option>
                <?php foreach ($tahun_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_tahun_akademik') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Program Studi</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="id_prodi" name="id_prodi" onchange="get_mk(this.value, $('#id_semester').val()); get_kelas(this.value, $('#id_semester').val(), $('#select2-1').val());">
                <option value="0" selected disabled>Pilih Data Program Studi</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Kelas</label>
            <div class="col-md-9">
              <label id="kelasnya">
                <select class="form-control select2-single" id="id_kelas" name="id_kelas">
                  <option value="0" selected disabled>Pilih Kelas</option>
                </select>
              </label>
              <small class="text-danger">
                <?php echo form_error('id_kelas') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Mata Kuliah</label>
            <div class="col-md-9">
              <label id="mknya">
                <select class="form-control select2-single" id="id_mata_kuliah" name="id_mata_kuliah">
                  <option value="0" selected disabled>Pilih Data Mata Kuliah</option>
                  <?php foreach ($mata_kuliah as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
              </label>
              <small class="text-danger">
                <?php echo form_error('id_mata_kuliah') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Maksimal Kuota Mhs</label>
            <div class="col-md-3">
              <input class="form-control" type="text" style="text-align: right;" name="max_kuota" value="<?= set_value('max_kuota'); ?>">
              <small class="text-danger">
                <?php echo form_error('max_kuota') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jenis Kuliah</label>
            <div class="col-md-9">
              <select class="form-control select2-single" id="jenis_kuliah" name="jenis_kuliah">
                <option value="1">Reguler</option>
                <option value="2">SP</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-form-label">Hari</label>
            <div class="col-md-3">
              <select class="form-control select2-single" id="select2-4" name="hari">
                <option value="0" selected disabled>Pilih Hari</option>
                <option value="Senin">Senin</option>
                <option value="Selasa">Selasa</option>
                <option value="Rabu">Rabu</option>
                <option value="Kamis">Kamis</option>
                <option value="Jumat">Jumat</option>
                <option value="Sabtu">Sabtu</option>
                <option value="Minggu">Minggu</option>
              </select>
              <small class="text-danger">
                <?php echo form_error('hari') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jam Mulai</label>
            <div class="col-md-2">
              <select class="form-control select2-single" id="jam1" name="jam1">
                <?php for ($i = 0; $i <= 23; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    echo "0" . $i;
                                  } else echo $i ?>"><?php if ($i < 10) {
                                                        echo "0" . $i;
                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-control select2-single" id="menit1" name="menit1">
                <?php for ($i = 0; $i <= 59; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    echo "0" . $i;
                                  } else echo $i ?>"><?php if ($i < 10) {
                                                        echo "0" . $i;
                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div> * hh:mm
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Jam Selesai</label>
            <div class="col-md-2">
              <select class="form-control select2-single" id="jam2" name="jam2">
                <?php for ($i = 0; $i <= 23; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    echo "0" . $i;
                                  } else echo $i ?>"><?php if ($i < 10) {
                                                        echo "0" . $i;
                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-control select2-single" id="menit2" name="menit2">
                <?php for ($i = 0; $i <= 59; $i++) { ?>
                  <option value="<?php if ($i < 10) {
                                    echo "0" . $i;
                                  } else echo $i ?>"><?php if ($i < 10) {
                                                        echo "0" . $i;
                                                      } else echo $i ?></option>
                <?php } ?>
              </select>
            </div> * hh:mm
          </div>

          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('jadwal_kuliah'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>