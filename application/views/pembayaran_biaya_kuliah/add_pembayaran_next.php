<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('pembayaran_biaya_kuliah'); ?>">Pembayaran Biaya Kuliah</a>
    </li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data Pembayaran Biaya Kuliah
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?><br>
          <form id="FrmAddPembayaran" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="id_ta" value="<?= $id_ta ?>">
            <input type="hidden" name="id_semester" value="<?= $id_semester ?>">
            <input type="hidden" name="is_simpan" value="1">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <b><?= $nama_ta ?></b>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <b><?= $nama_sem ?></b>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mahasiswa</label>
              <div class="col-md-4">
                <b><?= $mhs ?></b>
              </div>
            </div>

            <?php if ($jenis_biaya == 1) $nama_jenis = 'REGULER';
            else $nama_jenis = 'SP'; ?>

            Berikut ini list biaya kuliah <?= $nama_jenis ?>:<br>
            <?php
            if (is_array($data_biaya)) {
              for ($i = 0; $i < count($data_biaya); $i++) { ?>
                <?= $data_biaya[$i]['nama'] ?> Rp. <?= number_format($data_biaya[$i]['nominal']) ?><br>
            <?php }
            } ?>
            <br><b>Total Biaya: Rp. <?= number_format($jumlahnya) ?></b>

            <br><br>Silahkan melakukan pembayaran via tunai dengan datang langsung ke kampus, atau via transfer ke salah satu rekening berikut:<br>
            <?php
            if (is_array($data_rek)) {
              for ($i = 0; $i < count($data_rek); $i++) { ?>
                <?= $data_rek[$i]['nama_bank'] ?> No. rek <?= $data_rek[$i]['no_rekening'] ?> a.n. <?= $data_rek[$i]['atas_nama'] ?><br>
            <?php }
            } ?><br>
            Jika sudah membayar, silahkan input data konfirmasi pembayaran melalui form dibawah ini:<br>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Mahasiswa</label>
              <div class="col-md-4">
                <b><?= $mhs ?></b>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jenis Pembayaran</label>
              <div class="col-md-4">
                <select class="form-control" name="jenis_bayar">
                  <!-- <option value="0" selected disabled>Pilih Jenis Bayar</option> -->
                  <option value="1">Tunai</option>
                  <option value="2">Transfer</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Ditransfer Ke Rekening (abaikan jika via tunai)</label>
              <div class="col-md-4">
                <select class="form-control" name="id_rekening">
                  <option value="0" selected disabled>Pilih Rekening</option>
                  <?php for ($i = 0; $i < count($data_rek); $i++) { ?>
                    <option value="<?= $data_rek[$i]['id_rekening'] ?>"><?= $data_rek[$i]['nama_bank'] . " (No. rek " . $data_rek[$i]['no_rekening'] . " a.n. " . $data_rek[$i]['atas_nama'] . ")" ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nominal Pembayaran</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="jumbayar" value="<?= $jumlahnya ?>" style="text-align:right;">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Pembayaran</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_bayar" value="">
              </div>
            </div>

            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('pembayaran_biaya_kuliah'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>