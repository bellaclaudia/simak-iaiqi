<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Pembayaran Biaya Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Pembayaran Biaya Kuliah
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianCBiaya', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <option value="0">- All -</option>
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <option value="0">- All -</option>
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="id_prodi" name="id_prodi">
                  <option value="0">- All -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mahasiswa</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_mhs">
                  <option value="0">- All -</option>
                  <?php foreach ($data_mhs as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_mhs) { ?>selected<?php } ?>><?= $k['nim'] . " - " . $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jenis Biaya</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-5" name="jenis_biaya">
                  <option value="0">- All -</option>
                  <option value="1" <?php if ($jenis_biaya == "1") { ?>selected<?php } ?>>REGULER</option>
                  <option value="2" <?php if ($jenis_biaya == "2") { ?>selected<?php } ?>>SP</option>
                </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <!-- <a href="<?= base_url('calon_mhs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
            </form>
            <hr>

            <a href="<?= base_url('pembayaran_biaya_kuliah/add_pembayaran'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tahun Akademik</th>
                  <th>Semester</th>
                  <th>Mahasiswa</th>
                  <th>Program Studi</th>
                  <th>Jenis Biaya</th>
                  <th>Metode Pembayaran</th>
                  <th>Nominal</th>
                  <th>Tanggal Bayar</th>
                  <th>Status Verifikasi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_pembayaran as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_tahun_akademik ?></td>
                    <td><?= $row->nama_semester ?></td>
                    <td><?= $row->nim . " - " . $row->nama_mhs ?></td>
                    <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?php if ($row->jenis_biaya == 1) echo "REGULER";
                        else echo "SP" ?></td>
                    <td><?php if ($row->jenis_bayar == '1') {
                          echo "Tunai";
                        } else echo "Transfer (Ditransfer Ke: " . $row->nama_bank . ", Rekening: " . $row->no_rekening . " a.n. " . $row->atas_nama . ")"; ?></td>

                    <td style="text-align:right;"><?= number_format($row->nominal) ?></td>
                    <td><?php
                        $pisah1 = explode("-", $row->tgl_bayar);
                        $tgl1 = $pisah1[2];
                        $bln1 = $pisah1[1];
                        $thn1 = $pisah1[0];
                        $row->tgl_bayar = $tgl1 . "-" . $bln1 . "-" . $thn1;

                        echo $row->tgl_bayar ?></td>
                    <td><?php if ($row->status_verifikasi == 0) {
                          $boleh_approve = 1;
                          echo "Waiting";
                        } else {
                          $boleh_approve = 0;
                          echo "Approved admin";
                        }  ?></td>
                    <td>
                      <?php if ($boleh_approve == 1) { ?>
                        <a href="<?= base_url('pembayaran_biaya_kuliah/approve_pembayaran/' . $row->id); ?>" class="btn btn-success btn-circle" onclick="return confirm('Apakah Anda yakin akan melakukan approve pembayaran ?');">Approve Pembayaran</button></a><br><br>

                      <?php } else echo "&nbsp;" ?>
                      <a href="<?php echo site_url('pembayaran_biaya_kuliah/hapus_pembayaran/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data pembayaran ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>