<script>
  function get_mhs(id_prodi) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('pembayaran_biaya_kuliah/get_mhs_by_prodi'); ?>',
      data: 'id_prodi=' + id_prodi,
      success: function(response) {
        $("#mhsnya").html(response);
      }
    });
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('pembayaran_biaya_kuliah'); ?>">Pembayaran Biaya Kuliah</a>
    </li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data Pembayaran Biaya Kuliah
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?><br>
          <form id="FrmAddPembayaran" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="is_cek" value="1">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tahun Akademik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_ta">
                  <!-- <option value="0" selected disabled>Pilih Tahun Akademik</option> -->
                  <?php foreach ($data_thn_akademik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Semester</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_semester">
                  <!-- <option value="0" selected disabled>Pilih Semester</option> -->
                  <?php foreach ($data_semester as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="id_prodi" name="id_prodi" onchange="get_mhs(this.value);">
                  <option value="0">- Pilih -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Mahasiswa</label>
              <div class="col-md-4">
                <label id="mhsnya">
                  <select class="form-control select2-single" id="select2-3" name="id_mhs">
                    <?php foreach ($data_mhs as $k) : ?>
                      <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_mhs) { ?>selected<?php } ?>><?= $k['nim'] . " - " . $k['nama'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jenis Biaya</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="jenis_biaya">
                  <option value="1" <?php if ($jenis_biaya == "1") { ?>selected<?php } ?>>REGULER</option>
                  <option value="2" <?php if ($jenis_biaya == "2") { ?>selected<?php } ?>>SP</option>
                </select>
              </div>
            </div>

            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Cek Data Pembayaran</button>&nbsp;
              <a href="<?= base_url('pembayaran_biaya_kuliah'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>