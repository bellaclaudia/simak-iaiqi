<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Kalender Akademik</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Kalender Akademik
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Tanggal Awal</th>
                <th>Tanggal Akhir</th>
                <th>Kegiatan</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_kalender_akademik as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->tgl_awal ?></td>
                  <td><?= $row->tgl_akhir ?></td>
                  <td><?= $row->kegiatan ?></td>
                  <td><?= $row->deskripsi ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('kalender_akademik/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Kalender Akademik <?= $row->kegiatan; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddKalenderAkademik', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tanggal Awal</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_awal" required>
                      <small class="text-danger">
                        <?php echo form_error('tgl_awal') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tanggal Akhir</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_akhir" required>
                      <small class="text-danger">
                        <?php echo form_error('tgl_akhir') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Kegiatan</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="kegiatan" placeholder="Isi Kegiatan" required>
                      <small class="text-danger">
                        <?php echo form_error('kegiatan') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Deskripsi</label>
                    <div class="col-md-9">
                      <textarea class="form-control" name="deskripsi" placeholder="Isi Deskripsi" style="width: 100%; height: 200px" required></textarea>
                      <small class="text-danger">
                        <?php echo form_error('deskripsi') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_kalender_akademik as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('kalender_akademik/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tanggal Awal</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <input class="form-control" type="date" name="tgl_awal" value="<?= $row->tgl_awal; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('tgl_awal') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tanggal Akhir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="date" name="tgl_akhir" value="<?= $row->tgl_akhir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('tgl_akhir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Kegiatan</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="kegiatan" placeholder="Isi Kegiatan" value="<?= $row->kegiatan; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('kegiatan') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Deskripsi</label>
                        <div class="col-md-9">
                          <textarea class="form-control" name="deskripsi" placeholder="Isi Deskripsi" style="width: 100%; height: 200px" required><?= $row->deskripsi; ?></textarea>
                          <small class="text-danger">
                            <?php echo form_error('deskripsi') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>