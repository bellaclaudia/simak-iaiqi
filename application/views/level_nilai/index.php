<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Level Nilai</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Level Nilai
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Nilai Huruf</th>
                <th>Nilai Batas Awal</th>
                <th>Nilai Batas Akhir</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_level_nilai as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nilai_huruf ?></td>
                  <td><?= $row->nilai_batas_awal ?></td>
                  <td><?= $row->nilai_batas_akhir ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('master_level_nilai/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Level Nilai ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddLevelNilai', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Nilai Huruf</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-3" name="id_nilai_huruf" required>
                        <option value="0" selected disabled>Pilih Data Nilai Huruf</option>
                        <?php foreach ($list_nilai_huruf as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nilai Batas Awal</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nilai_batas_awal" placeholder="Isi Nilai Batas Awal" required>
                      <small class="text-danger">
                        <?php echo form_error('nilai_batas_awal') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nilai Batas Akhir</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nilai_batas_akhir" placeholder="Isi Nilai Batas Akhir" required>
                      <small class="text-danger">
                        <?php echo form_error('nilai_batas_akhir') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_level_nilai as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('master_level_nilai/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Nilai Huruf</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control select2-single" id="select2-1" name="id_nilai_huruf" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_nilai_huruf)) {
                              $lv = $row->id_nilai_huruf;
                            }
                            foreach ($list_nilai_huruf as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nilai Batas Awal</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nilai_batas_awal" placeholder="Isi Nilai Batas Awal" value="<?= $row->nilai_batas_awal; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nilai_batas_awal') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nilai Batas Akhir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nilai_batas_akhir" placeholder="Isi Nilai Batas Akhir" value="<?= $row->nilai_batas_akhir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nilai_batas_akhir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>