<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Biaya Calon Mahasiswa</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Biaya Calon Mahasiswa
        </div>
        <div class="card-body">

          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>

          <b>Filter Pencarian</b>
          <?php
          $attributes = array('id' => 'FrmPencarianBiaya', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tahun Akademik</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_ta">
                <option value="0">- All -</option>
                <?php foreach ($data_thn_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Program Studi</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-2" name="id_prodi">
                <option value="0">- All -</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
          </form>
          <hr>

          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>

          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Tahun Akademik</th>
                <th>Program Studi</th>
                <th>Kode</th>
                <th>Deskripsi</th>
                <th>Jenis Biaya</th>
                <th>Kategori Kelas</th>
                <th>Nominal (Rp.)</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_biaya_calon_mhs as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama_tahun_akademik ?></td>
                  <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                  <td><?= $row->kode ?></td>
                  <td><?= $row->nama ?></td>
                  <td><?php
                      if ($row->jenis_biaya == '1')
                        $nama_biaya = 'Biaya Pendaftaran';
                      else
                        $nama_biaya = 'Uang Pengembangan Pendidikan (UPP)';
                      echo $nama_biaya;
                      ?></td>
                  <td><?php if ($row->nama_kategori_kelas == '') echo "None";
                      else echo $row->nama_kategori_kelas ?></td>
                  <td align="right"><?= number_format($row->nominal) ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('master_biaya_calon_mhs/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Biaya Calon Mahasiswa <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBiayaCalonMhs', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tahun Akademik</label>
                    <div class="col-md-9">
                      <select class="form-control" name="id_tahun_akademik" id="select2-5">
                        <option value="" selected disabled>Pilih Tahun Akademik</option>
                        <?php foreach ($data_thn_akademik as $k) : ?>
                          <option value=" <?= $k['id']; ?>"><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Program Studi</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-4" name="id_program_studi">
                        <option value="" selected disabled>Pilih Program Studi</option>
                        <?php foreach ($data_prodi as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Kode</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="kode" placeholder="Isi Kode" required>
                      <small class="text-danger">
                        <?php echo form_error('kode') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Deskripsi</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama" placeholder="Isi Deskripsi" required>
                      <small class="text-danger">
                        <?php echo form_error('nama') ?>
                      </small>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jenis Biaya</label>
                    <div class="col-md-9">
                      <select class="form-control" name="jenis_biaya">
                        <option value="" selected disabled>Pilih Jenis Biaya</option>
                        <option value="1">Biaya Pendaftaran</option>
                        <option value="2">Uang Pengembangan Pendidikan (UPP)</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Kategori Kelas</label>
                    <div class="col-md-9">
                      <select class="form-control" name="id_kategori_kelas" id="select2-6">
                        <option value="" selected>None</option>
                        <?php foreach ($data_kategori_kelas as $k) : ?>
                          <option value=" <?= $k['id']; ?>"><?= "[" . $k['kode'] . "] " . $k['nama'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nominal</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="nominal" placeholder="Isi Nominal" required>
                      <small class="text-danger">
                        <?php echo form_error('nominal') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_biaya_calon_mhs as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('master_biaya_calon_mhs/edit'); ?>" method="post">

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tahun Akademik</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-1" name="id_tahun_akademik">
                            <option value="" selected disabled>Pilih Tahun Akademik</option>
                            <?php foreach ($data_thn_akademik as $k) : ?>
                              <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $row->id_tahun_akademik) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Program Studi</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                            <option value="" selected disabled>Pilih Program Studi</option>
                            <?php foreach ($data_prodi as $k) : ?>
                              <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $row->id_program_studi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Kode</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <input class="form-control" type="text" name="kode" placeholder="Isi Kode" required value="<?= $row->kode; ?>">
                          <small class="text-danger">
                            <?php echo form_error('kode') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Deskripsi</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama" placeholder="Isi Deskripsi" required value="<?= $row->nama; ?>">
                          <small class="text-danger">
                            <?php echo form_error('nama') ?>
                          </small>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jenis Biaya</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-1" name="jenis_biaya">
                            <option value="" selected disabled>Pilih Jenis Biaya</option>
                            <option value="1" <?php if ($row->jenis_biaya == '1') { ?>selected<?php } ?>>Biaya Pendaftaran</option>
                            <option value="2" <?php if ($row->jenis_biaya == '2') { ?>selected<?php } ?>>Uang Pengembangan Pendidikan (UPP)</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Kategori Kelas</label>
                        <div class="col-md-9">
                          <select class="form-control" name="id_kategori_kelas" id="select2-7">
                            <option value="" selected>None</option>
                            <?php foreach ($data_kategori_kelas as $k) : ?>
                              <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_kategori_kelas) { ?>selected<?php } ?>><?= "[" . $k['kode'] . "] " . $k['nama'] ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nominal</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="nominal" placeholder="Isi Nominal" required value="<?= $row->nominal; ?>">
                          <small class="text-danger">
                            <?php echo form_error('nominal') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>