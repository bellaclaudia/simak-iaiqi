<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>.:: SIMAK IAIQI ::.</title>
  <!-- Icons-->
  <link href="<?= base_url(); ?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application-->
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda-themeless.min.css" rel="stylesheet">
</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
            <div class="card-body">
              <center><img src="<?= base_url(); ?>assets/img/logo.jpeg" style="width: 100px">&nbsp;&nbsp;&nbsp;<img src="<?= base_url(); ?>assets/img/Simak.png" style="width: 100px"></center><br>
              <center>
                <h2>Sistem Informasi Akademik</h2>
                <p class="text-muted">Assalamualaikum, Login Please.<br>Masukkan Username dan Password</p>
              </center><br>
              <?= $this->session->flashdata('pesan') ?>

              <form method="post" action="<?= base_url('login/proses_login') ?>">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <input class="form-control" type="text" name="username" placeholder="Username">
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <input class="form-control" type="password" name="password" placeholder="Password">
                </div>
                <div class="row">
                  <div class="col-7">
                    <button class="btn btn-success btn-ladda" data-style="expand-right" name="login"><i class="fa fa-user"></i> LOGIN</button>
                  </div>
                  <div class="col-5">
                    <a href="<?= base_url('calon_mhs/pendaftaran'); ?>"><button class="btn btn-success btn-ladda" data-style="expand-right" type="button" style="margin-left: 18%">Pendaftaran Mahasiswa Baru</button></a>
                  </div>
                </div>
              </form>
            </div>
            <center>
              <p><?= date('Y') ?> &copy;. <a href="http://www.iaiqi.ac.id" target="_blank">Institut Agama Islam Al-Quran Al-Ittifaqiah (IAIQI)</a></p>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="<?= base_url(); ?>assets/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/pace-progress/pace.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/spin.min.js"></script>
  <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/loading-buttons.js"></script>
</body>

</html>