<script type="text/javascript">
  function cek_pernyataan_benar() {
    // alert($('#pernyataan_benar').is(":checked"));
    // return false;
    if ($('#pernyataan_benar').is(":checked") == false) {
      alert("Pernyataan harus diceklis");
      return false;
    }
  }
</script>

<main class="main">
  <!-- <ol class="breadcrumb">
    <li class="breadcrumb-item active">Pendaftaran Calon Mahasiswa Baru</li>
  </ol> -->
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Form Pendaftaran Calon Mahasiswa Baru
        </div>
        <div class="card-body">
          <form id="FrmAddCalonMahasiswa" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lengkap</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= set_value('nama'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NIK</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nik" placeholder="Isi NIK" value="<?= set_value('nik'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Jenis Kelamin</label>
              <div class="col-md-4">
                <input class="uniform" type="radio" value="L" name="jenis_kelamin"> Laki-laki &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="uniform" type="radio" value="P" name="jenis_kelamin"> Perempuan
                <small class="text-danger">
                  <?php echo form_error('jenis_kelamin') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Foto</label>
              <div class="col-md-4">
                <input class="form-control" type="file" name="file_foto" accept=".jpg,.png,.jpeg">
                <span style="color: red">*File type = .jpg,.png,.jpeg</span>
                <small class="text-danger">
                  <?php echo form_error('file_foto') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Gelombang Pendaftaran</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_gelombang_calon_mhs">
                  <option value="0" selected disabled>Pilih Gelombang</option>
                  <?php foreach ($gelombang as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama_tahun_akademik'] . " (Tahun " . $k['tahun'] . " " . $k['nama'] . ")"; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_gelombang_calon_mhs') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Program Studi Pilihan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_program_studi_pilihan">
                  <option value="0" selected disabled>Pilih Data Program Studi</option>
                  <?php foreach ($prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")"; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_program_studi_pilihan') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">&nbsp;</label>
              <div class="col-md-4">
                &nbsp;
              </div>
              <label class="col-md-2 col-form-label">Kategori Kelas</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-5" name="id_kategori_kelas">
                  <option value="0" selected disabled>Pilih Data Kategori Kelas</option>
                  <?php foreach ($data_kategori_kelas as $k) : ?>
                    <option value=" <?= $k['id']; ?>"><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kategori_kelas') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Alamat</label>
              <div class="col-md-4">
                <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= set_value('alamat'); ?></textarea>
                <small class="text-danger">
                  <?php echo form_error('alamat') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Provinsi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_provinsi">
                  <option value="0" selected disabled>Pilih Data Provinsi</option>
                  <?php foreach ($provinsi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_provinsi') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kabupaten / Kota</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_kabupaten_kota">
                  <option value="0" selected disabled>Pilih Data Kabupaten / Kota</option>
                  <?php foreach ($kab_kota as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_kabupaten_kota') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kelurahan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kelurahan" placeholder="Isi Kelurahan" value="<?= set_value('kelurahan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kelurahan') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kecamatan</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="kecamatan" placeholder="Isi Kecamatan" value="<?= set_value('kecamatan'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kecamatan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Kode Pos</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="kode_pos" placeholder="Isi Kode Pos" value="<?= set_value('kode_pos'); ?>">
                <small class="text-danger">
                  <?php echo form_error('kode_pos') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agama</label>
              <div class="col-md-4">
                <select class="form-control" name="id_agama">
                  <option value="0" selected disabled>Pilih Data Agama</option>
                  <?php foreach ($agama as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_agama') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">NISN</label>
              <div class="col-md-4">
                <input class="form-control" type="number" name="nisn" placeholder="Isi NISN" value="<?= set_value('nisn'); ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tempat Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tempat_lahir" value="<?= set_value('tempat_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tempat_lahir') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Lahir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_lahir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">

              <label class="col-md-2 col-form-label">Golongan Darah</label>
              <div class="col-md-4">
                <select class="form-control" name="id_golongan_darah">
                  <option value="0" selected disabled>Pilih Data Golongan Darah</option>
                  <?php foreach ($goldar as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_golongan_darah') ?>
                </small>
              </div>

              <label class="col-md-2 col-form-label">File Dokumen Persyaratan</label>
              <div class="col-md-4">
                <input class="form-control" type="file" name="file_dokumen" accept=".pdf">
                <span style="color: red">*File type = .pdf</span>
                <br>
                <i> File Dokumen harus sudah mencakup data berikut ini:</i><br>
                <?php foreach ($list_dokumen as $k) : ?>
                  <?= $k['nama'] . "; "; ?>
                <?php endforeach; ?>

                <small class="text-danger">
                  <?php echo form_error('file_dokumen') ?>
                </small>
              </div>

            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Email</label>
              <div class="col-md-4">
                <input class="form-control" type="email" name="email" placeholder="Isi Email" value="<?= set_value('email'); ?>">
                <small class="text-danger">
                  <?php echo form_error('email') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">No. Telepon</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_telp" placeholder="Isi No. Telepon" value="<?= set_value('no_telp'); ?>">
                <small class="text-danger">
                  <?php echo form_error('no_telp') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Ayah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ayah" placeholder="Isi Nama Ayah" value="<?= set_value('nama_ayah'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ayah') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Ibu</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= set_value('nama_ibu'); ?>">
                <small class="text-danger">
                  <?php echo form_error('nama_ibu') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Ukuran Jas</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="ukuran_jas" placeholder="Isi Ukuran Jas" value="<?= set_value('ukuran_jas'); ?>">
                <small class="text-danger">
                  <?php echo form_error('ukuran_jas') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Riwayat Penyakit</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="riwayat_penyakit" placeholder="Riwayat Penyakit (Jika Ada)">
                <small class="text-danger">
                  <?php echo form_error('riwayat_penyakit') ?>
                </small>
              </div>
            </div>
            <div class="modal-footer">
              <input type="checkbox" name="pernyataan_benar" id="pernyataan_benar" value="t"> Dengan ini saya menyatakan bahwa data yang diisikan adalah benar &nbsp;&nbsp;
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" title="Submit Pendaftaran" onclick="return cek_pernyataan_benar();"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('login'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>