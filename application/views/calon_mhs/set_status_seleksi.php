<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('calon_mhs'); ?>">Data Calon Mahasiswa</a></li>
    <li class="breadcrumb-item active">Set Status Seleksi</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Set Status Seleksi Calon Mhs
        </div>
        <div class="card-body">
          <?php
          $attributes = array('id' => 'FrmUpdateStatus', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <input type="hidden" name="is_simpan" value="1">
          <div class="form-group row">
            <label class="col-md-2 col-form-label">No Pendaftaran</label>
            <div class="col-md-4">
              <?= $no_pendaftaran ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Nama Lengkap</label>
            <div class="col-md-4">
              <?= $nama ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Set Status Seleksi</label>
            <div class="col-md-4">
              <select class="form-control" name="status_seleksi">
                <option value="0" selected disabled>Pilih Status</option>
                <option value="1" <?php if ($status_seleksi == '1') { ?>selected<?php } ?>>Diterima</option>
                <option value="2" <?php if ($status_seleksi == '2') { ?>selected<?php } ?>>Ditolak</option>
              </select>
            </div>
          </div>


          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('calon_mhs'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>