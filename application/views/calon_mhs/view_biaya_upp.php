<script type="text/javascript">
  function cek_input() {
    var jenis_bayar = $("#jenis_bayar").val();
    var id_rekening = $("#id_rekening").val();
    var tgl_bayar = $("#tgl_bayar").val();

    if (jenis_bayar == '2' && id_rekening == '0') {
      alert("Rekening harus dipilih..!");
      return false;
    }

    if (tgl_bayar == '') {
      alert("Tanggal Pembayaran harus diisi..!");
      return false;
    }
  }
</script>

<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Pembayaran Biaya UPP
        </div>
        <div class="card-body">
          <?php
          $attributes = array('id' => 'FrmBiayaUPP', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <input type="hidden" name="is_simpan" value="1">

          Berikut ini list biaya UPP calon mahasiswa baru:<br>
          <?php
          if (is_array($data_biaya)) {
            for ($i = 0; $i < count($data_biaya); $i++) { ?>
              <?= $data_biaya[$i]['nama'] . " Kategori Kelas " . $data_biaya[$i]['nama_kategori_kelas'] ?> Rp. <?= number_format($data_biaya[$i]['nominal']) ?><br>
          <?php }
          } ?>
          <br><b>Total Biaya: Rp. <?= number_format($jumlahnya) ?></b>

          <br><br>Silahkan melakukan pembayaran via tunai dengan datang langsung ke kampus, atau via transfer ke salah satu rekening berikut:<br>
          <?php
          if (is_array($data_rek)) {
            for ($i = 0; $i < count($data_rek); $i++) { ?>
              <?= $data_rek[$i]['nama_bank'] ?> No. rek <?= $data_rek[$i]['no_rekening'] ?> a.n. <?= $data_rek[$i]['atas_nama'] ?><br>
          <?php }
          } ?><br>
          Jika sudah membayar, silahkan input data konfirmasi pembayaran melalui form dibawah ini:<br>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">No Pendaftaran</label>
            <div class="col-md-4">
              <?= $no_pendaftaran ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Nama Calon Mahasiswa</label>
            <div class="col-md-4">
              <?= $nama_calon_mhs ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Jenis Pembayaran</label>
            <div class="col-md-4">
              <select class="form-control" name="jenis_bayar" id="jenis_bayar">
                <!-- <option value="0" selected disabled>Pilih Jenis Bayar</option> -->
                <option value="1">Tunai</option>
                <option value="2">Transfer</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Ditransfer Ke Rekening (abaikan jika via tunai)</label>
            <div class="col-md-4">
              <select class="form-control" name="id_rekening" id="id_rekening">
                <option value="0" selected>Pilih Rekening</option>
                <?php for ($i = 0; $i < count($data_rek); $i++) { ?>
                  <option value="<?= $data_rek[$i]['id_rekening'] ?>"><?= $data_rek[$i]['nama_bank'] . " (No. rek " . $data_rek[$i]['no_rekening'] . " a.n. " . $data_rek[$i]['atas_nama'] . ")" ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-2 col-form-label">Nominal Pembayaran</label>
            <div class="col-md-4">
              <input class="form-control" type="text" name="jumbayar" readonly value="<?= $jumlahnya ?>" style="text-align:right;">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tanggal Pembayaran</label>
            <div class="col-md-4">
              <input class="form-control" type="date" name="tgl_bayar" id="tgl_bayar" value="">
            </div>
          </div>

          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" onclick="return cek_input();"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>