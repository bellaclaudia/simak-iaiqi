<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Calon Mahasiswa</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Calon Mahasiswa
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarianCalonMhs', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Gelombang Pendaftaran</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_gelombang_calon_mhs">
                  <option value="0">- All -</option>
                  <?php foreach ($gelombang as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_gelombang) { ?>selected<?php } ?>><?= $k['nama_tahun_akademik'] . " (Tahun " . $k['tahun'] . " " . $k['nama'] . ")"; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ta') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Program Studi Pilihan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_prodi">
                  <option value="0">- All -</option>
                  <?php foreach ($data_prodi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_prodi') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel_pddikti" value="1">Export Excel PDDIKTI</button>&nbsp;
            </form>
            <hr>

            <a href="<?= base_url('calon_mhs/tambah'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>No Pendaftaran</th>
                  <th>Tgl Daftar</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Jenis Kelamin</th>
                  <th>Tempat/Tgl Lahir</th>
                  <!-- <th>Agama</th> -->
                  <!-- <th>Gol. Darah</th> -->
                  <th>Program Studi Pilihan</th>
                  <th>Kategori Kelas</th>
                  <th>No. Telepon</th>
                  <th>Email</th>
                  <th>Foto</th>
                  <th>Status Pembayaran</th>
                  <th>Status Seleksi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_calon_mhs as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->no_pendaftaran ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->alamat ?>, <?= $row->kelurahan ?>, <?= $row->kecamatan ?>, <?= $row->kab_kota ?>, <?= $row->provinsi ?>, <?= $row->kode_pos ?></td>
                    <td><?php if ($row->jenis_kelamin == 'L') echo "Laki-Laki";
                        else echo "Perempuan"; ?></td>
                    <td>
                      <?php
                      if ($row->tgl_lahir != '') {
                        $exptgl = explode('-', $row->tgl_lahir);
                        $thn = $exptgl[0];
                        $tgl = $exptgl[2];
                        $bln = $exptgl[1];
                        $row->tgl_lahir = $tgl . "-" . $bln . "-" . $thn;
                      }
                      ?>
                      <?= $row->tempat_lahir ?>, <?= $row->tgl_lahir ?></td>
                    <!-- <td><?= $row->agama ?></td> -->
                    <!-- <td><?= $row->goldar ?></td> -->
                    <td><?= $row->prodi . " (" . $row->nama_jenjang . ")" ?></td>
                    <td><?php if ($row->nama_kategori_kelas == '') echo "None";
                        else echo $row->nama_kategori_kelas ?></td>
                    <td><?= $row->no_telp ?></td>
                    <td><?= $row->email ?></td>
                    <td><?php if ($row->file_foto != '') { ?>
                        <img src="<?= base_url('uploads/' . $row->file_foto) ?>" alt="<?= $row->file_foto ?>" width="200" class="img-thumbnail rounded-circle">
                      <?php } ?>
                    </td>
                    <td><?php if ($row->status_pembayaran_pendaftaran == '0') {
                          echo "Belum Bayar Biaya Pendaftaran";
                        } else {
                          echo "Sudah Bayar Biaya Pendaftaran<br>";
                          $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
                                    FROM pembayaran_biaya_calon_mhs a 
                                    LEFT JOIN master_rekening b ON b.id = a.id_rekening
                                    WHERE a.id_calon_mhs = '$row->id' AND a.jenis_biaya = '1' ";
                          $queryxx = $this->db->query($sqlxx);
                          if ($queryxx->num_rows() > 0) {
                            $hasilxx = $queryxx->row();
                            if ($hasilxx->jenis_bayar == '1') {
                              echo "(Via Tunai,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ")<br><br>";
                            } else {
                              echo "(Via Transfer,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ",<br>Ditransfer Ke: " . $hasilxx->nama_bank . " (Rekening: " . $hasilxx->no_rekening . " a.n. " . $hasilxx->atas_nama . ")<br><br>";
                            }
                          }
                          //Tanggal Bayar: " . $row->tgl_bayar . "<br>Nominal: Rp. " . number_format($row->nominal) . "<br>Ditransfer Ke: " . $row->nama_bank . " (Rekening: " . $row->no_rekening . " a.n. " . $row->atas_nama;
                        } ?>

                      <?php if ($row->status_seleksi == '1' && $row->status_pembayaran_upp == '0') {
                        echo "Belum Bayar Biaya UPP";
                      } else if ($row->status_seleksi == '1' && $row->status_pembayaran_upp == '1') {
                        echo "Sudah Bayar Biaya UPP<br>";
                        $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
                        FROM pembayaran_biaya_calon_mhs a 
                        LEFT JOIN master_rekening b ON b.id = a.id_rekening
                        WHERE a.id_calon_mhs = '$row->id' AND a.jenis_biaya = '2' ";
                        $queryxx = $this->db->query($sqlxx);
                        if ($queryxx->num_rows() > 0) {
                          $hasilxx = $queryxx->row();
                          if ($hasilxx->jenis_bayar == '1') {
                            echo "(Via Tunai,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ")";
                          } else {
                            echo "(Via Transfer,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ",<br>Ditransfer Ke: " . $hasilxx->nama_bank . " (Rekening: " . $hasilxx->no_rekening . " a.n. " . $hasilxx->atas_nama . ")";
                          }
                        }
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      // cek apakah sudah terdaftar di tabel mahasiswa. jika sudah, maka tidak bisa set status seleksi, edit, ataupun hapus
                      $sqlxx = " SELECT id FROM mahasiswa WHERE id_calon_mhs = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_edit = 0;
                      } else
                        $boleh_edit = 1;

                      if ($row->status_seleksi == '0') echo "Waiting";
                      else if ($row->status_seleksi == '1') {
                        if ($boleh_edit == '0')
                          echo "Diterima (sudah registrasi ke data mahasiswa)";
                        else
                          echo "Diterima";
                      } else echo "Ditolak"; ?></td>
                    <td>


                      <?php if ($boleh_edit == '1') { ?>
                        <a href="<?= base_url('calon_mhs/set_status_seleksi/' . $row->id); ?>" class="btn btn-success btn-circle">Set Status Seleksi</a><br>
                        <a href="<?= base_url('calon_mhs/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></a><br>
                        <a href="<?php echo site_url('calon_mhs/hapus/' . $row->id . '/' . $row->no_pendaftaran); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Calon Mhs <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a><br>
                      <?php } ?>
                      <a href="<?= base_url('calon_mhs/view/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a><br>
                      <?php if ($row->status_seleksi == '1' && $boleh_edit == '1') { ?>
                        <a href="<?= base_url('calon_mhs/registrasi_mahasiswa/' . $row->id . '/' . $row->no_pendaftaran); ?>" class="btn btn-success btn-circle" onclick="return confirm('Apakah Anda yakin akan melakukan registrasi mahasiswa baru untuk calon mahasiswa <?= $row->nama; ?> ? Proses akan dilakukan secara otomatis');">Registrasi Menjadi Mahasiswa</a><br>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>