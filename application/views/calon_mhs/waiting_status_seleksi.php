<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Pembayaran Biaya UPP
        </div>
        <div class="card-body">

          <?php
          if ($status_seleksi == '0')
            $deskripsi = 'Anda belum dapat melakukan pembayaran biaya UPP karena status seleksi anda Waiting. Silahkan menunggu informasi selanjutnya dari panitia, terimakasih';
          else if ($status_seleksi == '2')
            $deskripsi = 'Mohon maaf, anda tidak dapat melanjutkan ke proses pembayaran biaya UPP karena status seleksi anda ditolak.';

          echo $deskripsi;
          ?>

        </div>
      </div>
    </div>
  </div>
</main>
</div>