<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <?php if ($jenis_biaya == '1')
            $nama_biaya = 'Biaya Pendaftaran';
          else
            $nama_biaya = 'Biaya UPP'; ?>
          <i class="fa fa-plus"></i> Pembayaran <?= $nama_biaya ?>
        </div>
        <div class="card-body">


          <?php
          if ($jenis_bayar == '1')
            $nama_jenis_bayar = 'Tunai';
          else
            $nama_jenis_bayar = 'Transfer';
          ?>
          Anda sudah melakukan pembayaran <?= $nama_biaya ?> dengan data sebagai berikut:<br>
          Tanggal Bayar : <?= $tgl_bayar ?><br>
          Nominal : Rp. <?= number_format($nominal) ?><br>
          Metode Pembayaran : <?= $nama_jenis_bayar ?><br>

          <?php if ($jenis_bayar == '2') { ?>
            Ditransfer Ke : <?= $nama_bank ?> (No. Rek <?= $no_rekening ?> a.n. <?= $atas_nama ?>)<br>
          <?php } ?>

          <br>
          <?php //if ($jenis_biaya == '1')
          //echo "Silahkan menunggu hasil status seleksi calon mahasiswa baru dari Panitia. Terimakasih";
          ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>