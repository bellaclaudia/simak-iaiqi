<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=calon_mahasiswa.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>SIMAK IAIQI | Calon Mahasiswa</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Calon Mahasiswa pada IAIQI Indralaya</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
      	<th class="allcen center bold">No Pendaftaran</th>
      	<th class="allcen center bold">Tgl Daftar</th>
      	<th class="allcen center bold">Nama</th>
      	<th class="allcen center bold">Alamat</th>
      	<th class="allcen center bold">Jenis Kelamin</th>
        <th class="allcen center bold">NIK</th>
        <th class="allcen center bold">NISN</th>
      	<th class="allcen center bold">Tempat/Tgl Lahir</th>
        <th class="allcen center bold">Agama</th>
        <th class="allcen center bold">Golongan Darah</th>
      	<th class="allcen center bold">Program Studi Pilihan</th>
      	<th class="allcen center bold">No. Telepon</th>
      	<th class="allcen center bold">Email</th>
        <th class="allcen center bold">Gelombang</th>
        <th class="allcen center bold">Nama Ayah</th>
        <th class="allcen center bold">Nama Ibu</th>
        <th class="allcen center bold">Ukuran Jas</th>
        <th class="allcen center bold">Total Biaya</th>
      	<th class="allcen center bold">Status Pembayaran</th>
      	<th class="allcen center bold">Status Seleksi</th>
        <th class="allcen center bold">Registered By</th>
      	<th class="allcen center bold">Tgl. Input</th>
      	<th class="allcen center bold">Tgl. Update</th>
      	<th class="allcen center bold">User Update By</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_calon_mhs as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
      	<td><?= $row->no_pendaftaran ?></td>
        <td><?= $row->tgl_input ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->alamat ?>, <?= $row->kelurahan ?>, <?= $row->kecamatan ?>, <?= $row->kab_kota ?>, <?= $row->provinsi ?>, <?= $row->kode_pos ?></td>
        <td><?php if ($row->jenis_kelamin == 'L') echo "Laki-Laki";
            else echo "Perempuan"; ?></td>
        <td>'<?= $row->nik ?></td>
        <td>'<?= $row->nisn ?></td>
        <td>
          <?php
          if ($row->tgl_lahir != '') {
            $exptgl = explode('-', $row->tgl_lahir);
            $thn = $exptgl[0];
            $tgl = $exptgl[2];
            $bln = $exptgl[1];
            $row->tgl_lahir = $tgl . "-" . $bln . "-" . $thn;
          }
          ?>
          <?= $row->tempat_lahir ?>, <?= $row->tgl_lahir ?></td>
        <td><?= $row->agama ?></td>
        <td><?= $row->goldar ?></td>
        <td><?= $row->prodi . " (" . $row->nama_jenjang . ")" ?></td>
        <td><?= $row->no_telp ?></td>
        <td><?= $row->email ?></td>
        <td><?= $row->gelombang ?></td>
        <td><?= $row->nama_ayah ?></td>
        <td><?= $row->nama_ibu ?></td>
        <td><?= $row->ukuran_jas ?></td>
        <td align="right"><?= number_format($row->total_biaya) ?></td>
        <td><?php if ($row->status_pembayaran_pendaftaran == '0') {
              echo "Belum Bayar Biaya Pendaftaran";
            } else {
              echo "Sudah Bayar Biaya Pendaftaran<br>";
              $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
                        FROM pembayaran_biaya_calon_mhs a 
                        LEFT JOIN master_rekening b ON b.id = a.id_rekening
                        WHERE a.id_calon_mhs = '$row->id' AND a.jenis_biaya = '1' ";
              $queryxx = $this->db->query($sqlxx);
              if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                if ($hasilxx->jenis_bayar == '1') {
                  echo "(Via Tunai,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ")<br><br>";
                } else {
                  echo "(Via Transfer,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ",<br>Ditransfer Ke: " . $hasilxx->nama_bank . " (Rekening: " . $hasilxx->no_rekening . " a.n. " . $hasilxx->atas_nama . ")<br><br>";
                }
              }
            } ?>

          <?php if ($row->status_seleksi == '1' && $row->status_pembayaran_upp == '0') {
            echo "Belum Bayar Biaya UPP";
          } else if ($row->status_seleksi == '1' && $row->status_pembayaran_upp == '1') {
            echo "Sudah Bayar Biaya UPP<br>";
            $sqlxx = " select a.id, a.nominal, a.jenis_bayar, a.id_rekening, a.tgl_bayar, b.nama_bank, b.no_rekening, b.atas_nama 
            FROM pembayaran_biaya_calon_mhs a 
            LEFT JOIN master_rekening b ON b.id = a.id_rekening
            WHERE a.id_calon_mhs = '$row->id' AND a.jenis_biaya = '2' ";
            $queryxx = $this->db->query($sqlxx);
            if ($queryxx->num_rows() > 0) {
              $hasilxx = $queryxx->row();
              if ($hasilxx->jenis_bayar == '1') {
                echo "(Via Tunai,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ")";
              } else {
                echo "(Via Transfer,<br>Tanggal Bayar: " . $hasilxx->tgl_bayar . ",<br>Nominal: Rp. " . number_format($hasilxx->nominal) . ",<br>Ditransfer Ke: " . $row->nama_bank . " (Rekening: " . $row->no_rekening . " a.n. " . $row->atas_nama . ")";
              }
            }
          }
          ?>
        </td>
        <td>
          <?php
          // cek apakah sudah terdaftar di tabel mahasiswa. jika sudah, maka tidak bisa set status seleksi, edit, ataupun hapus
          $sqlxx = " SELECT id FROM mahasiswa WHERE id_calon_mhs = '$row->id' ";
          $queryxx = $this->db->query($sqlxx);
          if ($queryxx->num_rows() > 0) {
            $boleh_edit = 0;
          } else
            $boleh_edit = 1;

          if ($row->status_seleksi == '0') echo "Waiting";
          else if ($row->status_seleksi == '1') {
            if ($boleh_edit == '0')
              echo "Diterima (sudah registrasi ke data mahasiswa)";
            else
              echo "Diterima";
          } else echo "Ditolak"; ?></td>
        <td><?= $row->registered_by ?></td>
        <td><?= $row->tgl_input ?></td>
        <td><?= $row->tgl_update ?></td>
        <td><?= $row->user_update_by ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>