<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('mahasiswa'); ?>">Data Calon Mahasiswa</a></li>
    <li class="breadcrumb-item active">View Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-eye"></i> View Data
        </div>
        <div class="card-body">
          <?php if ($biodata->file_foto != NULL) : ?>
            <center><a href="<?= base_url('uploads/' . $biodata->file_foto); ?>" target="_blank"><img src="<?= base_url('uploads/' . $biodata->file_foto); ?>" alt="<?= $biodata->file_foto ?>" width="150" class="img-thumbnail rounded-circle"></a><br>
              <a href="<?= base_url('uploads/' . $biodata->file_foto); ?>" download>Download</a>
            </center>
          <?php endif ?>
          <?php if ($biodata->file_foto == NULL and $biodata->jenis_kelamin == 'P') : ?>
            <a href="<?= base_url('uploads/icon/santriwati.png'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/santriwati.png'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?>
          <?php if ($biodata->file_foto == NULL and $biodata->jenis_kelamin == 'L') : ?>
            <a href="<?= base_url('uploads/icon/santri.jpg'); ?>" target="_blank"><img src="<?= base_url('uploads/icon/santri.jpg'); ?>" width="150" class="img-thumbnail rounded-circle"></a>
          <?php endif ?><br><br>
          <table class="table table-responsive-sm table-striped">
            <input type="hidden" name="id" value="<?= $biodata->id; ?>">
            <tr>
              <td>Nama Lengkap : <?= $biodata->nama; ?></td>
              <td>NISN : <?= $biodata->nisn; ?></td>
              <td>NIK : <?= $biodata->nik; ?></td>
            </tr>
            <tr>
              <td>Alamat : <?= $biodata->alamat; ?>, <?= $biodata->kelurahan; ?>, <?= $biodata->kecamatan; ?>, <?= $biodata->kab_kota; ?>, <?= $biodata->provinsi; ?>, <?= $biodata->kode_pos; ?></td>
              <td>Dokumen Persyaratan : <a href="<?= base_url('uploads/' . $biodata->file_dokumen); ?>" target="_blank">Lihat</a> | <a href="<?= base_url('uploads/' . $biodata->file_dokumen); ?>" download>Download</a></td>
              <td>Ukuran Jas : <?= $biodata->ukuran_jas; ?></td>
            </tr>
            <tr>
              <td>Gelombang Pendaftaran : <?= $biodata->gelombang; ?></td>
              <td>Program Studi Pilihan : <?= $biodata->prodi; ?></td>
              <td>No. Telepon : <?= $biodata->no_telp; ?></td>
            </tr>
            <tr>
              <td>Tempat Lahir : <?= $biodata->tempat_lahir; ?></td>
              <td>Tanggal Lahir : <?= $biodata->tgl_lahir; ?></td>
              <td>Jenis Kelamin : <?php if ($biodata->jenis_kelamin == 'L') echo "Laki-Laki";
                                  else echo "Perempuan"; ?></td>
            </tr>
            <tr>
              <td>Agama : <?= $biodata->agama; ?></td>
              <td>Golongan Darah : <?= $biodata->goldar; ?></td>
              <td>Riwayat Penyakit : <?php $biodata->riwayat_penyakit; ?></td>
            </tr>
            <tr>
              <td>Email : <?= $biodata->email; ?></td>
              <td>Nama Ayah : <?= $biodata->nama_ayah; ?></td>
              <td>Nama Ibu : <?= $biodata->nama_ibu; ?></td>
            </tr>
          </table>
          <div class="modal-footer">
            <a href="<?= base_url('calon_mhs'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Kembali</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>