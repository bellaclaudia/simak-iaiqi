<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <?php if ($this->session->userdata['id_grup_user'] != 3 && $this->session->userdata['id_grup_user'] != 4 && $this->session->userdata['id_grup_user'] != 5) { ?>
      <li class="breadcrumb-item">
        <a href="<?= base_url('dashboard'); ?>">Admin</a>
      </li>
    <?php } ?>
    <li class="breadcrumb-item active">Dashboard</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-lg-12">
          <div class="card-body">
            <div class="alert alert-success" role="alert"><i class="nav-icon icon-speedometer"></i> Dashboard</div>
            <?php //if ($this->session->userdata['id_grup_user'] == '1') {
            $this->db->where("id", $this->session->userdata['id_grup_user']);
            $cek = $this->db->get('master_grup_user');

            if ($cek->num_rows() > 0) {
              foreach ($cek->result() as $ck) {
                $nama_grup = $ck->nama_grup;
              }
            }

            ?>

            <div class="alert alert-success" role="alert">Selamat Datang di Sistem Informasi Akademik (SIMAK) IAIQI, Anda login sebagai <?= $nama_grup ?></div>

            <?php
            if ($this->session->userdata['id_grup_user'] == 3) {
              // cek status seleksi
              $sqlxx = " select a.status_seleksi, a.status_pembayaran_pendaftaran, a.status_pembayaran_upp, b.nim FROM calon_mahasiswa a
                LEFT JOIN mahasiswa b ON a.id = b.id_calon_mhs  
                  WHERE a.no_pendaftaran = '" . $this->session->userdata['username'] . "'";
              $queryxx = $this->db->query($sqlxx);
              if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                $status_seleksi    = $hasilxx->status_seleksi;
                $status_pembayaran_pendaftaran    = $hasilxx->status_pembayaran_pendaftaran;
                $status_pembayaran_upp    = $hasilxx->status_pembayaran_upp;
                $nim    = $hasilxx->nim;
              }

              if ($status_seleksi == '0')
                $nama_status_seleksi = 'Waiting (Menunggu Verifikasi Panitia Penerimaan Calon Mahasiswa Baru)';
              else if ($status_seleksi == '1')
                $nama_status_seleksi = 'Diterima';
              else
                $nama_status_seleksi = 'Ditolak';

              if ($status_pembayaran_pendaftaran == '0')
                $nama_status_pembayaran_pendaftaran = 'Belum Bayar';
              else
                $nama_status_pembayaran_pendaftaran = 'Sudah Bayar';

              if ($status_pembayaran_upp == '0')
                $nama_status_pembayaran_upp = 'Belum Bayar';
              else
                $nama_status_pembayaran_upp = 'Sudah Bayar';

            ?>
              <div class="alert alert-success" role="alert">Status seleksi penerimaan calon mahasiswa baru: <b><?= $nama_status_seleksi ?></b>
                <br>
                Status pembayaran biaya pendaftaran: <b><?= $nama_status_pembayaran_pendaftaran ?></b>
                <br>
                Status pembayaran biaya UPP: <b><?= $nama_status_pembayaran_upp ?></b><br><br>

                <?php if ($status_pembayaran_pendaftaran == '0' || $status_pembayaran_upp == '0') { ?>
                  Pembayaran dapat dilakukan secara transfer ke rekening kampus ataupun bisa tunai langsung dengan datang ke kampus. <br>Harap melakukan konfirmasi pembayaran di menu Pembayaran Biaya jika sudah membayar, terimakasih.
                <?php } ?>

                <?php if ($nim != '') { ?>
                  Anda sudah resmi diterima menjadi mahasiswa. Silahkan login menggunakan NIM anda berikut ini:<br>
                  Username: <b><?= $nim ?></b><br>
                  Password: <b><?= $nim ?></b>
                <?php } ?>
              </div>
            <?php } ?>
          </div>
        </div>

        <?php if ($this->session->userdata['id_grup_user'] != 3) { ?>
          <div class="col-sm-12 col-md-12">
            <div class="card border-secondary">
              <?php foreach ($data_pengumuman_akademik as $row) : ?>
                <div class="card-header"><b><?= $row->judul ?></b> <span class="badge badge-danger float-right">Posting by <?= $row->user_update_by ?></span></div>
                <div class="card-body"><?= $row->deskripsi ?><br><br><span class="badge badge-danger float-right">Tanggal Update :
                    <?php if ($row->tgl_update != NULL && $row->tgl_update != '0000-00-00 00:00:00') {
                      echo $row->tgl_update;
                    } else {
                      echo $row->tgl_input;
                    } ?></span>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="col-sm-12 col-md-12">
            <div class="card border-secondary">
              <div class="card-header">Kalender Akademik</div>
              <div class="card-body">
                <table class="table table-striped table-bordered datatable">
                  <thead>
                    <tr>
                      <th>Tgl. Awal</th>
                      <th>Tgl. Akhir</th>
                      <th>Kegiatan</th>
                      <th>Deskripsi</th>
                      <th>Tgl. Input</th>
                      <th>Tgl. Update</th>
                      <th>User Update By</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data_kalender_akademik as $row) : ?>
                      <tr>
                        <td><?= $row->tgl_awal ?></td>
                        <td><?= $row->tgl_akhir ?></td>
                        <td><?= $row->kegiatan ?></td>
                        <td><?= $row->deskripsi ?></td>
                        <td><?= $row->tgl_input ?></td>
                        <td><?= $row->tgl_update ?></td>
                        <td><?= $row->user_update_by ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</main>
</div>