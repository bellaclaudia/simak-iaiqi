<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('master_tahun_akademik'); ?>">Data Master Tahun Akademik</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddTahunAkademik', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Periode Tahun Akademik</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="nama" placeholder="Isi Periode Tahun Akademik" value="<?= set_value('nama'); ?>">
              <small class="text-danger">
                <?php echo form_error('nama') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tahun</label>
            <div class="col-md-3">
              <input class="form-control" type="text" name="tahun" maxlength="4" placeholder="Isi Tahun" value="<?= set_value('tahun'); ?>">
              <small class="text-danger">
                <?php echo form_error('tahun') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">&nbsp;</label>
            <div class="col-md-3">
              &nbsp;
            </div>
            <label class="col-md-3 col-form-label">Jenis Semester</label>
            <div class="col-md-3">
              <select class="form-control" name="jenis_semester" id="select2-2">
                <option value="0" selected disabled>Pilih Jenis Semester</option>
                <option value="1">Ganjil</option>
                <option value="2">Genap</option>
              </select>
              <small class="text-danger">
                <?php echo form_error('jenis_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai Semester</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_semester" value="<?= set_value('tgl_mulai_semester'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_semester') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai Semester</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_semester" value="<?= set_value('tgl_selesai_semester'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_semester') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai KRS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_krs" value="<?= set_value('tgl_mulai_krs'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_krs') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai KRS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_krs" value="<?= set_value('tgl_selesai_krs'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_krs') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai Kuliah</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_kuliah" value="<?= set_value('tgl_mulai_kuliah'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_kuliah') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai Kuliah</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_kuliah" value="<?= set_value('tgl_selesai_kuliah'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_kuliah') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai UTS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_uts" value="<?= set_value('tgl_mulai_uts'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_uts') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai UTS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_uts" value="<?= set_value('tgl_selesai_uts'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_uts') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai UAS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_uas" value="<?= set_value('tgl_mulai_uas'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_uas') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai UAS</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_uas" value="<?= set_value('tgl_selesai_uas'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_uas') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Tanggal Mulai Isi Nilai</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_mulai_isi_nilai" value="<?= set_value('tgl_mulai_isi_nilai'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_mulai_isi_nilai') ?>
              </small>
            </div>
            <label class="col-md-3 col-form-label">Tanggal Selesai Isi Nilai</label>
            <div class="col-md-3">
              <input class="form-control" type="date" name="tgl_selesai_isi_nilai" value="<?= set_value('tgl_selesai_isi_nilai'); ?>">
              <small class="text-danger">
                <?php echo form_error('tgl_selesai_isi_nilai') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('master_tahun_akademik'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>