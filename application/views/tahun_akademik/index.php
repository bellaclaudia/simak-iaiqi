<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Master Tahun Akademik</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Master Tahun Akademik
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <a href="<?= base_url('master_tahun_akademik/tambah/'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <!-- <th>ID Tahun Akademik</th> -->
                  <th>Periode Tahun Akademik</th>
                  <th>Tahun</th>
                  <th>Jenis Semester</th>
                  <th>Tanggal Mulai Semester</th>
                  <th>Tanggal Selesai Semester</th>
                  <th>Tanggal Mulai KRS</th>
                  <th>Tanggal Selesai KRS</th>
                  <th>Tanggal Mulai Kuliah</th>
                  <th>Tanggal Selesai Kuliah</th>
                  <th>Tanggal Mulai UTS</th>
                  <th>Tanggal Selesai UTS</th>
                  <th>Tanggal Mulai UAS</th>
                  <th>Tanggal Selesai UAS</th>
                  <th>Tanggal Mulai Isi Nilai</th>
                  <th>Tanggal Selesai Isi Nilai</th>
                  <th>Tgl. Input</th>
                  <th>Tgl. Update</th>
                  <th>User Update By</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_tahun_akademik as $row) :
                  $tgl_mulai_semester = explode("-", $row->tgl_mulai_semester);
                  $row->tgl_mulai_semester = $tgl_mulai_semester[2] . "-" . $tgl_mulai_semester[1] . "-" . $tgl_mulai_semester[0];
                  $tgl_selesai_semester = explode("-", $row->tgl_selesai_semester);
                  $row->tgl_selesai_semester = $tgl_selesai_semester[2] . "-" . $tgl_selesai_semester[1] . "-" . $tgl_selesai_semester[0];

                  $tgl_mulai_krs = explode("-", $row->tgl_mulai_krs);
                  $row->tgl_mulai_krs = $tgl_mulai_krs[2] . "-" . $tgl_mulai_krs[1] . "-" . $tgl_mulai_krs[0];
                  $tgl_selesai_krs = explode("-", $row->tgl_selesai_krs);
                  $row->tgl_selesai_krs = $tgl_selesai_krs[2] . "-" . $tgl_selesai_krs[1] . "-" . $tgl_selesai_krs[0];
                  $tgl_mulai_kuliah = explode("-", $row->tgl_mulai_kuliah);
                  $row->tgl_mulai_kuliah = $tgl_mulai_kuliah[2] . "-" . $tgl_mulai_kuliah[1] . "-" . $tgl_mulai_kuliah[0];
                  $tgl_selesai_kuliah = explode("-", $row->tgl_selesai_kuliah);
                  $row->tgl_selesai_kuliah = $tgl_selesai_kuliah[2] . "-" . $tgl_selesai_kuliah[1] . "-" . $tgl_selesai_kuliah[0];
                  $tgl_mulai_uts = explode("-", $row->tgl_mulai_uts);
                  $row->tgl_mulai_uts = $tgl_mulai_uts[2] . "-" . $tgl_mulai_uts[1] . "-" . $tgl_mulai_uts[0];
                  $tgl_selesai_uts = explode("-", $row->tgl_selesai_uts);
                  $row->tgl_selesai_uts = $tgl_selesai_uts[2] . "-" . $tgl_selesai_uts[1] . "-" . $tgl_selesai_uts[0];

                  $tgl_mulai_uas = explode("-", $row->tgl_mulai_uas);
                  $row->tgl_mulai_uas = $tgl_mulai_uas[2] . "-" . $tgl_mulai_uas[1] . "-" . $tgl_mulai_uas[0];
                  $tgl_selesai_uas = explode("-", $row->tgl_selesai_uas);
                  $row->tgl_selesai_uas = $tgl_selesai_uas[2] . "-" . $tgl_selesai_uas[1] . "-" . $tgl_selesai_uas[0];
                  $tgl_mulai_isi_nilai = explode("-", $row->tgl_mulai_isi_nilai);
                  $row->tgl_mulai_isi_nilai = $tgl_mulai_isi_nilai[2] . "-" . $tgl_mulai_isi_nilai[1] . "-" . $tgl_mulai_isi_nilai[0];
                  $tgl_selesai_isi_nilai = explode("-", $row->tgl_selesai_isi_nilai);
                  $row->tgl_selesai_isi_nilai = $tgl_selesai_isi_nilai[2] . "-" . $tgl_selesai_isi_nilai[1] . "-" . $tgl_selesai_isi_nilai[0];

                  if ($row->jenis_semester == '1')
                    $nama_jenis_semester = 'Ganjil';
                  else
                    $nama_jenis_semester = 'Genap';
                ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <!-- <td><?php //echo $row->id 
                              ?></td> -->
                    <td><?= $row->nama ?></td>
                    <td><?= $row->tahun ?></td>
                    <td><?= $nama_jenis_semester ?></td>
                    <td><?= $row->tgl_mulai_semester ?></td>
                    <td><?= $row->tgl_selesai_semester ?></td>
                    <td><?= $row->tgl_mulai_krs ?></td>
                    <td><?= $row->tgl_selesai_krs ?></td>
                    <td><?= $row->tgl_mulai_kuliah ?></td>
                    <td><?= $row->tgl_selesai_kuliah ?></td>
                    <td><?= $row->tgl_mulai_uts ?></td>
                    <td><?= $row->tgl_selesai_uts ?></td>
                    <td><?= $row->tgl_mulai_uas ?></td>
                    <td><?= $row->tgl_selesai_uas ?></td>
                    <td><?= $row->tgl_mulai_isi_nilai ?></td>
                    <td><?= $row->tgl_selesai_isi_nilai ?></td>
                    <td><?= $row->tgl_input ?></td>
                    <td><?= $row->tgl_update ?></td>
                    <td><?= $row->user_update_by ?></td>
                    <td style="text-align: center;">
                      <?php
                      $boleh_delete = 1;

                      // cek krs
                      $sqlxx = " SELECT id FROM krs_mhs WHERE id_tahun_akademik = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }

                      // cek khs
                      $sqlxx = " SELECT id FROM khs_mhs WHERE id_tahun_akademik = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }

                      // cek master_biaya_calon_mhs
                      $sqlxx = " SELECT id FROM master_biaya_calon_mhs WHERE id_tahun_akademik = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }

                      // cek biaya_kuliah_persemester
                      $sqlxx = " SELECT id FROM biaya_kuliah_persemester WHERE id_tahun_akademik = '$row->id' ";
                      $queryxx = $this->db->query($sqlxx);
                      if ($queryxx->num_rows() > 0) {
                        $boleh_delete = 0;
                      }

                      ?>
                      <a href="<?= base_url('master_tahun_akademik/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></button></a>
                      <?php if ($boleh_delete == 1) { ?>
                        <br><br>
                        <a href="<?php echo site_url('master_tahun_akademik/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Tahun Akademik <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>