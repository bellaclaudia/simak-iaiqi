<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Biaya Kuliah Reguler</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Biaya Kuliah Reguler
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?><br>

          <b>Filter Pencarian</b>
          <?php
          $attributes = array('id' => 'FrmPencarianBiaya', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tahun Akademik</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_ta2">
                <option value="0">- All -</option>
                <?php foreach ($tahun_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Semester</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_semester2">
                <option value="0">- All -</option>
                <?php foreach ($semester as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Program Studi</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_prodi2">
                <option value="0">- All -</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
          <!-- <a href="<?= base_url('krs'); ?>"><button class="btn btn-sm btn-danger btn-ladda" data-style="expand-right" type="reset">Ulangi</button></a>&nbsp; -->
          </form>
          <hr>

          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>

          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Semester</th>
                <th>Tahun Akademik</th>
                <th>Program Studi</th>
                <th>Biaya Kuliah</th>
                <th>Jumlah (Rp.)</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_biaya_kuliah_reguler as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->semester ?></td>
                  <td><?= $row->tahun_akademik ?></td>
                  <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                  <td><?= $row->biaya_kuliah ?></td>
                  <td align="right"><?= number_format($row->jumlah) ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('biaya_kuliah_reguler/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Biaya Kuliah Reguler <?= $row->biaya_kuliah; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddReguler', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Semester</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_semester" required>
                        <option value="0" selected disabled>Pilih Data Semester</option>
                        <?php foreach ($semester as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_semester') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="id_tahun_akademik" name="id_tahun_akademik" onchange="get_biaya($('#id_program_studi').val(), this.value);">
                        <option value="0" selected disabled>Pilih Data Tahun Akademik</option>
                        <?php foreach ($tahun_akademik as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_tahun_akademik') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Program Studi</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="id_program_studi" name="id_program_studi" onchange="get_biaya(this.value, $('#id_tahun_akademik').val());">
                        <option value="" selected disabled>Pilih Program Studi</option>
                        <?php foreach ($data_prodi as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Biaya Kuliah</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-3" name="id_biaya_kuliah" required>
                        <option value="0" selected disabled>Pilih Data Biaya Kuliah</option>
                        <?php foreach ($biaya_kuliah as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_biaya_kuliah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jumlah</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="jumlah" placeholder="Isi Jumlah" required>
                      <small class="text-danger">
                        <?php echo form_error('jumlah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_biaya_kuliah_reguler as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('biaya_kuliah_reguler/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Semester</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control select2-single" id="select2-1" name="id_semester" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_semester)) {
                              $lv = $row->id_semester;
                            }
                            foreach ($semester as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_semester') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Tahun Akademik</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-2" name="id_tahun_akademik" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_tahun_akademik)) {
                              $lv = $row->id_tahun_akademik;
                            }
                            foreach ($tahun_akademik as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_tahun_akademik') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Program Studi</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-1" name="id_program_studi">
                            <option value="" selected disabled>Pilih Program Studi</option>
                            <?php foreach ($data_prodi as $k) : ?>
                              <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $row->id_program_studi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Biaya Kuliah</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-3" name="id_biaya_kuliah" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_biaya_kuliah)) {
                              $lv = $row->id_biaya_kuliah;
                            }
                            foreach ($biaya_kuliah as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_biaya_kuliah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jumlah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="jumlah" placeholder="Isi Jumlah" value="<?= $row->jumlah; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('jumlah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>