<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('pengumuman_akademik'); ?>">Data Pengumuman Akademik</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <?php
          //create form
          $attributes = array('id' => 'FrmAddMataKuliah', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Judul</label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data_pengumuman_akademik->id; ?>">
              <input class="form-control" type="text" name="judul" value="<?= $data_pengumuman_akademik->judul; ?>">
              <small class="text-danger">
                <?php echo form_error('judul') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Deskripsi</label>
            <div class="col-md-9">
              <textarea class="form-control" name="deskripsi" style="width: 100%; height: 500px"><?= $data_pengumuman_akademik->deskripsi; ?></textarea>
              <small class="text-danger">
                <?php echo form_error('deskripsi') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('pengumuman_akademik'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>