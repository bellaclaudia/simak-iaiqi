<script>
  function get_jadwal(id_dosen) {
    $.ajax({
      type: 'post',
      url: '<?= base_url('pengajar_kuliah/get_jadwal_by_prodi_dosen'); ?>',
      data: 'id_dosen=' + id_dosen,
      success: function(response) {
        $("#jadwalnya").html(response);
      }
    });
  }
</script>

<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Dosen Pengajar Kuliah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Dosen Pengajar Kuliah
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?><br>

          <b>Filter Pencarian</b>
          <?php
          $attributes = array('id' => 'FrmPencarianPengajar', 'method' => "post", "autocomplete" => "off");
          echo form_open('', $attributes);
          ?>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Tahun Akademik</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-1" name="id_ta">
                <option value="0">- All -</option>
                <?php foreach ($data_thn_akademik as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ta) { ?>selected<?php } ?>><?= $k['tahun'] . " (" . $k['nama'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Semester</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-2" name="id_semester">
                <option value="0">- All -</option>
                <?php foreach ($data_semester as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_semester) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_ta') ?>
              </small>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label">Program Studi</label>
            <div class="col-md-4">
              <select class="form-control select2-single" id="select2-4" name="id_prodi">
                <option value="0">- All -</option>
                <?php foreach ($data_prodi as $k) : ?>
                  <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_prodi) { ?>selected<?php } ?>><?= $k['nama'] . " (" . $k['nama_jenjang'] . ")" ?></option>
                <?php endforeach; ?>
              </select>
              <small class="text-danger">
                <?php echo form_error('id_prodi') ?>
              </small>
            </div>
          </div>

          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
          <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
          </form>
          <hr>

          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>

          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Nama Dosen</th>
                <th>Semester</th>
                <th>Tahun Akademik</th>
                <th>Program Studi</th>
                <th>Mata Kuliah</th>
                <th>Hari</th>
                <th>Jam</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_pengajar_kuliah as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama_dosen ?></td>
                  <td><?= $row->semester ?></td>
                  <td><?= $row->tahun_akademik ?></td>
                  <td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
                  <td><?= $row->mata_kuliah ?></td>
                  <td><?= $row->hari ?></td>
                  <td><?= $row->jam_mulai ?> - <?= $row->jam_selesai ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('pengajar_kuliah/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Dosen Pengajar Kuliah ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBobotNilai', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Dosen</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-5" name="id_dosen" required onchange="get_jadwal(this.value);">
                        <option value="0" selected disabled>Pilih Data Dosen</option>
                        <?php foreach ($dosen as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nidn'] ?> | <?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Jadwal Kuliah</label>
                    <div class="col-md-9">
                      <label id="jadwalnya">
                        <select class="form-control select2-single" id="select2-6" name="id_jadwal_kuliah" required>
                          <option value="0" selected disabled>Pilih Data Jadwal Kuliah</option>
                        </select>
                      </label>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_pengajar_kuliah as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('pengajar_kuliah/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Dosen</label>
                        <div class="col-md-9">
                          <input type="hidden" name="id" id="id" value="<?= $row->id ?>">
                          <select class="form-control select2-single" id="id_dosen" name="id_dosen2" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_dosen)) {
                              $lv = $row->id_dosen;
                            }
                            foreach ($dosen as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nidn'] ?> | <?= $v['nama'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Jadwal Kuliah</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="id_jadwal_kuliah" name="id_jadwal_kuliah2" required>
                            <?php
                            $lv = '';
                            if (isset($row->id_jadwal_kuliah)) {
                              $lv = $row->id_jadwal_kuliah;
                            }
                            foreach ($jadwal_kuliah as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['semester'] ?> | <?= $v['tahun_akademik'] ?> | <?= $v['mata_kuliah'] ?> | <?= $v['hari'] ?> | <?= $v['jam_mulai'] ?> - <?= $v['jam_selesai'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>