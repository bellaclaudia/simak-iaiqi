<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=pengajar_kuliah.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>SIMAK IAIQI | Pengajar Kuliah</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Pengajar Kuliah pada IAIQI Indralaya</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
      	<th class="allcen center bold">Nama Dosen</th>
        <th class="allcen center bold">Semester</th>
        <th class="allcen center bold">Tahun Akademik</th>
        <th class="allcen center bold">Program Studi</th>
        <th class="allcen center bold">Mata Kuliah</th>
        <th class="allcen center bold">Hari</th>
        <th class="allcen center bold">Jam</th>
      	<th class="allcen center bold">Tgl. Input</th>
      	<th class="allcen center bold">Tgl. Update</th>
      	<th class="allcen center bold">User Update By</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_pengajar_kuliah as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->nama_dosen ?></td>
      	<td><?= $row->semester ?></td>
      	<td><?= $row->tahun_akademik ?></td>
      	<td><?= $row->nama_prodi . " (" . $row->nama_jenjang . ")" ?></td>
      	<td><?= $row->mata_kuliah ?></td>
      	<td><?= $row->hari ?></td>
      	<td><?= $row->jam_mulai ?> - <?= $row->jam_selesai ?></td>
        <td><?= $row->tgl_input ?></td>
        <td><?= $row->tgl_update ?></td>
        <td><?= $row->user_update_by ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>