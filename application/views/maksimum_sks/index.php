<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Maksimum SKS</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Maksimum SKS
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Max SKS</th>
                <th>IPK Batas Awal</th>
                <th>IPK Batas Akhir</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_maksimum_sks as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->max_sks ?></td>
                  <td><?= $row->ipk_batas_awal ?></td>
                  <td><?= $row->ipk_batas_akhir ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('master_maksimum_sks/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Maksimum SKS ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddMaxSKS', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Maksimum SKS</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="max_sks" placeholder="Isi Maksimum SKS" required>
                      <small class="text-danger">
                        <?php echo form_error('max_sks') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">IPK Batas Awal</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="ipk_batas_awal" placeholder="Isi IPK Batas Awal" required>
                      <small class="text-danger">
                        <?php echo form_error('ipk_batas_awal') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">IPK Batas Akhir</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="ipk_batas_akhir" placeholder="Isi IPK Batas Akhir" required>
                      <small class="text-danger">
                        <?php echo form_error('ipk_batas_akhir') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_maksimum_sks as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('master_maksimum_sks/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Maksimum SKS</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <input class="form-control" type="text" name="max_sks" placeholder="Isi Maksimum SKS" value="<?= $row->max_sks; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('max_sks') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">IPK Batas Awal</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="ipk_batas_awal" placeholder="Isi IPK Batas Awal" value="<?= $row->ipk_batas_awal; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('ipk_batas_awal') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">IPK Batas Akhir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="ipk_batas_akhir" placeholder="Isi IPK Batas Akhir" value="<?= $row->ipk_batas_akhir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('ipk_batas_akhir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>